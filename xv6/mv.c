#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

// Because I need to get used to documenting :'(
// mv.c is used for the purpose of moving  data from 1 file to another
// Usage: mv srcname destname

int
main(int argc, char *argv[])
{
  char buf[512];
  int filenameF, copynameF;
  int  r, w;
  char *orig;
  char *copy;

// If not the correct ammount of arguements throw an error at the user
  if(argc != 3){
    printf(2, "Please use the format specified in the documentation: mv originalfilename copyfilename\n");
    exit();
  }

// Read the first and second arguements into the orig and copy "strings"
  orig = argv[1];
  copy = argv[2];

// Check to see if orig can be opened and the copy can be made
  if ((filenameF = open(orig, O_RDONLY)) < 0) {
    printf(2, "Error, cannot move %s to %s\n", orig,copy);
    exit();
  }
  if ((copynameF = open(copy, O_CREATE|O_WRONLY)) < 0) {
    printf(2, "Error, cannot move %s to %s\n", orig,copy);
    exit();
  }
// Read from orig and write into copy simultaneously
// Breaks it into 512 bits per block or execution
  while ((r = read(filenameF, buf, sizeof(buf))) > 0) {
    w = write(copynameF, buf, r);
    if (w != r || w < 0) 
      break;
  }
// Check to see if we are actually reading or writing anything.
// Broad Error Catcher
  if (r < 0 || w < 0)
    printf(2, "mv: error copying %s to %s\n", orig, copy);
// Close the files

  if(unlink(argv[1]) > 0){
    printf(2, "mv: Could not delete orig %s\n", orig);
  }
  close(filenameF);
  close(copynameF);

  exit();
}
