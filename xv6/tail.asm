
_tail:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "stat.h"
#include "user.h"
char buf;

int main(int argc, char *argv[])
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 e4 f0             	and    $0xfffffff0,%esp
   6:	83 ec 30             	sub    $0x30,%esp
    int Fdata, NLines;
    int filename;
    int linecount = 0;
   9:	c7 44 24 28 00 00 00 	movl   $0x0,0x28(%esp)
  10:	00 
    int maxLines;
    if (argc<=1)
  11:	83 7d 08 01          	cmpl   $0x1,0x8(%ebp)
  15:	7f 25                	jg     3c <main+0x3c>
    {
        printf(1,"Usage: head file OR head file #oflines %sn", argv[1]);
  17:	8b 45 0c             	mov    0xc(%ebp),%eax
  1a:	83 c0 04             	add    $0x4,%eax
  1d:	8b 00                	mov    (%eax),%eax
  1f:	89 44 24 08          	mov    %eax,0x8(%esp)
  23:	c7 44 24 04 b0 09 00 	movl   $0x9b0,0x4(%esp)
  2a:	00 
  2b:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  32:	e8 b1 05 00 00       	call   5e8 <printf>
        exit();
  37:	e8 30 04 00 00       	call   46c <exit>
    }
    if((filename = open(argv[1],0)) < 0)
  3c:	8b 45 0c             	mov    0xc(%ebp),%eax
  3f:	83 c0 04             	add    $0x4,%eax
  42:	8b 00                	mov    (%eax),%eax
  44:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  4b:	00 
  4c:	89 04 24             	mov    %eax,(%esp)
  4f:	e8 58 04 00 00       	call   4ac <open>
  54:	89 44 24 24          	mov    %eax,0x24(%esp)
  58:	83 7c 24 24 00       	cmpl   $0x0,0x24(%esp)
  5d:	79 25                	jns    84 <main+0x84>
    {
        printf(1,"Error, cannot open %sn", argv[1]);
  5f:	8b 45 0c             	mov    0xc(%ebp),%eax
  62:	83 c0 04             	add    $0x4,%eax
  65:	8b 00                	mov    (%eax),%eax
  67:	89 44 24 08          	mov    %eax,0x8(%esp)
  6b:	c7 44 24 04 db 09 00 	movl   $0x9db,0x4(%esp)
  72:	00 
  73:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  7a:	e8 69 05 00 00       	call   5e8 <printf>
        exit();
  7f:	e8 e8 03 00 00       	call   46c <exit>
    }
    if(argc ==2)
  84:	83 7d 08 02          	cmpl   $0x2,0x8(%ebp)
  88:	75 0a                	jne    94 <main+0x94>
    {
        NLines = 10;
  8a:	c7 44 24 20 0a 00 00 	movl   $0xa,0x20(%esp)
  91:	00 
    else if(argc == 3)
    {
        NLines =  (int) atoi(argv[2]);
    }

    while((Fdata=read(filename, &buf, 1))>0)
  92:	eb 2f                	jmp    c3 <main+0xc3>
    }
    if(argc ==2)
    {
        NLines = 10;
    }
    else if(argc == 3)
  94:	83 7d 08 03          	cmpl   $0x3,0x8(%ebp)
  98:	75 28                	jne    c2 <main+0xc2>
    {
        NLines =  (int) atoi(argv[2]);
  9a:	8b 45 0c             	mov    0xc(%ebp),%eax
  9d:	83 c0 08             	add    $0x8,%eax
  a0:	8b 00                	mov    (%eax),%eax
  a2:	89 04 24             	mov    %eax,(%esp)
  a5:	e8 30 03 00 00       	call   3da <atoi>
  aa:	89 44 24 20          	mov    %eax,0x20(%esp)
    }

    while((Fdata=read(filename, &buf, 1))>0)
  ae:	eb 13                	jmp    c3 <main+0xc3>
    {
        if(buf == '\n')
  b0:	0f b6 05 30 0a 00 00 	movzbl 0xa30,%eax
  b7:	3c 0a                	cmp    $0xa,%al
  b9:	75 08                	jne    c3 <main+0xc3>
        {
            linecount++;
  bb:	83 44 24 28 01       	addl   $0x1,0x28(%esp)
  c0:	eb 01                	jmp    c3 <main+0xc3>
    else if(argc == 3)
    {
        NLines =  (int) atoi(argv[2]);
    }

    while((Fdata=read(filename, &buf, 1))>0)
  c2:	90                   	nop
  c3:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
  ca:	00 
  cb:	c7 44 24 04 30 0a 00 	movl   $0xa30,0x4(%esp)
  d2:	00 
  d3:	8b 44 24 24          	mov    0x24(%esp),%eax
  d7:	89 04 24             	mov    %eax,(%esp)
  da:	e8 a5 03 00 00       	call   484 <read>
  df:	89 44 24 1c          	mov    %eax,0x1c(%esp)
  e3:	83 7c 24 1c 00       	cmpl   $0x0,0x1c(%esp)
  e8:	7f c6                	jg     b0 <main+0xb0>
        {
            linecount++;
     	}
    }

    maxLines = linecount;
  ea:	8b 44 24 28          	mov    0x28(%esp),%eax
  ee:	89 44 24 2c          	mov    %eax,0x2c(%esp)
    linecount = 0;
  f2:	c7 44 24 28 00 00 00 	movl   $0x0,0x28(%esp)
  f9:	00 
    close(Fdata);
  fa:	8b 44 24 1c          	mov    0x1c(%esp),%eax
  fe:	89 04 24             	mov    %eax,(%esp)
 101:	e8 8e 03 00 00       	call   494 <close>

    if((filename = open(argv[1],0)) < 0)
 106:	8b 45 0c             	mov    0xc(%ebp),%eax
 109:	83 c0 04             	add    $0x4,%eax
 10c:	8b 00                	mov    (%eax),%eax
 10e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 115:	00 
 116:	89 04 24             	mov    %eax,(%esp)
 119:	e8 8e 03 00 00       	call   4ac <open>
 11e:	89 44 24 24          	mov    %eax,0x24(%esp)
 122:	83 7c 24 24 00       	cmpl   $0x0,0x24(%esp)
 127:	79 7f                	jns    1a8 <main+0x1a8>
    {
        printf(1,"Error, cannot open %sn", argv[1]);
 129:	8b 45 0c             	mov    0xc(%ebp),%eax
 12c:	83 c0 04             	add    $0x4,%eax
 12f:	8b 00                	mov    (%eax),%eax
 131:	89 44 24 08          	mov    %eax,0x8(%esp)
 135:	c7 44 24 04 db 09 00 	movl   $0x9db,0x4(%esp)
 13c:	00 
 13d:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 144:	e8 9f 04 00 00       	call   5e8 <printf>
        exit();
 149:	e8 1e 03 00 00       	call   46c <exit>
    }

    while((Fdata=read(filename, &buf, 1))>0)
    {
	if(linecount > maxLines -1){
 14e:	8b 44 24 2c          	mov    0x2c(%esp),%eax
 152:	83 e8 01             	sub    $0x1,%eax
 155:	3b 44 24 28          	cmp    0x28(%esp),%eax
 159:	7d 05                	jge    160 <main+0x160>
	    exit();
 15b:	e8 0c 03 00 00       	call   46c <exit>
	}
        if(linecount >= (maxLines - NLines)){
 160:	8b 44 24 20          	mov    0x20(%esp),%eax
 164:	8b 54 24 2c          	mov    0x2c(%esp),%edx
 168:	89 d1                	mov    %edx,%ecx
 16a:	29 c1                	sub    %eax,%ecx
 16c:	89 c8                	mov    %ecx,%eax
 16e:	3b 44 24 28          	cmp    0x28(%esp),%eax
 172:	7f 22                	jg     196 <main+0x196>
            printf(1,"%c",buf);
 174:	0f b6 05 30 0a 00 00 	movzbl 0xa30,%eax
 17b:	0f be c0             	movsbl %al,%eax
 17e:	89 44 24 08          	mov    %eax,0x8(%esp)
 182:	c7 44 24 04 f2 09 00 	movl   $0x9f2,0x4(%esp)
 189:	00 
 18a:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 191:	e8 52 04 00 00       	call   5e8 <printf>
        }
        if(buf == '\n')
 196:	0f b6 05 30 0a 00 00 	movzbl 0xa30,%eax
 19d:	3c 0a                	cmp    $0xa,%al
 19f:	75 08                	jne    1a9 <main+0x1a9>
        {
            linecount++;
 1a1:	83 44 24 28 01       	addl   $0x1,0x28(%esp)
 1a6:	eb 01                	jmp    1a9 <main+0x1a9>
    {
        printf(1,"Error, cannot open %sn", argv[1]);
        exit();
    }

    while((Fdata=read(filename, &buf, 1))>0)
 1a8:	90                   	nop
 1a9:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 1b0:	00 
 1b1:	c7 44 24 04 30 0a 00 	movl   $0xa30,0x4(%esp)
 1b8:	00 
 1b9:	8b 44 24 24          	mov    0x24(%esp),%eax
 1bd:	89 04 24             	mov    %eax,(%esp)
 1c0:	e8 bf 02 00 00       	call   484 <read>
 1c5:	89 44 24 1c          	mov    %eax,0x1c(%esp)
 1c9:	83 7c 24 1c 00       	cmpl   $0x0,0x1c(%esp)
 1ce:	0f 8f 7a ff ff ff    	jg     14e <main+0x14e>
        if(buf == '\n')
        {
            linecount++;
        }
    }
    if(Fdata<0)
 1d4:	83 7c 24 1c 00       	cmpl   $0x0,0x1c(%esp)
 1d9:	79 19                	jns    1f4 <main+0x1f4>
    {
        printf(1,"head: read errorn");
 1db:	c7 44 24 04 f5 09 00 	movl   $0x9f5,0x4(%esp)
 1e2:	00 
 1e3:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 1ea:	e8 f9 03 00 00       	call   5e8 <printf>
        exit();
 1ef:	e8 78 02 00 00       	call   46c <exit>
    }

    close(Fdata);
 1f4:	8b 44 24 1c          	mov    0x1c(%esp),%eax
 1f8:	89 04 24             	mov    %eax,(%esp)
 1fb:	e8 94 02 00 00       	call   494 <close>
    exit();
 200:	e8 67 02 00 00       	call   46c <exit>
 205:	90                   	nop
 206:	90                   	nop
 207:	90                   	nop

00000208 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 208:	55                   	push   %ebp
 209:	89 e5                	mov    %esp,%ebp
 20b:	57                   	push   %edi
 20c:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 20d:	8b 4d 08             	mov    0x8(%ebp),%ecx
 210:	8b 55 10             	mov    0x10(%ebp),%edx
 213:	8b 45 0c             	mov    0xc(%ebp),%eax
 216:	89 cb                	mov    %ecx,%ebx
 218:	89 df                	mov    %ebx,%edi
 21a:	89 d1                	mov    %edx,%ecx
 21c:	fc                   	cld    
 21d:	f3 aa                	rep stos %al,%es:(%edi)
 21f:	89 ca                	mov    %ecx,%edx
 221:	89 fb                	mov    %edi,%ebx
 223:	89 5d 08             	mov    %ebx,0x8(%ebp)
 226:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 229:	5b                   	pop    %ebx
 22a:	5f                   	pop    %edi
 22b:	5d                   	pop    %ebp
 22c:	c3                   	ret    

0000022d <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 22d:	55                   	push   %ebp
 22e:	89 e5                	mov    %esp,%ebp
 230:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 233:	8b 45 08             	mov    0x8(%ebp),%eax
 236:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 239:	8b 45 0c             	mov    0xc(%ebp),%eax
 23c:	0f b6 10             	movzbl (%eax),%edx
 23f:	8b 45 08             	mov    0x8(%ebp),%eax
 242:	88 10                	mov    %dl,(%eax)
 244:	8b 45 08             	mov    0x8(%ebp),%eax
 247:	0f b6 00             	movzbl (%eax),%eax
 24a:	84 c0                	test   %al,%al
 24c:	0f 95 c0             	setne  %al
 24f:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 253:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
 257:	84 c0                	test   %al,%al
 259:	75 de                	jne    239 <strcpy+0xc>
    ;
  return os;
 25b:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 25e:	c9                   	leave  
 25f:	c3                   	ret    

00000260 <strcmp>:

int
strcmp(const char *p, const char *q)
{
 260:	55                   	push   %ebp
 261:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 263:	eb 08                	jmp    26d <strcmp+0xd>
    p++, q++;
 265:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 269:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 26d:	8b 45 08             	mov    0x8(%ebp),%eax
 270:	0f b6 00             	movzbl (%eax),%eax
 273:	84 c0                	test   %al,%al
 275:	74 10                	je     287 <strcmp+0x27>
 277:	8b 45 08             	mov    0x8(%ebp),%eax
 27a:	0f b6 10             	movzbl (%eax),%edx
 27d:	8b 45 0c             	mov    0xc(%ebp),%eax
 280:	0f b6 00             	movzbl (%eax),%eax
 283:	38 c2                	cmp    %al,%dl
 285:	74 de                	je     265 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 287:	8b 45 08             	mov    0x8(%ebp),%eax
 28a:	0f b6 00             	movzbl (%eax),%eax
 28d:	0f b6 d0             	movzbl %al,%edx
 290:	8b 45 0c             	mov    0xc(%ebp),%eax
 293:	0f b6 00             	movzbl (%eax),%eax
 296:	0f b6 c0             	movzbl %al,%eax
 299:	89 d1                	mov    %edx,%ecx
 29b:	29 c1                	sub    %eax,%ecx
 29d:	89 c8                	mov    %ecx,%eax
}
 29f:	5d                   	pop    %ebp
 2a0:	c3                   	ret    

000002a1 <strlen>:

uint
strlen(char *s)
{
 2a1:	55                   	push   %ebp
 2a2:	89 e5                	mov    %esp,%ebp
 2a4:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 2a7:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 2ae:	eb 04                	jmp    2b4 <strlen+0x13>
 2b0:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 2b4:	8b 45 fc             	mov    -0x4(%ebp),%eax
 2b7:	03 45 08             	add    0x8(%ebp),%eax
 2ba:	0f b6 00             	movzbl (%eax),%eax
 2bd:	84 c0                	test   %al,%al
 2bf:	75 ef                	jne    2b0 <strlen+0xf>
    ;
  return n;
 2c1:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 2c4:	c9                   	leave  
 2c5:	c3                   	ret    

000002c6 <memset>:

void*
memset(void *dst, int c, uint n)
{
 2c6:	55                   	push   %ebp
 2c7:	89 e5                	mov    %esp,%ebp
 2c9:	83 ec 0c             	sub    $0xc,%esp
  stosb(dst, c, n);
 2cc:	8b 45 10             	mov    0x10(%ebp),%eax
 2cf:	89 44 24 08          	mov    %eax,0x8(%esp)
 2d3:	8b 45 0c             	mov    0xc(%ebp),%eax
 2d6:	89 44 24 04          	mov    %eax,0x4(%esp)
 2da:	8b 45 08             	mov    0x8(%ebp),%eax
 2dd:	89 04 24             	mov    %eax,(%esp)
 2e0:	e8 23 ff ff ff       	call   208 <stosb>
  return dst;
 2e5:	8b 45 08             	mov    0x8(%ebp),%eax
}
 2e8:	c9                   	leave  
 2e9:	c3                   	ret    

000002ea <strchr>:

char*
strchr(const char *s, char c)
{
 2ea:	55                   	push   %ebp
 2eb:	89 e5                	mov    %esp,%ebp
 2ed:	83 ec 04             	sub    $0x4,%esp
 2f0:	8b 45 0c             	mov    0xc(%ebp),%eax
 2f3:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 2f6:	eb 14                	jmp    30c <strchr+0x22>
    if(*s == c)
 2f8:	8b 45 08             	mov    0x8(%ebp),%eax
 2fb:	0f b6 00             	movzbl (%eax),%eax
 2fe:	3a 45 fc             	cmp    -0x4(%ebp),%al
 301:	75 05                	jne    308 <strchr+0x1e>
      return (char*)s;
 303:	8b 45 08             	mov    0x8(%ebp),%eax
 306:	eb 13                	jmp    31b <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 308:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 30c:	8b 45 08             	mov    0x8(%ebp),%eax
 30f:	0f b6 00             	movzbl (%eax),%eax
 312:	84 c0                	test   %al,%al
 314:	75 e2                	jne    2f8 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 316:	b8 00 00 00 00       	mov    $0x0,%eax
}
 31b:	c9                   	leave  
 31c:	c3                   	ret    

0000031d <gets>:

char*
gets(char *buf, int max)
{
 31d:	55                   	push   %ebp
 31e:	89 e5                	mov    %esp,%ebp
 320:	83 ec 28             	sub    $0x28,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 323:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 32a:	eb 44                	jmp    370 <gets+0x53>
    cc = read(0, &c, 1);
 32c:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 333:	00 
 334:	8d 45 ef             	lea    -0x11(%ebp),%eax
 337:	89 44 24 04          	mov    %eax,0x4(%esp)
 33b:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 342:	e8 3d 01 00 00       	call   484 <read>
 347:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(cc < 1)
 34a:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 34e:	7e 2d                	jle    37d <gets+0x60>
      break;
    buf[i++] = c;
 350:	8b 45 f0             	mov    -0x10(%ebp),%eax
 353:	03 45 08             	add    0x8(%ebp),%eax
 356:	0f b6 55 ef          	movzbl -0x11(%ebp),%edx
 35a:	88 10                	mov    %dl,(%eax)
 35c:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
    if(c == '\n' || c == '\r')
 360:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 364:	3c 0a                	cmp    $0xa,%al
 366:	74 16                	je     37e <gets+0x61>
 368:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 36c:	3c 0d                	cmp    $0xd,%al
 36e:	74 0e                	je     37e <gets+0x61>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 370:	8b 45 f0             	mov    -0x10(%ebp),%eax
 373:	83 c0 01             	add    $0x1,%eax
 376:	3b 45 0c             	cmp    0xc(%ebp),%eax
 379:	7c b1                	jl     32c <gets+0xf>
 37b:	eb 01                	jmp    37e <gets+0x61>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 37d:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 37e:	8b 45 f0             	mov    -0x10(%ebp),%eax
 381:	03 45 08             	add    0x8(%ebp),%eax
 384:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 387:	8b 45 08             	mov    0x8(%ebp),%eax
}
 38a:	c9                   	leave  
 38b:	c3                   	ret    

0000038c <stat>:

int
stat(char *n, struct stat *st)
{
 38c:	55                   	push   %ebp
 38d:	89 e5                	mov    %esp,%ebp
 38f:	83 ec 28             	sub    $0x28,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 392:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 399:	00 
 39a:	8b 45 08             	mov    0x8(%ebp),%eax
 39d:	89 04 24             	mov    %eax,(%esp)
 3a0:	e8 07 01 00 00       	call   4ac <open>
 3a5:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(fd < 0)
 3a8:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 3ac:	79 07                	jns    3b5 <stat+0x29>
    return -1;
 3ae:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 3b3:	eb 23                	jmp    3d8 <stat+0x4c>
  r = fstat(fd, st);
 3b5:	8b 45 0c             	mov    0xc(%ebp),%eax
 3b8:	89 44 24 04          	mov    %eax,0x4(%esp)
 3bc:	8b 45 f0             	mov    -0x10(%ebp),%eax
 3bf:	89 04 24             	mov    %eax,(%esp)
 3c2:	e8 fd 00 00 00       	call   4c4 <fstat>
 3c7:	89 45 f4             	mov    %eax,-0xc(%ebp)
  close(fd);
 3ca:	8b 45 f0             	mov    -0x10(%ebp),%eax
 3cd:	89 04 24             	mov    %eax,(%esp)
 3d0:	e8 bf 00 00 00       	call   494 <close>
  return r;
 3d5:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
 3d8:	c9                   	leave  
 3d9:	c3                   	ret    

000003da <atoi>:

int
atoi(const char *s)
{
 3da:	55                   	push   %ebp
 3db:	89 e5                	mov    %esp,%ebp
 3dd:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 3e0:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 3e7:	eb 24                	jmp    40d <atoi+0x33>
    n = n*10 + *s++ - '0';
 3e9:	8b 55 fc             	mov    -0x4(%ebp),%edx
 3ec:	89 d0                	mov    %edx,%eax
 3ee:	c1 e0 02             	shl    $0x2,%eax
 3f1:	01 d0                	add    %edx,%eax
 3f3:	01 c0                	add    %eax,%eax
 3f5:	89 c2                	mov    %eax,%edx
 3f7:	8b 45 08             	mov    0x8(%ebp),%eax
 3fa:	0f b6 00             	movzbl (%eax),%eax
 3fd:	0f be c0             	movsbl %al,%eax
 400:	8d 04 02             	lea    (%edx,%eax,1),%eax
 403:	83 e8 30             	sub    $0x30,%eax
 406:	89 45 fc             	mov    %eax,-0x4(%ebp)
 409:	83 45 08 01          	addl   $0x1,0x8(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 40d:	8b 45 08             	mov    0x8(%ebp),%eax
 410:	0f b6 00             	movzbl (%eax),%eax
 413:	3c 2f                	cmp    $0x2f,%al
 415:	7e 0a                	jle    421 <atoi+0x47>
 417:	8b 45 08             	mov    0x8(%ebp),%eax
 41a:	0f b6 00             	movzbl (%eax),%eax
 41d:	3c 39                	cmp    $0x39,%al
 41f:	7e c8                	jle    3e9 <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 421:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 424:	c9                   	leave  
 425:	c3                   	ret    

00000426 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 426:	55                   	push   %ebp
 427:	89 e5                	mov    %esp,%ebp
 429:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 42c:	8b 45 08             	mov    0x8(%ebp),%eax
 42f:	89 45 f8             	mov    %eax,-0x8(%ebp)
  src = vsrc;
 432:	8b 45 0c             	mov    0xc(%ebp),%eax
 435:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0)
 438:	eb 13                	jmp    44d <memmove+0x27>
    *dst++ = *src++;
 43a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 43d:	0f b6 10             	movzbl (%eax),%edx
 440:	8b 45 f8             	mov    -0x8(%ebp),%eax
 443:	88 10                	mov    %dl,(%eax)
 445:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
 449:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 44d:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
 451:	0f 9f c0             	setg   %al
 454:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
 458:	84 c0                	test   %al,%al
 45a:	75 de                	jne    43a <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 45c:	8b 45 08             	mov    0x8(%ebp),%eax
}
 45f:	c9                   	leave  
 460:	c3                   	ret    
 461:	90                   	nop
 462:	90                   	nop
 463:	90                   	nop

00000464 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 464:	b8 01 00 00 00       	mov    $0x1,%eax
 469:	cd 40                	int    $0x40
 46b:	c3                   	ret    

0000046c <exit>:
SYSCALL(exit)
 46c:	b8 02 00 00 00       	mov    $0x2,%eax
 471:	cd 40                	int    $0x40
 473:	c3                   	ret    

00000474 <wait>:
SYSCALL(wait)
 474:	b8 03 00 00 00       	mov    $0x3,%eax
 479:	cd 40                	int    $0x40
 47b:	c3                   	ret    

0000047c <pipe>:
SYSCALL(pipe)
 47c:	b8 04 00 00 00       	mov    $0x4,%eax
 481:	cd 40                	int    $0x40
 483:	c3                   	ret    

00000484 <read>:
SYSCALL(read)
 484:	b8 05 00 00 00       	mov    $0x5,%eax
 489:	cd 40                	int    $0x40
 48b:	c3                   	ret    

0000048c <write>:
SYSCALL(write)
 48c:	b8 10 00 00 00       	mov    $0x10,%eax
 491:	cd 40                	int    $0x40
 493:	c3                   	ret    

00000494 <close>:
SYSCALL(close)
 494:	b8 15 00 00 00       	mov    $0x15,%eax
 499:	cd 40                	int    $0x40
 49b:	c3                   	ret    

0000049c <kill>:
SYSCALL(kill)
 49c:	b8 06 00 00 00       	mov    $0x6,%eax
 4a1:	cd 40                	int    $0x40
 4a3:	c3                   	ret    

000004a4 <exec>:
SYSCALL(exec)
 4a4:	b8 07 00 00 00       	mov    $0x7,%eax
 4a9:	cd 40                	int    $0x40
 4ab:	c3                   	ret    

000004ac <open>:
SYSCALL(open)
 4ac:	b8 0f 00 00 00       	mov    $0xf,%eax
 4b1:	cd 40                	int    $0x40
 4b3:	c3                   	ret    

000004b4 <mknod>:
SYSCALL(mknod)
 4b4:	b8 11 00 00 00       	mov    $0x11,%eax
 4b9:	cd 40                	int    $0x40
 4bb:	c3                   	ret    

000004bc <unlink>:
SYSCALL(unlink)
 4bc:	b8 12 00 00 00       	mov    $0x12,%eax
 4c1:	cd 40                	int    $0x40
 4c3:	c3                   	ret    

000004c4 <fstat>:
SYSCALL(fstat)
 4c4:	b8 08 00 00 00       	mov    $0x8,%eax
 4c9:	cd 40                	int    $0x40
 4cb:	c3                   	ret    

000004cc <link>:
SYSCALL(link)
 4cc:	b8 13 00 00 00       	mov    $0x13,%eax
 4d1:	cd 40                	int    $0x40
 4d3:	c3                   	ret    

000004d4 <mkdir>:
SYSCALL(mkdir)
 4d4:	b8 14 00 00 00       	mov    $0x14,%eax
 4d9:	cd 40                	int    $0x40
 4db:	c3                   	ret    

000004dc <chdir>:
SYSCALL(chdir)
 4dc:	b8 09 00 00 00       	mov    $0x9,%eax
 4e1:	cd 40                	int    $0x40
 4e3:	c3                   	ret    

000004e4 <dup>:
SYSCALL(dup)
 4e4:	b8 0a 00 00 00       	mov    $0xa,%eax
 4e9:	cd 40                	int    $0x40
 4eb:	c3                   	ret    

000004ec <getpid>:
SYSCALL(getpid)
 4ec:	b8 0b 00 00 00       	mov    $0xb,%eax
 4f1:	cd 40                	int    $0x40
 4f3:	c3                   	ret    

000004f4 <sbrk>:
SYSCALL(sbrk)
 4f4:	b8 0c 00 00 00       	mov    $0xc,%eax
 4f9:	cd 40                	int    $0x40
 4fb:	c3                   	ret    

000004fc <sleep>:
SYSCALL(sleep)
 4fc:	b8 0d 00 00 00       	mov    $0xd,%eax
 501:	cd 40                	int    $0x40
 503:	c3                   	ret    

00000504 <uptime>:
SYSCALL(uptime)
 504:	b8 0e 00 00 00       	mov    $0xe,%eax
 509:	cd 40                	int    $0x40
 50b:	c3                   	ret    

0000050c <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 50c:	55                   	push   %ebp
 50d:	89 e5                	mov    %esp,%ebp
 50f:	83 ec 28             	sub    $0x28,%esp
 512:	8b 45 0c             	mov    0xc(%ebp),%eax
 515:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 518:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 51f:	00 
 520:	8d 45 f4             	lea    -0xc(%ebp),%eax
 523:	89 44 24 04          	mov    %eax,0x4(%esp)
 527:	8b 45 08             	mov    0x8(%ebp),%eax
 52a:	89 04 24             	mov    %eax,(%esp)
 52d:	e8 5a ff ff ff       	call   48c <write>
}
 532:	c9                   	leave  
 533:	c3                   	ret    

00000534 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 534:	55                   	push   %ebp
 535:	89 e5                	mov    %esp,%ebp
 537:	53                   	push   %ebx
 538:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 53b:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 542:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 546:	74 17                	je     55f <printint+0x2b>
 548:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 54c:	79 11                	jns    55f <printint+0x2b>
    neg = 1;
 54e:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 555:	8b 45 0c             	mov    0xc(%ebp),%eax
 558:	f7 d8                	neg    %eax
 55a:	89 45 f4             	mov    %eax,-0xc(%ebp)
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 55d:	eb 06                	jmp    565 <printint+0x31>
    neg = 1;
    x = -xx;
  } else {
    x = xx;
 55f:	8b 45 0c             	mov    0xc(%ebp),%eax
 562:	89 45 f4             	mov    %eax,-0xc(%ebp)
  }

  i = 0;
 565:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  do{
    buf[i++] = digits[x % base];
 56c:	8b 4d ec             	mov    -0x14(%ebp),%ecx
 56f:	8b 5d 10             	mov    0x10(%ebp),%ebx
 572:	8b 45 f4             	mov    -0xc(%ebp),%eax
 575:	ba 00 00 00 00       	mov    $0x0,%edx
 57a:	f7 f3                	div    %ebx
 57c:	89 d0                	mov    %edx,%eax
 57e:	0f b6 80 10 0a 00 00 	movzbl 0xa10(%eax),%eax
 585:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
 589:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
  }while((x /= base) != 0);
 58d:	8b 45 10             	mov    0x10(%ebp),%eax
 590:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 593:	8b 45 f4             	mov    -0xc(%ebp),%eax
 596:	ba 00 00 00 00       	mov    $0x0,%edx
 59b:	f7 75 d4             	divl   -0x2c(%ebp)
 59e:	89 45 f4             	mov    %eax,-0xc(%ebp)
 5a1:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 5a5:	75 c5                	jne    56c <printint+0x38>
  if(neg)
 5a7:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 5ab:	74 2a                	je     5d7 <printint+0xa3>
    buf[i++] = '-';
 5ad:	8b 45 ec             	mov    -0x14(%ebp),%eax
 5b0:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)
 5b5:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)

  while(--i >= 0)
 5b9:	eb 1d                	jmp    5d8 <printint+0xa4>
    putc(fd, buf[i]);
 5bb:	8b 45 ec             	mov    -0x14(%ebp),%eax
 5be:	0f b6 44 05 dc       	movzbl -0x24(%ebp,%eax,1),%eax
 5c3:	0f be c0             	movsbl %al,%eax
 5c6:	89 44 24 04          	mov    %eax,0x4(%esp)
 5ca:	8b 45 08             	mov    0x8(%ebp),%eax
 5cd:	89 04 24             	mov    %eax,(%esp)
 5d0:	e8 37 ff ff ff       	call   50c <putc>
 5d5:	eb 01                	jmp    5d8 <printint+0xa4>
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 5d7:	90                   	nop
 5d8:	83 6d ec 01          	subl   $0x1,-0x14(%ebp)
 5dc:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 5e0:	79 d9                	jns    5bb <printint+0x87>
    putc(fd, buf[i]);
}
 5e2:	83 c4 44             	add    $0x44,%esp
 5e5:	5b                   	pop    %ebx
 5e6:	5d                   	pop    %ebp
 5e7:	c3                   	ret    

000005e8 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 5e8:	55                   	push   %ebp
 5e9:	89 e5                	mov    %esp,%ebp
 5eb:	83 ec 38             	sub    $0x38,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 5ee:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 5f5:	8d 45 0c             	lea    0xc(%ebp),%eax
 5f8:	83 c0 04             	add    $0x4,%eax
 5fb:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(i = 0; fmt[i]; i++){
 5fe:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 605:	e9 7e 01 00 00       	jmp    788 <printf+0x1a0>
    c = fmt[i] & 0xff;
 60a:	8b 55 0c             	mov    0xc(%ebp),%edx
 60d:	8b 45 ec             	mov    -0x14(%ebp),%eax
 610:	8d 04 02             	lea    (%edx,%eax,1),%eax
 613:	0f b6 00             	movzbl (%eax),%eax
 616:	0f be c0             	movsbl %al,%eax
 619:	25 ff 00 00 00       	and    $0xff,%eax
 61e:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(state == 0){
 621:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 625:	75 2c                	jne    653 <printf+0x6b>
      if(c == '%'){
 627:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 62b:	75 0c                	jne    639 <printf+0x51>
        state = '%';
 62d:	c7 45 f0 25 00 00 00 	movl   $0x25,-0x10(%ebp)
 634:	e9 4b 01 00 00       	jmp    784 <printf+0x19c>
      } else {
        putc(fd, c);
 639:	8b 45 e8             	mov    -0x18(%ebp),%eax
 63c:	0f be c0             	movsbl %al,%eax
 63f:	89 44 24 04          	mov    %eax,0x4(%esp)
 643:	8b 45 08             	mov    0x8(%ebp),%eax
 646:	89 04 24             	mov    %eax,(%esp)
 649:	e8 be fe ff ff       	call   50c <putc>
 64e:	e9 31 01 00 00       	jmp    784 <printf+0x19c>
      }
    } else if(state == '%'){
 653:	83 7d f0 25          	cmpl   $0x25,-0x10(%ebp)
 657:	0f 85 27 01 00 00    	jne    784 <printf+0x19c>
      if(c == 'd'){
 65d:	83 7d e8 64          	cmpl   $0x64,-0x18(%ebp)
 661:	75 2d                	jne    690 <printf+0xa8>
        printint(fd, *ap, 10, 1);
 663:	8b 45 f4             	mov    -0xc(%ebp),%eax
 666:	8b 00                	mov    (%eax),%eax
 668:	c7 44 24 0c 01 00 00 	movl   $0x1,0xc(%esp)
 66f:	00 
 670:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
 677:	00 
 678:	89 44 24 04          	mov    %eax,0x4(%esp)
 67c:	8b 45 08             	mov    0x8(%ebp),%eax
 67f:	89 04 24             	mov    %eax,(%esp)
 682:	e8 ad fe ff ff       	call   534 <printint>
        ap++;
 687:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 68b:	e9 ed 00 00 00       	jmp    77d <printf+0x195>
      } else if(c == 'x' || c == 'p'){
 690:	83 7d e8 78          	cmpl   $0x78,-0x18(%ebp)
 694:	74 06                	je     69c <printf+0xb4>
 696:	83 7d e8 70          	cmpl   $0x70,-0x18(%ebp)
 69a:	75 2d                	jne    6c9 <printf+0xe1>
        printint(fd, *ap, 16, 0);
 69c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 69f:	8b 00                	mov    (%eax),%eax
 6a1:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
 6a8:	00 
 6a9:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
 6b0:	00 
 6b1:	89 44 24 04          	mov    %eax,0x4(%esp)
 6b5:	8b 45 08             	mov    0x8(%ebp),%eax
 6b8:	89 04 24             	mov    %eax,(%esp)
 6bb:	e8 74 fe ff ff       	call   534 <printint>
        ap++;
 6c0:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
      }
    } else if(state == '%'){
      if(c == 'd'){
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 6c4:	e9 b4 00 00 00       	jmp    77d <printf+0x195>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 6c9:	83 7d e8 73          	cmpl   $0x73,-0x18(%ebp)
 6cd:	75 46                	jne    715 <printf+0x12d>
        s = (char*)*ap;
 6cf:	8b 45 f4             	mov    -0xc(%ebp),%eax
 6d2:	8b 00                	mov    (%eax),%eax
 6d4:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        ap++;
 6d7:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
        if(s == 0)
 6db:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 6df:	75 27                	jne    708 <printf+0x120>
          s = "(null)";
 6e1:	c7 45 e4 07 0a 00 00 	movl   $0xa07,-0x1c(%ebp)
        while(*s != 0){
 6e8:	eb 1f                	jmp    709 <printf+0x121>
          putc(fd, *s);
 6ea:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 6ed:	0f b6 00             	movzbl (%eax),%eax
 6f0:	0f be c0             	movsbl %al,%eax
 6f3:	89 44 24 04          	mov    %eax,0x4(%esp)
 6f7:	8b 45 08             	mov    0x8(%ebp),%eax
 6fa:	89 04 24             	mov    %eax,(%esp)
 6fd:	e8 0a fe ff ff       	call   50c <putc>
          s++;
 702:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
 706:	eb 01                	jmp    709 <printf+0x121>
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 708:	90                   	nop
 709:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 70c:	0f b6 00             	movzbl (%eax),%eax
 70f:	84 c0                	test   %al,%al
 711:	75 d7                	jne    6ea <printf+0x102>
 713:	eb 68                	jmp    77d <printf+0x195>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 715:	83 7d e8 63          	cmpl   $0x63,-0x18(%ebp)
 719:	75 1d                	jne    738 <printf+0x150>
        putc(fd, *ap);
 71b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 71e:	8b 00                	mov    (%eax),%eax
 720:	0f be c0             	movsbl %al,%eax
 723:	89 44 24 04          	mov    %eax,0x4(%esp)
 727:	8b 45 08             	mov    0x8(%ebp),%eax
 72a:	89 04 24             	mov    %eax,(%esp)
 72d:	e8 da fd ff ff       	call   50c <putc>
        ap++;
 732:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 736:	eb 45                	jmp    77d <printf+0x195>
      } else if(c == '%'){
 738:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 73c:	75 17                	jne    755 <printf+0x16d>
        putc(fd, c);
 73e:	8b 45 e8             	mov    -0x18(%ebp),%eax
 741:	0f be c0             	movsbl %al,%eax
 744:	89 44 24 04          	mov    %eax,0x4(%esp)
 748:	8b 45 08             	mov    0x8(%ebp),%eax
 74b:	89 04 24             	mov    %eax,(%esp)
 74e:	e8 b9 fd ff ff       	call   50c <putc>
 753:	eb 28                	jmp    77d <printf+0x195>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 755:	c7 44 24 04 25 00 00 	movl   $0x25,0x4(%esp)
 75c:	00 
 75d:	8b 45 08             	mov    0x8(%ebp),%eax
 760:	89 04 24             	mov    %eax,(%esp)
 763:	e8 a4 fd ff ff       	call   50c <putc>
        putc(fd, c);
 768:	8b 45 e8             	mov    -0x18(%ebp),%eax
 76b:	0f be c0             	movsbl %al,%eax
 76e:	89 44 24 04          	mov    %eax,0x4(%esp)
 772:	8b 45 08             	mov    0x8(%ebp),%eax
 775:	89 04 24             	mov    %eax,(%esp)
 778:	e8 8f fd ff ff       	call   50c <putc>
      }
      state = 0;
 77d:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 784:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
 788:	8b 55 0c             	mov    0xc(%ebp),%edx
 78b:	8b 45 ec             	mov    -0x14(%ebp),%eax
 78e:	8d 04 02             	lea    (%edx,%eax,1),%eax
 791:	0f b6 00             	movzbl (%eax),%eax
 794:	84 c0                	test   %al,%al
 796:	0f 85 6e fe ff ff    	jne    60a <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 79c:	c9                   	leave  
 79d:	c3                   	ret    
 79e:	90                   	nop
 79f:	90                   	nop

000007a0 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 7a0:	55                   	push   %ebp
 7a1:	89 e5                	mov    %esp,%ebp
 7a3:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 7a6:	8b 45 08             	mov    0x8(%ebp),%eax
 7a9:	83 e8 08             	sub    $0x8,%eax
 7ac:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 7af:	a1 2c 0a 00 00       	mov    0xa2c,%eax
 7b4:	89 45 fc             	mov    %eax,-0x4(%ebp)
 7b7:	eb 24                	jmp    7dd <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 7b9:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7bc:	8b 00                	mov    (%eax),%eax
 7be:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 7c1:	77 12                	ja     7d5 <free+0x35>
 7c3:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7c6:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 7c9:	77 24                	ja     7ef <free+0x4f>
 7cb:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7ce:	8b 00                	mov    (%eax),%eax
 7d0:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 7d3:	77 1a                	ja     7ef <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 7d5:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7d8:	8b 00                	mov    (%eax),%eax
 7da:	89 45 fc             	mov    %eax,-0x4(%ebp)
 7dd:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7e0:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 7e3:	76 d4                	jbe    7b9 <free+0x19>
 7e5:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7e8:	8b 00                	mov    (%eax),%eax
 7ea:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 7ed:	76 ca                	jbe    7b9 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 7ef:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7f2:	8b 40 04             	mov    0x4(%eax),%eax
 7f5:	c1 e0 03             	shl    $0x3,%eax
 7f8:	89 c2                	mov    %eax,%edx
 7fa:	03 55 f8             	add    -0x8(%ebp),%edx
 7fd:	8b 45 fc             	mov    -0x4(%ebp),%eax
 800:	8b 00                	mov    (%eax),%eax
 802:	39 c2                	cmp    %eax,%edx
 804:	75 24                	jne    82a <free+0x8a>
    bp->s.size += p->s.ptr->s.size;
 806:	8b 45 f8             	mov    -0x8(%ebp),%eax
 809:	8b 50 04             	mov    0x4(%eax),%edx
 80c:	8b 45 fc             	mov    -0x4(%ebp),%eax
 80f:	8b 00                	mov    (%eax),%eax
 811:	8b 40 04             	mov    0x4(%eax),%eax
 814:	01 c2                	add    %eax,%edx
 816:	8b 45 f8             	mov    -0x8(%ebp),%eax
 819:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 81c:	8b 45 fc             	mov    -0x4(%ebp),%eax
 81f:	8b 00                	mov    (%eax),%eax
 821:	8b 10                	mov    (%eax),%edx
 823:	8b 45 f8             	mov    -0x8(%ebp),%eax
 826:	89 10                	mov    %edx,(%eax)
 828:	eb 0a                	jmp    834 <free+0x94>
  } else
    bp->s.ptr = p->s.ptr;
 82a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 82d:	8b 10                	mov    (%eax),%edx
 82f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 832:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 834:	8b 45 fc             	mov    -0x4(%ebp),%eax
 837:	8b 40 04             	mov    0x4(%eax),%eax
 83a:	c1 e0 03             	shl    $0x3,%eax
 83d:	03 45 fc             	add    -0x4(%ebp),%eax
 840:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 843:	75 20                	jne    865 <free+0xc5>
    p->s.size += bp->s.size;
 845:	8b 45 fc             	mov    -0x4(%ebp),%eax
 848:	8b 50 04             	mov    0x4(%eax),%edx
 84b:	8b 45 f8             	mov    -0x8(%ebp),%eax
 84e:	8b 40 04             	mov    0x4(%eax),%eax
 851:	01 c2                	add    %eax,%edx
 853:	8b 45 fc             	mov    -0x4(%ebp),%eax
 856:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 859:	8b 45 f8             	mov    -0x8(%ebp),%eax
 85c:	8b 10                	mov    (%eax),%edx
 85e:	8b 45 fc             	mov    -0x4(%ebp),%eax
 861:	89 10                	mov    %edx,(%eax)
 863:	eb 08                	jmp    86d <free+0xcd>
  } else
    p->s.ptr = bp;
 865:	8b 45 fc             	mov    -0x4(%ebp),%eax
 868:	8b 55 f8             	mov    -0x8(%ebp),%edx
 86b:	89 10                	mov    %edx,(%eax)
  freep = p;
 86d:	8b 45 fc             	mov    -0x4(%ebp),%eax
 870:	a3 2c 0a 00 00       	mov    %eax,0xa2c
}
 875:	c9                   	leave  
 876:	c3                   	ret    

00000877 <morecore>:

static Header*
morecore(uint nu)
{
 877:	55                   	push   %ebp
 878:	89 e5                	mov    %esp,%ebp
 87a:	83 ec 28             	sub    $0x28,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 87d:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 884:	77 07                	ja     88d <morecore+0x16>
    nu = 4096;
 886:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 88d:	8b 45 08             	mov    0x8(%ebp),%eax
 890:	c1 e0 03             	shl    $0x3,%eax
 893:	89 04 24             	mov    %eax,(%esp)
 896:	e8 59 fc ff ff       	call   4f4 <sbrk>
 89b:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(p == (char*)-1)
 89e:	83 7d f0 ff          	cmpl   $0xffffffff,-0x10(%ebp)
 8a2:	75 07                	jne    8ab <morecore+0x34>
    return 0;
 8a4:	b8 00 00 00 00       	mov    $0x0,%eax
 8a9:	eb 22                	jmp    8cd <morecore+0x56>
  hp = (Header*)p;
 8ab:	8b 45 f0             	mov    -0x10(%ebp),%eax
 8ae:	89 45 f4             	mov    %eax,-0xc(%ebp)
  hp->s.size = nu;
 8b1:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8b4:	8b 55 08             	mov    0x8(%ebp),%edx
 8b7:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 8ba:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8bd:	83 c0 08             	add    $0x8,%eax
 8c0:	89 04 24             	mov    %eax,(%esp)
 8c3:	e8 d8 fe ff ff       	call   7a0 <free>
  return freep;
 8c8:	a1 2c 0a 00 00       	mov    0xa2c,%eax
}
 8cd:	c9                   	leave  
 8ce:	c3                   	ret    

000008cf <malloc>:

void*
malloc(uint nbytes)
{
 8cf:	55                   	push   %ebp
 8d0:	89 e5                	mov    %esp,%ebp
 8d2:	83 ec 28             	sub    $0x28,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 8d5:	8b 45 08             	mov    0x8(%ebp),%eax
 8d8:	83 c0 07             	add    $0x7,%eax
 8db:	c1 e8 03             	shr    $0x3,%eax
 8de:	83 c0 01             	add    $0x1,%eax
 8e1:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((prevp = freep) == 0){
 8e4:	a1 2c 0a 00 00       	mov    0xa2c,%eax
 8e9:	89 45 f0             	mov    %eax,-0x10(%ebp)
 8ec:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 8f0:	75 23                	jne    915 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 8f2:	c7 45 f0 24 0a 00 00 	movl   $0xa24,-0x10(%ebp)
 8f9:	8b 45 f0             	mov    -0x10(%ebp),%eax
 8fc:	a3 2c 0a 00 00       	mov    %eax,0xa2c
 901:	a1 2c 0a 00 00       	mov    0xa2c,%eax
 906:	a3 24 0a 00 00       	mov    %eax,0xa24
    base.s.size = 0;
 90b:	c7 05 28 0a 00 00 00 	movl   $0x0,0xa28
 912:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 915:	8b 45 f0             	mov    -0x10(%ebp),%eax
 918:	8b 00                	mov    (%eax),%eax
 91a:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(p->s.size >= nunits){
 91d:	8b 45 ec             	mov    -0x14(%ebp),%eax
 920:	8b 40 04             	mov    0x4(%eax),%eax
 923:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 926:	72 4d                	jb     975 <malloc+0xa6>
      if(p->s.size == nunits)
 928:	8b 45 ec             	mov    -0x14(%ebp),%eax
 92b:	8b 40 04             	mov    0x4(%eax),%eax
 92e:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 931:	75 0c                	jne    93f <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 933:	8b 45 ec             	mov    -0x14(%ebp),%eax
 936:	8b 10                	mov    (%eax),%edx
 938:	8b 45 f0             	mov    -0x10(%ebp),%eax
 93b:	89 10                	mov    %edx,(%eax)
 93d:	eb 26                	jmp    965 <malloc+0x96>
      else {
        p->s.size -= nunits;
 93f:	8b 45 ec             	mov    -0x14(%ebp),%eax
 942:	8b 40 04             	mov    0x4(%eax),%eax
 945:	89 c2                	mov    %eax,%edx
 947:	2b 55 f4             	sub    -0xc(%ebp),%edx
 94a:	8b 45 ec             	mov    -0x14(%ebp),%eax
 94d:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 950:	8b 45 ec             	mov    -0x14(%ebp),%eax
 953:	8b 40 04             	mov    0x4(%eax),%eax
 956:	c1 e0 03             	shl    $0x3,%eax
 959:	01 45 ec             	add    %eax,-0x14(%ebp)
        p->s.size = nunits;
 95c:	8b 45 ec             	mov    -0x14(%ebp),%eax
 95f:	8b 55 f4             	mov    -0xc(%ebp),%edx
 962:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 965:	8b 45 f0             	mov    -0x10(%ebp),%eax
 968:	a3 2c 0a 00 00       	mov    %eax,0xa2c
      return (void*)(p + 1);
 96d:	8b 45 ec             	mov    -0x14(%ebp),%eax
 970:	83 c0 08             	add    $0x8,%eax
 973:	eb 38                	jmp    9ad <malloc+0xde>
    }
    if(p == freep)
 975:	a1 2c 0a 00 00       	mov    0xa2c,%eax
 97a:	39 45 ec             	cmp    %eax,-0x14(%ebp)
 97d:	75 1b                	jne    99a <malloc+0xcb>
      if((p = morecore(nunits)) == 0)
 97f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 982:	89 04 24             	mov    %eax,(%esp)
 985:	e8 ed fe ff ff       	call   877 <morecore>
 98a:	89 45 ec             	mov    %eax,-0x14(%ebp)
 98d:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 991:	75 07                	jne    99a <malloc+0xcb>
        return 0;
 993:	b8 00 00 00 00       	mov    $0x0,%eax
 998:	eb 13                	jmp    9ad <malloc+0xde>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 99a:	8b 45 ec             	mov    -0x14(%ebp),%eax
 99d:	89 45 f0             	mov    %eax,-0x10(%ebp)
 9a0:	8b 45 ec             	mov    -0x14(%ebp),%eax
 9a3:	8b 00                	mov    (%eax),%eax
 9a5:	89 45 ec             	mov    %eax,-0x14(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 9a8:	e9 70 ff ff ff       	jmp    91d <malloc+0x4e>
}
 9ad:	c9                   	leave  
 9ae:	c3                   	ret    
