
kernel:     file format elf32-i386


Disassembly of section .text:

80100000 <multiboot_header>:
80100000:	02 b0 ad 1b 00 00    	add    0x1bad(%eax),%dh
80100006:	00 00                	add    %al,(%eax)
80100008:	fe 4f 52             	decb   0x52(%edi)
8010000b:	e4 0f                	in     $0xf,%al

8010000c <entry>:

# Entering xv6 on boot processor, with paging off.
.globl entry
entry:
  # Turn on page size extension for 4Mbyte pages
  movl    %cr4, %eax
8010000c:	0f 20 e0             	mov    %cr4,%eax
  orl     $(CR4_PSE), %eax
8010000f:	83 c8 10             	or     $0x10,%eax
  movl    %eax, %cr4
80100012:	0f 22 e0             	mov    %eax,%cr4
  # Set page directory
  movl    $(V2P_WO(entrypgdir)), %eax
80100015:	b8 00 b0 10 00       	mov    $0x10b000,%eax
  movl    %eax, %cr3
8010001a:	0f 22 d8             	mov    %eax,%cr3
  # Turn on paging.
  movl    %cr0, %eax
8010001d:	0f 20 c0             	mov    %cr0,%eax
  orl     $(CR0_PG|CR0_WP), %eax
80100020:	0d 00 00 01 80       	or     $0x80010000,%eax
  movl    %eax, %cr0
80100025:	0f 22 c0             	mov    %eax,%cr0

  # Set up the stack pointer.
  movl    $(stack + KSTACKSIZE), %esp
80100028:	bc 70 de 10 80       	mov    $0x8010de70,%esp
  # Jump to main() and switch to executing at high addresses.
  # Need an explicit long jump (indirect jump would work as
  # well, as would a push/ret trick to save 1 byte) because
  # the assembler produces a PC-relative instruction for a
  # direct jump.
  ljmp    $(SEG_KCODE<<3), $main
8010002d:	ea 56 3d 10 80 08 00 	ljmp   $0x8,$0x80103d56

80100034 <binit>:
  struct buf head;
} bcache;

void
binit(void)
{
80100034:	55                   	push   %ebp
80100035:	89 e5                	mov    %esp,%ebp
80100037:	83 ec 28             	sub    $0x28,%esp
  struct buf *b;

  initlock(&bcache.lock, "bcache");
8010003a:	c7 44 24 04 04 8b 10 	movl   $0x80108b04,0x4(%esp)
80100041:	80 
80100042:	c7 04 24 80 de 10 80 	movl   $0x8010de80,(%esp)
80100049:	e8 98 54 00 00       	call   801054e6 <initlock>

//PAGEBREAK!
  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
8010004e:	c7 05 90 1d 11 80 84 	movl   $0x80111d84,0x80111d90
80100055:	1d 11 80 
  bcache.head.next = &bcache.head;
80100058:	c7 05 94 1d 11 80 84 	movl   $0x80111d84,0x80111d94
8010005f:	1d 11 80 
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
80100062:	c7 45 f4 b4 de 10 80 	movl   $0x8010deb4,-0xc(%ebp)
80100069:	eb 3a                	jmp    801000a5 <binit+0x71>
    b->next = bcache.head.next;
8010006b:	8b 15 94 1d 11 80    	mov    0x80111d94,%edx
80100071:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100074:	89 50 10             	mov    %edx,0x10(%eax)
    b->prev = &bcache.head;
80100077:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010007a:	c7 40 0c 84 1d 11 80 	movl   $0x80111d84,0xc(%eax)
    b->dev = -1;
80100081:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100084:	c7 40 04 ff ff ff ff 	movl   $0xffffffff,0x4(%eax)
    bcache.head.next->prev = b;
8010008b:	a1 94 1d 11 80       	mov    0x80111d94,%eax
80100090:	8b 55 f4             	mov    -0xc(%ebp),%edx
80100093:	89 50 0c             	mov    %edx,0xc(%eax)
    bcache.head.next = b;
80100096:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100099:	a3 94 1d 11 80       	mov    %eax,0x80111d94

//PAGEBREAK!
  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
  bcache.head.next = &bcache.head;
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
8010009e:	81 45 f4 18 02 00 00 	addl   $0x218,-0xc(%ebp)
801000a5:	b8 84 1d 11 80       	mov    $0x80111d84,%eax
801000aa:	39 45 f4             	cmp    %eax,-0xc(%ebp)
801000ad:	72 bc                	jb     8010006b <binit+0x37>
    b->prev = &bcache.head;
    b->dev = -1;
    bcache.head.next->prev = b;
    bcache.head.next = b;
  }
}
801000af:	c9                   	leave  
801000b0:	c3                   	ret    

801000b1 <bget>:
// Look through buffer cache for block on device dev.
// If not found, allocate a buffer.
// In either case, return B_BUSY buffer.
static struct buf*
bget(uint dev, uint blockno)
{
801000b1:	55                   	push   %ebp
801000b2:	89 e5                	mov    %esp,%ebp
801000b4:	83 ec 28             	sub    $0x28,%esp
  struct buf *b;

  acquire(&bcache.lock);
801000b7:	c7 04 24 80 de 10 80 	movl   $0x8010de80,(%esp)
801000be:	e8 44 54 00 00       	call   80105507 <acquire>

 loop:
  // Is the block already cached?
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
801000c3:	a1 94 1d 11 80       	mov    0x80111d94,%eax
801000c8:	89 45 f4             	mov    %eax,-0xc(%ebp)
801000cb:	eb 63                	jmp    80100130 <bget+0x7f>
    if(b->dev == dev && b->blockno == blockno){
801000cd:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000d0:	8b 40 04             	mov    0x4(%eax),%eax
801000d3:	3b 45 08             	cmp    0x8(%ebp),%eax
801000d6:	75 4f                	jne    80100127 <bget+0x76>
801000d8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000db:	8b 40 08             	mov    0x8(%eax),%eax
801000de:	3b 45 0c             	cmp    0xc(%ebp),%eax
801000e1:	75 44                	jne    80100127 <bget+0x76>
      if(!(b->flags & B_BUSY)){
801000e3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000e6:	8b 00                	mov    (%eax),%eax
801000e8:	83 e0 01             	and    $0x1,%eax
801000eb:	85 c0                	test   %eax,%eax
801000ed:	75 23                	jne    80100112 <bget+0x61>
        b->flags |= B_BUSY;
801000ef:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000f2:	8b 00                	mov    (%eax),%eax
801000f4:	89 c2                	mov    %eax,%edx
801000f6:	83 ca 01             	or     $0x1,%edx
801000f9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000fc:	89 10                	mov    %edx,(%eax)
        release(&bcache.lock);
801000fe:	c7 04 24 80 de 10 80 	movl   $0x8010de80,(%esp)
80100105:	e8 66 54 00 00       	call   80105570 <release>
        return b;
8010010a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010010d:	e9 93 00 00 00       	jmp    801001a5 <bget+0xf4>
      }
      sleep(b, &bcache.lock);
80100112:	c7 44 24 04 80 de 10 	movl   $0x8010de80,0x4(%esp)
80100119:	80 
8010011a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010011d:	89 04 24             	mov    %eax,(%esp)
80100120:	e8 09 51 00 00       	call   8010522e <sleep>
      goto loop;
80100125:	eb 9c                	jmp    801000c3 <bget+0x12>

  acquire(&bcache.lock);

 loop:
  // Is the block already cached?
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
80100127:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010012a:	8b 40 10             	mov    0x10(%eax),%eax
8010012d:	89 45 f4             	mov    %eax,-0xc(%ebp)
80100130:	81 7d f4 84 1d 11 80 	cmpl   $0x80111d84,-0xc(%ebp)
80100137:	75 94                	jne    801000cd <bget+0x1c>
  }

  // Not cached; recycle some non-busy and clean buffer.
  // "clean" because B_DIRTY and !B_BUSY means log.c
  // hasn't yet committed the changes to the buffer.
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100139:	a1 90 1d 11 80       	mov    0x80111d90,%eax
8010013e:	89 45 f4             	mov    %eax,-0xc(%ebp)
80100141:	eb 4d                	jmp    80100190 <bget+0xdf>
    if((b->flags & B_BUSY) == 0 && (b->flags & B_DIRTY) == 0){
80100143:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100146:	8b 00                	mov    (%eax),%eax
80100148:	83 e0 01             	and    $0x1,%eax
8010014b:	85 c0                	test   %eax,%eax
8010014d:	75 38                	jne    80100187 <bget+0xd6>
8010014f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100152:	8b 00                	mov    (%eax),%eax
80100154:	83 e0 04             	and    $0x4,%eax
80100157:	85 c0                	test   %eax,%eax
80100159:	75 2c                	jne    80100187 <bget+0xd6>
      b->dev = dev;
8010015b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010015e:	8b 55 08             	mov    0x8(%ebp),%edx
80100161:	89 50 04             	mov    %edx,0x4(%eax)
      b->blockno = blockno;
80100164:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100167:	8b 55 0c             	mov    0xc(%ebp),%edx
8010016a:	89 50 08             	mov    %edx,0x8(%eax)
      b->flags = B_BUSY;
8010016d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100170:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
      release(&bcache.lock);
80100176:	c7 04 24 80 de 10 80 	movl   $0x8010de80,(%esp)
8010017d:	e8 ee 53 00 00       	call   80105570 <release>
      return b;
80100182:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100185:	eb 1e                	jmp    801001a5 <bget+0xf4>
  }

  // Not cached; recycle some non-busy and clean buffer.
  // "clean" because B_DIRTY and !B_BUSY means log.c
  // hasn't yet committed the changes to the buffer.
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100187:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010018a:	8b 40 0c             	mov    0xc(%eax),%eax
8010018d:	89 45 f4             	mov    %eax,-0xc(%ebp)
80100190:	81 7d f4 84 1d 11 80 	cmpl   $0x80111d84,-0xc(%ebp)
80100197:	75 aa                	jne    80100143 <bget+0x92>
      b->flags = B_BUSY;
      release(&bcache.lock);
      return b;
    }
  }
  panic("bget: no buffers");
80100199:	c7 04 24 0b 8b 10 80 	movl   $0x80108b0b,(%esp)
801001a0:	e8 c4 06 00 00       	call   80100869 <panic>
}
801001a5:	c9                   	leave  
801001a6:	c3                   	ret    

801001a7 <bread>:

// Return a B_BUSY buf with the contents of the indicated block.
struct buf*
bread(uint dev, uint blockno)
{
801001a7:	55                   	push   %ebp
801001a8:	89 e5                	mov    %esp,%ebp
801001aa:	83 ec 28             	sub    $0x28,%esp
  struct buf *b;

  b = bget(dev, blockno);
801001ad:	8b 45 0c             	mov    0xc(%ebp),%eax
801001b0:	89 44 24 04          	mov    %eax,0x4(%esp)
801001b4:	8b 45 08             	mov    0x8(%ebp),%eax
801001b7:	89 04 24             	mov    %eax,(%esp)
801001ba:	e8 f2 fe ff ff       	call   801000b1 <bget>
801001bf:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(!(b->flags & B_VALID)) {
801001c2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801001c5:	8b 00                	mov    (%eax),%eax
801001c7:	83 e0 02             	and    $0x2,%eax
801001ca:	85 c0                	test   %eax,%eax
801001cc:	75 0b                	jne    801001d9 <bread+0x32>
    iderw(b);
801001ce:	8b 45 f4             	mov    -0xc(%ebp),%eax
801001d1:	89 04 24             	mov    %eax,(%esp)
801001d4:	e8 37 2e 00 00       	call   80103010 <iderw>
  }
  return b;
801001d9:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801001dc:	c9                   	leave  
801001dd:	c3                   	ret    

801001de <bwrite>:

// Write b's contents to disk.  Must be B_BUSY.
void
bwrite(struct buf *b)
{
801001de:	55                   	push   %ebp
801001df:	89 e5                	mov    %esp,%ebp
801001e1:	83 ec 18             	sub    $0x18,%esp
  if((b->flags & B_BUSY) == 0)
801001e4:	8b 45 08             	mov    0x8(%ebp),%eax
801001e7:	8b 00                	mov    (%eax),%eax
801001e9:	83 e0 01             	and    $0x1,%eax
801001ec:	85 c0                	test   %eax,%eax
801001ee:	75 0c                	jne    801001fc <bwrite+0x1e>
    panic("bwrite");
801001f0:	c7 04 24 1c 8b 10 80 	movl   $0x80108b1c,(%esp)
801001f7:	e8 6d 06 00 00       	call   80100869 <panic>
  b->flags |= B_DIRTY;
801001fc:	8b 45 08             	mov    0x8(%ebp),%eax
801001ff:	8b 00                	mov    (%eax),%eax
80100201:	89 c2                	mov    %eax,%edx
80100203:	83 ca 04             	or     $0x4,%edx
80100206:	8b 45 08             	mov    0x8(%ebp),%eax
80100209:	89 10                	mov    %edx,(%eax)
  iderw(b);
8010020b:	8b 45 08             	mov    0x8(%ebp),%eax
8010020e:	89 04 24             	mov    %eax,(%esp)
80100211:	e8 fa 2d 00 00       	call   80103010 <iderw>
}
80100216:	c9                   	leave  
80100217:	c3                   	ret    

80100218 <brelse>:

// Release a B_BUSY buffer.
// Move to the head of the MRU list.
void
brelse(struct buf *b)
{
80100218:	55                   	push   %ebp
80100219:	89 e5                	mov    %esp,%ebp
8010021b:	83 ec 18             	sub    $0x18,%esp
  if((b->flags & B_BUSY) == 0)
8010021e:	8b 45 08             	mov    0x8(%ebp),%eax
80100221:	8b 00                	mov    (%eax),%eax
80100223:	83 e0 01             	and    $0x1,%eax
80100226:	85 c0                	test   %eax,%eax
80100228:	75 0c                	jne    80100236 <brelse+0x1e>
    panic("brelse");
8010022a:	c7 04 24 23 8b 10 80 	movl   $0x80108b23,(%esp)
80100231:	e8 33 06 00 00       	call   80100869 <panic>

  acquire(&bcache.lock);
80100236:	c7 04 24 80 de 10 80 	movl   $0x8010de80,(%esp)
8010023d:	e8 c5 52 00 00       	call   80105507 <acquire>

  b->next->prev = b->prev;
80100242:	8b 45 08             	mov    0x8(%ebp),%eax
80100245:	8b 40 10             	mov    0x10(%eax),%eax
80100248:	8b 55 08             	mov    0x8(%ebp),%edx
8010024b:	8b 52 0c             	mov    0xc(%edx),%edx
8010024e:	89 50 0c             	mov    %edx,0xc(%eax)
  b->prev->next = b->next;
80100251:	8b 45 08             	mov    0x8(%ebp),%eax
80100254:	8b 40 0c             	mov    0xc(%eax),%eax
80100257:	8b 55 08             	mov    0x8(%ebp),%edx
8010025a:	8b 52 10             	mov    0x10(%edx),%edx
8010025d:	89 50 10             	mov    %edx,0x10(%eax)
  b->next = bcache.head.next;
80100260:	8b 15 94 1d 11 80    	mov    0x80111d94,%edx
80100266:	8b 45 08             	mov    0x8(%ebp),%eax
80100269:	89 50 10             	mov    %edx,0x10(%eax)
  b->prev = &bcache.head;
8010026c:	8b 45 08             	mov    0x8(%ebp),%eax
8010026f:	c7 40 0c 84 1d 11 80 	movl   $0x80111d84,0xc(%eax)
  bcache.head.next->prev = b;
80100276:	a1 94 1d 11 80       	mov    0x80111d94,%eax
8010027b:	8b 55 08             	mov    0x8(%ebp),%edx
8010027e:	89 50 0c             	mov    %edx,0xc(%eax)
  bcache.head.next = b;
80100281:	8b 45 08             	mov    0x8(%ebp),%eax
80100284:	a3 94 1d 11 80       	mov    %eax,0x80111d94

  b->flags &= ~B_BUSY;
80100289:	8b 45 08             	mov    0x8(%ebp),%eax
8010028c:	8b 00                	mov    (%eax),%eax
8010028e:	89 c2                	mov    %eax,%edx
80100290:	83 e2 fe             	and    $0xfffffffe,%edx
80100293:	8b 45 08             	mov    0x8(%ebp),%eax
80100296:	89 10                	mov    %edx,(%eax)
  wakeup(b);
80100298:	8b 45 08             	mov    0x8(%ebp),%eax
8010029b:	89 04 24             	mov    %eax,(%esp)
8010029e:	e8 65 50 00 00       	call   80105308 <wakeup>

  release(&bcache.lock);
801002a3:	c7 04 24 80 de 10 80 	movl   $0x8010de80,(%esp)
801002aa:	e8 c1 52 00 00       	call   80105570 <release>
}
801002af:	c9                   	leave  
801002b0:	c3                   	ret    
801002b1:	00 00                	add    %al,(%eax)
	...

801002b4 <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
801002b4:	55                   	push   %ebp
801002b5:	89 e5                	mov    %esp,%ebp
801002b7:	83 ec 14             	sub    $0x14,%esp
801002ba:	8b 45 08             	mov    0x8(%ebp),%eax
801002bd:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801002c1:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
801002c5:	89 c2                	mov    %eax,%edx
801002c7:	ec                   	in     (%dx),%al
801002c8:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
801002cb:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
801002cf:	c9                   	leave  
801002d0:	c3                   	ret    

801002d1 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
801002d1:	55                   	push   %ebp
801002d2:	89 e5                	mov    %esp,%ebp
801002d4:	83 ec 08             	sub    $0x8,%esp
801002d7:	8b 55 08             	mov    0x8(%ebp),%edx
801002da:	8b 45 0c             	mov    0xc(%ebp),%eax
801002dd:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
801002e1:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801002e4:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
801002e8:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
801002ec:	ee                   	out    %al,(%dx)
}
801002ed:	c9                   	leave  
801002ee:	c3                   	ret    

801002ef <cmosread>:
#include "date.h"
#include "x86.h"
#include "cmos.h"

uint cmosread(uint reg)
{
801002ef:	55                   	push   %ebp
801002f0:	89 e5                	mov    %esp,%ebp
801002f2:	83 ec 18             	sub    $0x18,%esp
  if(reg >= CMOS_NMI_BIT)
801002f5:	83 7d 08 7f          	cmpl   $0x7f,0x8(%ebp)
801002f9:	76 0c                	jbe    80100307 <cmosread+0x18>
    panic("cmosread: invalid register");
801002fb:	c7 04 24 2a 8b 10 80 	movl   $0x80108b2a,(%esp)
80100302:	e8 62 05 00 00       	call   80100869 <panic>

  outb(CMOS_PORT, reg);
80100307:	8b 45 08             	mov    0x8(%ebp),%eax
8010030a:	0f b6 c0             	movzbl %al,%eax
8010030d:	89 44 24 04          	mov    %eax,0x4(%esp)
80100311:	c7 04 24 70 00 00 00 	movl   $0x70,(%esp)
80100318:	e8 b4 ff ff ff       	call   801002d1 <outb>
  microdelay(200);
8010031d:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
80100324:	e8 52 34 00 00       	call   8010377b <microdelay>
  return inb(CMOS_RETURN);
80100329:	c7 04 24 71 00 00 00 	movl   $0x71,(%esp)
80100330:	e8 7f ff ff ff       	call   801002b4 <inb>
80100335:	0f b6 c0             	movzbl %al,%eax
}
80100338:	c9                   	leave  
80100339:	c3                   	ret    

8010033a <cmoswrite>:

void cmoswrite(uint reg, uint data)
{
8010033a:	55                   	push   %ebp
8010033b:	89 e5                	mov    %esp,%ebp
8010033d:	83 ec 18             	sub    $0x18,%esp
  if(reg >= CMOS_NMI_BIT)
80100340:	83 7d 08 7f          	cmpl   $0x7f,0x8(%ebp)
80100344:	76 0c                	jbe    80100352 <cmoswrite+0x18>
    panic("cmoswrite: invalid register");
80100346:	c7 04 24 45 8b 10 80 	movl   $0x80108b45,(%esp)
8010034d:	e8 17 05 00 00       	call   80100869 <panic>
  if(data > 0xff)
80100352:	81 7d 0c ff 00 00 00 	cmpl   $0xff,0xc(%ebp)
80100359:	76 0c                	jbe    80100367 <cmoswrite+0x2d>
    panic("cmoswrite: invalid data");
8010035b:	c7 04 24 61 8b 10 80 	movl   $0x80108b61,(%esp)
80100362:	e8 02 05 00 00       	call   80100869 <panic>

  outb(CMOS_PORT,  reg);
80100367:	8b 45 08             	mov    0x8(%ebp),%eax
8010036a:	0f b6 c0             	movzbl %al,%eax
8010036d:	89 44 24 04          	mov    %eax,0x4(%esp)
80100371:	c7 04 24 70 00 00 00 	movl   $0x70,(%esp)
80100378:	e8 54 ff ff ff       	call   801002d1 <outb>
  microdelay(200);
8010037d:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
80100384:	e8 f2 33 00 00       	call   8010377b <microdelay>
  outb(CMOS_RETURN, data);
80100389:	8b 45 0c             	mov    0xc(%ebp),%eax
8010038c:	0f b6 c0             	movzbl %al,%eax
8010038f:	89 44 24 04          	mov    %eax,0x4(%esp)
80100393:	c7 04 24 71 00 00 00 	movl   $0x71,(%esp)
8010039a:	e8 32 ff ff ff       	call   801002d1 <outb>
}
8010039f:	c9                   	leave  
801003a0:	c3                   	ret    

801003a1 <fill_rtcdate>:

static void fill_rtcdate(struct rtcdate *r)
{
801003a1:	55                   	push   %ebp
801003a2:	89 e5                	mov    %esp,%ebp
801003a4:	83 ec 18             	sub    $0x18,%esp
  r->second = cmosread(CMOS_SECS);
801003a7:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801003ae:	e8 3c ff ff ff       	call   801002ef <cmosread>
801003b3:	8b 55 08             	mov    0x8(%ebp),%edx
801003b6:	89 02                	mov    %eax,(%edx)
  r->minute = cmosread(CMOS_MINS);
801003b8:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
801003bf:	e8 2b ff ff ff       	call   801002ef <cmosread>
801003c4:	8b 55 08             	mov    0x8(%ebp),%edx
801003c7:	89 42 04             	mov    %eax,0x4(%edx)
  r->hour   = cmosread(CMOS_HOURS);
801003ca:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
801003d1:	e8 19 ff ff ff       	call   801002ef <cmosread>
801003d6:	8b 55 08             	mov    0x8(%ebp),%edx
801003d9:	89 42 08             	mov    %eax,0x8(%edx)
  r->day    = cmosread(CMOS_DAY);
801003dc:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
801003e3:	e8 07 ff ff ff       	call   801002ef <cmosread>
801003e8:	8b 55 08             	mov    0x8(%ebp),%edx
801003eb:	89 42 0c             	mov    %eax,0xc(%edx)
  r->month  = cmosread(CMOS_MONTH);
801003ee:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
801003f5:	e8 f5 fe ff ff       	call   801002ef <cmosread>
801003fa:	8b 55 08             	mov    0x8(%ebp),%edx
801003fd:	89 42 10             	mov    %eax,0x10(%edx)
  r->year   = cmosread(CMOS_YEAR);
80100400:	c7 04 24 09 00 00 00 	movl   $0x9,(%esp)
80100407:	e8 e3 fe ff ff       	call   801002ef <cmosread>
8010040c:	8b 55 08             	mov    0x8(%ebp),%edx
8010040f:	89 42 14             	mov    %eax,0x14(%edx)
}
80100412:	c9                   	leave  
80100413:	c3                   	ret    

80100414 <cmostime>:

// qemu seems to use 24-hour GWT and the values are BCD encoded
void cmostime(struct rtcdate *r)
{
80100414:	55                   	push   %ebp
80100415:	89 e5                	mov    %esp,%ebp
80100417:	83 ec 58             	sub    $0x58,%esp
  struct rtcdate t1, t2;
  int sb, bcd, tf;

  sb = cmosread(CMOS_STATB);
8010041a:	c7 04 24 0b 00 00 00 	movl   $0xb,(%esp)
80100421:	e8 c9 fe ff ff       	call   801002ef <cmosread>
80100426:	89 45 ec             	mov    %eax,-0x14(%ebp)

  bcd = (sb & CMOS_BINARY_BIT) == 0;
80100429:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010042c:	83 e0 04             	and    $0x4,%eax
8010042f:	85 c0                	test   %eax,%eax
80100431:	0f 94 c0             	sete   %al
80100434:	0f b6 c0             	movzbl %al,%eax
80100437:	89 45 f0             	mov    %eax,-0x10(%ebp)
  tf = (sb & CMOS_24H_BIT) != 0;
8010043a:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010043d:	83 e0 02             	and    $0x2,%eax
80100440:	85 c0                	test   %eax,%eax
80100442:	0f 95 c0             	setne  %al
80100445:	0f b6 c0             	movzbl %al,%eax
80100448:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010044b:	eb 01                	jmp    8010044e <cmostime+0x3a>
    if(cmosread(CMOS_STATA) & CMOS_UIP_BIT)
      continue;
    fill_rtcdate(&t2);
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
      break;
  }
8010044d:	90                   	nop
  bcd = (sb & CMOS_BINARY_BIT) == 0;
  tf = (sb & CMOS_24H_BIT) != 0;

  // make sure CMOS doesn't modify time while we read it
  for(;;){
    fill_rtcdate(&t1);
8010044e:	8d 45 d4             	lea    -0x2c(%ebp),%eax
80100451:	89 04 24             	mov    %eax,(%esp)
80100454:	e8 48 ff ff ff       	call   801003a1 <fill_rtcdate>
    if(cmosread(CMOS_STATA) & CMOS_UIP_BIT)
80100459:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
80100460:	e8 8a fe ff ff       	call   801002ef <cmosread>
80100465:	25 80 00 00 00       	and    $0x80,%eax
8010046a:	85 c0                	test   %eax,%eax
8010046c:	74 03                	je     80100471 <cmostime+0x5d>
      continue;
8010046e:	90                   	nop
    fill_rtcdate(&t2);
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
      break;
  }
8010046f:	eb dd                	jmp    8010044e <cmostime+0x3a>
  // make sure CMOS doesn't modify time while we read it
  for(;;){
    fill_rtcdate(&t1);
    if(cmosread(CMOS_STATA) & CMOS_UIP_BIT)
      continue;
    fill_rtcdate(&t2);
80100471:	8d 45 bc             	lea    -0x44(%ebp),%eax
80100474:	89 04 24             	mov    %eax,(%esp)
80100477:	e8 25 ff ff ff       	call   801003a1 <fill_rtcdate>
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
8010047c:	c7 44 24 08 18 00 00 	movl   $0x18,0x8(%esp)
80100483:	00 
80100484:	8d 45 bc             	lea    -0x44(%ebp),%eax
80100487:	89 44 24 04          	mov    %eax,0x4(%esp)
8010048b:	8d 45 d4             	lea    -0x2c(%ebp),%eax
8010048e:	89 04 24             	mov    %eax,(%esp)
80100491:	e8 4b 53 00 00       	call   801057e1 <memcmp>
80100496:	85 c0                	test   %eax,%eax
80100498:	75 b3                	jne    8010044d <cmostime+0x39>
      break;
8010049a:	90                   	nop
  }

  // backup raw values since BCD conversion removes PM bit from hour
  t2 = t1;
8010049b:	8b 45 d4             	mov    -0x2c(%ebp),%eax
8010049e:	89 45 bc             	mov    %eax,-0x44(%ebp)
801004a1:	8b 45 d8             	mov    -0x28(%ebp),%eax
801004a4:	89 45 c0             	mov    %eax,-0x40(%ebp)
801004a7:	8b 45 dc             	mov    -0x24(%ebp),%eax
801004aa:	89 45 c4             	mov    %eax,-0x3c(%ebp)
801004ad:	8b 45 e0             	mov    -0x20(%ebp),%eax
801004b0:	89 45 c8             	mov    %eax,-0x38(%ebp)
801004b3:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801004b6:	89 45 cc             	mov    %eax,-0x34(%ebp)
801004b9:	8b 45 e8             	mov    -0x18(%ebp),%eax
801004bc:	89 45 d0             	mov    %eax,-0x30(%ebp)

  // convert t1 from BCD
  if(bcd){
801004bf:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801004c3:	0f 84 a8 00 00 00    	je     80100571 <cmostime+0x15d>
#define    CONV(x)     (t1.x = ((t1.x >> 4) * 10) + (t1.x & 0xf))
    CONV(second);
801004c9:	8b 45 d4             	mov    -0x2c(%ebp),%eax
801004cc:	89 c2                	mov    %eax,%edx
801004ce:	c1 ea 04             	shr    $0x4,%edx
801004d1:	89 d0                	mov    %edx,%eax
801004d3:	c1 e0 02             	shl    $0x2,%eax
801004d6:	01 d0                	add    %edx,%eax
801004d8:	01 c0                	add    %eax,%eax
801004da:	8b 55 d4             	mov    -0x2c(%ebp),%edx
801004dd:	83 e2 0f             	and    $0xf,%edx
801004e0:	01 d0                	add    %edx,%eax
801004e2:	89 45 d4             	mov    %eax,-0x2c(%ebp)
    CONV(minute);
801004e5:	8b 45 d8             	mov    -0x28(%ebp),%eax
801004e8:	89 c2                	mov    %eax,%edx
801004ea:	c1 ea 04             	shr    $0x4,%edx
801004ed:	89 d0                	mov    %edx,%eax
801004ef:	c1 e0 02             	shl    $0x2,%eax
801004f2:	01 d0                	add    %edx,%eax
801004f4:	01 c0                	add    %eax,%eax
801004f6:	8b 55 d8             	mov    -0x28(%ebp),%edx
801004f9:	83 e2 0f             	and    $0xf,%edx
801004fc:	01 d0                	add    %edx,%eax
801004fe:	89 45 d8             	mov    %eax,-0x28(%ebp)
    CONV(hour  );
80100501:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100504:	89 c2                	mov    %eax,%edx
80100506:	c1 ea 04             	shr    $0x4,%edx
80100509:	89 d0                	mov    %edx,%eax
8010050b:	c1 e0 02             	shl    $0x2,%eax
8010050e:	01 d0                	add    %edx,%eax
80100510:	01 c0                	add    %eax,%eax
80100512:	8b 55 dc             	mov    -0x24(%ebp),%edx
80100515:	83 e2 0f             	and    $0xf,%edx
80100518:	01 d0                	add    %edx,%eax
8010051a:	89 45 dc             	mov    %eax,-0x24(%ebp)
    CONV(day   );
8010051d:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100520:	89 c2                	mov    %eax,%edx
80100522:	c1 ea 04             	shr    $0x4,%edx
80100525:	89 d0                	mov    %edx,%eax
80100527:	c1 e0 02             	shl    $0x2,%eax
8010052a:	01 d0                	add    %edx,%eax
8010052c:	01 c0                	add    %eax,%eax
8010052e:	8b 55 e0             	mov    -0x20(%ebp),%edx
80100531:	83 e2 0f             	and    $0xf,%edx
80100534:	01 d0                	add    %edx,%eax
80100536:	89 45 e0             	mov    %eax,-0x20(%ebp)
    CONV(month );
80100539:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010053c:	89 c2                	mov    %eax,%edx
8010053e:	c1 ea 04             	shr    $0x4,%edx
80100541:	89 d0                	mov    %edx,%eax
80100543:	c1 e0 02             	shl    $0x2,%eax
80100546:	01 d0                	add    %edx,%eax
80100548:	01 c0                	add    %eax,%eax
8010054a:	8b 55 e4             	mov    -0x1c(%ebp),%edx
8010054d:	83 e2 0f             	and    $0xf,%edx
80100550:	01 d0                	add    %edx,%eax
80100552:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    CONV(year  );
80100555:	8b 45 e8             	mov    -0x18(%ebp),%eax
80100558:	89 c2                	mov    %eax,%edx
8010055a:	c1 ea 04             	shr    $0x4,%edx
8010055d:	89 d0                	mov    %edx,%eax
8010055f:	c1 e0 02             	shl    $0x2,%eax
80100562:	01 d0                	add    %edx,%eax
80100564:	01 c0                	add    %eax,%eax
80100566:	8b 55 e8             	mov    -0x18(%ebp),%edx
80100569:	83 e2 0f             	and    $0xf,%edx
8010056c:	01 d0                	add    %edx,%eax
8010056e:	89 45 e8             	mov    %eax,-0x18(%ebp)
#undef     CONV
  }

  // convert 12 hour format to 24 hour format
  if(!tf){
80100571:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80100575:	75 2e                	jne    801005a5 <cmostime+0x191>
    if(t2.hour & CMOS_PM_BIT){
80100577:	8b 45 c4             	mov    -0x3c(%ebp),%eax
8010057a:	25 80 00 00 00       	and    $0x80,%eax
8010057f:	85 c0                	test   %eax,%eax
80100581:	74 22                	je     801005a5 <cmostime+0x191>
      t1.hour = (t1.hour + 12) % 24;
80100583:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100586:	8d 48 0c             	lea    0xc(%eax),%ecx
80100589:	ba ab aa aa aa       	mov    $0xaaaaaaab,%edx
8010058e:	89 c8                	mov    %ecx,%eax
80100590:	f7 e2                	mul    %edx
80100592:	c1 ea 04             	shr    $0x4,%edx
80100595:	89 d0                	mov    %edx,%eax
80100597:	01 c0                	add    %eax,%eax
80100599:	01 d0                	add    %edx,%eax
8010059b:	c1 e0 03             	shl    $0x3,%eax
8010059e:	89 ca                	mov    %ecx,%edx
801005a0:	29 c2                	sub    %eax,%edx
801005a2:	89 55 dc             	mov    %edx,-0x24(%ebp)
    }
  }

  *r = t1;
801005a5:	8b 45 08             	mov    0x8(%ebp),%eax
801005a8:	8b 55 d4             	mov    -0x2c(%ebp),%edx
801005ab:	89 10                	mov    %edx,(%eax)
801005ad:	8b 55 d8             	mov    -0x28(%ebp),%edx
801005b0:	89 50 04             	mov    %edx,0x4(%eax)
801005b3:	8b 55 dc             	mov    -0x24(%ebp),%edx
801005b6:	89 50 08             	mov    %edx,0x8(%eax)
801005b9:	8b 55 e0             	mov    -0x20(%ebp),%edx
801005bc:	89 50 0c             	mov    %edx,0xc(%eax)
801005bf:	8b 55 e4             	mov    -0x1c(%ebp),%edx
801005c2:	89 50 10             	mov    %edx,0x10(%eax)
801005c5:	8b 55 e8             	mov    -0x18(%ebp),%edx
801005c8:	89 50 14             	mov    %edx,0x14(%eax)
  r->year += 2000;
801005cb:	8b 45 08             	mov    0x8(%ebp),%eax
801005ce:	8b 40 14             	mov    0x14(%eax),%eax
801005d1:	8d 90 d0 07 00 00    	lea    0x7d0(%eax),%edx
801005d7:	8b 45 08             	mov    0x8(%ebp),%eax
801005da:	89 50 14             	mov    %edx,0x14(%eax)
}
801005dd:	c9                   	leave  
801005de:	c3                   	ret    
	...

801005e0 <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
801005e0:	55                   	push   %ebp
801005e1:	89 e5                	mov    %esp,%ebp
801005e3:	83 ec 14             	sub    $0x14,%esp
801005e6:	8b 45 08             	mov    0x8(%ebp),%eax
801005e9:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801005ed:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
801005f1:	89 c2                	mov    %eax,%edx
801005f3:	ec                   	in     (%dx),%al
801005f4:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
801005f7:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
801005fb:	c9                   	leave  
801005fc:	c3                   	ret    

801005fd <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
801005fd:	55                   	push   %ebp
801005fe:	89 e5                	mov    %esp,%ebp
80100600:	83 ec 08             	sub    $0x8,%esp
80100603:	8b 55 08             	mov    0x8(%ebp),%edx
80100606:	8b 45 0c             	mov    0xc(%ebp),%eax
80100609:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
8010060d:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100610:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80100614:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80100618:	ee                   	out    %al,(%dx)
}
80100619:	c9                   	leave  
8010061a:	c3                   	ret    

8010061b <cli>:
  asm volatile("movw %0, %%gs" : : "r" (v));
}

static inline void
cli(void)
{
8010061b:	55                   	push   %ebp
8010061c:	89 e5                	mov    %esp,%ebp
  asm volatile("cli");
8010061e:	fa                   	cli    
}
8010061f:	5d                   	pop    %ebp
80100620:	c3                   	ret    

80100621 <printint>:
  int locking;
} cons;

static void
printint(int xx, int base, int sign)
{
80100621:	55                   	push   %ebp
80100622:	89 e5                	mov    %esp,%ebp
80100624:	53                   	push   %ebx
80100625:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = (xx < 0)))
80100628:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
8010062c:	74 19                	je     80100647 <printint+0x26>
8010062e:	8b 45 08             	mov    0x8(%ebp),%eax
80100631:	c1 e8 1f             	shr    $0x1f,%eax
80100634:	89 45 10             	mov    %eax,0x10(%ebp)
80100637:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
8010063b:	74 0a                	je     80100647 <printint+0x26>
    x = -xx;
8010063d:	8b 45 08             	mov    0x8(%ebp),%eax
80100640:	f7 d8                	neg    %eax
80100642:	89 45 f4             	mov    %eax,-0xc(%ebp)
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = (xx < 0)))
80100645:	eb 06                	jmp    8010064d <printint+0x2c>
    x = -xx;
  else
    x = xx;
80100647:	8b 45 08             	mov    0x8(%ebp),%eax
8010064a:	89 45 f4             	mov    %eax,-0xc(%ebp)

  i = 0;
8010064d:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  do{
    buf[i++] = digits[x % base];
80100654:	8b 4d f0             	mov    -0x10(%ebp),%ecx
80100657:	8b 5d 0c             	mov    0xc(%ebp),%ebx
8010065a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010065d:	ba 00 00 00 00       	mov    $0x0,%edx
80100662:	f7 f3                	div    %ebx
80100664:	89 d0                	mov    %edx,%eax
80100666:	0f b6 80 04 a0 10 80 	movzbl -0x7fef5ffc(%eax),%eax
8010066d:	88 44 0d e0          	mov    %al,-0x20(%ebp,%ecx,1)
80100671:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
  }while((x /= base) != 0);
80100675:	8b 45 0c             	mov    0xc(%ebp),%eax
80100678:	89 45 d4             	mov    %eax,-0x2c(%ebp)
8010067b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010067e:	ba 00 00 00 00       	mov    $0x0,%edx
80100683:	f7 75 d4             	divl   -0x2c(%ebp)
80100686:	89 45 f4             	mov    %eax,-0xc(%ebp)
80100689:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010068d:	75 c5                	jne    80100654 <printint+0x33>

  if(sign)
8010068f:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80100693:	74 23                	je     801006b8 <printint+0x97>
    buf[i++] = '-';
80100695:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100698:	c6 44 05 e0 2d       	movb   $0x2d,-0x20(%ebp,%eax,1)
8010069d:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)

  while(--i >= 0)
801006a1:	eb 16                	jmp    801006b9 <printint+0x98>
    consputc(buf[i]);
801006a3:	8b 45 f0             	mov    -0x10(%ebp),%eax
801006a6:	0f b6 44 05 e0       	movzbl -0x20(%ebp,%eax,1),%eax
801006ab:	0f be c0             	movsbl %al,%eax
801006ae:	89 04 24             	mov    %eax,(%esp)
801006b1:	e8 ea 03 00 00       	call   80100aa0 <consputc>
801006b6:	eb 01                	jmp    801006b9 <printint+0x98>
  }while((x /= base) != 0);

  if(sign)
    buf[i++] = '-';

  while(--i >= 0)
801006b8:	90                   	nop
801006b9:	83 6d f0 01          	subl   $0x1,-0x10(%ebp)
801006bd:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801006c1:	79 e0                	jns    801006a3 <printint+0x82>
    consputc(buf[i]);
}
801006c3:	83 c4 44             	add    $0x44,%esp
801006c6:	5b                   	pop    %ebx
801006c7:	5d                   	pop    %ebp
801006c8:	c3                   	ret    

801006c9 <cprintf>:
//PAGEBREAK: 50

// Print to the console. only understands %d, %x, %p, %s.
void
cprintf(char *fmt, ...)
{
801006c9:	55                   	push   %ebp
801006ca:	89 e5                	mov    %esp,%ebp
801006cc:	83 ec 38             	sub    $0x38,%esp
  int i, c, locking;
  uint *argp;
  char *s;

  locking = cons.locking;
801006cf:	a1 f4 c5 10 80       	mov    0x8010c5f4,%eax
801006d4:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if(locking)
801006d7:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
801006db:	74 0c                	je     801006e9 <cprintf+0x20>
    acquire(&cons.lock);
801006dd:	c7 04 24 c0 c5 10 80 	movl   $0x8010c5c0,(%esp)
801006e4:	e8 1e 4e 00 00       	call   80105507 <acquire>

  if (fmt == 0)
801006e9:	8b 45 08             	mov    0x8(%ebp),%eax
801006ec:	85 c0                	test   %eax,%eax
801006ee:	75 0c                	jne    801006fc <cprintf+0x33>
    panic("null fmt");
801006f0:	c7 04 24 79 8b 10 80 	movl   $0x80108b79,(%esp)
801006f7:	e8 6d 01 00 00       	call   80100869 <panic>

  argp = (uint*)(void*)(&fmt + 1);
801006fc:	8d 45 08             	lea    0x8(%ebp),%eax
801006ff:	83 c0 04             	add    $0x4,%eax
80100702:	89 45 f0             	mov    %eax,-0x10(%ebp)
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100705:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
8010070c:	e9 20 01 00 00       	jmp    80100831 <cprintf+0x168>
    if(c != '%'){
80100711:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
80100715:	74 10                	je     80100727 <cprintf+0x5e>
      consputc(c);
80100717:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010071a:	89 04 24             	mov    %eax,(%esp)
8010071d:	e8 7e 03 00 00       	call   80100aa0 <consputc>
      continue;
80100722:	e9 06 01 00 00       	jmp    8010082d <cprintf+0x164>
    }
    c = fmt[++i] & 0xff;
80100727:	8b 55 08             	mov    0x8(%ebp),%edx
8010072a:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
8010072e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100731:	8d 04 02             	lea    (%edx,%eax,1),%eax
80100734:	0f b6 00             	movzbl (%eax),%eax
80100737:	0f be c0             	movsbl %al,%eax
8010073a:	25 ff 00 00 00       	and    $0xff,%eax
8010073f:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(c == 0)
80100742:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
80100746:	0f 84 08 01 00 00    	je     80100854 <cprintf+0x18b>
      break;
    switch(c){
8010074c:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010074f:	83 f8 70             	cmp    $0x70,%eax
80100752:	74 4d                	je     801007a1 <cprintf+0xd8>
80100754:	83 f8 70             	cmp    $0x70,%eax
80100757:	7f 13                	jg     8010076c <cprintf+0xa3>
80100759:	83 f8 25             	cmp    $0x25,%eax
8010075c:	0f 84 a6 00 00 00    	je     80100808 <cprintf+0x13f>
80100762:	83 f8 64             	cmp    $0x64,%eax
80100765:	74 14                	je     8010077b <cprintf+0xb2>
80100767:	e9 aa 00 00 00       	jmp    80100816 <cprintf+0x14d>
8010076c:	83 f8 73             	cmp    $0x73,%eax
8010076f:	74 53                	je     801007c4 <cprintf+0xfb>
80100771:	83 f8 78             	cmp    $0x78,%eax
80100774:	74 2b                	je     801007a1 <cprintf+0xd8>
80100776:	e9 9b 00 00 00       	jmp    80100816 <cprintf+0x14d>
    case 'd':
      printint(*argp++, 10, 1);
8010077b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010077e:	8b 00                	mov    (%eax),%eax
80100780:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
80100784:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
8010078b:	00 
8010078c:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
80100793:	00 
80100794:	89 04 24             	mov    %eax,(%esp)
80100797:	e8 85 fe ff ff       	call   80100621 <printint>
      break;
8010079c:	e9 8c 00 00 00       	jmp    8010082d <cprintf+0x164>
    case 'x':
    case 'p':
      printint(*argp++, 16, 0);
801007a1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801007a4:	8b 00                	mov    (%eax),%eax
801007a6:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
801007aa:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
801007b1:	00 
801007b2:	c7 44 24 04 10 00 00 	movl   $0x10,0x4(%esp)
801007b9:	00 
801007ba:	89 04 24             	mov    %eax,(%esp)
801007bd:	e8 5f fe ff ff       	call   80100621 <printint>
      break;
801007c2:	eb 69                	jmp    8010082d <cprintf+0x164>
    case 's':
      if((s = (char*)*argp++) == 0)
801007c4:	8b 45 f0             	mov    -0x10(%ebp),%eax
801007c7:	8b 00                	mov    (%eax),%eax
801007c9:	89 45 f4             	mov    %eax,-0xc(%ebp)
801007cc:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801007d0:	0f 94 c0             	sete   %al
801007d3:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
801007d7:	84 c0                	test   %al,%al
801007d9:	74 20                	je     801007fb <cprintf+0x132>
        s = "(null)";
801007db:	c7 45 f4 82 8b 10 80 	movl   $0x80108b82,-0xc(%ebp)
      for(; *s; s++)
801007e2:	eb 18                	jmp    801007fc <cprintf+0x133>
        consputc(*s);
801007e4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801007e7:	0f b6 00             	movzbl (%eax),%eax
801007ea:	0f be c0             	movsbl %al,%eax
801007ed:	89 04 24             	mov    %eax,(%esp)
801007f0:	e8 ab 02 00 00       	call   80100aa0 <consputc>
      printint(*argp++, 16, 0);
      break;
    case 's':
      if((s = (char*)*argp++) == 0)
        s = "(null)";
      for(; *s; s++)
801007f5:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801007f9:	eb 01                	jmp    801007fc <cprintf+0x133>
801007fb:	90                   	nop
801007fc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801007ff:	0f b6 00             	movzbl (%eax),%eax
80100802:	84 c0                	test   %al,%al
80100804:	75 de                	jne    801007e4 <cprintf+0x11b>
        consputc(*s);
      break;
80100806:	eb 25                	jmp    8010082d <cprintf+0x164>
    case '%':
      consputc('%');
80100808:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
8010080f:	e8 8c 02 00 00       	call   80100aa0 <consputc>
      break;
80100814:	eb 17                	jmp    8010082d <cprintf+0x164>
    default:
      // Print unknown % sequence to draw attention.
      consputc('%');
80100816:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
8010081d:	e8 7e 02 00 00       	call   80100aa0 <consputc>
      consputc(c);
80100822:	8b 45 e8             	mov    -0x18(%ebp),%eax
80100825:	89 04 24             	mov    %eax,(%esp)
80100828:	e8 73 02 00 00       	call   80100aa0 <consputc>

  if (fmt == 0)
    panic("null fmt");

  argp = (uint*)(void*)(&fmt + 1);
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
8010082d:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
80100831:	8b 55 08             	mov    0x8(%ebp),%edx
80100834:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100837:	8d 04 02             	lea    (%edx,%eax,1),%eax
8010083a:	0f b6 00             	movzbl (%eax),%eax
8010083d:	0f be c0             	movsbl %al,%eax
80100840:	25 ff 00 00 00       	and    $0xff,%eax
80100845:	89 45 e8             	mov    %eax,-0x18(%ebp)
80100848:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
8010084c:	0f 85 bf fe ff ff    	jne    80100711 <cprintf+0x48>
80100852:	eb 01                	jmp    80100855 <cprintf+0x18c>
      consputc(c);
      continue;
    }
    c = fmt[++i] & 0xff;
    if(c == 0)
      break;
80100854:	90                   	nop
      consputc(c);
      break;
    }
  }

  if(locking)
80100855:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80100859:	74 0c                	je     80100867 <cprintf+0x19e>
    release(&cons.lock);
8010085b:	c7 04 24 c0 c5 10 80 	movl   $0x8010c5c0,(%esp)
80100862:	e8 09 4d 00 00       	call   80105570 <release>
}
80100867:	c9                   	leave  
80100868:	c3                   	ret    

80100869 <panic>:

void
panic(char *s)
{
80100869:	55                   	push   %ebp
8010086a:	89 e5                	mov    %esp,%ebp
8010086c:	83 ec 48             	sub    $0x48,%esp
  int i;
  uint pcs[10];
  
  cli();
8010086f:	e8 a7 fd ff ff       	call   8010061b <cli>
  cons.locking = 0;
80100874:	c7 05 f4 c5 10 80 00 	movl   $0x0,0x8010c5f4
8010087b:	00 00 00 
  cprintf("cpu%d: panic: ", cpu->id);
8010087e:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80100884:	0f b6 00             	movzbl (%eax),%eax
80100887:	0f b6 c0             	movzbl %al,%eax
8010088a:	89 44 24 04          	mov    %eax,0x4(%esp)
8010088e:	c7 04 24 89 8b 10 80 	movl   $0x80108b89,(%esp)
80100895:	e8 2f fe ff ff       	call   801006c9 <cprintf>
  cprintf(s);
8010089a:	8b 45 08             	mov    0x8(%ebp),%eax
8010089d:	89 04 24             	mov    %eax,(%esp)
801008a0:	e8 24 fe ff ff       	call   801006c9 <cprintf>
  cprintf("\n");
801008a5:	c7 04 24 98 8b 10 80 	movl   $0x80108b98,(%esp)
801008ac:	e8 18 fe ff ff       	call   801006c9 <cprintf>
  getcallerpcs(&s, NELEM(pcs), pcs);
801008b1:	8d 45 cc             	lea    -0x34(%ebp),%eax
801008b4:	89 44 24 08          	mov    %eax,0x8(%esp)
801008b8:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
801008bf:	00 
801008c0:	8d 45 08             	lea    0x8(%ebp),%eax
801008c3:	89 04 24             	mov    %eax,(%esp)
801008c6:	e8 f4 4c 00 00       	call   801055bf <getcallerpcs>
  for(i=0; i<10; i++)
801008cb:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
801008d2:	eb 1b                	jmp    801008ef <panic+0x86>
    cprintf(" %p", pcs[i]);
801008d4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801008d7:	8b 44 85 cc          	mov    -0x34(%ebp,%eax,4),%eax
801008db:	89 44 24 04          	mov    %eax,0x4(%esp)
801008df:	c7 04 24 9a 8b 10 80 	movl   $0x80108b9a,(%esp)
801008e6:	e8 de fd ff ff       	call   801006c9 <cprintf>
  cons.locking = 0;
  cprintf("cpu%d: panic: ", cpu->id);
  cprintf(s);
  cprintf("\n");
  getcallerpcs(&s, NELEM(pcs), pcs);
  for(i=0; i<10; i++)
801008eb:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801008ef:	83 7d f4 09          	cmpl   $0x9,-0xc(%ebp)
801008f3:	7e df                	jle    801008d4 <panic+0x6b>
    cprintf(" %p", pcs[i]);
  panicked = 1; // freeze other CPU
801008f5:	c7 05 a0 c5 10 80 01 	movl   $0x1,0x8010c5a0
801008fc:	00 00 00 
  for(;;)
    ;
801008ff:	eb fe                	jmp    801008ff <panic+0x96>

80100901 <cgaputc>:
#define CRTPORT 0x3d4
static ushort *crt = (ushort*)P2V(0xb8000);  // CGA memory

static void
cgaputc(int c)
{
80100901:	55                   	push   %ebp
80100902:	89 e5                	mov    %esp,%ebp
80100904:	83 ec 28             	sub    $0x28,%esp
  int pos;
  
  // Cursor position: col + 80*row.
  outb(CRTPORT, 14);
80100907:	c7 44 24 04 0e 00 00 	movl   $0xe,0x4(%esp)
8010090e:	00 
8010090f:	c7 04 24 d4 03 00 00 	movl   $0x3d4,(%esp)
80100916:	e8 e2 fc ff ff       	call   801005fd <outb>
  pos = inb(CRTPORT+1) << 8;
8010091b:	c7 04 24 d5 03 00 00 	movl   $0x3d5,(%esp)
80100922:	e8 b9 fc ff ff       	call   801005e0 <inb>
80100927:	0f b6 c0             	movzbl %al,%eax
8010092a:	c1 e0 08             	shl    $0x8,%eax
8010092d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  outb(CRTPORT, 15);
80100930:	c7 44 24 04 0f 00 00 	movl   $0xf,0x4(%esp)
80100937:	00 
80100938:	c7 04 24 d4 03 00 00 	movl   $0x3d4,(%esp)
8010093f:	e8 b9 fc ff ff       	call   801005fd <outb>
  pos |= inb(CRTPORT+1);
80100944:	c7 04 24 d5 03 00 00 	movl   $0x3d5,(%esp)
8010094b:	e8 90 fc ff ff       	call   801005e0 <inb>
80100950:	0f b6 c0             	movzbl %al,%eax
80100953:	09 45 f4             	or     %eax,-0xc(%ebp)

  if(c == '\n')
80100956:	83 7d 08 0a          	cmpl   $0xa,0x8(%ebp)
8010095a:	75 30                	jne    8010098c <cgaputc+0x8b>
    pos += 80 - pos%80;
8010095c:	8b 4d f4             	mov    -0xc(%ebp),%ecx
8010095f:	ba 67 66 66 66       	mov    $0x66666667,%edx
80100964:	89 c8                	mov    %ecx,%eax
80100966:	f7 ea                	imul   %edx
80100968:	c1 fa 05             	sar    $0x5,%edx
8010096b:	89 c8                	mov    %ecx,%eax
8010096d:	c1 f8 1f             	sar    $0x1f,%eax
80100970:	29 c2                	sub    %eax,%edx
80100972:	89 d0                	mov    %edx,%eax
80100974:	c1 e0 02             	shl    $0x2,%eax
80100977:	01 d0                	add    %edx,%eax
80100979:	c1 e0 04             	shl    $0x4,%eax
8010097c:	89 ca                	mov    %ecx,%edx
8010097e:	29 c2                	sub    %eax,%edx
80100980:	b8 50 00 00 00       	mov    $0x50,%eax
80100985:	29 d0                	sub    %edx,%eax
80100987:	01 45 f4             	add    %eax,-0xc(%ebp)
8010098a:	eb 33                	jmp    801009bf <cgaputc+0xbe>
  else if(c == BACKSPACE){
8010098c:	81 7d 08 00 01 00 00 	cmpl   $0x100,0x8(%ebp)
80100993:	75 0c                	jne    801009a1 <cgaputc+0xa0>
    if(pos > 0) --pos;
80100995:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80100999:	7e 24                	jle    801009bf <cgaputc+0xbe>
8010099b:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
8010099f:	eb 1e                	jmp    801009bf <cgaputc+0xbe>
  } else
    crt[pos++] = (c&0xff) | 0x0700;  // black on white
801009a1:	a1 00 a0 10 80       	mov    0x8010a000,%eax
801009a6:	8b 55 f4             	mov    -0xc(%ebp),%edx
801009a9:	01 d2                	add    %edx,%edx
801009ab:	8d 14 10             	lea    (%eax,%edx,1),%edx
801009ae:	8b 45 08             	mov    0x8(%ebp),%eax
801009b1:	66 25 ff 00          	and    $0xff,%ax
801009b5:	80 cc 07             	or     $0x7,%ah
801009b8:	66 89 02             	mov    %ax,(%edx)
801009bb:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)

  if(pos < 0 || pos > 25*80)
801009bf:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801009c3:	78 09                	js     801009ce <cgaputc+0xcd>
801009c5:	81 7d f4 d0 07 00 00 	cmpl   $0x7d0,-0xc(%ebp)
801009cc:	7e 0c                	jle    801009da <cgaputc+0xd9>
    panic("pos under/overflow");
801009ce:	c7 04 24 9e 8b 10 80 	movl   $0x80108b9e,(%esp)
801009d5:	e8 8f fe ff ff       	call   80100869 <panic>
  
  if((pos/80) >= 24){  // Scroll up.
801009da:	81 7d f4 7f 07 00 00 	cmpl   $0x77f,-0xc(%ebp)
801009e1:	7e 53                	jle    80100a36 <cgaputc+0x135>
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
801009e3:	a1 00 a0 10 80       	mov    0x8010a000,%eax
801009e8:	8d 90 a0 00 00 00    	lea    0xa0(%eax),%edx
801009ee:	a1 00 a0 10 80       	mov    0x8010a000,%eax
801009f3:	c7 44 24 08 60 0e 00 	movl   $0xe60,0x8(%esp)
801009fa:	00 
801009fb:	89 54 24 04          	mov    %edx,0x4(%esp)
801009ff:	89 04 24             	mov    %eax,(%esp)
80100a02:	e8 36 4e 00 00       	call   8010583d <memmove>
    pos -= 80;
80100a07:	83 6d f4 50          	subl   $0x50,-0xc(%ebp)
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
80100a0b:	b8 80 07 00 00       	mov    $0x780,%eax
80100a10:	2b 45 f4             	sub    -0xc(%ebp),%eax
80100a13:	8d 14 00             	lea    (%eax,%eax,1),%edx
80100a16:	a1 00 a0 10 80       	mov    0x8010a000,%eax
80100a1b:	8b 4d f4             	mov    -0xc(%ebp),%ecx
80100a1e:	01 c9                	add    %ecx,%ecx
80100a20:	01 c8                	add    %ecx,%eax
80100a22:	89 54 24 08          	mov    %edx,0x8(%esp)
80100a26:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100a2d:	00 
80100a2e:	89 04 24             	mov    %eax,(%esp)
80100a31:	e8 34 4d 00 00       	call   8010576a <memset>
  }
  
  outb(CRTPORT, 14);
80100a36:	c7 44 24 04 0e 00 00 	movl   $0xe,0x4(%esp)
80100a3d:	00 
80100a3e:	c7 04 24 d4 03 00 00 	movl   $0x3d4,(%esp)
80100a45:	e8 b3 fb ff ff       	call   801005fd <outb>
  outb(CRTPORT+1, pos>>8);
80100a4a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100a4d:	c1 f8 08             	sar    $0x8,%eax
80100a50:	0f b6 c0             	movzbl %al,%eax
80100a53:	89 44 24 04          	mov    %eax,0x4(%esp)
80100a57:	c7 04 24 d5 03 00 00 	movl   $0x3d5,(%esp)
80100a5e:	e8 9a fb ff ff       	call   801005fd <outb>
  outb(CRTPORT, 15);
80100a63:	c7 44 24 04 0f 00 00 	movl   $0xf,0x4(%esp)
80100a6a:	00 
80100a6b:	c7 04 24 d4 03 00 00 	movl   $0x3d4,(%esp)
80100a72:	e8 86 fb ff ff       	call   801005fd <outb>
  outb(CRTPORT+1, pos);
80100a77:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100a7a:	0f b6 c0             	movzbl %al,%eax
80100a7d:	89 44 24 04          	mov    %eax,0x4(%esp)
80100a81:	c7 04 24 d5 03 00 00 	movl   $0x3d5,(%esp)
80100a88:	e8 70 fb ff ff       	call   801005fd <outb>
  crt[pos] = ' ' | 0x0700;
80100a8d:	a1 00 a0 10 80       	mov    0x8010a000,%eax
80100a92:	8b 55 f4             	mov    -0xc(%ebp),%edx
80100a95:	01 d2                	add    %edx,%edx
80100a97:	01 d0                	add    %edx,%eax
80100a99:	66 c7 00 20 07       	movw   $0x720,(%eax)
}
80100a9e:	c9                   	leave  
80100a9f:	c3                   	ret    

80100aa0 <consputc>:

void
consputc(int c)
{
80100aa0:	55                   	push   %ebp
80100aa1:	89 e5                	mov    %esp,%ebp
80100aa3:	83 ec 18             	sub    $0x18,%esp
  if(panicked){
80100aa6:	a1 a0 c5 10 80       	mov    0x8010c5a0,%eax
80100aab:	85 c0                	test   %eax,%eax
80100aad:	74 07                	je     80100ab6 <consputc+0x16>
    cli();
80100aaf:	e8 67 fb ff ff       	call   8010061b <cli>
    for(;;)
      ;
80100ab4:	eb fe                	jmp    80100ab4 <consputc+0x14>
  }

  if(c == BACKSPACE){
80100ab6:	81 7d 08 00 01 00 00 	cmpl   $0x100,0x8(%ebp)
80100abd:	75 26                	jne    80100ae5 <consputc+0x45>
    uartputc('\b'); uartputc(' '); uartputc('\b');
80100abf:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
80100ac6:	e8 c5 66 00 00       	call   80107190 <uartputc>
80100acb:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80100ad2:	e8 b9 66 00 00       	call   80107190 <uartputc>
80100ad7:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
80100ade:	e8 ad 66 00 00       	call   80107190 <uartputc>
80100ae3:	eb 0b                	jmp    80100af0 <consputc+0x50>
  } else
    uartputc(c);
80100ae5:	8b 45 08             	mov    0x8(%ebp),%eax
80100ae8:	89 04 24             	mov    %eax,(%esp)
80100aeb:	e8 a0 66 00 00       	call   80107190 <uartputc>
  cgaputc(c);
80100af0:	8b 45 08             	mov    0x8(%ebp),%eax
80100af3:	89 04 24             	mov    %eax,(%esp)
80100af6:	e8 06 fe ff ff       	call   80100901 <cgaputc>
}
80100afb:	c9                   	leave  
80100afc:	c3                   	ret    

80100afd <consoleintr>:

#define C(x)  ((x)-'@')  // Control-x

void
consoleintr(int (*getc)(void))
{
80100afd:	55                   	push   %ebp
80100afe:	89 e5                	mov    %esp,%ebp
80100b00:	83 ec 28             	sub    $0x28,%esp
  int c, doprocdump = 0;
80100b03:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

  acquire(&cons.lock);
80100b0a:	c7 04 24 c0 c5 10 80 	movl   $0x8010c5c0,(%esp)
80100b11:	e8 f1 49 00 00       	call   80105507 <acquire>
  while((c = getc()) >= 0){
80100b16:	e9 40 01 00 00       	jmp    80100c5b <consoleintr+0x15e>
    switch(c){
80100b1b:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100b1e:	83 f8 10             	cmp    $0x10,%eax
80100b21:	74 1e                	je     80100b41 <consoleintr+0x44>
80100b23:	83 f8 10             	cmp    $0x10,%eax
80100b26:	7f 0a                	jg     80100b32 <consoleintr+0x35>
80100b28:	83 f8 08             	cmp    $0x8,%eax
80100b2b:	74 6a                	je     80100b97 <consoleintr+0x9a>
80100b2d:	e9 96 00 00 00       	jmp    80100bc8 <consoleintr+0xcb>
80100b32:	83 f8 15             	cmp    $0x15,%eax
80100b35:	74 31                	je     80100b68 <consoleintr+0x6b>
80100b37:	83 f8 7f             	cmp    $0x7f,%eax
80100b3a:	74 5b                	je     80100b97 <consoleintr+0x9a>
80100b3c:	e9 87 00 00 00       	jmp    80100bc8 <consoleintr+0xcb>
    case C('P'):  // Process listing.
      doprocdump = 1;   // procdump() locks cons.lock indirectly; invoke later
80100b41:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
      break;
80100b48:	e9 0e 01 00 00       	jmp    80100c5b <consoleintr+0x15e>
    case C('U'):  // Kill line.
      while(input.e != input.w &&
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.e--;
80100b4d:	a1 28 20 11 80       	mov    0x80112028,%eax
80100b52:	83 e8 01             	sub    $0x1,%eax
80100b55:	a3 28 20 11 80       	mov    %eax,0x80112028
        consputc(BACKSPACE);
80100b5a:	c7 04 24 00 01 00 00 	movl   $0x100,(%esp)
80100b61:	e8 3a ff ff ff       	call   80100aa0 <consputc>
80100b66:	eb 01                	jmp    80100b69 <consoleintr+0x6c>
    switch(c){
    case C('P'):  // Process listing.
      doprocdump = 1;   // procdump() locks cons.lock indirectly; invoke later
      break;
    case C('U'):  // Kill line.
      while(input.e != input.w &&
80100b68:	90                   	nop
80100b69:	8b 15 28 20 11 80    	mov    0x80112028,%edx
80100b6f:	a1 24 20 11 80       	mov    0x80112024,%eax
80100b74:	39 c2                	cmp    %eax,%edx
80100b76:	0f 84 db 00 00 00    	je     80100c57 <consoleintr+0x15a>
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
80100b7c:	a1 28 20 11 80       	mov    0x80112028,%eax
80100b81:	83 e8 01             	sub    $0x1,%eax
80100b84:	83 e0 7f             	and    $0x7f,%eax
80100b87:	0f b6 80 a0 1f 11 80 	movzbl -0x7feee060(%eax),%eax
    switch(c){
    case C('P'):  // Process listing.
      doprocdump = 1;   // procdump() locks cons.lock indirectly; invoke later
      break;
    case C('U'):  // Kill line.
      while(input.e != input.w &&
80100b8e:	3c 0a                	cmp    $0xa,%al
80100b90:	75 bb                	jne    80100b4d <consoleintr+0x50>
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.e--;
        consputc(BACKSPACE);
      }
      break;
80100b92:	e9 c4 00 00 00       	jmp    80100c5b <consoleintr+0x15e>
    case C('H'): case '\x7f':  // Backspace
      if(input.e != input.w){
80100b97:	8b 15 28 20 11 80    	mov    0x80112028,%edx
80100b9d:	a1 24 20 11 80       	mov    0x80112024,%eax
80100ba2:	39 c2                	cmp    %eax,%edx
80100ba4:	0f 84 b0 00 00 00    	je     80100c5a <consoleintr+0x15d>
        input.e--;
80100baa:	a1 28 20 11 80       	mov    0x80112028,%eax
80100baf:	83 e8 01             	sub    $0x1,%eax
80100bb2:	a3 28 20 11 80       	mov    %eax,0x80112028
        consputc(BACKSPACE);
80100bb7:	c7 04 24 00 01 00 00 	movl   $0x100,(%esp)
80100bbe:	e8 dd fe ff ff       	call   80100aa0 <consputc>
      }
      break;
80100bc3:	e9 93 00 00 00       	jmp    80100c5b <consoleintr+0x15e>
    default:
      if(c != 0 && input.e-input.r < INPUT_BUF){
80100bc8:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80100bcc:	0f 84 89 00 00 00    	je     80100c5b <consoleintr+0x15e>
80100bd2:	8b 15 28 20 11 80    	mov    0x80112028,%edx
80100bd8:	a1 20 20 11 80       	mov    0x80112020,%eax
80100bdd:	89 d1                	mov    %edx,%ecx
80100bdf:	29 c1                	sub    %eax,%ecx
80100be1:	89 c8                	mov    %ecx,%eax
80100be3:	83 f8 7f             	cmp    $0x7f,%eax
80100be6:	77 73                	ja     80100c5b <consoleintr+0x15e>
        c = (c == '\r') ? '\n' : c;
80100be8:	83 7d f0 0d          	cmpl   $0xd,-0x10(%ebp)
80100bec:	74 05                	je     80100bf3 <consoleintr+0xf6>
80100bee:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100bf1:	eb 05                	jmp    80100bf8 <consoleintr+0xfb>
80100bf3:	b8 0a 00 00 00       	mov    $0xa,%eax
80100bf8:	89 45 f0             	mov    %eax,-0x10(%ebp)
        input.buf[input.e++ % INPUT_BUF] = c;
80100bfb:	a1 28 20 11 80       	mov    0x80112028,%eax
80100c00:	89 c1                	mov    %eax,%ecx
80100c02:	83 e1 7f             	and    $0x7f,%ecx
80100c05:	8b 55 f0             	mov    -0x10(%ebp),%edx
80100c08:	88 91 a0 1f 11 80    	mov    %dl,-0x7feee060(%ecx)
80100c0e:	83 c0 01             	add    $0x1,%eax
80100c11:	a3 28 20 11 80       	mov    %eax,0x80112028
        consputc(c);
80100c16:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100c19:	89 04 24             	mov    %eax,(%esp)
80100c1c:	e8 7f fe ff ff       	call   80100aa0 <consputc>
        if(c == '\n' || c == C('D') || input.e == input.r+INPUT_BUF){
80100c21:	83 7d f0 0a          	cmpl   $0xa,-0x10(%ebp)
80100c25:	74 18                	je     80100c3f <consoleintr+0x142>
80100c27:	83 7d f0 04          	cmpl   $0x4,-0x10(%ebp)
80100c2b:	74 12                	je     80100c3f <consoleintr+0x142>
80100c2d:	a1 28 20 11 80       	mov    0x80112028,%eax
80100c32:	8b 15 20 20 11 80    	mov    0x80112020,%edx
80100c38:	83 ea 80             	sub    $0xffffff80,%edx
80100c3b:	39 d0                	cmp    %edx,%eax
80100c3d:	75 1c                	jne    80100c5b <consoleintr+0x15e>
          input.w = input.e;
80100c3f:	a1 28 20 11 80       	mov    0x80112028,%eax
80100c44:	a3 24 20 11 80       	mov    %eax,0x80112024
          wakeup(&input.r);
80100c49:	c7 04 24 20 20 11 80 	movl   $0x80112020,(%esp)
80100c50:	e8 b3 46 00 00       	call   80105308 <wakeup>
80100c55:	eb 04                	jmp    80100c5b <consoleintr+0x15e>
      while(input.e != input.w &&
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.e--;
        consputc(BACKSPACE);
      }
      break;
80100c57:	90                   	nop
80100c58:	eb 01                	jmp    80100c5b <consoleintr+0x15e>
    case C('H'): case '\x7f':  // Backspace
      if(input.e != input.w){
        input.e--;
        consputc(BACKSPACE);
      }
      break;
80100c5a:	90                   	nop
consoleintr(int (*getc)(void))
{
  int c, doprocdump = 0;

  acquire(&cons.lock);
  while((c = getc()) >= 0){
80100c5b:	8b 45 08             	mov    0x8(%ebp),%eax
80100c5e:	ff d0                	call   *%eax
80100c60:	89 45 f0             	mov    %eax,-0x10(%ebp)
80100c63:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80100c67:	0f 89 ae fe ff ff    	jns    80100b1b <consoleintr+0x1e>
        }
      }
      break;
    }
  }
  release(&cons.lock);
80100c6d:	c7 04 24 c0 c5 10 80 	movl   $0x8010c5c0,(%esp)
80100c74:	e8 f7 48 00 00       	call   80105570 <release>
  if(doprocdump) {
80100c79:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80100c7d:	74 05                	je     80100c84 <consoleintr+0x187>
    procdump();  // now call procdump() wo. cons.lock held
80100c7f:	e8 28 47 00 00       	call   801053ac <procdump>
  }
}
80100c84:	c9                   	leave  
80100c85:	c3                   	ret    

80100c86 <consoleread>:

int
consoleread(struct inode *ip, char *dst, int n)
{
80100c86:	55                   	push   %ebp
80100c87:	89 e5                	mov    %esp,%ebp
80100c89:	83 ec 28             	sub    $0x28,%esp
  uint target;
  int c;

  iunlock(ip);
80100c8c:	8b 45 08             	mov    0x8(%ebp),%eax
80100c8f:	89 04 24             	mov    %eax,(%esp)
80100c92:	e8 31 15 00 00       	call   801021c8 <iunlock>
  target = n;
80100c97:	8b 45 10             	mov    0x10(%ebp),%eax
80100c9a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  acquire(&cons.lock);
80100c9d:	c7 04 24 c0 c5 10 80 	movl   $0x8010c5c0,(%esp)
80100ca4:	e8 5e 48 00 00       	call   80105507 <acquire>
  while(n > 0){
80100ca9:	e9 a8 00 00 00       	jmp    80100d56 <consoleread+0xd0>
    while(input.r == input.w){
      if(proc->killed){
80100cae:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100cb4:	8b 40 24             	mov    0x24(%eax),%eax
80100cb7:	85 c0                	test   %eax,%eax
80100cb9:	74 21                	je     80100cdc <consoleread+0x56>
        release(&cons.lock);
80100cbb:	c7 04 24 c0 c5 10 80 	movl   $0x8010c5c0,(%esp)
80100cc2:	e8 a9 48 00 00       	call   80105570 <release>
        ilock(ip);
80100cc7:	8b 45 08             	mov    0x8(%ebp),%eax
80100cca:	89 04 24             	mov    %eax,(%esp)
80100ccd:	e8 9f 13 00 00       	call   80102071 <ilock>
        return -1;
80100cd2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100cd7:	e9 a9 00 00 00       	jmp    80100d85 <consoleread+0xff>
      }
      sleep(&input.r, &cons.lock);
80100cdc:	c7 44 24 04 c0 c5 10 	movl   $0x8010c5c0,0x4(%esp)
80100ce3:	80 
80100ce4:	c7 04 24 20 20 11 80 	movl   $0x80112020,(%esp)
80100ceb:	e8 3e 45 00 00       	call   8010522e <sleep>
80100cf0:	eb 01                	jmp    80100cf3 <consoleread+0x6d>

  iunlock(ip);
  target = n;
  acquire(&cons.lock);
  while(n > 0){
    while(input.r == input.w){
80100cf2:	90                   	nop
80100cf3:	8b 15 20 20 11 80    	mov    0x80112020,%edx
80100cf9:	a1 24 20 11 80       	mov    0x80112024,%eax
80100cfe:	39 c2                	cmp    %eax,%edx
80100d00:	74 ac                	je     80100cae <consoleread+0x28>
        ilock(ip);
        return -1;
      }
      sleep(&input.r, &cons.lock);
    }
    c = input.buf[input.r++ % INPUT_BUF];
80100d02:	a1 20 20 11 80       	mov    0x80112020,%eax
80100d07:	89 c2                	mov    %eax,%edx
80100d09:	83 e2 7f             	and    $0x7f,%edx
80100d0c:	0f b6 92 a0 1f 11 80 	movzbl -0x7feee060(%edx),%edx
80100d13:	0f be d2             	movsbl %dl,%edx
80100d16:	89 55 f4             	mov    %edx,-0xc(%ebp)
80100d19:	83 c0 01             	add    $0x1,%eax
80100d1c:	a3 20 20 11 80       	mov    %eax,0x80112020
    if(c == C('D')){  // EOF
80100d21:	83 7d f4 04          	cmpl   $0x4,-0xc(%ebp)
80100d25:	75 17                	jne    80100d3e <consoleread+0xb8>
      if(n < target){
80100d27:	8b 45 10             	mov    0x10(%ebp),%eax
80100d2a:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80100d2d:	73 2f                	jae    80100d5e <consoleread+0xd8>
        // Save ^D for next time, to make sure
        // caller gets a 0-byte result.
        input.r--;
80100d2f:	a1 20 20 11 80       	mov    0x80112020,%eax
80100d34:	83 e8 01             	sub    $0x1,%eax
80100d37:	a3 20 20 11 80       	mov    %eax,0x80112020
      }
      break;
80100d3c:	eb 24                	jmp    80100d62 <consoleread+0xdc>
    }
    *dst++ = c;
80100d3e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100d41:	89 c2                	mov    %eax,%edx
80100d43:	8b 45 0c             	mov    0xc(%ebp),%eax
80100d46:	88 10                	mov    %dl,(%eax)
80100d48:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
    --n;
80100d4c:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
    if(c == '\n')
80100d50:	83 7d f4 0a          	cmpl   $0xa,-0xc(%ebp)
80100d54:	74 0b                	je     80100d61 <consoleread+0xdb>
  int c;

  iunlock(ip);
  target = n;
  acquire(&cons.lock);
  while(n > 0){
80100d56:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80100d5a:	7f 96                	jg     80100cf2 <consoleread+0x6c>
80100d5c:	eb 04                	jmp    80100d62 <consoleread+0xdc>
      if(n < target){
        // Save ^D for next time, to make sure
        // caller gets a 0-byte result.
        input.r--;
      }
      break;
80100d5e:	90                   	nop
80100d5f:	eb 01                	jmp    80100d62 <consoleread+0xdc>
    }
    *dst++ = c;
    --n;
    if(c == '\n')
      break;
80100d61:	90                   	nop
  }
  release(&cons.lock);
80100d62:	c7 04 24 c0 c5 10 80 	movl   $0x8010c5c0,(%esp)
80100d69:	e8 02 48 00 00       	call   80105570 <release>
  ilock(ip);
80100d6e:	8b 45 08             	mov    0x8(%ebp),%eax
80100d71:	89 04 24             	mov    %eax,(%esp)
80100d74:	e8 f8 12 00 00       	call   80102071 <ilock>

  return target - n;
80100d79:	8b 45 10             	mov    0x10(%ebp),%eax
80100d7c:	8b 55 f0             	mov    -0x10(%ebp),%edx
80100d7f:	89 d1                	mov    %edx,%ecx
80100d81:	29 c1                	sub    %eax,%ecx
80100d83:	89 c8                	mov    %ecx,%eax
}
80100d85:	c9                   	leave  
80100d86:	c3                   	ret    

80100d87 <consolewrite>:

int
consolewrite(struct inode *ip, char *buf, int n)
{
80100d87:	55                   	push   %ebp
80100d88:	89 e5                	mov    %esp,%ebp
80100d8a:	83 ec 28             	sub    $0x28,%esp
  int i;

  iunlock(ip);
80100d8d:	8b 45 08             	mov    0x8(%ebp),%eax
80100d90:	89 04 24             	mov    %eax,(%esp)
80100d93:	e8 30 14 00 00       	call   801021c8 <iunlock>
  acquire(&cons.lock);
80100d98:	c7 04 24 c0 c5 10 80 	movl   $0x8010c5c0,(%esp)
80100d9f:	e8 63 47 00 00       	call   80105507 <acquire>
  for(i = 0; i < n; i++)
80100da4:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80100dab:	eb 1d                	jmp    80100dca <consolewrite+0x43>
    consputc(buf[i] & 0xff);
80100dad:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100db0:	03 45 0c             	add    0xc(%ebp),%eax
80100db3:	0f b6 00             	movzbl (%eax),%eax
80100db6:	0f be c0             	movsbl %al,%eax
80100db9:	25 ff 00 00 00       	and    $0xff,%eax
80100dbe:	89 04 24             	mov    %eax,(%esp)
80100dc1:	e8 da fc ff ff       	call   80100aa0 <consputc>
{
  int i;

  iunlock(ip);
  acquire(&cons.lock);
  for(i = 0; i < n; i++)
80100dc6:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80100dca:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100dcd:	3b 45 10             	cmp    0x10(%ebp),%eax
80100dd0:	7c db                	jl     80100dad <consolewrite+0x26>
    consputc(buf[i] & 0xff);
  release(&cons.lock);
80100dd2:	c7 04 24 c0 c5 10 80 	movl   $0x8010c5c0,(%esp)
80100dd9:	e8 92 47 00 00       	call   80105570 <release>
  ilock(ip);
80100dde:	8b 45 08             	mov    0x8(%ebp),%eax
80100de1:	89 04 24             	mov    %eax,(%esp)
80100de4:	e8 88 12 00 00       	call   80102071 <ilock>

  return n;
80100de9:	8b 45 10             	mov    0x10(%ebp),%eax
}
80100dec:	c9                   	leave  
80100ded:	c3                   	ret    

80100dee <consoleinit>:

void
consoleinit(void)
{
80100dee:	55                   	push   %ebp
80100def:	89 e5                	mov    %esp,%ebp
80100df1:	83 ec 18             	sub    $0x18,%esp
  initlock(&cons.lock, "console");
80100df4:	c7 44 24 04 b1 8b 10 	movl   $0x80108bb1,0x4(%esp)
80100dfb:	80 
80100dfc:	c7 04 24 c0 c5 10 80 	movl   $0x8010c5c0,(%esp)
80100e03:	e8 de 46 00 00       	call   801054e6 <initlock>

  devsw[CONSOLE].write = consolewrite;
80100e08:	c7 05 ec 29 11 80 87 	movl   $0x80100d87,0x801129ec
80100e0f:	0d 10 80 
  devsw[CONSOLE].read = consoleread;
80100e12:	c7 05 e8 29 11 80 86 	movl   $0x80100c86,0x801129e8
80100e19:	0c 10 80 
  cons.locking = 1;
80100e1c:	c7 05 f4 c5 10 80 01 	movl   $0x1,0x8010c5f4
80100e23:	00 00 00 

  picenable(IRQ_KBD);
80100e26:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80100e2d:	e8 6b 36 00 00       	call   8010449d <picenable>
  ioapicenable(IRQ_KBD, 0);
80100e32:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100e39:	00 
80100e3a:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80100e41:	e8 b4 23 00 00       	call   801031fa <ioapicenable>
}
80100e46:	c9                   	leave  
80100e47:	c3                   	ret    

80100e48 <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
80100e48:	55                   	push   %ebp
80100e49:	89 e5                	mov    %esp,%ebp
80100e4b:	83 ec 14             	sub    $0x14,%esp
80100e4e:	8b 45 08             	mov    0x8(%ebp),%eax
80100e51:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80100e55:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
80100e59:	89 c2                	mov    %eax,%edx
80100e5b:	ec                   	in     (%dx),%al
80100e5c:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80100e5f:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80100e63:	c9                   	leave  
80100e64:	c3                   	ret    

80100e65 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80100e65:	55                   	push   %ebp
80100e66:	89 e5                	mov    %esp,%ebp
80100e68:	83 ec 08             	sub    $0x8,%esp
80100e6b:	8b 55 08             	mov    0x8(%ebp),%edx
80100e6e:	8b 45 0c             	mov    0xc(%ebp),%eax
80100e71:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80100e75:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100e78:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80100e7c:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80100e80:	ee                   	out    %al,(%dx)
}
80100e81:	c9                   	leave  
80100e82:	c3                   	ret    

80100e83 <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
80100e83:	55                   	push   %ebp
80100e84:	89 e5                	mov    %esp,%ebp
80100e86:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80100e89:	9c                   	pushf  
80100e8a:	58                   	pop    %eax
80100e8b:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return eflags;
80100e8e:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80100e91:	c9                   	leave  
80100e92:	c3                   	ret    

80100e93 <cli>:
  asm volatile("movw %0, %%gs" : : "r" (v));
}

static inline void
cli(void)
{
80100e93:	55                   	push   %ebp
80100e94:	89 e5                	mov    %esp,%ebp
  asm volatile("cli");
80100e96:	fa                   	cli    
}
80100e97:	5d                   	pop    %ebp
80100e98:	c3                   	ret    

80100e99 <sti>:

static inline void
sti(void)
{
80100e99:	55                   	push   %ebp
80100e9a:	89 e5                	mov    %esp,%ebp
  asm volatile("sti");
80100e9c:	fb                   	sti    
}
80100e9d:	5d                   	pop    %ebp
80100e9e:	c3                   	ret    

80100e9f <xchg>:

static inline uint
xchg(volatile uint *addr, uint newval)
{
80100e9f:	55                   	push   %ebp
80100ea0:	89 e5                	mov    %esp,%ebp
80100ea2:	83 ec 10             	sub    $0x10,%esp
  uint result;

  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
80100ea5:	8b 55 08             	mov    0x8(%ebp),%edx
80100ea8:	8b 45 0c             	mov    0xc(%ebp),%eax
80100eab:	8b 4d 08             	mov    0x8(%ebp),%ecx
80100eae:	f0 87 02             	lock xchg %eax,(%edx)
80100eb1:	89 45 fc             	mov    %eax,-0x4(%ebp)
               "+m" (*addr), "=a" (result) :
               "1" (newval) :
               "cc");
  return result;
80100eb4:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80100eb7:	c9                   	leave  
80100eb8:	c3                   	ret    

80100eb9 <lock>:
// debugf(), we don't have to worry about which CPU we're on. The spinlocks in
// xv6 presumably pay that prize to make their interface simpler?

static void
lock(int *intena)
{
80100eb9:	55                   	push   %ebp
80100eba:	89 e5                	mov    %esp,%ebp
80100ebc:	83 ec 08             	sub    $0x8,%esp
  *intena = readeflags() & FL_IF;
80100ebf:	e8 bf ff ff ff       	call   80100e83 <readeflags>
80100ec4:	89 c2                	mov    %eax,%edx
80100ec6:	81 e2 00 02 00 00    	and    $0x200,%edx
80100ecc:	8b 45 08             	mov    0x8(%ebp),%eax
80100ecf:	89 10                	mov    %edx,(%eax)
  // Migration is possible *right here*, but if so, FL_IF was asserted when
  // we sampled it and is still asserted now. That holds true up until the
  // CLI instruction itself. (Also see TRICKS.)
  cli();
80100ed1:	e8 bd ff ff ff       	call   80100e93 <cli>
  while(xchg(&locked, 1) != 0)
80100ed6:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80100edd:	00 
80100ede:	c7 04 24 fc c5 10 80 	movl   $0x8010c5fc,(%esp)
80100ee5:	e8 b5 ff ff ff       	call   80100e9f <xchg>
80100eea:	85 c0                	test   %eax,%eax
80100eec:	75 e8                	jne    80100ed6 <lock+0x1d>
    ;
}
80100eee:	c9                   	leave  
80100eef:	c3                   	ret    

80100ef0 <unlock>:

static void
unlock(int intena)
{
80100ef0:	55                   	push   %ebp
80100ef1:	89 e5                	mov    %esp,%ebp
80100ef3:	83 ec 08             	sub    $0x8,%esp
  xchg(&locked, 0);
80100ef6:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100efd:	00 
80100efe:	c7 04 24 fc c5 10 80 	movl   $0x8010c5fc,(%esp)
80100f05:	e8 95 ff ff ff       	call   80100e9f <xchg>
  if(intena)
80100f0a:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80100f0e:	74 05                	je     80100f15 <unlock+0x25>
    sti();
80100f10:	e8 84 ff ff ff       	call   80100e99 <sti>
}
80100f15:	c9                   	leave  
80100f16:	c3                   	ret    

80100f17 <cantransmit>:

static int
cantransmit(void)
{
80100f17:	55                   	push   %ebp
80100f18:	89 e5                	mov    %esp,%ebp
80100f1a:	83 ec 04             	sub    $0x4,%esp
  return inb(COM2+UART_LINE_STATUS) & UART_TRANSMIT_READY;
80100f1d:	c7 04 24 fd 02 00 00 	movl   $0x2fd,(%esp)
80100f24:	e8 1f ff ff ff       	call   80100e48 <inb>
80100f29:	0f b6 c0             	movzbl %al,%eax
80100f2c:	83 e0 20             	and    $0x20,%eax
}
80100f2f:	c9                   	leave  
80100f30:	c3                   	ret    

80100f31 <debugputc>:

static void
debugputc(int c)
{
80100f31:	55                   	push   %ebp
80100f32:	89 e5                	mov    %esp,%ebp
80100f34:	83 ec 28             	sub    $0x28,%esp
  int i;

  if(!uart)
80100f37:	a1 f8 c5 10 80       	mov    0x8010c5f8,%eax
80100f3c:	85 c0                	test   %eax,%eax
80100f3e:	74 40                	je     80100f80 <debugputc+0x4f>
    return;
  for(i = 0; i < 128 && !cantransmit(); i++)
80100f40:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80100f47:	eb 10                	jmp    80100f59 <debugputc+0x28>
    microdelay(10);
80100f49:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
80100f50:	e8 26 28 00 00       	call   8010377b <microdelay>
{
  int i;

  if(!uart)
    return;
  for(i = 0; i < 128 && !cantransmit(); i++)
80100f55:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80100f59:	83 7d f4 7f          	cmpl   $0x7f,-0xc(%ebp)
80100f5d:	7f 09                	jg     80100f68 <debugputc+0x37>
80100f5f:	e8 b3 ff ff ff       	call   80100f17 <cantransmit>
80100f64:	85 c0                	test   %eax,%eax
80100f66:	74 e1                	je     80100f49 <debugputc+0x18>
    microdelay(10);
  outb(COM2+UART_TRANSMIT_BUFFER, c);
80100f68:	8b 45 08             	mov    0x8(%ebp),%eax
80100f6b:	0f b6 c0             	movzbl %al,%eax
80100f6e:	89 44 24 04          	mov    %eax,0x4(%esp)
80100f72:	c7 04 24 f8 02 00 00 	movl   $0x2f8,(%esp)
80100f79:	e8 e7 fe ff ff       	call   80100e65 <outb>
80100f7e:	eb 01                	jmp    80100f81 <debugputc+0x50>
debugputc(int c)
{
  int i;

  if(!uart)
    return;
80100f80:	90                   	nop
  for(i = 0; i < 128 && !cantransmit(); i++)
    microdelay(10);
  outb(COM2+UART_TRANSMIT_BUFFER, c);
}
80100f81:	c9                   	leave  
80100f82:	c3                   	ret    

80100f83 <debuginit>:

void
debuginit(void)
{
80100f83:	55                   	push   %ebp
80100f84:	89 e5                	mov    %esp,%ebp
80100f86:	83 ec 08             	sub    $0x8,%esp
  // Turn off the FIFO
  outb(COM2+UART_FIFO_CONTROL, 0);
80100f89:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100f90:	00 
80100f91:	c7 04 24 fa 02 00 00 	movl   $0x2fa,(%esp)
80100f98:	e8 c8 fe ff ff       	call   80100e65 <outb>

  // 9600 baud, 8 data bits, 1 stop bit, parity off.
  outb(COM2+UART_LINE_CONTROL, UART_DIVISOR_LATCH);
80100f9d:	c7 44 24 04 80 00 00 	movl   $0x80,0x4(%esp)
80100fa4:	00 
80100fa5:	c7 04 24 fb 02 00 00 	movl   $0x2fb,(%esp)
80100fac:	e8 b4 fe ff ff       	call   80100e65 <outb>
  outb(COM2+UART_DIVISOR_LOW, 115200/9600);
80100fb1:	c7 44 24 04 0c 00 00 	movl   $0xc,0x4(%esp)
80100fb8:	00 
80100fb9:	c7 04 24 f8 02 00 00 	movl   $0x2f8,(%esp)
80100fc0:	e8 a0 fe ff ff       	call   80100e65 <outb>
  outb(COM2+UART_DIVISOR_HIGH, 0);
80100fc5:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100fcc:	00 
80100fcd:	c7 04 24 f9 02 00 00 	movl   $0x2f9,(%esp)
80100fd4:	e8 8c fe ff ff       	call   80100e65 <outb>
  outb(COM2+UART_LINE_CONTROL, 0x03);   // 8 data bits.
80100fd9:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
80100fe0:	00 
80100fe1:	c7 04 24 fb 02 00 00 	movl   $0x2fb,(%esp)
80100fe8:	e8 78 fe ff ff       	call   80100e65 <outb>
  outb(COM2+UART_MODEM_CONTROL, 0);     // No RTS/DTR handshake.
80100fed:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100ff4:	00 
80100ff5:	c7 04 24 fc 02 00 00 	movl   $0x2fc,(%esp)
80100ffc:	e8 64 fe ff ff       	call   80100e65 <outb>
  outb(COM2+UART_INTERRUPT_ENABLE, 0);  // Disable interrupts.
80101001:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80101008:	00 
80101009:	c7 04 24 f9 02 00 00 	movl   $0x2f9,(%esp)
80101010:	e8 50 fe ff ff       	call   80100e65 <outb>

  // If status is 0xFF, no serial port.
  if(inb(COM2+UART_LINE_STATUS) == 0xFF)
80101015:	c7 04 24 fd 02 00 00 	movl   $0x2fd,(%esp)
8010101c:	e8 27 fe ff ff       	call   80100e48 <inb>
80101021:	3c ff                	cmp    $0xff,%al
80101023:	74 24                	je     80101049 <debuginit+0xc6>
    return;
  uart = 1;
80101025:	c7 05 f8 c5 10 80 01 	movl   $0x1,0x8010c5f8
8010102c:	00 00 00 

  // Acknowledge pre-existing interrupt conditions, if any.
  // We don't use any of the interrupt stuff ourselves.
  inb(COM2+UART_INTERRUPT_ID);
8010102f:	c7 04 24 fa 02 00 00 	movl   $0x2fa,(%esp)
80101036:	e8 0d fe ff ff       	call   80100e48 <inb>
  inb(COM2+UART_RECEIVE_BUFFER);
8010103b:	c7 04 24 f8 02 00 00 	movl   $0x2f8,(%esp)
80101042:	e8 01 fe ff ff       	call   80100e48 <inb>
80101047:	eb 01                	jmp    8010104a <debuginit+0xc7>
  outb(COM2+UART_MODEM_CONTROL, 0);     // No RTS/DTR handshake.
  outb(COM2+UART_INTERRUPT_ENABLE, 0);  // Disable interrupts.

  // If status is 0xFF, no serial port.
  if(inb(COM2+UART_LINE_STATUS) == 0xFF)
    return;
80101049:	90                   	nop

  // Acknowledge pre-existing interrupt conditions, if any.
  // We don't use any of the interrupt stuff ourselves.
  inb(COM2+UART_INTERRUPT_ID);
  inb(COM2+UART_RECEIVE_BUFFER);
}
8010104a:	c9                   	leave  
8010104b:	c3                   	ret    

8010104c <printint>:
// It's certainly sad that we have to replicate the printf code *again* for
// this module. Alas there doesn't seem to be a nice way to modularize.

static void
printint(int xx, int base, int sign)
{
8010104c:	55                   	push   %ebp
8010104d:	89 e5                	mov    %esp,%ebp
8010104f:	53                   	push   %ebx
80101050:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = (xx < 0)))
80101053:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80101057:	74 19                	je     80101072 <printint+0x26>
80101059:	8b 45 08             	mov    0x8(%ebp),%eax
8010105c:	c1 e8 1f             	shr    $0x1f,%eax
8010105f:	89 45 10             	mov    %eax,0x10(%ebp)
80101062:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80101066:	74 0a                	je     80101072 <printint+0x26>
    x = -xx;
80101068:	8b 45 08             	mov    0x8(%ebp),%eax
8010106b:	f7 d8                	neg    %eax
8010106d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = (xx < 0)))
80101070:	eb 06                	jmp    80101078 <printint+0x2c>
    x = -xx;
  else
    x = xx;
80101072:	8b 45 08             	mov    0x8(%ebp),%eax
80101075:	89 45 f4             	mov    %eax,-0xc(%ebp)

  i = 0;
80101078:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  do{
    buf[i++] = digits[x % base];
8010107f:	8b 4d f0             	mov    -0x10(%ebp),%ecx
80101082:	8b 5d 0c             	mov    0xc(%ebp),%ebx
80101085:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101088:	ba 00 00 00 00       	mov    $0x0,%edx
8010108d:	f7 f3                	div    %ebx
8010108f:	89 d0                	mov    %edx,%eax
80101091:	0f b6 80 18 a0 10 80 	movzbl -0x7fef5fe8(%eax),%eax
80101098:	88 44 0d e0          	mov    %al,-0x20(%ebp,%ecx,1)
8010109c:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
  }while((x /= base) != 0);
801010a0:	8b 45 0c             	mov    0xc(%ebp),%eax
801010a3:	89 45 d4             	mov    %eax,-0x2c(%ebp)
801010a6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801010a9:	ba 00 00 00 00       	mov    $0x0,%edx
801010ae:	f7 75 d4             	divl   -0x2c(%ebp)
801010b1:	89 45 f4             	mov    %eax,-0xc(%ebp)
801010b4:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801010b8:	75 c5                	jne    8010107f <printint+0x33>

  if(sign)
801010ba:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
801010be:	74 23                	je     801010e3 <printint+0x97>
    buf[i++] = '-';
801010c0:	8b 45 f0             	mov    -0x10(%ebp),%eax
801010c3:	c6 44 05 e0 2d       	movb   $0x2d,-0x20(%ebp,%eax,1)
801010c8:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)

  while(--i >= 0)
801010cc:	eb 16                	jmp    801010e4 <printint+0x98>
    debugputc(buf[i]);
801010ce:	8b 45 f0             	mov    -0x10(%ebp),%eax
801010d1:	0f b6 44 05 e0       	movzbl -0x20(%ebp,%eax,1),%eax
801010d6:	0f be c0             	movsbl %al,%eax
801010d9:	89 04 24             	mov    %eax,(%esp)
801010dc:	e8 50 fe ff ff       	call   80100f31 <debugputc>
801010e1:	eb 01                	jmp    801010e4 <printint+0x98>
  }while((x /= base) != 0);

  if(sign)
    buf[i++] = '-';

  while(--i >= 0)
801010e3:	90                   	nop
801010e4:	83 6d f0 01          	subl   $0x1,-0x10(%ebp)
801010e8:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801010ec:	79 e0                	jns    801010ce <printint+0x82>
    debugputc(buf[i]);
}
801010ee:	83 c4 44             	add    $0x44,%esp
801010f1:	5b                   	pop    %ebx
801010f2:	5d                   	pop    %ebp
801010f3:	c3                   	ret    

801010f4 <debugf>:

void
debugf(char *fmt, ...)
{
801010f4:	55                   	push   %ebp
801010f5:	89 e5                	mov    %esp,%ebp
801010f7:	83 ec 38             	sub    $0x38,%esp
  int i, c;
  uint *argp;
  char *s;
  int intena;

  if(fmt == 0){
801010fa:	8b 45 08             	mov    0x8(%ebp),%eax
801010fd:	85 c0                	test   %eax,%eax
801010ff:	75 11                	jne    80101112 <debugf+0x1e>
    debugf("debugf: null fmt\n");  // no panic(), no console.c dependency
80101101:	c7 04 24 b9 8b 10 80 	movl   $0x80108bb9,(%esp)
80101108:	e8 e7 ff ff ff       	call   801010f4 <debugf>
    return;
8010110d:	e9 6f 01 00 00       	jmp    80101281 <debugf+0x18d>
  }

  lock(&intena);
80101112:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80101115:	89 04 24             	mov    %eax,(%esp)
80101118:	e8 9c fd ff ff       	call   80100eb9 <lock>

  argp = (uint*)(void*)(&fmt + 1);
8010111d:	8d 45 08             	lea    0x8(%ebp),%eax
80101120:	83 c0 04             	add    $0x4,%eax
80101123:	89 45 f0             	mov    %eax,-0x10(%ebp)
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80101126:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
8010112d:	e9 20 01 00 00       	jmp    80101252 <debugf+0x15e>
    if(c != '%'){
80101132:	83 7d ec 25          	cmpl   $0x25,-0x14(%ebp)
80101136:	74 10                	je     80101148 <debugf+0x54>
      debugputc(c);
80101138:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010113b:	89 04 24             	mov    %eax,(%esp)
8010113e:	e8 ee fd ff ff       	call   80100f31 <debugputc>
      continue;
80101143:	e9 06 01 00 00       	jmp    8010124e <debugf+0x15a>
    }
    c = fmt[++i] & 0xff;
80101148:	8b 55 08             	mov    0x8(%ebp),%edx
8010114b:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
8010114f:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101152:	8d 04 02             	lea    (%edx,%eax,1),%eax
80101155:	0f b6 00             	movzbl (%eax),%eax
80101158:	0f be c0             	movsbl %al,%eax
8010115b:	25 ff 00 00 00       	and    $0xff,%eax
80101160:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(c == 0)
80101163:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80101167:	0f 84 08 01 00 00    	je     80101275 <debugf+0x181>
      break;
    switch(c){
8010116d:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101170:	83 f8 70             	cmp    $0x70,%eax
80101173:	74 4d                	je     801011c2 <debugf+0xce>
80101175:	83 f8 70             	cmp    $0x70,%eax
80101178:	7f 13                	jg     8010118d <debugf+0x99>
8010117a:	83 f8 25             	cmp    $0x25,%eax
8010117d:	0f 84 a6 00 00 00    	je     80101229 <debugf+0x135>
80101183:	83 f8 64             	cmp    $0x64,%eax
80101186:	74 14                	je     8010119c <debugf+0xa8>
80101188:	e9 aa 00 00 00       	jmp    80101237 <debugf+0x143>
8010118d:	83 f8 73             	cmp    $0x73,%eax
80101190:	74 53                	je     801011e5 <debugf+0xf1>
80101192:	83 f8 78             	cmp    $0x78,%eax
80101195:	74 2b                	je     801011c2 <debugf+0xce>
80101197:	e9 9b 00 00 00       	jmp    80101237 <debugf+0x143>
    case 'd':
      printint(*argp++, 10, 1);
8010119c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010119f:	8b 00                	mov    (%eax),%eax
801011a1:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
801011a5:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
801011ac:	00 
801011ad:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
801011b4:	00 
801011b5:	89 04 24             	mov    %eax,(%esp)
801011b8:	e8 8f fe ff ff       	call   8010104c <printint>
      break;
801011bd:	e9 8c 00 00 00       	jmp    8010124e <debugf+0x15a>
    case 'x':
    case 'p':
      printint(*argp++, 16, 0);
801011c2:	8b 45 f0             	mov    -0x10(%ebp),%eax
801011c5:	8b 00                	mov    (%eax),%eax
801011c7:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
801011cb:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
801011d2:	00 
801011d3:	c7 44 24 04 10 00 00 	movl   $0x10,0x4(%esp)
801011da:	00 
801011db:	89 04 24             	mov    %eax,(%esp)
801011de:	e8 69 fe ff ff       	call   8010104c <printint>
      break;
801011e3:	eb 69                	jmp    8010124e <debugf+0x15a>
    case 's':
      if((s = (char*)*argp++) == 0)
801011e5:	8b 45 f0             	mov    -0x10(%ebp),%eax
801011e8:	8b 00                	mov    (%eax),%eax
801011ea:	89 45 f4             	mov    %eax,-0xc(%ebp)
801011ed:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801011f1:	0f 94 c0             	sete   %al
801011f4:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
801011f8:	84 c0                	test   %al,%al
801011fa:	74 20                	je     8010121c <debugf+0x128>
        s = "(null)";
801011fc:	c7 45 f4 cb 8b 10 80 	movl   $0x80108bcb,-0xc(%ebp)
      for(; *s; s++)
80101203:	eb 18                	jmp    8010121d <debugf+0x129>
        debugputc(*s);
80101205:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101208:	0f b6 00             	movzbl (%eax),%eax
8010120b:	0f be c0             	movsbl %al,%eax
8010120e:	89 04 24             	mov    %eax,(%esp)
80101211:	e8 1b fd ff ff       	call   80100f31 <debugputc>
      printint(*argp++, 16, 0);
      break;
    case 's':
      if((s = (char*)*argp++) == 0)
        s = "(null)";
      for(; *s; s++)
80101216:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010121a:	eb 01                	jmp    8010121d <debugf+0x129>
8010121c:	90                   	nop
8010121d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101220:	0f b6 00             	movzbl (%eax),%eax
80101223:	84 c0                	test   %al,%al
80101225:	75 de                	jne    80101205 <debugf+0x111>
        debugputc(*s);
      break;
80101227:	eb 25                	jmp    8010124e <debugf+0x15a>
    case '%':
      debugputc('%');
80101229:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
80101230:	e8 fc fc ff ff       	call   80100f31 <debugputc>
      break;
80101235:	eb 17                	jmp    8010124e <debugf+0x15a>
    default:
      // Print unknown % sequence to draw attention.
      debugputc('%');
80101237:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
8010123e:	e8 ee fc ff ff       	call   80100f31 <debugputc>
      debugputc(c);
80101243:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101246:	89 04 24             	mov    %eax,(%esp)
80101249:	e8 e3 fc ff ff       	call   80100f31 <debugputc>
  }

  lock(&intena);

  argp = (uint*)(void*)(&fmt + 1);
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
8010124e:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
80101252:	8b 55 08             	mov    0x8(%ebp),%edx
80101255:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101258:	8d 04 02             	lea    (%edx,%eax,1),%eax
8010125b:	0f b6 00             	movzbl (%eax),%eax
8010125e:	0f be c0             	movsbl %al,%eax
80101261:	25 ff 00 00 00       	and    $0xff,%eax
80101266:	89 45 ec             	mov    %eax,-0x14(%ebp)
80101269:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
8010126d:	0f 85 bf fe ff ff    	jne    80101132 <debugf+0x3e>
80101273:	eb 01                	jmp    80101276 <debugf+0x182>
      debugputc(c);
      continue;
    }
    c = fmt[++i] & 0xff;
    if(c == 0)
      break;
80101275:	90                   	nop
      debugputc(c);
      break;
    }
  }

  unlock(intena);
80101276:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101279:	89 04 24             	mov    %eax,(%esp)
8010127c:	e8 6f fc ff ff       	call   80100ef0 <unlock>
}
80101281:	c9                   	leave  
80101282:	c3                   	ret    
	...

80101284 <exec>:
#include "x86.h"
#include "elf.h"

int
exec(char *path, char **argv)
{
80101284:	55                   	push   %ebp
80101285:	89 e5                	mov    %esp,%ebp
80101287:	81 ec 38 01 00 00    	sub    $0x138,%esp
  struct elfhdr elf;
  struct inode *ip;
  struct proghdr ph;
  pde_t *pgdir, *oldpgdir;

  begin_op();
8010128d:	e8 d8 27 00 00       	call   80103a6a <begin_op>
  if((ip = namei(path)) == 0){
80101292:	8b 45 08             	mov    0x8(%ebp),%eax
80101295:	89 04 24             	mov    %eax,(%esp)
80101298:	e8 82 19 00 00       	call   80102c1f <namei>
8010129d:	89 45 ec             	mov    %eax,-0x14(%ebp)
801012a0:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
801012a4:	75 0f                	jne    801012b5 <exec+0x31>
    end_op();
801012a6:	e8 41 28 00 00       	call   80103aec <end_op>
    return -1;
801012ab:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801012b0:	e9 e2 03 00 00       	jmp    80101697 <exec+0x413>
  }
  ilock(ip);
801012b5:	8b 45 ec             	mov    -0x14(%ebp),%eax
801012b8:	89 04 24             	mov    %eax,(%esp)
801012bb:	e8 b1 0d 00 00       	call   80102071 <ilock>
  pgdir = 0;
801012c0:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) < sizeof(elf))
801012c7:	8d 85 0c ff ff ff    	lea    -0xf4(%ebp),%eax
801012cd:	c7 44 24 0c 34 00 00 	movl   $0x34,0xc(%esp)
801012d4:	00 
801012d5:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
801012dc:	00 
801012dd:	89 44 24 04          	mov    %eax,0x4(%esp)
801012e1:	8b 45 ec             	mov    -0x14(%ebp),%eax
801012e4:	89 04 24             	mov    %eax,(%esp)
801012e7:	e8 84 12 00 00       	call   80102570 <readi>
801012ec:	83 f8 33             	cmp    $0x33,%eax
801012ef:	0f 86 57 03 00 00    	jbe    8010164c <exec+0x3c8>
    goto bad;
  if(elf.magic != ELF_MAGIC)
801012f5:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
801012fb:	3d 7f 45 4c 46       	cmp    $0x464c457f,%eax
80101300:	0f 85 49 03 00 00    	jne    8010164f <exec+0x3cb>
    goto bad;

  if((pgdir = setupkvm()) == 0)
80101306:	e8 92 6f 00 00       	call   8010829d <setupkvm>
8010130b:	89 45 f0             	mov    %eax,-0x10(%ebp)
8010130e:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80101312:	0f 84 3a 03 00 00    	je     80101652 <exec+0x3ce>
    goto bad;

  // Load program into memory.
  sz = 0;
80101318:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
8010131f:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
80101326:	8b 85 28 ff ff ff    	mov    -0xd8(%ebp),%eax
8010132c:	89 45 dc             	mov    %eax,-0x24(%ebp)
8010132f:	e9 ca 00 00 00       	jmp    801013fe <exec+0x17a>
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
80101334:	8b 55 dc             	mov    -0x24(%ebp),%edx
80101337:	8d 85 ec fe ff ff    	lea    -0x114(%ebp),%eax
8010133d:	c7 44 24 0c 20 00 00 	movl   $0x20,0xc(%esp)
80101344:	00 
80101345:	89 54 24 08          	mov    %edx,0x8(%esp)
80101349:	89 44 24 04          	mov    %eax,0x4(%esp)
8010134d:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101350:	89 04 24             	mov    %eax,(%esp)
80101353:	e8 18 12 00 00       	call   80102570 <readi>
80101358:	83 f8 20             	cmp    $0x20,%eax
8010135b:	0f 85 f4 02 00 00    	jne    80101655 <exec+0x3d1>
      goto bad;
    if(ph.type != ELF_PROG_LOAD)
80101361:	8b 85 ec fe ff ff    	mov    -0x114(%ebp),%eax
80101367:	83 f8 01             	cmp    $0x1,%eax
8010136a:	0f 85 80 00 00 00    	jne    801013f0 <exec+0x16c>
      continue;
    if(ph.memsz < ph.filesz)
80101370:	8b 95 00 ff ff ff    	mov    -0x100(%ebp),%edx
80101376:	8b 85 fc fe ff ff    	mov    -0x104(%ebp),%eax
8010137c:	39 c2                	cmp    %eax,%edx
8010137e:	0f 82 d4 02 00 00    	jb     80101658 <exec+0x3d4>
      goto bad;
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
80101384:	8b 95 f4 fe ff ff    	mov    -0x10c(%ebp),%edx
8010138a:	8b 85 00 ff ff ff    	mov    -0x100(%ebp),%eax
80101390:	8d 04 02             	lea    (%edx,%eax,1),%eax
80101393:	89 44 24 08          	mov    %eax,0x8(%esp)
80101397:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010139a:	89 44 24 04          	mov    %eax,0x4(%esp)
8010139e:	8b 45 f0             	mov    -0x10(%ebp),%eax
801013a1:	89 04 24             	mov    %eax,(%esp)
801013a4:	e8 cd 72 00 00       	call   80108676 <allocuvm>
801013a9:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801013ac:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
801013b0:	0f 84 a5 02 00 00    	je     8010165b <exec+0x3d7>
      goto bad;
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
801013b6:	8b 8d fc fe ff ff    	mov    -0x104(%ebp),%ecx
801013bc:	8b 95 f0 fe ff ff    	mov    -0x110(%ebp),%edx
801013c2:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
801013c8:	89 4c 24 10          	mov    %ecx,0x10(%esp)
801013cc:	89 54 24 0c          	mov    %edx,0xc(%esp)
801013d0:	8b 55 ec             	mov    -0x14(%ebp),%edx
801013d3:	89 54 24 08          	mov    %edx,0x8(%esp)
801013d7:	89 44 24 04          	mov    %eax,0x4(%esp)
801013db:	8b 45 f0             	mov    -0x10(%ebp),%eax
801013de:	89 04 24             	mov    %eax,(%esp)
801013e1:	e8 a8 71 00 00       	call   8010858e <loaduvm>
801013e6:	85 c0                	test   %eax,%eax
801013e8:	0f 88 70 02 00 00    	js     8010165e <exec+0x3da>
801013ee:	eb 01                	jmp    801013f1 <exec+0x16d>
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
      goto bad;
    if(ph.type != ELF_PROG_LOAD)
      continue;
801013f0:	90                   	nop
  if((pgdir = setupkvm()) == 0)
    goto bad;

  // Load program into memory.
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
801013f1:	83 45 d8 01          	addl   $0x1,-0x28(%ebp)
801013f5:	8b 45 dc             	mov    -0x24(%ebp),%eax
801013f8:	83 c0 20             	add    $0x20,%eax
801013fb:	89 45 dc             	mov    %eax,-0x24(%ebp)
801013fe:	0f b7 85 38 ff ff ff 	movzwl -0xc8(%ebp),%eax
80101405:	0f b7 c0             	movzwl %ax,%eax
80101408:	3b 45 d8             	cmp    -0x28(%ebp),%eax
8010140b:	0f 8f 23 ff ff ff    	jg     80101334 <exec+0xb0>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
      goto bad;
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
      goto bad;
  }
  iunlockput(ip);
80101411:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101414:	89 04 24             	mov    %eax,(%esp)
80101417:	e8 e2 0e 00 00       	call   801022fe <iunlockput>
  end_op();
8010141c:	e8 cb 26 00 00       	call   80103aec <end_op>
  ip = 0;
80101421:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)

  // Allocate two pages at the next page boundary.
  // Make the first inaccessible.  Use the second as the user stack.
  sz = PGROUNDUP(sz);
80101428:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010142b:	05 ff 0f 00 00       	add    $0xfff,%eax
80101430:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80101435:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
80101438:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010143b:	05 00 20 00 00       	add    $0x2000,%eax
80101440:	89 44 24 08          	mov    %eax,0x8(%esp)
80101444:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101447:	89 44 24 04          	mov    %eax,0x4(%esp)
8010144b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010144e:	89 04 24             	mov    %eax,(%esp)
80101451:	e8 20 72 00 00       	call   80108676 <allocuvm>
80101456:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80101459:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
8010145d:	0f 84 fe 01 00 00    	je     80101661 <exec+0x3dd>
    goto bad;
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
80101463:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101466:	2d 00 20 00 00       	sub    $0x2000,%eax
8010146b:	89 44 24 04          	mov    %eax,0x4(%esp)
8010146f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101472:	89 04 24             	mov    %eax,(%esp)
80101475:	e8 25 74 00 00       	call   8010889f <clearpteu>
  sp = sz;
8010147a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010147d:	89 45 e8             	mov    %eax,-0x18(%ebp)

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
80101480:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
80101487:	e9 81 00 00 00       	jmp    8010150d <exec+0x289>
    if(argc >= MAXARG)
8010148c:	83 7d e0 1f          	cmpl   $0x1f,-0x20(%ebp)
80101490:	0f 87 ce 01 00 00    	ja     80101664 <exec+0x3e0>
      goto bad;
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
80101496:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101499:	c1 e0 02             	shl    $0x2,%eax
8010149c:	03 45 0c             	add    0xc(%ebp),%eax
8010149f:	8b 00                	mov    (%eax),%eax
801014a1:	89 04 24             	mov    %eax,(%esp)
801014a4:	e8 42 45 00 00       	call   801059eb <strlen>
801014a9:	f7 d0                	not    %eax
801014ab:	03 45 e8             	add    -0x18(%ebp),%eax
801014ae:	83 e0 fc             	and    $0xfffffffc,%eax
801014b1:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
801014b4:	8b 45 e0             	mov    -0x20(%ebp),%eax
801014b7:	c1 e0 02             	shl    $0x2,%eax
801014ba:	03 45 0c             	add    0xc(%ebp),%eax
801014bd:	8b 00                	mov    (%eax),%eax
801014bf:	89 04 24             	mov    %eax,(%esp)
801014c2:	e8 24 45 00 00       	call   801059eb <strlen>
801014c7:	83 c0 01             	add    $0x1,%eax
801014ca:	89 c2                	mov    %eax,%edx
801014cc:	8b 45 e0             	mov    -0x20(%ebp),%eax
801014cf:	c1 e0 02             	shl    $0x2,%eax
801014d2:	03 45 0c             	add    0xc(%ebp),%eax
801014d5:	8b 00                	mov    (%eax),%eax
801014d7:	89 54 24 0c          	mov    %edx,0xc(%esp)
801014db:	89 44 24 08          	mov    %eax,0x8(%esp)
801014df:	8b 45 e8             	mov    -0x18(%ebp),%eax
801014e2:	89 44 24 04          	mov    %eax,0x4(%esp)
801014e6:	8b 45 f0             	mov    -0x10(%ebp),%eax
801014e9:	89 04 24             	mov    %eax,(%esp)
801014ec:	e8 66 75 00 00       	call   80108a57 <copyout>
801014f1:	85 c0                	test   %eax,%eax
801014f3:	0f 88 6e 01 00 00    	js     80101667 <exec+0x3e3>
      goto bad;
    ustack[3+argc] = sp;
801014f9:	8b 45 e0             	mov    -0x20(%ebp),%eax
801014fc:	8d 50 03             	lea    0x3(%eax),%edx
801014ff:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101502:	89 84 95 40 ff ff ff 	mov    %eax,-0xc0(%ebp,%edx,4)
    goto bad;
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
  sp = sz;

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
80101509:	83 45 e0 01          	addl   $0x1,-0x20(%ebp)
8010150d:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101510:	c1 e0 02             	shl    $0x2,%eax
80101513:	03 45 0c             	add    0xc(%ebp),%eax
80101516:	8b 00                	mov    (%eax),%eax
80101518:	85 c0                	test   %eax,%eax
8010151a:	0f 85 6c ff ff ff    	jne    8010148c <exec+0x208>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
    ustack[3+argc] = sp;
  }
  ustack[3+argc] = 0;
80101520:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101523:	83 c0 03             	add    $0x3,%eax
80101526:	c7 84 85 40 ff ff ff 	movl   $0x0,-0xc0(%ebp,%eax,4)
8010152d:	00 00 00 00 

  ustack[0] = 0xffffffff;  // fake return PC
80101531:	c7 85 40 ff ff ff ff 	movl   $0xffffffff,-0xc0(%ebp)
80101538:	ff ff ff 
  ustack[1] = argc;
8010153b:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010153e:	89 85 44 ff ff ff    	mov    %eax,-0xbc(%ebp)
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80101544:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101547:	83 c0 01             	add    $0x1,%eax
8010154a:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80101551:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101554:	29 d0                	sub    %edx,%eax
80101556:	89 85 48 ff ff ff    	mov    %eax,-0xb8(%ebp)

  sp -= (3+argc+1) * 4;
8010155c:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010155f:	83 c0 04             	add    $0x4,%eax
80101562:	c1 e0 02             	shl    $0x2,%eax
80101565:	29 45 e8             	sub    %eax,-0x18(%ebp)
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
80101568:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010156b:	83 c0 04             	add    $0x4,%eax
8010156e:	c1 e0 02             	shl    $0x2,%eax
80101571:	89 44 24 0c          	mov    %eax,0xc(%esp)
80101575:	8d 85 40 ff ff ff    	lea    -0xc0(%ebp),%eax
8010157b:	89 44 24 08          	mov    %eax,0x8(%esp)
8010157f:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101582:	89 44 24 04          	mov    %eax,0x4(%esp)
80101586:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101589:	89 04 24             	mov    %eax,(%esp)
8010158c:	e8 c6 74 00 00       	call   80108a57 <copyout>
80101591:	85 c0                	test   %eax,%eax
80101593:	0f 88 d1 00 00 00    	js     8010166a <exec+0x3e6>
    goto bad;

  // Save program name for debugging.
  for(last=s=path; *s; s++)
80101599:	8b 45 08             	mov    0x8(%ebp),%eax
8010159c:	89 45 d0             	mov    %eax,-0x30(%ebp)
8010159f:	8b 45 d0             	mov    -0x30(%ebp),%eax
801015a2:	89 45 d4             	mov    %eax,-0x2c(%ebp)
801015a5:	eb 17                	jmp    801015be <exec+0x33a>
    if(*s == '/')
801015a7:	8b 45 d0             	mov    -0x30(%ebp),%eax
801015aa:	0f b6 00             	movzbl (%eax),%eax
801015ad:	3c 2f                	cmp    $0x2f,%al
801015af:	75 09                	jne    801015ba <exec+0x336>
      last = s+1;
801015b1:	8b 45 d0             	mov    -0x30(%ebp),%eax
801015b4:	83 c0 01             	add    $0x1,%eax
801015b7:	89 45 d4             	mov    %eax,-0x2c(%ebp)
  sp -= (3+argc+1) * 4;
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
    goto bad;

  // Save program name for debugging.
  for(last=s=path; *s; s++)
801015ba:	83 45 d0 01          	addl   $0x1,-0x30(%ebp)
801015be:	8b 45 d0             	mov    -0x30(%ebp),%eax
801015c1:	0f b6 00             	movzbl (%eax),%eax
801015c4:	84 c0                	test   %al,%al
801015c6:	75 df                	jne    801015a7 <exec+0x323>
    if(*s == '/')
      last = s+1;
  safestrcpy(proc->name, last, sizeof(proc->name));
801015c8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801015ce:	8d 50 6c             	lea    0x6c(%eax),%edx
801015d1:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
801015d8:	00 
801015d9:	8b 45 d4             	mov    -0x2c(%ebp),%eax
801015dc:	89 44 24 04          	mov    %eax,0x4(%esp)
801015e0:	89 14 24             	mov    %edx,(%esp)
801015e3:	e8 b5 43 00 00       	call   8010599d <safestrcpy>

  // Commit to the user image.
  oldpgdir = proc->pgdir;
801015e8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801015ee:	8b 40 04             	mov    0x4(%eax),%eax
801015f1:	89 45 f4             	mov    %eax,-0xc(%ebp)
  proc->pgdir = pgdir;
801015f4:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801015fa:	8b 55 f0             	mov    -0x10(%ebp),%edx
801015fd:	89 50 04             	mov    %edx,0x4(%eax)
  proc->sz = sz;
80101600:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80101606:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80101609:	89 10                	mov    %edx,(%eax)
  proc->tf->eip = elf.entry;  // main
8010160b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80101611:	8b 40 18             	mov    0x18(%eax),%eax
80101614:	8b 95 24 ff ff ff    	mov    -0xdc(%ebp),%edx
8010161a:	89 50 38             	mov    %edx,0x38(%eax)
  proc->tf->esp = sp;
8010161d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80101623:	8b 40 18             	mov    0x18(%eax),%eax
80101626:	8b 55 e8             	mov    -0x18(%ebp),%edx
80101629:	89 50 44             	mov    %edx,0x44(%eax)
  switchuvm(proc);
8010162c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80101632:	89 04 24             	mov    %eax,(%esp)
80101635:	e8 46 6d 00 00       	call   80108380 <switchuvm>
  freevm(oldpgdir);
8010163a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010163d:	89 04 24             	mov    %eax,(%esp)
80101640:	e8 cf 71 00 00       	call   80108814 <freevm>
  return 0;
80101645:	b8 00 00 00 00       	mov    $0x0,%eax
8010164a:	eb 4b                	jmp    80101697 <exec+0x413>
  ilock(ip);
  pgdir = 0;

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) < sizeof(elf))
    goto bad;
8010164c:	90                   	nop
8010164d:	eb 1c                	jmp    8010166b <exec+0x3e7>
  if(elf.magic != ELF_MAGIC)
    goto bad;
8010164f:	90                   	nop
80101650:	eb 19                	jmp    8010166b <exec+0x3e7>

  if((pgdir = setupkvm()) == 0)
    goto bad;
80101652:	90                   	nop
80101653:	eb 16                	jmp    8010166b <exec+0x3e7>

  // Load program into memory.
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
      goto bad;
80101655:	90                   	nop
80101656:	eb 13                	jmp    8010166b <exec+0x3e7>
    if(ph.type != ELF_PROG_LOAD)
      continue;
    if(ph.memsz < ph.filesz)
      goto bad;
80101658:	90                   	nop
80101659:	eb 10                	jmp    8010166b <exec+0x3e7>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
      goto bad;
8010165b:	90                   	nop
8010165c:	eb 0d                	jmp    8010166b <exec+0x3e7>
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
      goto bad;
8010165e:	90                   	nop
8010165f:	eb 0a                	jmp    8010166b <exec+0x3e7>

  // Allocate two pages at the next page boundary.
  // Make the first inaccessible.  Use the second as the user stack.
  sz = PGROUNDUP(sz);
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
    goto bad;
80101661:	90                   	nop
80101662:	eb 07                	jmp    8010166b <exec+0x3e7>
  sp = sz;

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
    if(argc >= MAXARG)
      goto bad;
80101664:	90                   	nop
80101665:	eb 04                	jmp    8010166b <exec+0x3e7>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
80101667:	90                   	nop
80101668:	eb 01                	jmp    8010166b <exec+0x3e7>
  ustack[1] = argc;
  ustack[2] = sp - (argc+1)*4;  // argv pointer

  sp -= (3+argc+1) * 4;
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
    goto bad;
8010166a:	90                   	nop
  switchuvm(proc);
  freevm(oldpgdir);
  return 0;

 bad:
  if(pgdir)
8010166b:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010166f:	74 0b                	je     8010167c <exec+0x3f8>
    freevm(pgdir);
80101671:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101674:	89 04 24             	mov    %eax,(%esp)
80101677:	e8 98 71 00 00       	call   80108814 <freevm>
  if(ip){
8010167c:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80101680:	74 10                	je     80101692 <exec+0x40e>
    iunlockput(ip);
80101682:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101685:	89 04 24             	mov    %eax,(%esp)
80101688:	e8 71 0c 00 00       	call   801022fe <iunlockput>
    end_op();
8010168d:	e8 5a 24 00 00       	call   80103aec <end_op>
  }
  return -1;
80101692:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80101697:	c9                   	leave  
80101698:	c3                   	ret    
80101699:	00 00                	add    %al,(%eax)
	...

8010169c <fileinit>:
  struct file file[NFILE];
} ftable;

void
fileinit(void)
{
8010169c:	55                   	push   %ebp
8010169d:	89 e5                	mov    %esp,%ebp
8010169f:	83 ec 18             	sub    $0x18,%esp
  initlock(&ftable.lock, "ftable");
801016a2:	c7 44 24 04 d2 8b 10 	movl   $0x80108bd2,0x4(%esp)
801016a9:	80 
801016aa:	c7 04 24 40 20 11 80 	movl   $0x80112040,(%esp)
801016b1:	e8 30 3e 00 00       	call   801054e6 <initlock>
}
801016b6:	c9                   	leave  
801016b7:	c3                   	ret    

801016b8 <filealloc>:

// Allocate a file structure.
struct file*
filealloc(void)
{
801016b8:	55                   	push   %ebp
801016b9:	89 e5                	mov    %esp,%ebp
801016bb:	83 ec 28             	sub    $0x28,%esp
  struct file *f;

  acquire(&ftable.lock);
801016be:	c7 04 24 40 20 11 80 	movl   $0x80112040,(%esp)
801016c5:	e8 3d 3e 00 00       	call   80105507 <acquire>
  for(f = ftable.file; f < ftable.file + NFILE; f++){
801016ca:	c7 45 f4 74 20 11 80 	movl   $0x80112074,-0xc(%ebp)
801016d1:	eb 29                	jmp    801016fc <filealloc+0x44>
    if(f->ref == 0){
801016d3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801016d6:	8b 40 04             	mov    0x4(%eax),%eax
801016d9:	85 c0                	test   %eax,%eax
801016db:	75 1b                	jne    801016f8 <filealloc+0x40>
      f->ref = 1;
801016dd:	8b 45 f4             	mov    -0xc(%ebp),%eax
801016e0:	c7 40 04 01 00 00 00 	movl   $0x1,0x4(%eax)
      release(&ftable.lock);
801016e7:	c7 04 24 40 20 11 80 	movl   $0x80112040,(%esp)
801016ee:	e8 7d 3e 00 00       	call   80105570 <release>
      return f;
801016f3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801016f6:	eb 1f                	jmp    80101717 <filealloc+0x5f>
filealloc(void)
{
  struct file *f;

  acquire(&ftable.lock);
  for(f = ftable.file; f < ftable.file + NFILE; f++){
801016f8:	83 45 f4 18          	addl   $0x18,-0xc(%ebp)
801016fc:	b8 d4 29 11 80       	mov    $0x801129d4,%eax
80101701:	39 45 f4             	cmp    %eax,-0xc(%ebp)
80101704:	72 cd                	jb     801016d3 <filealloc+0x1b>
      f->ref = 1;
      release(&ftable.lock);
      return f;
    }
  }
  release(&ftable.lock);
80101706:	c7 04 24 40 20 11 80 	movl   $0x80112040,(%esp)
8010170d:	e8 5e 3e 00 00       	call   80105570 <release>
  return 0;
80101712:	b8 00 00 00 00       	mov    $0x0,%eax
}
80101717:	c9                   	leave  
80101718:	c3                   	ret    

80101719 <filedup>:

// Increment ref count for file f.
struct file*
filedup(struct file *f)
{
80101719:	55                   	push   %ebp
8010171a:	89 e5                	mov    %esp,%ebp
8010171c:	83 ec 18             	sub    $0x18,%esp
  acquire(&ftable.lock);
8010171f:	c7 04 24 40 20 11 80 	movl   $0x80112040,(%esp)
80101726:	e8 dc 3d 00 00       	call   80105507 <acquire>
  if(f->ref < 1)
8010172b:	8b 45 08             	mov    0x8(%ebp),%eax
8010172e:	8b 40 04             	mov    0x4(%eax),%eax
80101731:	85 c0                	test   %eax,%eax
80101733:	7f 0c                	jg     80101741 <filedup+0x28>
    panic("filedup");
80101735:	c7 04 24 d9 8b 10 80 	movl   $0x80108bd9,(%esp)
8010173c:	e8 28 f1 ff ff       	call   80100869 <panic>
  f->ref++;
80101741:	8b 45 08             	mov    0x8(%ebp),%eax
80101744:	8b 40 04             	mov    0x4(%eax),%eax
80101747:	8d 50 01             	lea    0x1(%eax),%edx
8010174a:	8b 45 08             	mov    0x8(%ebp),%eax
8010174d:	89 50 04             	mov    %edx,0x4(%eax)
  release(&ftable.lock);
80101750:	c7 04 24 40 20 11 80 	movl   $0x80112040,(%esp)
80101757:	e8 14 3e 00 00       	call   80105570 <release>
  return f;
8010175c:	8b 45 08             	mov    0x8(%ebp),%eax
}
8010175f:	c9                   	leave  
80101760:	c3                   	ret    

80101761 <fileclose>:

// Close file f.  (Decrement ref count, close when reaches 0.)
void
fileclose(struct file *f)
{
80101761:	55                   	push   %ebp
80101762:	89 e5                	mov    %esp,%ebp
80101764:	83 ec 38             	sub    $0x38,%esp
  struct file ff;

  acquire(&ftable.lock);
80101767:	c7 04 24 40 20 11 80 	movl   $0x80112040,(%esp)
8010176e:	e8 94 3d 00 00       	call   80105507 <acquire>
  if(f->ref < 1)
80101773:	8b 45 08             	mov    0x8(%ebp),%eax
80101776:	8b 40 04             	mov    0x4(%eax),%eax
80101779:	85 c0                	test   %eax,%eax
8010177b:	7f 0c                	jg     80101789 <fileclose+0x28>
    panic("fileclose");
8010177d:	c7 04 24 e1 8b 10 80 	movl   $0x80108be1,(%esp)
80101784:	e8 e0 f0 ff ff       	call   80100869 <panic>
  if(--f->ref > 0){
80101789:	8b 45 08             	mov    0x8(%ebp),%eax
8010178c:	8b 40 04             	mov    0x4(%eax),%eax
8010178f:	8d 50 ff             	lea    -0x1(%eax),%edx
80101792:	8b 45 08             	mov    0x8(%ebp),%eax
80101795:	89 50 04             	mov    %edx,0x4(%eax)
80101798:	8b 45 08             	mov    0x8(%ebp),%eax
8010179b:	8b 40 04             	mov    0x4(%eax),%eax
8010179e:	85 c0                	test   %eax,%eax
801017a0:	7e 11                	jle    801017b3 <fileclose+0x52>
    release(&ftable.lock);
801017a2:	c7 04 24 40 20 11 80 	movl   $0x80112040,(%esp)
801017a9:	e8 c2 3d 00 00       	call   80105570 <release>
    return;
801017ae:	e9 82 00 00 00       	jmp    80101835 <fileclose+0xd4>
  }
  ff = *f;
801017b3:	8b 45 08             	mov    0x8(%ebp),%eax
801017b6:	8b 10                	mov    (%eax),%edx
801017b8:	89 55 e0             	mov    %edx,-0x20(%ebp)
801017bb:	8b 50 04             	mov    0x4(%eax),%edx
801017be:	89 55 e4             	mov    %edx,-0x1c(%ebp)
801017c1:	8b 50 08             	mov    0x8(%eax),%edx
801017c4:	89 55 e8             	mov    %edx,-0x18(%ebp)
801017c7:	8b 50 0c             	mov    0xc(%eax),%edx
801017ca:	89 55 ec             	mov    %edx,-0x14(%ebp)
801017cd:	8b 50 10             	mov    0x10(%eax),%edx
801017d0:	89 55 f0             	mov    %edx,-0x10(%ebp)
801017d3:	8b 40 14             	mov    0x14(%eax),%eax
801017d6:	89 45 f4             	mov    %eax,-0xc(%ebp)
  f->ref = 0;
801017d9:	8b 45 08             	mov    0x8(%ebp),%eax
801017dc:	c7 40 04 00 00 00 00 	movl   $0x0,0x4(%eax)
  f->type = FD_NONE;
801017e3:	8b 45 08             	mov    0x8(%ebp),%eax
801017e6:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  release(&ftable.lock);
801017ec:	c7 04 24 40 20 11 80 	movl   $0x80112040,(%esp)
801017f3:	e8 78 3d 00 00       	call   80105570 <release>
  
  if(ff.type == FD_PIPE)
801017f8:	8b 45 e0             	mov    -0x20(%ebp),%eax
801017fb:	83 f8 01             	cmp    $0x1,%eax
801017fe:	75 18                	jne    80101818 <fileclose+0xb7>
    pipeclose(ff.pipe, ff.writable);
80101800:	0f b6 45 e9          	movzbl -0x17(%ebp),%eax
80101804:	0f be d0             	movsbl %al,%edx
80101807:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010180a:	89 54 24 04          	mov    %edx,0x4(%esp)
8010180e:	89 04 24             	mov    %eax,(%esp)
80101811:	e8 41 2f 00 00       	call   80104757 <pipeclose>
80101816:	eb 1d                	jmp    80101835 <fileclose+0xd4>
  else if(ff.type == FD_INODE){
80101818:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010181b:	83 f8 02             	cmp    $0x2,%eax
8010181e:	75 15                	jne    80101835 <fileclose+0xd4>
    begin_op();
80101820:	e8 45 22 00 00       	call   80103a6a <begin_op>
    iput(ff.ip);
80101825:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101828:	89 04 24             	mov    %eax,(%esp)
8010182b:	e8 fd 09 00 00       	call   8010222d <iput>
    end_op();
80101830:	e8 b7 22 00 00       	call   80103aec <end_op>
  }
}
80101835:	c9                   	leave  
80101836:	c3                   	ret    

80101837 <filestat>:

// Get metadata about file f.
int
filestat(struct file *f, struct stat *st)
{
80101837:	55                   	push   %ebp
80101838:	89 e5                	mov    %esp,%ebp
8010183a:	83 ec 18             	sub    $0x18,%esp
  if(f->type == FD_INODE){
8010183d:	8b 45 08             	mov    0x8(%ebp),%eax
80101840:	8b 00                	mov    (%eax),%eax
80101842:	83 f8 02             	cmp    $0x2,%eax
80101845:	75 38                	jne    8010187f <filestat+0x48>
    ilock(f->ip);
80101847:	8b 45 08             	mov    0x8(%ebp),%eax
8010184a:	8b 40 10             	mov    0x10(%eax),%eax
8010184d:	89 04 24             	mov    %eax,(%esp)
80101850:	e8 1c 08 00 00       	call   80102071 <ilock>
    stati(f->ip, st);
80101855:	8b 45 08             	mov    0x8(%ebp),%eax
80101858:	8b 40 10             	mov    0x10(%eax),%eax
8010185b:	8b 55 0c             	mov    0xc(%ebp),%edx
8010185e:	89 54 24 04          	mov    %edx,0x4(%esp)
80101862:	89 04 24             	mov    %eax,(%esp)
80101865:	e8 c1 0c 00 00       	call   8010252b <stati>
    iunlock(f->ip);
8010186a:	8b 45 08             	mov    0x8(%ebp),%eax
8010186d:	8b 40 10             	mov    0x10(%eax),%eax
80101870:	89 04 24             	mov    %eax,(%esp)
80101873:	e8 50 09 00 00       	call   801021c8 <iunlock>
    return 0;
80101878:	b8 00 00 00 00       	mov    $0x0,%eax
8010187d:	eb 05                	jmp    80101884 <filestat+0x4d>
  }
  return -1;
8010187f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80101884:	c9                   	leave  
80101885:	c3                   	ret    

80101886 <fileread>:

// Read from file f.
int
fileread(struct file *f, char *addr, int n)
{
80101886:	55                   	push   %ebp
80101887:	89 e5                	mov    %esp,%ebp
80101889:	83 ec 28             	sub    $0x28,%esp
  int r;

  if(f->readable == 0)
8010188c:	8b 45 08             	mov    0x8(%ebp),%eax
8010188f:	0f b6 40 08          	movzbl 0x8(%eax),%eax
80101893:	84 c0                	test   %al,%al
80101895:	75 0a                	jne    801018a1 <fileread+0x1b>
    return -1;
80101897:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010189c:	e9 9f 00 00 00       	jmp    80101940 <fileread+0xba>
  if(f->type == FD_PIPE)
801018a1:	8b 45 08             	mov    0x8(%ebp),%eax
801018a4:	8b 00                	mov    (%eax),%eax
801018a6:	83 f8 01             	cmp    $0x1,%eax
801018a9:	75 1e                	jne    801018c9 <fileread+0x43>
    return piperead(f->pipe, addr, n);
801018ab:	8b 45 08             	mov    0x8(%ebp),%eax
801018ae:	8b 40 0c             	mov    0xc(%eax),%eax
801018b1:	8b 55 10             	mov    0x10(%ebp),%edx
801018b4:	89 54 24 08          	mov    %edx,0x8(%esp)
801018b8:	8b 55 0c             	mov    0xc(%ebp),%edx
801018bb:	89 54 24 04          	mov    %edx,0x4(%esp)
801018bf:	89 04 24             	mov    %eax,(%esp)
801018c2:	e8 12 30 00 00       	call   801048d9 <piperead>
801018c7:	eb 77                	jmp    80101940 <fileread+0xba>
  if(f->type == FD_INODE){
801018c9:	8b 45 08             	mov    0x8(%ebp),%eax
801018cc:	8b 00                	mov    (%eax),%eax
801018ce:	83 f8 02             	cmp    $0x2,%eax
801018d1:	75 61                	jne    80101934 <fileread+0xae>
    ilock(f->ip);
801018d3:	8b 45 08             	mov    0x8(%ebp),%eax
801018d6:	8b 40 10             	mov    0x10(%eax),%eax
801018d9:	89 04 24             	mov    %eax,(%esp)
801018dc:	e8 90 07 00 00       	call   80102071 <ilock>
    if((r = readi(f->ip, addr, f->off, n)) > 0)
801018e1:	8b 4d 10             	mov    0x10(%ebp),%ecx
801018e4:	8b 45 08             	mov    0x8(%ebp),%eax
801018e7:	8b 50 14             	mov    0x14(%eax),%edx
801018ea:	8b 45 08             	mov    0x8(%ebp),%eax
801018ed:	8b 40 10             	mov    0x10(%eax),%eax
801018f0:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
801018f4:	89 54 24 08          	mov    %edx,0x8(%esp)
801018f8:	8b 55 0c             	mov    0xc(%ebp),%edx
801018fb:	89 54 24 04          	mov    %edx,0x4(%esp)
801018ff:	89 04 24             	mov    %eax,(%esp)
80101902:	e8 69 0c 00 00       	call   80102570 <readi>
80101907:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010190a:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010190e:	7e 11                	jle    80101921 <fileread+0x9b>
      f->off += r;
80101910:	8b 45 08             	mov    0x8(%ebp),%eax
80101913:	8b 50 14             	mov    0x14(%eax),%edx
80101916:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101919:	01 c2                	add    %eax,%edx
8010191b:	8b 45 08             	mov    0x8(%ebp),%eax
8010191e:	89 50 14             	mov    %edx,0x14(%eax)
    iunlock(f->ip);
80101921:	8b 45 08             	mov    0x8(%ebp),%eax
80101924:	8b 40 10             	mov    0x10(%eax),%eax
80101927:	89 04 24             	mov    %eax,(%esp)
8010192a:	e8 99 08 00 00       	call   801021c8 <iunlock>
    return r;
8010192f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101932:	eb 0c                	jmp    80101940 <fileread+0xba>
  }
  panic("fileread");
80101934:	c7 04 24 eb 8b 10 80 	movl   $0x80108beb,(%esp)
8010193b:	e8 29 ef ff ff       	call   80100869 <panic>
}
80101940:	c9                   	leave  
80101941:	c3                   	ret    

80101942 <filewrite>:

//PAGEBREAK!
// Write to file f.
int
filewrite(struct file *f, char *addr, int n)
{
80101942:	55                   	push   %ebp
80101943:	89 e5                	mov    %esp,%ebp
80101945:	53                   	push   %ebx
80101946:	83 ec 24             	sub    $0x24,%esp
  int r;

  if(f->writable == 0)
80101949:	8b 45 08             	mov    0x8(%ebp),%eax
8010194c:	0f b6 40 09          	movzbl 0x9(%eax),%eax
80101950:	84 c0                	test   %al,%al
80101952:	75 0a                	jne    8010195e <filewrite+0x1c>
    return -1;
80101954:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101959:	e9 23 01 00 00       	jmp    80101a81 <filewrite+0x13f>
  if(f->type == FD_PIPE)
8010195e:	8b 45 08             	mov    0x8(%ebp),%eax
80101961:	8b 00                	mov    (%eax),%eax
80101963:	83 f8 01             	cmp    $0x1,%eax
80101966:	75 21                	jne    80101989 <filewrite+0x47>
    return pipewrite(f->pipe, addr, n);
80101968:	8b 45 08             	mov    0x8(%ebp),%eax
8010196b:	8b 40 0c             	mov    0xc(%eax),%eax
8010196e:	8b 55 10             	mov    0x10(%ebp),%edx
80101971:	89 54 24 08          	mov    %edx,0x8(%esp)
80101975:	8b 55 0c             	mov    0xc(%ebp),%edx
80101978:	89 54 24 04          	mov    %edx,0x4(%esp)
8010197c:	89 04 24             	mov    %eax,(%esp)
8010197f:	e8 65 2e 00 00       	call   801047e9 <pipewrite>
80101984:	e9 f8 00 00 00       	jmp    80101a81 <filewrite+0x13f>
  if(f->type == FD_INODE){
80101989:	8b 45 08             	mov    0x8(%ebp),%eax
8010198c:	8b 00                	mov    (%eax),%eax
8010198e:	83 f8 02             	cmp    $0x2,%eax
80101991:	0f 85 de 00 00 00    	jne    80101a75 <filewrite+0x133>
    // the maximum log transaction size, including
    // i-node, indirect block, allocation blocks,
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((LOGSIZE-1-1-2) / 2) * BSIZE;
80101997:	c7 45 ec 00 1a 00 00 	movl   $0x1a00,-0x14(%ebp)
    int i = 0;
8010199e:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
    while(i < n){
801019a5:	e9 a8 00 00 00       	jmp    80101a52 <filewrite+0x110>
      int n1 = n - i;
801019aa:	8b 45 f0             	mov    -0x10(%ebp),%eax
801019ad:	8b 55 10             	mov    0x10(%ebp),%edx
801019b0:	89 d1                	mov    %edx,%ecx
801019b2:	29 c1                	sub    %eax,%ecx
801019b4:	89 c8                	mov    %ecx,%eax
801019b6:	89 45 f4             	mov    %eax,-0xc(%ebp)
      if(n1 > max)
801019b9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801019bc:	3b 45 ec             	cmp    -0x14(%ebp),%eax
801019bf:	7e 06                	jle    801019c7 <filewrite+0x85>
        n1 = max;
801019c1:	8b 45 ec             	mov    -0x14(%ebp),%eax
801019c4:	89 45 f4             	mov    %eax,-0xc(%ebp)

      begin_op();
801019c7:	e8 9e 20 00 00       	call   80103a6a <begin_op>
      ilock(f->ip);
801019cc:	8b 45 08             	mov    0x8(%ebp),%eax
801019cf:	8b 40 10             	mov    0x10(%eax),%eax
801019d2:	89 04 24             	mov    %eax,(%esp)
801019d5:	e8 97 06 00 00       	call   80102071 <ilock>
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
801019da:	8b 5d f4             	mov    -0xc(%ebp),%ebx
801019dd:	8b 45 08             	mov    0x8(%ebp),%eax
801019e0:	8b 48 14             	mov    0x14(%eax),%ecx
801019e3:	8b 45 f0             	mov    -0x10(%ebp),%eax
801019e6:	89 c2                	mov    %eax,%edx
801019e8:	03 55 0c             	add    0xc(%ebp),%edx
801019eb:	8b 45 08             	mov    0x8(%ebp),%eax
801019ee:	8b 40 10             	mov    0x10(%eax),%eax
801019f1:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
801019f5:	89 4c 24 08          	mov    %ecx,0x8(%esp)
801019f9:	89 54 24 04          	mov    %edx,0x4(%esp)
801019fd:	89 04 24             	mov    %eax,(%esp)
80101a00:	e8 d7 0c 00 00       	call   801026dc <writei>
80101a05:	89 45 e8             	mov    %eax,-0x18(%ebp)
80101a08:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
80101a0c:	7e 11                	jle    80101a1f <filewrite+0xdd>
        f->off += r;
80101a0e:	8b 45 08             	mov    0x8(%ebp),%eax
80101a11:	8b 50 14             	mov    0x14(%eax),%edx
80101a14:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101a17:	01 c2                	add    %eax,%edx
80101a19:	8b 45 08             	mov    0x8(%ebp),%eax
80101a1c:	89 50 14             	mov    %edx,0x14(%eax)
      iunlock(f->ip);
80101a1f:	8b 45 08             	mov    0x8(%ebp),%eax
80101a22:	8b 40 10             	mov    0x10(%eax),%eax
80101a25:	89 04 24             	mov    %eax,(%esp)
80101a28:	e8 9b 07 00 00       	call   801021c8 <iunlock>
      end_op();
80101a2d:	e8 ba 20 00 00       	call   80103aec <end_op>

      if(r < 0)
80101a32:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
80101a36:	78 28                	js     80101a60 <filewrite+0x11e>
        break;
      if(r != n1)
80101a38:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101a3b:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80101a3e:	74 0c                	je     80101a4c <filewrite+0x10a>
        panic("short filewrite");
80101a40:	c7 04 24 f4 8b 10 80 	movl   $0x80108bf4,(%esp)
80101a47:	e8 1d ee ff ff       	call   80100869 <panic>
      i += r;
80101a4c:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101a4f:	01 45 f0             	add    %eax,-0x10(%ebp)
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((LOGSIZE-1-1-2) / 2) * BSIZE;
    int i = 0;
    while(i < n){
80101a52:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101a55:	3b 45 10             	cmp    0x10(%ebp),%eax
80101a58:	0f 8c 4c ff ff ff    	jl     801019aa <filewrite+0x68>
80101a5e:	eb 01                	jmp    80101a61 <filewrite+0x11f>
        f->off += r;
      iunlock(f->ip);
      end_op();

      if(r < 0)
        break;
80101a60:	90                   	nop
      if(r != n1)
        panic("short filewrite");
      i += r;
    }
    return i == n ? n : -1;
80101a61:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101a64:	3b 45 10             	cmp    0x10(%ebp),%eax
80101a67:	75 05                	jne    80101a6e <filewrite+0x12c>
80101a69:	8b 45 10             	mov    0x10(%ebp),%eax
80101a6c:	eb 05                	jmp    80101a73 <filewrite+0x131>
80101a6e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101a73:	eb 0c                	jmp    80101a81 <filewrite+0x13f>
  }
  panic("filewrite");
80101a75:	c7 04 24 04 8c 10 80 	movl   $0x80108c04,(%esp)
80101a7c:	e8 e8 ed ff ff       	call   80100869 <panic>
}
80101a81:	83 c4 24             	add    $0x24,%esp
80101a84:	5b                   	pop    %ebx
80101a85:	5d                   	pop    %ebp
80101a86:	c3                   	ret    
	...

80101a88 <readsb>:
struct superblock sb;

// Read the super block.
void
readsb(int dev, struct superblock *sb)
{
80101a88:	55                   	push   %ebp
80101a89:	89 e5                	mov    %esp,%ebp
80101a8b:	83 ec 28             	sub    $0x28,%esp
  struct buf *bp;

  bp = bread(dev, 1);
80101a8e:	8b 45 08             	mov    0x8(%ebp),%eax
80101a91:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80101a98:	00 
80101a99:	89 04 24             	mov    %eax,(%esp)
80101a9c:	e8 06 e7 ff ff       	call   801001a7 <bread>
80101aa1:	89 45 f4             	mov    %eax,-0xc(%ebp)
  memmove(sb, bp->data, sizeof(*sb));
80101aa4:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101aa7:	83 c0 18             	add    $0x18,%eax
80101aaa:	c7 44 24 08 1c 00 00 	movl   $0x1c,0x8(%esp)
80101ab1:	00 
80101ab2:	89 44 24 04          	mov    %eax,0x4(%esp)
80101ab6:	8b 45 0c             	mov    0xc(%ebp),%eax
80101ab9:	89 04 24             	mov    %eax,(%esp)
80101abc:	e8 7c 3d 00 00       	call   8010583d <memmove>
  brelse(bp);
80101ac1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101ac4:	89 04 24             	mov    %eax,(%esp)
80101ac7:	e8 4c e7 ff ff       	call   80100218 <brelse>
}
80101acc:	c9                   	leave  
80101acd:	c3                   	ret    

80101ace <bzero>:

// Zero a block.
static void
bzero(int dev, int bno)
{
80101ace:	55                   	push   %ebp
80101acf:	89 e5                	mov    %esp,%ebp
80101ad1:	83 ec 28             	sub    $0x28,%esp
  struct buf *bp;

  bp = bread(dev, bno);
80101ad4:	8b 55 0c             	mov    0xc(%ebp),%edx
80101ad7:	8b 45 08             	mov    0x8(%ebp),%eax
80101ada:	89 54 24 04          	mov    %edx,0x4(%esp)
80101ade:	89 04 24             	mov    %eax,(%esp)
80101ae1:	e8 c1 e6 ff ff       	call   801001a7 <bread>
80101ae6:	89 45 f4             	mov    %eax,-0xc(%ebp)
  memset(bp->data, 0, BSIZE);
80101ae9:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101aec:	83 c0 18             	add    $0x18,%eax
80101aef:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
80101af6:	00 
80101af7:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80101afe:	00 
80101aff:	89 04 24             	mov    %eax,(%esp)
80101b02:	e8 63 3c 00 00       	call   8010576a <memset>
  log_write(bp);
80101b07:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101b0a:	89 04 24             	mov    %eax,(%esp)
80101b0d:	e8 5e 21 00 00       	call   80103c70 <log_write>
  brelse(bp);
80101b12:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101b15:	89 04 24             	mov    %eax,(%esp)
80101b18:	e8 fb e6 ff ff       	call   80100218 <brelse>
}
80101b1d:	c9                   	leave  
80101b1e:	c3                   	ret    

80101b1f <balloc>:
// Blocks.

// Allocate a zeroed disk block.
static uint
balloc(uint dev)
{
80101b1f:	55                   	push   %ebp
80101b20:	89 e5                	mov    %esp,%ebp
80101b22:	53                   	push   %ebx
80101b23:	83 ec 24             	sub    $0x24,%esp
  int b, bi, m;
  struct buf *bp;

  bp = 0;
80101b26:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  for(b = 0; b < sb.size; b += BPB){
80101b2d:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
80101b34:	e9 16 01 00 00       	jmp    80101c4f <balloc+0x130>
    bp = bread(dev, BBLOCK(b, sb));
80101b39:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101b3c:	8d 90 ff 0f 00 00    	lea    0xfff(%eax),%edx
80101b42:	85 c0                	test   %eax,%eax
80101b44:	0f 48 c2             	cmovs  %edx,%eax
80101b47:	c1 f8 0c             	sar    $0xc,%eax
80101b4a:	89 c2                	mov    %eax,%edx
80101b4c:	a1 58 2a 11 80       	mov    0x80112a58,%eax
80101b51:	8d 04 02             	lea    (%edx,%eax,1),%eax
80101b54:	89 44 24 04          	mov    %eax,0x4(%esp)
80101b58:	8b 45 08             	mov    0x8(%ebp),%eax
80101b5b:	89 04 24             	mov    %eax,(%esp)
80101b5e:	e8 44 e6 ff ff       	call   801001a7 <bread>
80101b63:	89 45 f4             	mov    %eax,-0xc(%ebp)
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
80101b66:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80101b6d:	e9 aa 00 00 00       	jmp    80101c1c <balloc+0xfd>
      m = 1 << (bi % 8);
80101b72:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101b75:	89 c2                	mov    %eax,%edx
80101b77:	c1 fa 1f             	sar    $0x1f,%edx
80101b7a:	c1 ea 1d             	shr    $0x1d,%edx
80101b7d:	01 d0                	add    %edx,%eax
80101b7f:	83 e0 07             	and    $0x7,%eax
80101b82:	29 d0                	sub    %edx,%eax
80101b84:	ba 01 00 00 00       	mov    $0x1,%edx
80101b89:	89 d3                	mov    %edx,%ebx
80101b8b:	89 c1                	mov    %eax,%ecx
80101b8d:	d3 e3                	shl    %cl,%ebx
80101b8f:	89 d8                	mov    %ebx,%eax
80101b91:	89 45 f0             	mov    %eax,-0x10(%ebp)
      if((bp->data[bi/8] & m) == 0){  // Is block free?
80101b94:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101b97:	8d 50 07             	lea    0x7(%eax),%edx
80101b9a:	85 c0                	test   %eax,%eax
80101b9c:	0f 48 c2             	cmovs  %edx,%eax
80101b9f:	c1 f8 03             	sar    $0x3,%eax
80101ba2:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101ba5:	0f b6 44 02 18       	movzbl 0x18(%edx,%eax,1),%eax
80101baa:	0f b6 c0             	movzbl %al,%eax
80101bad:	23 45 f0             	and    -0x10(%ebp),%eax
80101bb0:	85 c0                	test   %eax,%eax
80101bb2:	75 64                	jne    80101c18 <balloc+0xf9>
        bp->data[bi/8] |= m;  // Mark block in use.
80101bb4:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101bb7:	8d 50 07             	lea    0x7(%eax),%edx
80101bba:	85 c0                	test   %eax,%eax
80101bbc:	0f 48 c2             	cmovs  %edx,%eax
80101bbf:	c1 f8 03             	sar    $0x3,%eax
80101bc2:	89 c2                	mov    %eax,%edx
80101bc4:	8b 4d f4             	mov    -0xc(%ebp),%ecx
80101bc7:	0f b6 44 01 18       	movzbl 0x18(%ecx,%eax,1),%eax
80101bcc:	89 c1                	mov    %eax,%ecx
80101bce:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101bd1:	09 c8                	or     %ecx,%eax
80101bd3:	89 c1                	mov    %eax,%ecx
80101bd5:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101bd8:	88 4c 10 18          	mov    %cl,0x18(%eax,%edx,1)
        log_write(bp);
80101bdc:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101bdf:	89 04 24             	mov    %eax,(%esp)
80101be2:	e8 89 20 00 00       	call   80103c70 <log_write>
        brelse(bp);
80101be7:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101bea:	89 04 24             	mov    %eax,(%esp)
80101bed:	e8 26 e6 ff ff       	call   80100218 <brelse>
        bzero(dev, b + bi);
80101bf2:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101bf5:	8b 55 e8             	mov    -0x18(%ebp),%edx
80101bf8:	01 c2                	add    %eax,%edx
80101bfa:	8b 45 08             	mov    0x8(%ebp),%eax
80101bfd:	89 54 24 04          	mov    %edx,0x4(%esp)
80101c01:	89 04 24             	mov    %eax,(%esp)
80101c04:	e8 c5 fe ff ff       	call   80101ace <bzero>
        return b + bi;
80101c09:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101c0c:	8b 55 e8             	mov    -0x18(%ebp),%edx
80101c0f:	8d 04 02             	lea    (%edx,%eax,1),%eax
      }
    }
    brelse(bp);
  }
  panic("balloc: out of blocks");
}
80101c12:	83 c4 24             	add    $0x24,%esp
80101c15:	5b                   	pop    %ebx
80101c16:	5d                   	pop    %ebp
80101c17:	c3                   	ret    
  struct buf *bp;

  bp = 0;
  for(b = 0; b < sb.size; b += BPB){
    bp = bread(dev, BBLOCK(b, sb));
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
80101c18:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
80101c1c:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%ebp)
80101c23:	7f 18                	jg     80101c3d <balloc+0x11e>
80101c25:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101c28:	8b 55 e8             	mov    -0x18(%ebp),%edx
80101c2b:	8d 04 02             	lea    (%edx,%eax,1),%eax
80101c2e:	89 c2                	mov    %eax,%edx
80101c30:	a1 40 2a 11 80       	mov    0x80112a40,%eax
80101c35:	39 c2                	cmp    %eax,%edx
80101c37:	0f 82 35 ff ff ff    	jb     80101b72 <balloc+0x53>
        brelse(bp);
        bzero(dev, b + bi);
        return b + bi;
      }
    }
    brelse(bp);
80101c3d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101c40:	89 04 24             	mov    %eax,(%esp)
80101c43:	e8 d0 e5 ff ff       	call   80100218 <brelse>
{
  int b, bi, m;
  struct buf *bp;

  bp = 0;
  for(b = 0; b < sb.size; b += BPB){
80101c48:	81 45 e8 00 10 00 00 	addl   $0x1000,-0x18(%ebp)
80101c4f:	8b 55 e8             	mov    -0x18(%ebp),%edx
80101c52:	a1 40 2a 11 80       	mov    0x80112a40,%eax
80101c57:	39 c2                	cmp    %eax,%edx
80101c59:	0f 82 da fe ff ff    	jb     80101b39 <balloc+0x1a>
        return b + bi;
      }
    }
    brelse(bp);
  }
  panic("balloc: out of blocks");
80101c5f:	c7 04 24 10 8c 10 80 	movl   $0x80108c10,(%esp)
80101c66:	e8 fe eb ff ff       	call   80100869 <panic>

80101c6b <bfree>:
}

// Free a disk block.
static void
bfree(int dev, uint b)
{
80101c6b:	55                   	push   %ebp
80101c6c:	89 e5                	mov    %esp,%ebp
80101c6e:	53                   	push   %ebx
80101c6f:	83 ec 24             	sub    $0x24,%esp
  struct buf *bp;
  int bi, m;

  readsb(dev, &sb);
80101c72:	c7 44 24 04 40 2a 11 	movl   $0x80112a40,0x4(%esp)
80101c79:	80 
80101c7a:	8b 45 08             	mov    0x8(%ebp),%eax
80101c7d:	89 04 24             	mov    %eax,(%esp)
80101c80:	e8 03 fe ff ff       	call   80101a88 <readsb>
  bp = bread(dev, BBLOCK(b, sb));
80101c85:	8b 45 0c             	mov    0xc(%ebp),%eax
80101c88:	89 c2                	mov    %eax,%edx
80101c8a:	c1 ea 0c             	shr    $0xc,%edx
80101c8d:	a1 58 2a 11 80       	mov    0x80112a58,%eax
80101c92:	01 c2                	add    %eax,%edx
80101c94:	8b 45 08             	mov    0x8(%ebp),%eax
80101c97:	89 54 24 04          	mov    %edx,0x4(%esp)
80101c9b:	89 04 24             	mov    %eax,(%esp)
80101c9e:	e8 04 e5 ff ff       	call   801001a7 <bread>
80101ca3:	89 45 ec             	mov    %eax,-0x14(%ebp)
  bi = b % BPB;
80101ca6:	8b 45 0c             	mov    0xc(%ebp),%eax
80101ca9:	25 ff 0f 00 00       	and    $0xfff,%eax
80101cae:	89 45 f0             	mov    %eax,-0x10(%ebp)
  m = 1 << (bi % 8);
80101cb1:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101cb4:	89 c2                	mov    %eax,%edx
80101cb6:	c1 fa 1f             	sar    $0x1f,%edx
80101cb9:	c1 ea 1d             	shr    $0x1d,%edx
80101cbc:	01 d0                	add    %edx,%eax
80101cbe:	83 e0 07             	and    $0x7,%eax
80101cc1:	29 d0                	sub    %edx,%eax
80101cc3:	ba 01 00 00 00       	mov    $0x1,%edx
80101cc8:	89 d3                	mov    %edx,%ebx
80101cca:	89 c1                	mov    %eax,%ecx
80101ccc:	d3 e3                	shl    %cl,%ebx
80101cce:	89 d8                	mov    %ebx,%eax
80101cd0:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((bp->data[bi/8] & m) == 0)
80101cd3:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101cd6:	8d 50 07             	lea    0x7(%eax),%edx
80101cd9:	85 c0                	test   %eax,%eax
80101cdb:	0f 48 c2             	cmovs  %edx,%eax
80101cde:	c1 f8 03             	sar    $0x3,%eax
80101ce1:	8b 55 ec             	mov    -0x14(%ebp),%edx
80101ce4:	0f b6 44 02 18       	movzbl 0x18(%edx,%eax,1),%eax
80101ce9:	0f b6 c0             	movzbl %al,%eax
80101cec:	23 45 f4             	and    -0xc(%ebp),%eax
80101cef:	85 c0                	test   %eax,%eax
80101cf1:	75 0c                	jne    80101cff <bfree+0x94>
    panic("freeing free block");
80101cf3:	c7 04 24 26 8c 10 80 	movl   $0x80108c26,(%esp)
80101cfa:	e8 6a eb ff ff       	call   80100869 <panic>
  bp->data[bi/8] &= ~m;
80101cff:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101d02:	8d 50 07             	lea    0x7(%eax),%edx
80101d05:	85 c0                	test   %eax,%eax
80101d07:	0f 48 c2             	cmovs  %edx,%eax
80101d0a:	c1 f8 03             	sar    $0x3,%eax
80101d0d:	89 c2                	mov    %eax,%edx
80101d0f:	8b 4d ec             	mov    -0x14(%ebp),%ecx
80101d12:	0f b6 44 01 18       	movzbl 0x18(%ecx,%eax,1),%eax
80101d17:	8b 4d f4             	mov    -0xc(%ebp),%ecx
80101d1a:	f7 d1                	not    %ecx
80101d1c:	21 c8                	and    %ecx,%eax
80101d1e:	89 c1                	mov    %eax,%ecx
80101d20:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101d23:	88 4c 10 18          	mov    %cl,0x18(%eax,%edx,1)
  log_write(bp);
80101d27:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101d2a:	89 04 24             	mov    %eax,(%esp)
80101d2d:	e8 3e 1f 00 00       	call   80103c70 <log_write>
  brelse(bp);
80101d32:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101d35:	89 04 24             	mov    %eax,(%esp)
80101d38:	e8 db e4 ff ff       	call   80100218 <brelse>
}
80101d3d:	83 c4 24             	add    $0x24,%esp
80101d40:	5b                   	pop    %ebx
80101d41:	5d                   	pop    %ebp
80101d42:	c3                   	ret    

80101d43 <iinit>:
  struct inode inode[NINODE];
} icache;

void
iinit(int dev)
{
80101d43:	55                   	push   %ebp
80101d44:	89 e5                	mov    %esp,%ebp
80101d46:	57                   	push   %edi
80101d47:	56                   	push   %esi
80101d48:	53                   	push   %ebx
80101d49:	83 ec 4c             	sub    $0x4c,%esp
  initlock(&icache.lock, "icache");
80101d4c:	c7 44 24 04 39 8c 10 	movl   $0x80108c39,0x4(%esp)
80101d53:	80 
80101d54:	c7 04 24 60 2a 11 80 	movl   $0x80112a60,(%esp)
80101d5b:	e8 86 37 00 00       	call   801054e6 <initlock>
  readsb(dev, &sb);
80101d60:	c7 44 24 04 40 2a 11 	movl   $0x80112a40,0x4(%esp)
80101d67:	80 
80101d68:	8b 45 08             	mov    0x8(%ebp),%eax
80101d6b:	89 04 24             	mov    %eax,(%esp)
80101d6e:	e8 15 fd ff ff       	call   80101a88 <readsb>
  cprintf(
80101d73:	a1 58 2a 11 80       	mov    0x80112a58,%eax
80101d78:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80101d7b:	8b 3d 54 2a 11 80    	mov    0x80112a54,%edi
80101d81:	8b 35 50 2a 11 80    	mov    0x80112a50,%esi
80101d87:	8b 1d 4c 2a 11 80    	mov    0x80112a4c,%ebx
80101d8d:	8b 0d 48 2a 11 80    	mov    0x80112a48,%ecx
80101d93:	8b 15 44 2a 11 80    	mov    0x80112a44,%edx
80101d99:	a1 40 2a 11 80       	mov    0x80112a40,%eax
80101d9e:	89 45 d4             	mov    %eax,-0x2c(%ebp)
80101da1:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101da4:	89 44 24 1c          	mov    %eax,0x1c(%esp)
80101da8:	89 7c 24 18          	mov    %edi,0x18(%esp)
80101dac:	89 74 24 14          	mov    %esi,0x14(%esp)
80101db0:	89 5c 24 10          	mov    %ebx,0x10(%esp)
80101db4:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
80101db8:	89 54 24 08          	mov    %edx,0x8(%esp)
80101dbc:	8b 45 d4             	mov    -0x2c(%ebp),%eax
80101dbf:	89 44 24 04          	mov    %eax,0x4(%esp)
80101dc3:	c7 04 24 40 8c 10 80 	movl   $0x80108c40,(%esp)
80101dca:	e8 fa e8 ff ff       	call   801006c9 <cprintf>
    "sb: size %d nblocks %d ninodes %d nlog %d logstart %d "
    "inodestart %d bmap start %d\n", sb.size, sb.nblocks, sb.ninodes,
    sb.nlog, sb.logstart, sb.inodestart, sb.bmapstart);
}
80101dcf:	83 c4 4c             	add    $0x4c,%esp
80101dd2:	5b                   	pop    %ebx
80101dd3:	5e                   	pop    %esi
80101dd4:	5f                   	pop    %edi
80101dd5:	5d                   	pop    %ebp
80101dd6:	c3                   	ret    

80101dd7 <ialloc>:
//PAGEBREAK!
// Allocate a new inode with the given type on device dev.
// A free inode has a type of zero.
struct inode*
ialloc(uint dev, short type)
{
80101dd7:	55                   	push   %ebp
80101dd8:	89 e5                	mov    %esp,%ebp
80101dda:	83 ec 38             	sub    $0x38,%esp
80101ddd:	8b 45 0c             	mov    0xc(%ebp),%eax
80101de0:	66 89 45 e4          	mov    %ax,-0x1c(%ebp)
  int inum;
  struct buf *bp;
  struct dinode *dip;

  for(inum = 1; inum < sb.ninodes; inum++){
80101de4:	c7 45 ec 01 00 00 00 	movl   $0x1,-0x14(%ebp)
80101deb:	e9 9f 00 00 00       	jmp    80101e8f <ialloc+0xb8>
    bp = bread(dev, IBLOCK(inum, sb));
80101df0:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101df3:	89 c2                	mov    %eax,%edx
80101df5:	c1 ea 03             	shr    $0x3,%edx
80101df8:	a1 54 2a 11 80       	mov    0x80112a54,%eax
80101dfd:	8d 04 02             	lea    (%edx,%eax,1),%eax
80101e00:	89 44 24 04          	mov    %eax,0x4(%esp)
80101e04:	8b 45 08             	mov    0x8(%ebp),%eax
80101e07:	89 04 24             	mov    %eax,(%esp)
80101e0a:	e8 98 e3 ff ff       	call   801001a7 <bread>
80101e0f:	89 45 f0             	mov    %eax,-0x10(%ebp)
    dip = (struct dinode*)bp->data + inum%IPB;
80101e12:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101e15:	83 c0 18             	add    $0x18,%eax
80101e18:	8b 55 ec             	mov    -0x14(%ebp),%edx
80101e1b:	83 e2 07             	and    $0x7,%edx
80101e1e:	c1 e2 06             	shl    $0x6,%edx
80101e21:	01 d0                	add    %edx,%eax
80101e23:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(dip->type == 0){  // a free inode
80101e26:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101e29:	0f b7 00             	movzwl (%eax),%eax
80101e2c:	66 85 c0             	test   %ax,%ax
80101e2f:	75 4f                	jne    80101e80 <ialloc+0xa9>
      memset(dip, 0, sizeof(*dip));
80101e31:	c7 44 24 08 40 00 00 	movl   $0x40,0x8(%esp)
80101e38:	00 
80101e39:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80101e40:	00 
80101e41:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101e44:	89 04 24             	mov    %eax,(%esp)
80101e47:	e8 1e 39 00 00       	call   8010576a <memset>
      dip->type = type;
80101e4c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101e4f:	0f b7 55 e4          	movzwl -0x1c(%ebp),%edx
80101e53:	66 89 10             	mov    %dx,(%eax)
      log_write(bp);   // mark it allocated on the disk
80101e56:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101e59:	89 04 24             	mov    %eax,(%esp)
80101e5c:	e8 0f 1e 00 00       	call   80103c70 <log_write>
      brelse(bp);
80101e61:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101e64:	89 04 24             	mov    %eax,(%esp)
80101e67:	e8 ac e3 ff ff       	call   80100218 <brelse>
      return iget(dev, inum);
80101e6c:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101e6f:	89 44 24 04          	mov    %eax,0x4(%esp)
80101e73:	8b 45 08             	mov    0x8(%ebp),%eax
80101e76:	89 04 24             	mov    %eax,(%esp)
80101e79:	e8 ee 00 00 00       	call   80101f6c <iget>
    }
    brelse(bp);
  }
  panic("ialloc: no inodes");
}
80101e7e:	c9                   	leave  
80101e7f:	c3                   	ret    
      dip->type = type;
      log_write(bp);   // mark it allocated on the disk
      brelse(bp);
      return iget(dev, inum);
    }
    brelse(bp);
80101e80:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101e83:	89 04 24             	mov    %eax,(%esp)
80101e86:	e8 8d e3 ff ff       	call   80100218 <brelse>
{
  int inum;
  struct buf *bp;
  struct dinode *dip;

  for(inum = 1; inum < sb.ninodes; inum++){
80101e8b:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
80101e8f:	8b 55 ec             	mov    -0x14(%ebp),%edx
80101e92:	a1 48 2a 11 80       	mov    0x80112a48,%eax
80101e97:	39 c2                	cmp    %eax,%edx
80101e99:	0f 82 51 ff ff ff    	jb     80101df0 <ialloc+0x19>
      brelse(bp);
      return iget(dev, inum);
    }
    brelse(bp);
  }
  panic("ialloc: no inodes");
80101e9f:	c7 04 24 93 8c 10 80 	movl   $0x80108c93,(%esp)
80101ea6:	e8 be e9 ff ff       	call   80100869 <panic>

80101eab <iupdate>:
}

// Copy a modified in-memory inode to disk.
void
iupdate(struct inode *ip)
{
80101eab:	55                   	push   %ebp
80101eac:	89 e5                	mov    %esp,%ebp
80101eae:	83 ec 28             	sub    $0x28,%esp
  struct buf *bp;
  struct dinode *dip;

  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
80101eb1:	8b 45 08             	mov    0x8(%ebp),%eax
80101eb4:	8b 40 04             	mov    0x4(%eax),%eax
80101eb7:	89 c2                	mov    %eax,%edx
80101eb9:	c1 ea 03             	shr    $0x3,%edx
80101ebc:	a1 54 2a 11 80       	mov    0x80112a54,%eax
80101ec1:	01 c2                	add    %eax,%edx
80101ec3:	8b 45 08             	mov    0x8(%ebp),%eax
80101ec6:	8b 00                	mov    (%eax),%eax
80101ec8:	89 54 24 04          	mov    %edx,0x4(%esp)
80101ecc:	89 04 24             	mov    %eax,(%esp)
80101ecf:	e8 d3 e2 ff ff       	call   801001a7 <bread>
80101ed4:	89 45 f0             	mov    %eax,-0x10(%ebp)
  dip = (struct dinode*)bp->data + ip->inum%IPB;
80101ed7:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101eda:	83 c0 18             	add    $0x18,%eax
80101edd:	89 c2                	mov    %eax,%edx
80101edf:	8b 45 08             	mov    0x8(%ebp),%eax
80101ee2:	8b 40 04             	mov    0x4(%eax),%eax
80101ee5:	83 e0 07             	and    $0x7,%eax
80101ee8:	c1 e0 06             	shl    $0x6,%eax
80101eeb:	8d 04 02             	lea    (%edx,%eax,1),%eax
80101eee:	89 45 f4             	mov    %eax,-0xc(%ebp)
  dip->type = ip->type;
80101ef1:	8b 45 08             	mov    0x8(%ebp),%eax
80101ef4:	0f b7 50 10          	movzwl 0x10(%eax),%edx
80101ef8:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101efb:	66 89 10             	mov    %dx,(%eax)
  dip->major = ip->major;
80101efe:	8b 45 08             	mov    0x8(%ebp),%eax
80101f01:	0f b7 50 12          	movzwl 0x12(%eax),%edx
80101f05:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101f08:	66 89 50 02          	mov    %dx,0x2(%eax)
  dip->minor = ip->minor;
80101f0c:	8b 45 08             	mov    0x8(%ebp),%eax
80101f0f:	0f b7 50 14          	movzwl 0x14(%eax),%edx
80101f13:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101f16:	66 89 50 04          	mov    %dx,0x4(%eax)
  dip->nlink = ip->nlink;
80101f1a:	8b 45 08             	mov    0x8(%ebp),%eax
80101f1d:	0f b7 50 16          	movzwl 0x16(%eax),%edx
80101f21:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101f24:	66 89 50 06          	mov    %dx,0x6(%eax)
  dip->size = ip->size;
80101f28:	8b 45 08             	mov    0x8(%ebp),%eax
80101f2b:	8b 50 18             	mov    0x18(%eax),%edx
80101f2e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101f31:	89 50 08             	mov    %edx,0x8(%eax)
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
80101f34:	8b 45 08             	mov    0x8(%ebp),%eax
80101f37:	8d 50 1c             	lea    0x1c(%eax),%edx
80101f3a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101f3d:	83 c0 0c             	add    $0xc,%eax
80101f40:	c7 44 24 08 34 00 00 	movl   $0x34,0x8(%esp)
80101f47:	00 
80101f48:	89 54 24 04          	mov    %edx,0x4(%esp)
80101f4c:	89 04 24             	mov    %eax,(%esp)
80101f4f:	e8 e9 38 00 00       	call   8010583d <memmove>
  log_write(bp);
80101f54:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101f57:	89 04 24             	mov    %eax,(%esp)
80101f5a:	e8 11 1d 00 00       	call   80103c70 <log_write>
  brelse(bp);
80101f5f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101f62:	89 04 24             	mov    %eax,(%esp)
80101f65:	e8 ae e2 ff ff       	call   80100218 <brelse>
}
80101f6a:	c9                   	leave  
80101f6b:	c3                   	ret    

80101f6c <iget>:
// Find the inode with number inum on device dev
// and return the in-memory copy. Does not lock
// the inode and does not read it from disk.
static struct inode*
iget(uint dev, uint inum)
{
80101f6c:	55                   	push   %ebp
80101f6d:	89 e5                	mov    %esp,%ebp
80101f6f:	83 ec 28             	sub    $0x28,%esp
  struct inode *ip, *empty;

  acquire(&icache.lock);
80101f72:	c7 04 24 60 2a 11 80 	movl   $0x80112a60,(%esp)
80101f79:	e8 89 35 00 00       	call   80105507 <acquire>

  // Is the inode already cached?
  empty = 0;
80101f7e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
80101f85:	c7 45 f0 94 2a 11 80 	movl   $0x80112a94,-0x10(%ebp)
80101f8c:	eb 59                	jmp    80101fe7 <iget+0x7b>
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
80101f8e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101f91:	8b 40 08             	mov    0x8(%eax),%eax
80101f94:	85 c0                	test   %eax,%eax
80101f96:	7e 35                	jle    80101fcd <iget+0x61>
80101f98:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101f9b:	8b 00                	mov    (%eax),%eax
80101f9d:	3b 45 08             	cmp    0x8(%ebp),%eax
80101fa0:	75 2b                	jne    80101fcd <iget+0x61>
80101fa2:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101fa5:	8b 40 04             	mov    0x4(%eax),%eax
80101fa8:	3b 45 0c             	cmp    0xc(%ebp),%eax
80101fab:	75 20                	jne    80101fcd <iget+0x61>
      ip->ref++;
80101fad:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101fb0:	8b 40 08             	mov    0x8(%eax),%eax
80101fb3:	8d 50 01             	lea    0x1(%eax),%edx
80101fb6:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101fb9:	89 50 08             	mov    %edx,0x8(%eax)
      release(&icache.lock);
80101fbc:	c7 04 24 60 2a 11 80 	movl   $0x80112a60,(%esp)
80101fc3:	e8 a8 35 00 00       	call   80105570 <release>
      return ip;
80101fc8:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101fcb:	eb 70                	jmp    8010203d <iget+0xd1>
    }
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
80101fcd:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80101fd1:	75 10                	jne    80101fe3 <iget+0x77>
80101fd3:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101fd6:	8b 40 08             	mov    0x8(%eax),%eax
80101fd9:	85 c0                	test   %eax,%eax
80101fdb:	75 06                	jne    80101fe3 <iget+0x77>
      empty = ip;
80101fdd:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101fe0:	89 45 f4             	mov    %eax,-0xc(%ebp)

  acquire(&icache.lock);

  // Is the inode already cached?
  empty = 0;
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
80101fe3:	83 45 f0 50          	addl   $0x50,-0x10(%ebp)
80101fe7:	b8 34 3a 11 80       	mov    $0x80113a34,%eax
80101fec:	39 45 f0             	cmp    %eax,-0x10(%ebp)
80101fef:	72 9d                	jb     80101f8e <iget+0x22>
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
      empty = ip;
  }

  // Recycle an inode cache entry.
  if(empty == 0)
80101ff1:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80101ff5:	75 0c                	jne    80102003 <iget+0x97>
    panic("iget: no inodes");
80101ff7:	c7 04 24 a5 8c 10 80 	movl   $0x80108ca5,(%esp)
80101ffe:	e8 66 e8 ff ff       	call   80100869 <panic>

  ip = empty;
80102003:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102006:	89 45 f0             	mov    %eax,-0x10(%ebp)
  ip->dev = dev;
80102009:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010200c:	8b 55 08             	mov    0x8(%ebp),%edx
8010200f:	89 10                	mov    %edx,(%eax)
  ip->inum = inum;
80102011:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102014:	8b 55 0c             	mov    0xc(%ebp),%edx
80102017:	89 50 04             	mov    %edx,0x4(%eax)
  ip->ref = 1;
8010201a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010201d:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)
  ip->flags = 0;
80102024:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102027:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
  release(&icache.lock);
8010202e:	c7 04 24 60 2a 11 80 	movl   $0x80112a60,(%esp)
80102035:	e8 36 35 00 00       	call   80105570 <release>

  return ip;
8010203a:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
8010203d:	c9                   	leave  
8010203e:	c3                   	ret    

8010203f <idup>:

// Increment reference count for ip.
// Returns ip to enable ip = idup(ip1) idiom.
struct inode*
idup(struct inode *ip)
{
8010203f:	55                   	push   %ebp
80102040:	89 e5                	mov    %esp,%ebp
80102042:	83 ec 18             	sub    $0x18,%esp
  acquire(&icache.lock);
80102045:	c7 04 24 60 2a 11 80 	movl   $0x80112a60,(%esp)
8010204c:	e8 b6 34 00 00       	call   80105507 <acquire>
  ip->ref++;
80102051:	8b 45 08             	mov    0x8(%ebp),%eax
80102054:	8b 40 08             	mov    0x8(%eax),%eax
80102057:	8d 50 01             	lea    0x1(%eax),%edx
8010205a:	8b 45 08             	mov    0x8(%ebp),%eax
8010205d:	89 50 08             	mov    %edx,0x8(%eax)
  release(&icache.lock);
80102060:	c7 04 24 60 2a 11 80 	movl   $0x80112a60,(%esp)
80102067:	e8 04 35 00 00       	call   80105570 <release>
  return ip;
8010206c:	8b 45 08             	mov    0x8(%ebp),%eax
}
8010206f:	c9                   	leave  
80102070:	c3                   	ret    

80102071 <ilock>:

// Lock the given inode.
// Reads the inode from disk if necessary.
void
ilock(struct inode *ip)
{
80102071:	55                   	push   %ebp
80102072:	89 e5                	mov    %esp,%ebp
80102074:	83 ec 28             	sub    $0x28,%esp
  struct buf *bp;
  struct dinode *dip;

  if(ip == 0 || ip->ref < 1)
80102077:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
8010207b:	74 0a                	je     80102087 <ilock+0x16>
8010207d:	8b 45 08             	mov    0x8(%ebp),%eax
80102080:	8b 40 08             	mov    0x8(%eax),%eax
80102083:	85 c0                	test   %eax,%eax
80102085:	7f 0c                	jg     80102093 <ilock+0x22>
    panic("ilock");
80102087:	c7 04 24 b5 8c 10 80 	movl   $0x80108cb5,(%esp)
8010208e:	e8 d6 e7 ff ff       	call   80100869 <panic>

  acquire(&icache.lock);
80102093:	c7 04 24 60 2a 11 80 	movl   $0x80112a60,(%esp)
8010209a:	e8 68 34 00 00       	call   80105507 <acquire>
  while(ip->flags & I_BUSY)
8010209f:	eb 13                	jmp    801020b4 <ilock+0x43>
    sleep(ip, &icache.lock);
801020a1:	c7 44 24 04 60 2a 11 	movl   $0x80112a60,0x4(%esp)
801020a8:	80 
801020a9:	8b 45 08             	mov    0x8(%ebp),%eax
801020ac:	89 04 24             	mov    %eax,(%esp)
801020af:	e8 7a 31 00 00       	call   8010522e <sleep>

  if(ip == 0 || ip->ref < 1)
    panic("ilock");

  acquire(&icache.lock);
  while(ip->flags & I_BUSY)
801020b4:	8b 45 08             	mov    0x8(%ebp),%eax
801020b7:	8b 40 0c             	mov    0xc(%eax),%eax
801020ba:	83 e0 01             	and    $0x1,%eax
801020bd:	84 c0                	test   %al,%al
801020bf:	75 e0                	jne    801020a1 <ilock+0x30>
    sleep(ip, &icache.lock);
  ip->flags |= I_BUSY;
801020c1:	8b 45 08             	mov    0x8(%ebp),%eax
801020c4:	8b 40 0c             	mov    0xc(%eax),%eax
801020c7:	89 c2                	mov    %eax,%edx
801020c9:	83 ca 01             	or     $0x1,%edx
801020cc:	8b 45 08             	mov    0x8(%ebp),%eax
801020cf:	89 50 0c             	mov    %edx,0xc(%eax)
  release(&icache.lock);
801020d2:	c7 04 24 60 2a 11 80 	movl   $0x80112a60,(%esp)
801020d9:	e8 92 34 00 00       	call   80105570 <release>

  if(!(ip->flags & I_VALID)){
801020de:	8b 45 08             	mov    0x8(%ebp),%eax
801020e1:	8b 40 0c             	mov    0xc(%eax),%eax
801020e4:	83 e0 02             	and    $0x2,%eax
801020e7:	85 c0                	test   %eax,%eax
801020e9:	0f 85 d7 00 00 00    	jne    801021c6 <ilock+0x155>
    bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801020ef:	8b 45 08             	mov    0x8(%ebp),%eax
801020f2:	8b 40 04             	mov    0x4(%eax),%eax
801020f5:	89 c2                	mov    %eax,%edx
801020f7:	c1 ea 03             	shr    $0x3,%edx
801020fa:	a1 54 2a 11 80       	mov    0x80112a54,%eax
801020ff:	01 c2                	add    %eax,%edx
80102101:	8b 45 08             	mov    0x8(%ebp),%eax
80102104:	8b 00                	mov    (%eax),%eax
80102106:	89 54 24 04          	mov    %edx,0x4(%esp)
8010210a:	89 04 24             	mov    %eax,(%esp)
8010210d:	e8 95 e0 ff ff       	call   801001a7 <bread>
80102112:	89 45 f0             	mov    %eax,-0x10(%ebp)
    dip = (struct dinode*)bp->data + ip->inum%IPB;
80102115:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102118:	83 c0 18             	add    $0x18,%eax
8010211b:	89 c2                	mov    %eax,%edx
8010211d:	8b 45 08             	mov    0x8(%ebp),%eax
80102120:	8b 40 04             	mov    0x4(%eax),%eax
80102123:	83 e0 07             	and    $0x7,%eax
80102126:	c1 e0 06             	shl    $0x6,%eax
80102129:	8d 04 02             	lea    (%edx,%eax,1),%eax
8010212c:	89 45 f4             	mov    %eax,-0xc(%ebp)
    ip->type = dip->type;
8010212f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102132:	0f b7 10             	movzwl (%eax),%edx
80102135:	8b 45 08             	mov    0x8(%ebp),%eax
80102138:	66 89 50 10          	mov    %dx,0x10(%eax)
    ip->major = dip->major;
8010213c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010213f:	0f b7 50 02          	movzwl 0x2(%eax),%edx
80102143:	8b 45 08             	mov    0x8(%ebp),%eax
80102146:	66 89 50 12          	mov    %dx,0x12(%eax)
    ip->minor = dip->minor;
8010214a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010214d:	0f b7 50 04          	movzwl 0x4(%eax),%edx
80102151:	8b 45 08             	mov    0x8(%ebp),%eax
80102154:	66 89 50 14          	mov    %dx,0x14(%eax)
    ip->nlink = dip->nlink;
80102158:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010215b:	0f b7 50 06          	movzwl 0x6(%eax),%edx
8010215f:	8b 45 08             	mov    0x8(%ebp),%eax
80102162:	66 89 50 16          	mov    %dx,0x16(%eax)
    ip->size = dip->size;
80102166:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102169:	8b 50 08             	mov    0x8(%eax),%edx
8010216c:	8b 45 08             	mov    0x8(%ebp),%eax
8010216f:	89 50 18             	mov    %edx,0x18(%eax)
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
80102172:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102175:	8d 50 0c             	lea    0xc(%eax),%edx
80102178:	8b 45 08             	mov    0x8(%ebp),%eax
8010217b:	83 c0 1c             	add    $0x1c,%eax
8010217e:	c7 44 24 08 34 00 00 	movl   $0x34,0x8(%esp)
80102185:	00 
80102186:	89 54 24 04          	mov    %edx,0x4(%esp)
8010218a:	89 04 24             	mov    %eax,(%esp)
8010218d:	e8 ab 36 00 00       	call   8010583d <memmove>
    brelse(bp);
80102192:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102195:	89 04 24             	mov    %eax,(%esp)
80102198:	e8 7b e0 ff ff       	call   80100218 <brelse>
    ip->flags |= I_VALID;
8010219d:	8b 45 08             	mov    0x8(%ebp),%eax
801021a0:	8b 40 0c             	mov    0xc(%eax),%eax
801021a3:	89 c2                	mov    %eax,%edx
801021a5:	83 ca 02             	or     $0x2,%edx
801021a8:	8b 45 08             	mov    0x8(%ebp),%eax
801021ab:	89 50 0c             	mov    %edx,0xc(%eax)
    if(ip->type == 0)
801021ae:	8b 45 08             	mov    0x8(%ebp),%eax
801021b1:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801021b5:	66 85 c0             	test   %ax,%ax
801021b8:	75 0c                	jne    801021c6 <ilock+0x155>
      panic("ilock: no type");
801021ba:	c7 04 24 bb 8c 10 80 	movl   $0x80108cbb,(%esp)
801021c1:	e8 a3 e6 ff ff       	call   80100869 <panic>
  }
}
801021c6:	c9                   	leave  
801021c7:	c3                   	ret    

801021c8 <iunlock>:

// Unlock the given inode.
void
iunlock(struct inode *ip)
{
801021c8:	55                   	push   %ebp
801021c9:	89 e5                	mov    %esp,%ebp
801021cb:	83 ec 18             	sub    $0x18,%esp
  if(ip == 0 || !(ip->flags & I_BUSY) || ip->ref < 1)
801021ce:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
801021d2:	74 17                	je     801021eb <iunlock+0x23>
801021d4:	8b 45 08             	mov    0x8(%ebp),%eax
801021d7:	8b 40 0c             	mov    0xc(%eax),%eax
801021da:	83 e0 01             	and    $0x1,%eax
801021dd:	85 c0                	test   %eax,%eax
801021df:	74 0a                	je     801021eb <iunlock+0x23>
801021e1:	8b 45 08             	mov    0x8(%ebp),%eax
801021e4:	8b 40 08             	mov    0x8(%eax),%eax
801021e7:	85 c0                	test   %eax,%eax
801021e9:	7f 0c                	jg     801021f7 <iunlock+0x2f>
    panic("iunlock");
801021eb:	c7 04 24 ca 8c 10 80 	movl   $0x80108cca,(%esp)
801021f2:	e8 72 e6 ff ff       	call   80100869 <panic>

  acquire(&icache.lock);
801021f7:	c7 04 24 60 2a 11 80 	movl   $0x80112a60,(%esp)
801021fe:	e8 04 33 00 00       	call   80105507 <acquire>
  ip->flags &= ~I_BUSY;
80102203:	8b 45 08             	mov    0x8(%ebp),%eax
80102206:	8b 40 0c             	mov    0xc(%eax),%eax
80102209:	89 c2                	mov    %eax,%edx
8010220b:	83 e2 fe             	and    $0xfffffffe,%edx
8010220e:	8b 45 08             	mov    0x8(%ebp),%eax
80102211:	89 50 0c             	mov    %edx,0xc(%eax)
  wakeup(ip);
80102214:	8b 45 08             	mov    0x8(%ebp),%eax
80102217:	89 04 24             	mov    %eax,(%esp)
8010221a:	e8 e9 30 00 00       	call   80105308 <wakeup>
  release(&icache.lock);
8010221f:	c7 04 24 60 2a 11 80 	movl   $0x80112a60,(%esp)
80102226:	e8 45 33 00 00       	call   80105570 <release>
}
8010222b:	c9                   	leave  
8010222c:	c3                   	ret    

8010222d <iput>:
// to it, free the inode (and its content) on disk.
// All calls to iput() must be inside a transaction in
// case it has to free the inode.
void
iput(struct inode *ip)
{
8010222d:	55                   	push   %ebp
8010222e:	89 e5                	mov    %esp,%ebp
80102230:	83 ec 18             	sub    $0x18,%esp
  acquire(&icache.lock);
80102233:	c7 04 24 60 2a 11 80 	movl   $0x80112a60,(%esp)
8010223a:	e8 c8 32 00 00       	call   80105507 <acquire>
  if(ip->ref == 1 && (ip->flags & I_VALID) && ip->nlink == 0){
8010223f:	8b 45 08             	mov    0x8(%ebp),%eax
80102242:	8b 40 08             	mov    0x8(%eax),%eax
80102245:	83 f8 01             	cmp    $0x1,%eax
80102248:	0f 85 93 00 00 00    	jne    801022e1 <iput+0xb4>
8010224e:	8b 45 08             	mov    0x8(%ebp),%eax
80102251:	8b 40 0c             	mov    0xc(%eax),%eax
80102254:	83 e0 02             	and    $0x2,%eax
80102257:	85 c0                	test   %eax,%eax
80102259:	0f 84 82 00 00 00    	je     801022e1 <iput+0xb4>
8010225f:	8b 45 08             	mov    0x8(%ebp),%eax
80102262:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80102266:	66 85 c0             	test   %ax,%ax
80102269:	75 76                	jne    801022e1 <iput+0xb4>
    // inode has no links and no other references: truncate and free.
    if(ip->flags & I_BUSY)
8010226b:	8b 45 08             	mov    0x8(%ebp),%eax
8010226e:	8b 40 0c             	mov    0xc(%eax),%eax
80102271:	83 e0 01             	and    $0x1,%eax
80102274:	84 c0                	test   %al,%al
80102276:	74 0c                	je     80102284 <iput+0x57>
      panic("iput busy");
80102278:	c7 04 24 d2 8c 10 80 	movl   $0x80108cd2,(%esp)
8010227f:	e8 e5 e5 ff ff       	call   80100869 <panic>
    ip->flags |= I_BUSY;
80102284:	8b 45 08             	mov    0x8(%ebp),%eax
80102287:	8b 40 0c             	mov    0xc(%eax),%eax
8010228a:	89 c2                	mov    %eax,%edx
8010228c:	83 ca 01             	or     $0x1,%edx
8010228f:	8b 45 08             	mov    0x8(%ebp),%eax
80102292:	89 50 0c             	mov    %edx,0xc(%eax)
    release(&icache.lock);
80102295:	c7 04 24 60 2a 11 80 	movl   $0x80112a60,(%esp)
8010229c:	e8 cf 32 00 00       	call   80105570 <release>
    itrunc(ip);
801022a1:	8b 45 08             	mov    0x8(%ebp),%eax
801022a4:	89 04 24             	mov    %eax,(%esp)
801022a7:	e8 72 01 00 00       	call   8010241e <itrunc>
    ip->type = 0;
801022ac:	8b 45 08             	mov    0x8(%ebp),%eax
801022af:	66 c7 40 10 00 00    	movw   $0x0,0x10(%eax)
    iupdate(ip);
801022b5:	8b 45 08             	mov    0x8(%ebp),%eax
801022b8:	89 04 24             	mov    %eax,(%esp)
801022bb:	e8 eb fb ff ff       	call   80101eab <iupdate>
    acquire(&icache.lock);
801022c0:	c7 04 24 60 2a 11 80 	movl   $0x80112a60,(%esp)
801022c7:	e8 3b 32 00 00       	call   80105507 <acquire>
    ip->flags = 0;
801022cc:	8b 45 08             	mov    0x8(%ebp),%eax
801022cf:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
    wakeup(ip);
801022d6:	8b 45 08             	mov    0x8(%ebp),%eax
801022d9:	89 04 24             	mov    %eax,(%esp)
801022dc:	e8 27 30 00 00       	call   80105308 <wakeup>
  }
  ip->ref--;
801022e1:	8b 45 08             	mov    0x8(%ebp),%eax
801022e4:	8b 40 08             	mov    0x8(%eax),%eax
801022e7:	8d 50 ff             	lea    -0x1(%eax),%edx
801022ea:	8b 45 08             	mov    0x8(%ebp),%eax
801022ed:	89 50 08             	mov    %edx,0x8(%eax)
  release(&icache.lock);
801022f0:	c7 04 24 60 2a 11 80 	movl   $0x80112a60,(%esp)
801022f7:	e8 74 32 00 00       	call   80105570 <release>
}
801022fc:	c9                   	leave  
801022fd:	c3                   	ret    

801022fe <iunlockput>:

// Common idiom: unlock, then put.
void
iunlockput(struct inode *ip)
{
801022fe:	55                   	push   %ebp
801022ff:	89 e5                	mov    %esp,%ebp
80102301:	83 ec 18             	sub    $0x18,%esp
  iunlock(ip);
80102304:	8b 45 08             	mov    0x8(%ebp),%eax
80102307:	89 04 24             	mov    %eax,(%esp)
8010230a:	e8 b9 fe ff ff       	call   801021c8 <iunlock>
  iput(ip);
8010230f:	8b 45 08             	mov    0x8(%ebp),%eax
80102312:	89 04 24             	mov    %eax,(%esp)
80102315:	e8 13 ff ff ff       	call   8010222d <iput>
}
8010231a:	c9                   	leave  
8010231b:	c3                   	ret    

8010231c <bmap>:

// Return the disk block address of the nth block in inode ip.
// If there is no such block, bmap allocates one.
static uint
bmap(struct inode *ip, uint bn)
{
8010231c:	55                   	push   %ebp
8010231d:	89 e5                	mov    %esp,%ebp
8010231f:	53                   	push   %ebx
80102320:	83 ec 24             	sub    $0x24,%esp
  uint addr, *a;
  struct buf *bp;

  if(bn < NDIRECT){
80102323:	83 7d 0c 0b          	cmpl   $0xb,0xc(%ebp)
80102327:	77 3e                	ja     80102367 <bmap+0x4b>
    if((addr = ip->addrs[bn]) == 0)
80102329:	8b 55 0c             	mov    0xc(%ebp),%edx
8010232c:	8b 45 08             	mov    0x8(%ebp),%eax
8010232f:	83 c2 04             	add    $0x4,%edx
80102332:	8b 44 90 0c          	mov    0xc(%eax,%edx,4),%eax
80102336:	89 45 ec             	mov    %eax,-0x14(%ebp)
80102339:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
8010233d:	75 20                	jne    8010235f <bmap+0x43>
      ip->addrs[bn] = addr = balloc(ip->dev);
8010233f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
80102342:	8b 45 08             	mov    0x8(%ebp),%eax
80102345:	8b 00                	mov    (%eax),%eax
80102347:	89 04 24             	mov    %eax,(%esp)
8010234a:	e8 d0 f7 ff ff       	call   80101b1f <balloc>
8010234f:	89 45 ec             	mov    %eax,-0x14(%ebp)
80102352:	8b 45 08             	mov    0x8(%ebp),%eax
80102355:	8d 4b 04             	lea    0x4(%ebx),%ecx
80102358:	8b 55 ec             	mov    -0x14(%ebp),%edx
8010235b:	89 54 88 0c          	mov    %edx,0xc(%eax,%ecx,4)
    return addr;
8010235f:	8b 45 ec             	mov    -0x14(%ebp),%eax
80102362:	e9 b1 00 00 00       	jmp    80102418 <bmap+0xfc>
  }
  bn -= NDIRECT;
80102367:	83 6d 0c 0c          	subl   $0xc,0xc(%ebp)

  if(bn < NINDIRECT){
8010236b:	83 7d 0c 7f          	cmpl   $0x7f,0xc(%ebp)
8010236f:	0f 87 97 00 00 00    	ja     8010240c <bmap+0xf0>
    // Load indirect block, allocating if necessary.
    if((addr = ip->addrs[NDIRECT]) == 0)
80102375:	8b 45 08             	mov    0x8(%ebp),%eax
80102378:	8b 40 4c             	mov    0x4c(%eax),%eax
8010237b:	89 45 ec             	mov    %eax,-0x14(%ebp)
8010237e:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80102382:	75 19                	jne    8010239d <bmap+0x81>
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
80102384:	8b 45 08             	mov    0x8(%ebp),%eax
80102387:	8b 00                	mov    (%eax),%eax
80102389:	89 04 24             	mov    %eax,(%esp)
8010238c:	e8 8e f7 ff ff       	call   80101b1f <balloc>
80102391:	89 45 ec             	mov    %eax,-0x14(%ebp)
80102394:	8b 45 08             	mov    0x8(%ebp),%eax
80102397:	8b 55 ec             	mov    -0x14(%ebp),%edx
8010239a:	89 50 4c             	mov    %edx,0x4c(%eax)
    bp = bread(ip->dev, addr);
8010239d:	8b 45 08             	mov    0x8(%ebp),%eax
801023a0:	8b 00                	mov    (%eax),%eax
801023a2:	8b 55 ec             	mov    -0x14(%ebp),%edx
801023a5:	89 54 24 04          	mov    %edx,0x4(%esp)
801023a9:	89 04 24             	mov    %eax,(%esp)
801023ac:	e8 f6 dd ff ff       	call   801001a7 <bread>
801023b1:	89 45 f4             	mov    %eax,-0xc(%ebp)
    a = (uint*)bp->data;
801023b4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801023b7:	83 c0 18             	add    $0x18,%eax
801023ba:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if((addr = a[bn]) == 0){
801023bd:	8b 45 0c             	mov    0xc(%ebp),%eax
801023c0:	c1 e0 02             	shl    $0x2,%eax
801023c3:	03 45 f0             	add    -0x10(%ebp),%eax
801023c6:	8b 00                	mov    (%eax),%eax
801023c8:	89 45 ec             	mov    %eax,-0x14(%ebp)
801023cb:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
801023cf:	75 2b                	jne    801023fc <bmap+0xe0>
      a[bn] = addr = balloc(ip->dev);
801023d1:	8b 45 0c             	mov    0xc(%ebp),%eax
801023d4:	c1 e0 02             	shl    $0x2,%eax
801023d7:	89 c3                	mov    %eax,%ebx
801023d9:	03 5d f0             	add    -0x10(%ebp),%ebx
801023dc:	8b 45 08             	mov    0x8(%ebp),%eax
801023df:	8b 00                	mov    (%eax),%eax
801023e1:	89 04 24             	mov    %eax,(%esp)
801023e4:	e8 36 f7 ff ff       	call   80101b1f <balloc>
801023e9:	89 45 ec             	mov    %eax,-0x14(%ebp)
801023ec:	8b 45 ec             	mov    -0x14(%ebp),%eax
801023ef:	89 03                	mov    %eax,(%ebx)
      log_write(bp);
801023f1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801023f4:	89 04 24             	mov    %eax,(%esp)
801023f7:	e8 74 18 00 00       	call   80103c70 <log_write>
    }
    brelse(bp);
801023fc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801023ff:	89 04 24             	mov    %eax,(%esp)
80102402:	e8 11 de ff ff       	call   80100218 <brelse>
    return addr;
80102407:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010240a:	eb 0c                	jmp    80102418 <bmap+0xfc>
  }

  panic("bmap: out of range");
8010240c:	c7 04 24 dc 8c 10 80 	movl   $0x80108cdc,(%esp)
80102413:	e8 51 e4 ff ff       	call   80100869 <panic>
}
80102418:	83 c4 24             	add    $0x24,%esp
8010241b:	5b                   	pop    %ebx
8010241c:	5d                   	pop    %ebp
8010241d:	c3                   	ret    

8010241e <itrunc>:
// to it (no directory entries referring to it)
// and has no in-memory reference to it (is
// not an open file or current directory).
static void
itrunc(struct inode *ip)
{
8010241e:	55                   	push   %ebp
8010241f:	89 e5                	mov    %esp,%ebp
80102421:	83 ec 28             	sub    $0x28,%esp
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
80102424:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
8010242b:	eb 44                	jmp    80102471 <itrunc+0x53>
    if(ip->addrs[i]){
8010242d:	8b 55 e8             	mov    -0x18(%ebp),%edx
80102430:	8b 45 08             	mov    0x8(%ebp),%eax
80102433:	83 c2 04             	add    $0x4,%edx
80102436:	8b 44 90 0c          	mov    0xc(%eax,%edx,4),%eax
8010243a:	85 c0                	test   %eax,%eax
8010243c:	74 2f                	je     8010246d <itrunc+0x4f>
      bfree(ip->dev, ip->addrs[i]);
8010243e:	8b 55 e8             	mov    -0x18(%ebp),%edx
80102441:	8b 45 08             	mov    0x8(%ebp),%eax
80102444:	83 c2 04             	add    $0x4,%edx
80102447:	8b 54 90 0c          	mov    0xc(%eax,%edx,4),%edx
8010244b:	8b 45 08             	mov    0x8(%ebp),%eax
8010244e:	8b 00                	mov    (%eax),%eax
80102450:	89 54 24 04          	mov    %edx,0x4(%esp)
80102454:	89 04 24             	mov    %eax,(%esp)
80102457:	e8 0f f8 ff ff       	call   80101c6b <bfree>
      ip->addrs[i] = 0;
8010245c:	8b 55 e8             	mov    -0x18(%ebp),%edx
8010245f:	8b 45 08             	mov    0x8(%ebp),%eax
80102462:	83 c2 04             	add    $0x4,%edx
80102465:	c7 44 90 0c 00 00 00 	movl   $0x0,0xc(%eax,%edx,4)
8010246c:	00 
{
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
8010246d:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
80102471:	83 7d e8 0b          	cmpl   $0xb,-0x18(%ebp)
80102475:	7e b6                	jle    8010242d <itrunc+0xf>
      bfree(ip->dev, ip->addrs[i]);
      ip->addrs[i] = 0;
    }
  }

  if(ip->addrs[NDIRECT]){
80102477:	8b 45 08             	mov    0x8(%ebp),%eax
8010247a:	8b 40 4c             	mov    0x4c(%eax),%eax
8010247d:	85 c0                	test   %eax,%eax
8010247f:	0f 84 8f 00 00 00    	je     80102514 <itrunc+0xf6>
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
80102485:	8b 45 08             	mov    0x8(%ebp),%eax
80102488:	8b 50 4c             	mov    0x4c(%eax),%edx
8010248b:	8b 45 08             	mov    0x8(%ebp),%eax
8010248e:	8b 00                	mov    (%eax),%eax
80102490:	89 54 24 04          	mov    %edx,0x4(%esp)
80102494:	89 04 24             	mov    %eax,(%esp)
80102497:	e8 0b dd ff ff       	call   801001a7 <bread>
8010249c:	89 45 f0             	mov    %eax,-0x10(%ebp)
    a = (uint*)bp->data;
8010249f:	8b 45 f0             	mov    -0x10(%ebp),%eax
801024a2:	83 c0 18             	add    $0x18,%eax
801024a5:	89 45 f4             	mov    %eax,-0xc(%ebp)
    for(j = 0; j < NINDIRECT; j++){
801024a8:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
801024af:	eb 2f                	jmp    801024e0 <itrunc+0xc2>
      if(a[j])
801024b1:	8b 45 ec             	mov    -0x14(%ebp),%eax
801024b4:	c1 e0 02             	shl    $0x2,%eax
801024b7:	03 45 f4             	add    -0xc(%ebp),%eax
801024ba:	8b 00                	mov    (%eax),%eax
801024bc:	85 c0                	test   %eax,%eax
801024be:	74 1c                	je     801024dc <itrunc+0xbe>
        bfree(ip->dev, a[j]);
801024c0:	8b 45 ec             	mov    -0x14(%ebp),%eax
801024c3:	c1 e0 02             	shl    $0x2,%eax
801024c6:	03 45 f4             	add    -0xc(%ebp),%eax
801024c9:	8b 10                	mov    (%eax),%edx
801024cb:	8b 45 08             	mov    0x8(%ebp),%eax
801024ce:	8b 00                	mov    (%eax),%eax
801024d0:	89 54 24 04          	mov    %edx,0x4(%esp)
801024d4:	89 04 24             	mov    %eax,(%esp)
801024d7:	e8 8f f7 ff ff       	call   80101c6b <bfree>
  }

  if(ip->addrs[NDIRECT]){
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
    a = (uint*)bp->data;
    for(j = 0; j < NINDIRECT; j++){
801024dc:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
801024e0:	8b 45 ec             	mov    -0x14(%ebp),%eax
801024e3:	83 f8 7f             	cmp    $0x7f,%eax
801024e6:	76 c9                	jbe    801024b1 <itrunc+0x93>
      if(a[j])
        bfree(ip->dev, a[j]);
    }
    brelse(bp);
801024e8:	8b 45 f0             	mov    -0x10(%ebp),%eax
801024eb:	89 04 24             	mov    %eax,(%esp)
801024ee:	e8 25 dd ff ff       	call   80100218 <brelse>
    bfree(ip->dev, ip->addrs[NDIRECT]);
801024f3:	8b 45 08             	mov    0x8(%ebp),%eax
801024f6:	8b 50 4c             	mov    0x4c(%eax),%edx
801024f9:	8b 45 08             	mov    0x8(%ebp),%eax
801024fc:	8b 00                	mov    (%eax),%eax
801024fe:	89 54 24 04          	mov    %edx,0x4(%esp)
80102502:	89 04 24             	mov    %eax,(%esp)
80102505:	e8 61 f7 ff ff       	call   80101c6b <bfree>
    ip->addrs[NDIRECT] = 0;
8010250a:	8b 45 08             	mov    0x8(%ebp),%eax
8010250d:	c7 40 4c 00 00 00 00 	movl   $0x0,0x4c(%eax)
  }

  ip->size = 0;
80102514:	8b 45 08             	mov    0x8(%ebp),%eax
80102517:	c7 40 18 00 00 00 00 	movl   $0x0,0x18(%eax)
  iupdate(ip);
8010251e:	8b 45 08             	mov    0x8(%ebp),%eax
80102521:	89 04 24             	mov    %eax,(%esp)
80102524:	e8 82 f9 ff ff       	call   80101eab <iupdate>
}
80102529:	c9                   	leave  
8010252a:	c3                   	ret    

8010252b <stati>:

// Copy stat information from inode.
void
stati(struct inode *ip, struct stat *st)
{
8010252b:	55                   	push   %ebp
8010252c:	89 e5                	mov    %esp,%ebp
  st->dev = ip->dev;
8010252e:	8b 45 08             	mov    0x8(%ebp),%eax
80102531:	8b 00                	mov    (%eax),%eax
80102533:	89 c2                	mov    %eax,%edx
80102535:	8b 45 0c             	mov    0xc(%ebp),%eax
80102538:	89 50 04             	mov    %edx,0x4(%eax)
  st->ino = ip->inum;
8010253b:	8b 45 08             	mov    0x8(%ebp),%eax
8010253e:	8b 50 04             	mov    0x4(%eax),%edx
80102541:	8b 45 0c             	mov    0xc(%ebp),%eax
80102544:	89 50 08             	mov    %edx,0x8(%eax)
  st->type = ip->type;
80102547:	8b 45 08             	mov    0x8(%ebp),%eax
8010254a:	0f b7 50 10          	movzwl 0x10(%eax),%edx
8010254e:	8b 45 0c             	mov    0xc(%ebp),%eax
80102551:	66 89 10             	mov    %dx,(%eax)
  st->nlink = ip->nlink;
80102554:	8b 45 08             	mov    0x8(%ebp),%eax
80102557:	0f b7 50 16          	movzwl 0x16(%eax),%edx
8010255b:	8b 45 0c             	mov    0xc(%ebp),%eax
8010255e:	66 89 50 0c          	mov    %dx,0xc(%eax)
  st->size = ip->size;
80102562:	8b 45 08             	mov    0x8(%ebp),%eax
80102565:	8b 50 18             	mov    0x18(%eax),%edx
80102568:	8b 45 0c             	mov    0xc(%ebp),%eax
8010256b:	89 50 10             	mov    %edx,0x10(%eax)
}
8010256e:	5d                   	pop    %ebp
8010256f:	c3                   	ret    

80102570 <readi>:

//PAGEBREAK!
// Read data from inode.
int
readi(struct inode *ip, char *dst, uint off, uint n)
{
80102570:	55                   	push   %ebp
80102571:	89 e5                	mov    %esp,%ebp
80102573:	53                   	push   %ebx
80102574:	83 ec 24             	sub    $0x24,%esp
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
80102577:	8b 45 08             	mov    0x8(%ebp),%eax
8010257a:	0f b7 40 10          	movzwl 0x10(%eax),%eax
8010257e:	66 83 f8 03          	cmp    $0x3,%ax
80102582:	75 60                	jne    801025e4 <readi+0x74>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
80102584:	8b 45 08             	mov    0x8(%ebp),%eax
80102587:	0f b7 40 12          	movzwl 0x12(%eax),%eax
8010258b:	66 85 c0             	test   %ax,%ax
8010258e:	78 20                	js     801025b0 <readi+0x40>
80102590:	8b 45 08             	mov    0x8(%ebp),%eax
80102593:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80102597:	66 83 f8 09          	cmp    $0x9,%ax
8010259b:	7f 13                	jg     801025b0 <readi+0x40>
8010259d:	8b 45 08             	mov    0x8(%ebp),%eax
801025a0:	0f b7 40 12          	movzwl 0x12(%eax),%eax
801025a4:	98                   	cwtl   
801025a5:	8b 04 c5 e0 29 11 80 	mov    -0x7feed620(,%eax,8),%eax
801025ac:	85 c0                	test   %eax,%eax
801025ae:	75 0a                	jne    801025ba <readi+0x4a>
      return -1;
801025b0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801025b5:	e9 1c 01 00 00       	jmp    801026d6 <readi+0x166>
    return devsw[ip->major].read(ip, dst, n);
801025ba:	8b 45 08             	mov    0x8(%ebp),%eax
801025bd:	0f b7 40 12          	movzwl 0x12(%eax),%eax
801025c1:	98                   	cwtl   
801025c2:	8b 14 c5 e0 29 11 80 	mov    -0x7feed620(,%eax,8),%edx
801025c9:	8b 45 14             	mov    0x14(%ebp),%eax
801025cc:	89 44 24 08          	mov    %eax,0x8(%esp)
801025d0:	8b 45 0c             	mov    0xc(%ebp),%eax
801025d3:	89 44 24 04          	mov    %eax,0x4(%esp)
801025d7:	8b 45 08             	mov    0x8(%ebp),%eax
801025da:	89 04 24             	mov    %eax,(%esp)
801025dd:	ff d2                	call   *%edx
801025df:	e9 f2 00 00 00       	jmp    801026d6 <readi+0x166>
  }

  if(off > ip->size || off + n < off)
801025e4:	8b 45 08             	mov    0x8(%ebp),%eax
801025e7:	8b 40 18             	mov    0x18(%eax),%eax
801025ea:	3b 45 10             	cmp    0x10(%ebp),%eax
801025ed:	72 0e                	jb     801025fd <readi+0x8d>
801025ef:	8b 45 14             	mov    0x14(%ebp),%eax
801025f2:	8b 55 10             	mov    0x10(%ebp),%edx
801025f5:	8d 04 02             	lea    (%edx,%eax,1),%eax
801025f8:	3b 45 10             	cmp    0x10(%ebp),%eax
801025fb:	73 0a                	jae    80102607 <readi+0x97>
    return -1;
801025fd:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102602:	e9 cf 00 00 00       	jmp    801026d6 <readi+0x166>
  if(off + n > ip->size)
80102607:	8b 45 14             	mov    0x14(%ebp),%eax
8010260a:	8b 55 10             	mov    0x10(%ebp),%edx
8010260d:	01 c2                	add    %eax,%edx
8010260f:	8b 45 08             	mov    0x8(%ebp),%eax
80102612:	8b 40 18             	mov    0x18(%eax),%eax
80102615:	39 c2                	cmp    %eax,%edx
80102617:	76 0c                	jbe    80102625 <readi+0xb5>
    n = ip->size - off;
80102619:	8b 45 08             	mov    0x8(%ebp),%eax
8010261c:	8b 40 18             	mov    0x18(%eax),%eax
8010261f:	2b 45 10             	sub    0x10(%ebp),%eax
80102622:	89 45 14             	mov    %eax,0x14(%ebp)

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80102625:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
8010262c:	e9 96 00 00 00       	jmp    801026c7 <readi+0x157>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80102631:	8b 45 10             	mov    0x10(%ebp),%eax
80102634:	c1 e8 09             	shr    $0x9,%eax
80102637:	89 44 24 04          	mov    %eax,0x4(%esp)
8010263b:	8b 45 08             	mov    0x8(%ebp),%eax
8010263e:	89 04 24             	mov    %eax,(%esp)
80102641:	e8 d6 fc ff ff       	call   8010231c <bmap>
80102646:	8b 55 08             	mov    0x8(%ebp),%edx
80102649:	8b 12                	mov    (%edx),%edx
8010264b:	89 44 24 04          	mov    %eax,0x4(%esp)
8010264f:	89 14 24             	mov    %edx,(%esp)
80102652:	e8 50 db ff ff       	call   801001a7 <bread>
80102657:	89 45 f4             	mov    %eax,-0xc(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
8010265a:	8b 45 10             	mov    0x10(%ebp),%eax
8010265d:	89 c2                	mov    %eax,%edx
8010265f:	81 e2 ff 01 00 00    	and    $0x1ff,%edx
80102665:	b8 00 02 00 00       	mov    $0x200,%eax
8010266a:	89 c1                	mov    %eax,%ecx
8010266c:	29 d1                	sub    %edx,%ecx
8010266e:	89 ca                	mov    %ecx,%edx
80102670:	8b 45 ec             	mov    -0x14(%ebp),%eax
80102673:	8b 4d 14             	mov    0x14(%ebp),%ecx
80102676:	89 cb                	mov    %ecx,%ebx
80102678:	29 c3                	sub    %eax,%ebx
8010267a:	89 d8                	mov    %ebx,%eax
8010267c:	39 c2                	cmp    %eax,%edx
8010267e:	0f 46 c2             	cmovbe %edx,%eax
80102681:	89 45 f0             	mov    %eax,-0x10(%ebp)
    memmove(dst, bp->data + off%BSIZE, m);
80102684:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102687:	8d 50 18             	lea    0x18(%eax),%edx
8010268a:	8b 45 10             	mov    0x10(%ebp),%eax
8010268d:	25 ff 01 00 00       	and    $0x1ff,%eax
80102692:	01 c2                	add    %eax,%edx
80102694:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102697:	89 44 24 08          	mov    %eax,0x8(%esp)
8010269b:	89 54 24 04          	mov    %edx,0x4(%esp)
8010269f:	8b 45 0c             	mov    0xc(%ebp),%eax
801026a2:	89 04 24             	mov    %eax,(%esp)
801026a5:	e8 93 31 00 00       	call   8010583d <memmove>
    brelse(bp);
801026aa:	8b 45 f4             	mov    -0xc(%ebp),%eax
801026ad:	89 04 24             	mov    %eax,(%esp)
801026b0:	e8 63 db ff ff       	call   80100218 <brelse>
  if(off > ip->size || off + n < off)
    return -1;
  if(off + n > ip->size)
    n = ip->size - off;

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
801026b5:	8b 45 f0             	mov    -0x10(%ebp),%eax
801026b8:	01 45 ec             	add    %eax,-0x14(%ebp)
801026bb:	8b 45 f0             	mov    -0x10(%ebp),%eax
801026be:	01 45 10             	add    %eax,0x10(%ebp)
801026c1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801026c4:	01 45 0c             	add    %eax,0xc(%ebp)
801026c7:	8b 45 ec             	mov    -0x14(%ebp),%eax
801026ca:	3b 45 14             	cmp    0x14(%ebp),%eax
801026cd:	0f 82 5e ff ff ff    	jb     80102631 <readi+0xc1>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
    m = min(n - tot, BSIZE - off%BSIZE);
    memmove(dst, bp->data + off%BSIZE, m);
    brelse(bp);
  }
  return n;
801026d3:	8b 45 14             	mov    0x14(%ebp),%eax
}
801026d6:	83 c4 24             	add    $0x24,%esp
801026d9:	5b                   	pop    %ebx
801026da:	5d                   	pop    %ebp
801026db:	c3                   	ret    

801026dc <writei>:

// PAGEBREAK!
// Write data to inode.
int
writei(struct inode *ip, char *src, uint off, uint n)
{
801026dc:	55                   	push   %ebp
801026dd:	89 e5                	mov    %esp,%ebp
801026df:	53                   	push   %ebx
801026e0:	83 ec 24             	sub    $0x24,%esp
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
801026e3:	8b 45 08             	mov    0x8(%ebp),%eax
801026e6:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801026ea:	66 83 f8 03          	cmp    $0x3,%ax
801026ee:	75 60                	jne    80102750 <writei+0x74>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
801026f0:	8b 45 08             	mov    0x8(%ebp),%eax
801026f3:	0f b7 40 12          	movzwl 0x12(%eax),%eax
801026f7:	66 85 c0             	test   %ax,%ax
801026fa:	78 20                	js     8010271c <writei+0x40>
801026fc:	8b 45 08             	mov    0x8(%ebp),%eax
801026ff:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80102703:	66 83 f8 09          	cmp    $0x9,%ax
80102707:	7f 13                	jg     8010271c <writei+0x40>
80102709:	8b 45 08             	mov    0x8(%ebp),%eax
8010270c:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80102710:	98                   	cwtl   
80102711:	8b 04 c5 e4 29 11 80 	mov    -0x7feed61c(,%eax,8),%eax
80102718:	85 c0                	test   %eax,%eax
8010271a:	75 0a                	jne    80102726 <writei+0x4a>
      return -1;
8010271c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102721:	e9 48 01 00 00       	jmp    8010286e <writei+0x192>
    return devsw[ip->major].write(ip, src, n);
80102726:	8b 45 08             	mov    0x8(%ebp),%eax
80102729:	0f b7 40 12          	movzwl 0x12(%eax),%eax
8010272d:	98                   	cwtl   
8010272e:	8b 14 c5 e4 29 11 80 	mov    -0x7feed61c(,%eax,8),%edx
80102735:	8b 45 14             	mov    0x14(%ebp),%eax
80102738:	89 44 24 08          	mov    %eax,0x8(%esp)
8010273c:	8b 45 0c             	mov    0xc(%ebp),%eax
8010273f:	89 44 24 04          	mov    %eax,0x4(%esp)
80102743:	8b 45 08             	mov    0x8(%ebp),%eax
80102746:	89 04 24             	mov    %eax,(%esp)
80102749:	ff d2                	call   *%edx
8010274b:	e9 1e 01 00 00       	jmp    8010286e <writei+0x192>
  }

  if(off > ip->size || off + n < off)
80102750:	8b 45 08             	mov    0x8(%ebp),%eax
80102753:	8b 40 18             	mov    0x18(%eax),%eax
80102756:	3b 45 10             	cmp    0x10(%ebp),%eax
80102759:	72 0e                	jb     80102769 <writei+0x8d>
8010275b:	8b 45 14             	mov    0x14(%ebp),%eax
8010275e:	8b 55 10             	mov    0x10(%ebp),%edx
80102761:	8d 04 02             	lea    (%edx,%eax,1),%eax
80102764:	3b 45 10             	cmp    0x10(%ebp),%eax
80102767:	73 0a                	jae    80102773 <writei+0x97>
    return -1;
80102769:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010276e:	e9 fb 00 00 00       	jmp    8010286e <writei+0x192>
  if(off + n > MAXFILE*BSIZE)
80102773:	8b 45 14             	mov    0x14(%ebp),%eax
80102776:	8b 55 10             	mov    0x10(%ebp),%edx
80102779:	8d 04 02             	lea    (%edx,%eax,1),%eax
8010277c:	3d 00 18 01 00       	cmp    $0x11800,%eax
80102781:	76 0a                	jbe    8010278d <writei+0xb1>
    return -1;
80102783:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102788:	e9 e1 00 00 00       	jmp    8010286e <writei+0x192>

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
8010278d:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80102794:	e9 a1 00 00 00       	jmp    8010283a <writei+0x15e>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80102799:	8b 45 10             	mov    0x10(%ebp),%eax
8010279c:	c1 e8 09             	shr    $0x9,%eax
8010279f:	89 44 24 04          	mov    %eax,0x4(%esp)
801027a3:	8b 45 08             	mov    0x8(%ebp),%eax
801027a6:	89 04 24             	mov    %eax,(%esp)
801027a9:	e8 6e fb ff ff       	call   8010231c <bmap>
801027ae:	8b 55 08             	mov    0x8(%ebp),%edx
801027b1:	8b 12                	mov    (%edx),%edx
801027b3:	89 44 24 04          	mov    %eax,0x4(%esp)
801027b7:	89 14 24             	mov    %edx,(%esp)
801027ba:	e8 e8 d9 ff ff       	call   801001a7 <bread>
801027bf:	89 45 f4             	mov    %eax,-0xc(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
801027c2:	8b 45 10             	mov    0x10(%ebp),%eax
801027c5:	89 c2                	mov    %eax,%edx
801027c7:	81 e2 ff 01 00 00    	and    $0x1ff,%edx
801027cd:	b8 00 02 00 00       	mov    $0x200,%eax
801027d2:	89 c1                	mov    %eax,%ecx
801027d4:	29 d1                	sub    %edx,%ecx
801027d6:	89 ca                	mov    %ecx,%edx
801027d8:	8b 45 ec             	mov    -0x14(%ebp),%eax
801027db:	8b 4d 14             	mov    0x14(%ebp),%ecx
801027de:	89 cb                	mov    %ecx,%ebx
801027e0:	29 c3                	sub    %eax,%ebx
801027e2:	89 d8                	mov    %ebx,%eax
801027e4:	39 c2                	cmp    %eax,%edx
801027e6:	0f 46 c2             	cmovbe %edx,%eax
801027e9:	89 45 f0             	mov    %eax,-0x10(%ebp)
    memmove(bp->data + off%BSIZE, src, m);
801027ec:	8b 45 f4             	mov    -0xc(%ebp),%eax
801027ef:	8d 50 18             	lea    0x18(%eax),%edx
801027f2:	8b 45 10             	mov    0x10(%ebp),%eax
801027f5:	25 ff 01 00 00       	and    $0x1ff,%eax
801027fa:	01 c2                	add    %eax,%edx
801027fc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801027ff:	89 44 24 08          	mov    %eax,0x8(%esp)
80102803:	8b 45 0c             	mov    0xc(%ebp),%eax
80102806:	89 44 24 04          	mov    %eax,0x4(%esp)
8010280a:	89 14 24             	mov    %edx,(%esp)
8010280d:	e8 2b 30 00 00       	call   8010583d <memmove>
    log_write(bp);
80102812:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102815:	89 04 24             	mov    %eax,(%esp)
80102818:	e8 53 14 00 00       	call   80103c70 <log_write>
    brelse(bp);
8010281d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102820:	89 04 24             	mov    %eax,(%esp)
80102823:	e8 f0 d9 ff ff       	call   80100218 <brelse>
  if(off > ip->size || off + n < off)
    return -1;
  if(off + n > MAXFILE*BSIZE)
    return -1;

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80102828:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010282b:	01 45 ec             	add    %eax,-0x14(%ebp)
8010282e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102831:	01 45 10             	add    %eax,0x10(%ebp)
80102834:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102837:	01 45 0c             	add    %eax,0xc(%ebp)
8010283a:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010283d:	3b 45 14             	cmp    0x14(%ebp),%eax
80102840:	0f 82 53 ff ff ff    	jb     80102799 <writei+0xbd>
    memmove(bp->data + off%BSIZE, src, m);
    log_write(bp);
    brelse(bp);
  }

  if(n > 0 && off > ip->size){
80102846:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
8010284a:	74 1f                	je     8010286b <writei+0x18f>
8010284c:	8b 45 08             	mov    0x8(%ebp),%eax
8010284f:	8b 40 18             	mov    0x18(%eax),%eax
80102852:	3b 45 10             	cmp    0x10(%ebp),%eax
80102855:	73 14                	jae    8010286b <writei+0x18f>
    ip->size = off;
80102857:	8b 45 08             	mov    0x8(%ebp),%eax
8010285a:	8b 55 10             	mov    0x10(%ebp),%edx
8010285d:	89 50 18             	mov    %edx,0x18(%eax)
    iupdate(ip);
80102860:	8b 45 08             	mov    0x8(%ebp),%eax
80102863:	89 04 24             	mov    %eax,(%esp)
80102866:	e8 40 f6 ff ff       	call   80101eab <iupdate>
  }
  return n;
8010286b:	8b 45 14             	mov    0x14(%ebp),%eax
}
8010286e:	83 c4 24             	add    $0x24,%esp
80102871:	5b                   	pop    %ebx
80102872:	5d                   	pop    %ebp
80102873:	c3                   	ret    

80102874 <namecmp>:
//PAGEBREAK!
// Directories

int
namecmp(const char *s, const char *t)
{
80102874:	55                   	push   %ebp
80102875:	89 e5                	mov    %esp,%ebp
80102877:	83 ec 18             	sub    $0x18,%esp
  return strncmp(s, t, DIRSIZ);
8010287a:	c7 44 24 08 0e 00 00 	movl   $0xe,0x8(%esp)
80102881:	00 
80102882:	8b 45 0c             	mov    0xc(%ebp),%eax
80102885:	89 44 24 04          	mov    %eax,0x4(%esp)
80102889:	8b 45 08             	mov    0x8(%ebp),%eax
8010288c:	89 04 24             	mov    %eax,(%esp)
8010288f:	e8 51 30 00 00       	call   801058e5 <strncmp>
}
80102894:	c9                   	leave  
80102895:	c3                   	ret    

80102896 <dirlookup>:

// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
struct inode*
dirlookup(struct inode *dp, char *name, uint *poff)
{
80102896:	55                   	push   %ebp
80102897:	89 e5                	mov    %esp,%ebp
80102899:	83 ec 38             	sub    $0x38,%esp
  uint off, inum;
  struct dirent de;

  if(dp->type != T_DIR)
8010289c:	8b 45 08             	mov    0x8(%ebp),%eax
8010289f:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801028a3:	66 83 f8 01          	cmp    $0x1,%ax
801028a7:	74 0c                	je     801028b5 <dirlookup+0x1f>
    panic("dirlookup not DIR");
801028a9:	c7 04 24 ef 8c 10 80 	movl   $0x80108cef,(%esp)
801028b0:	e8 b4 df ff ff       	call   80100869 <panic>

  for(off = 0; off < dp->size; off += sizeof(de)){
801028b5:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
801028bc:	e9 87 00 00 00       	jmp    80102948 <dirlookup+0xb2>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801028c1:	8d 45 e0             	lea    -0x20(%ebp),%eax
801028c4:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
801028cb:	00 
801028cc:	8b 55 f0             	mov    -0x10(%ebp),%edx
801028cf:	89 54 24 08          	mov    %edx,0x8(%esp)
801028d3:	89 44 24 04          	mov    %eax,0x4(%esp)
801028d7:	8b 45 08             	mov    0x8(%ebp),%eax
801028da:	89 04 24             	mov    %eax,(%esp)
801028dd:	e8 8e fc ff ff       	call   80102570 <readi>
801028e2:	83 f8 10             	cmp    $0x10,%eax
801028e5:	74 0c                	je     801028f3 <dirlookup+0x5d>
      panic("dirlink read");
801028e7:	c7 04 24 01 8d 10 80 	movl   $0x80108d01,(%esp)
801028ee:	e8 76 df ff ff       	call   80100869 <panic>
    if(de.inum == 0)
801028f3:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
801028f7:	66 85 c0             	test   %ax,%ax
801028fa:	74 47                	je     80102943 <dirlookup+0xad>
      continue;
    if(namecmp(name, de.name) == 0){
801028fc:	8d 45 e0             	lea    -0x20(%ebp),%eax
801028ff:	83 c0 02             	add    $0x2,%eax
80102902:	89 44 24 04          	mov    %eax,0x4(%esp)
80102906:	8b 45 0c             	mov    0xc(%ebp),%eax
80102909:	89 04 24             	mov    %eax,(%esp)
8010290c:	e8 63 ff ff ff       	call   80102874 <namecmp>
80102911:	85 c0                	test   %eax,%eax
80102913:	75 2f                	jne    80102944 <dirlookup+0xae>
      // entry matches path element
      if(poff)
80102915:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80102919:	74 08                	je     80102923 <dirlookup+0x8d>
        *poff = off;
8010291b:	8b 45 10             	mov    0x10(%ebp),%eax
8010291e:	8b 55 f0             	mov    -0x10(%ebp),%edx
80102921:	89 10                	mov    %edx,(%eax)
      inum = de.inum;
80102923:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
80102927:	0f b7 c0             	movzwl %ax,%eax
8010292a:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return iget(dp->dev, inum);
8010292d:	8b 45 08             	mov    0x8(%ebp),%eax
80102930:	8b 00                	mov    (%eax),%eax
80102932:	8b 55 f4             	mov    -0xc(%ebp),%edx
80102935:	89 54 24 04          	mov    %edx,0x4(%esp)
80102939:	89 04 24             	mov    %eax,(%esp)
8010293c:	e8 2b f6 ff ff       	call   80101f6c <iget>
80102941:	eb 19                	jmp    8010295c <dirlookup+0xc6>

  for(off = 0; off < dp->size; off += sizeof(de)){
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("dirlink read");
    if(de.inum == 0)
      continue;
80102943:	90                   	nop
  struct dirent de;

  if(dp->type != T_DIR)
    panic("dirlookup not DIR");

  for(off = 0; off < dp->size; off += sizeof(de)){
80102944:	83 45 f0 10          	addl   $0x10,-0x10(%ebp)
80102948:	8b 45 08             	mov    0x8(%ebp),%eax
8010294b:	8b 40 18             	mov    0x18(%eax),%eax
8010294e:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80102951:	0f 87 6a ff ff ff    	ja     801028c1 <dirlookup+0x2b>
      inum = de.inum;
      return iget(dp->dev, inum);
    }
  }

  return 0;
80102957:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010295c:	c9                   	leave  
8010295d:	c3                   	ret    

8010295e <dirlink>:

// Write a new directory entry (name, inum) into the directory dp.
int
dirlink(struct inode *dp, char *name, uint inum)
{
8010295e:	55                   	push   %ebp
8010295f:	89 e5                	mov    %esp,%ebp
80102961:	83 ec 38             	sub    $0x38,%esp
  int off;
  struct dirent de;
  struct inode *ip;

  // Check that name is not present.
  if((ip = dirlookup(dp, name, 0)) != 0){
80102964:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
8010296b:	00 
8010296c:	8b 45 0c             	mov    0xc(%ebp),%eax
8010296f:	89 44 24 04          	mov    %eax,0x4(%esp)
80102973:	8b 45 08             	mov    0x8(%ebp),%eax
80102976:	89 04 24             	mov    %eax,(%esp)
80102979:	e8 18 ff ff ff       	call   80102896 <dirlookup>
8010297e:	89 45 f4             	mov    %eax,-0xc(%ebp)
80102981:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80102985:	74 15                	je     8010299c <dirlink+0x3e>
    iput(ip);
80102987:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010298a:	89 04 24             	mov    %eax,(%esp)
8010298d:	e8 9b f8 ff ff       	call   8010222d <iput>
    return -1;
80102992:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102997:	e9 b8 00 00 00       	jmp    80102a54 <dirlink+0xf6>
  }

  // Look for an empty dirent.
  for(off = 0; off < dp->size; off += sizeof(de)){
8010299c:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
801029a3:	eb 44                	jmp    801029e9 <dirlink+0x8b>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801029a5:	8b 55 f0             	mov    -0x10(%ebp),%edx
801029a8:	8d 45 e0             	lea    -0x20(%ebp),%eax
801029ab:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
801029b2:	00 
801029b3:	89 54 24 08          	mov    %edx,0x8(%esp)
801029b7:	89 44 24 04          	mov    %eax,0x4(%esp)
801029bb:	8b 45 08             	mov    0x8(%ebp),%eax
801029be:	89 04 24             	mov    %eax,(%esp)
801029c1:	e8 aa fb ff ff       	call   80102570 <readi>
801029c6:	83 f8 10             	cmp    $0x10,%eax
801029c9:	74 0c                	je     801029d7 <dirlink+0x79>
      panic("dirlink read");
801029cb:	c7 04 24 01 8d 10 80 	movl   $0x80108d01,(%esp)
801029d2:	e8 92 de ff ff       	call   80100869 <panic>
    if(de.inum == 0)
801029d7:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
801029db:	66 85 c0             	test   %ax,%ax
801029de:	74 18                	je     801029f8 <dirlink+0x9a>
    iput(ip);
    return -1;
  }

  // Look for an empty dirent.
  for(off = 0; off < dp->size; off += sizeof(de)){
801029e0:	8b 45 f0             	mov    -0x10(%ebp),%eax
801029e3:	83 c0 10             	add    $0x10,%eax
801029e6:	89 45 f0             	mov    %eax,-0x10(%ebp)
801029e9:	8b 55 f0             	mov    -0x10(%ebp),%edx
801029ec:	8b 45 08             	mov    0x8(%ebp),%eax
801029ef:	8b 40 18             	mov    0x18(%eax),%eax
801029f2:	39 c2                	cmp    %eax,%edx
801029f4:	72 af                	jb     801029a5 <dirlink+0x47>
801029f6:	eb 01                	jmp    801029f9 <dirlink+0x9b>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("dirlink read");
    if(de.inum == 0)
      break;
801029f8:	90                   	nop
  }

  strncpy(de.name, name, DIRSIZ);
801029f9:	c7 44 24 08 0e 00 00 	movl   $0xe,0x8(%esp)
80102a00:	00 
80102a01:	8b 45 0c             	mov    0xc(%ebp),%eax
80102a04:	89 44 24 04          	mov    %eax,0x4(%esp)
80102a08:	8d 45 e0             	lea    -0x20(%ebp),%eax
80102a0b:	83 c0 02             	add    $0x2,%eax
80102a0e:	89 04 24             	mov    %eax,(%esp)
80102a11:	e8 27 2f 00 00       	call   8010593d <strncpy>
  de.inum = inum;
80102a16:	8b 45 10             	mov    0x10(%ebp),%eax
80102a19:	66 89 45 e0          	mov    %ax,-0x20(%ebp)
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80102a1d:	8b 55 f0             	mov    -0x10(%ebp),%edx
80102a20:	8d 45 e0             	lea    -0x20(%ebp),%eax
80102a23:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
80102a2a:	00 
80102a2b:	89 54 24 08          	mov    %edx,0x8(%esp)
80102a2f:	89 44 24 04          	mov    %eax,0x4(%esp)
80102a33:	8b 45 08             	mov    0x8(%ebp),%eax
80102a36:	89 04 24             	mov    %eax,(%esp)
80102a39:	e8 9e fc ff ff       	call   801026dc <writei>
80102a3e:	83 f8 10             	cmp    $0x10,%eax
80102a41:	74 0c                	je     80102a4f <dirlink+0xf1>
    panic("dirlink");
80102a43:	c7 04 24 0e 8d 10 80 	movl   $0x80108d0e,(%esp)
80102a4a:	e8 1a de ff ff       	call   80100869 <panic>

  return 0;
80102a4f:	b8 00 00 00 00       	mov    $0x0,%eax
}
80102a54:	c9                   	leave  
80102a55:	c3                   	ret    

80102a56 <skipelem>:
//   skipelem("a", name) = "", setting name = "a"
//   skipelem("", name) = skipelem("////", name) = 0
//
static char*
skipelem(char *path, char *name)
{
80102a56:	55                   	push   %ebp
80102a57:	89 e5                	mov    %esp,%ebp
80102a59:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int len;

  while(*path == '/')
80102a5c:	eb 04                	jmp    80102a62 <skipelem+0xc>
    path++;
80102a5e:	83 45 08 01          	addl   $0x1,0x8(%ebp)
skipelem(char *path, char *name)
{
  char *s;
  int len;

  while(*path == '/')
80102a62:	8b 45 08             	mov    0x8(%ebp),%eax
80102a65:	0f b6 00             	movzbl (%eax),%eax
80102a68:	3c 2f                	cmp    $0x2f,%al
80102a6a:	74 f2                	je     80102a5e <skipelem+0x8>
    path++;
  if(*path == 0)
80102a6c:	8b 45 08             	mov    0x8(%ebp),%eax
80102a6f:	0f b6 00             	movzbl (%eax),%eax
80102a72:	84 c0                	test   %al,%al
80102a74:	75 0a                	jne    80102a80 <skipelem+0x2a>
    return 0;
80102a76:	b8 00 00 00 00       	mov    $0x0,%eax
80102a7b:	e9 86 00 00 00       	jmp    80102b06 <skipelem+0xb0>
  s = path;
80102a80:	8b 45 08             	mov    0x8(%ebp),%eax
80102a83:	89 45 f0             	mov    %eax,-0x10(%ebp)
  while(*path != '/' && *path != 0)
80102a86:	eb 04                	jmp    80102a8c <skipelem+0x36>
    path++;
80102a88:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  while(*path == '/')
    path++;
  if(*path == 0)
    return 0;
  s = path;
  while(*path != '/' && *path != 0)
80102a8c:	8b 45 08             	mov    0x8(%ebp),%eax
80102a8f:	0f b6 00             	movzbl (%eax),%eax
80102a92:	3c 2f                	cmp    $0x2f,%al
80102a94:	74 0a                	je     80102aa0 <skipelem+0x4a>
80102a96:	8b 45 08             	mov    0x8(%ebp),%eax
80102a99:	0f b6 00             	movzbl (%eax),%eax
80102a9c:	84 c0                	test   %al,%al
80102a9e:	75 e8                	jne    80102a88 <skipelem+0x32>
    path++;
  len = path - s;
80102aa0:	8b 55 08             	mov    0x8(%ebp),%edx
80102aa3:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102aa6:	89 d1                	mov    %edx,%ecx
80102aa8:	29 c1                	sub    %eax,%ecx
80102aaa:	89 c8                	mov    %ecx,%eax
80102aac:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(len >= DIRSIZ)
80102aaf:	83 7d f4 0d          	cmpl   $0xd,-0xc(%ebp)
80102ab3:	7e 1c                	jle    80102ad1 <skipelem+0x7b>
    memmove(name, s, DIRSIZ);
80102ab5:	c7 44 24 08 0e 00 00 	movl   $0xe,0x8(%esp)
80102abc:	00 
80102abd:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102ac0:	89 44 24 04          	mov    %eax,0x4(%esp)
80102ac4:	8b 45 0c             	mov    0xc(%ebp),%eax
80102ac7:	89 04 24             	mov    %eax,(%esp)
80102aca:	e8 6e 2d 00 00       	call   8010583d <memmove>
  else {
    memmove(name, s, len);
    name[len] = 0;
  }
  while(*path == '/')
80102acf:	eb 28                	jmp    80102af9 <skipelem+0xa3>
    path++;
  len = path - s;
  if(len >= DIRSIZ)
    memmove(name, s, DIRSIZ);
  else {
    memmove(name, s, len);
80102ad1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102ad4:	89 44 24 08          	mov    %eax,0x8(%esp)
80102ad8:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102adb:	89 44 24 04          	mov    %eax,0x4(%esp)
80102adf:	8b 45 0c             	mov    0xc(%ebp),%eax
80102ae2:	89 04 24             	mov    %eax,(%esp)
80102ae5:	e8 53 2d 00 00       	call   8010583d <memmove>
    name[len] = 0;
80102aea:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102aed:	03 45 0c             	add    0xc(%ebp),%eax
80102af0:	c6 00 00             	movb   $0x0,(%eax)
  }
  while(*path == '/')
80102af3:	eb 04                	jmp    80102af9 <skipelem+0xa3>
    path++;
80102af5:	83 45 08 01          	addl   $0x1,0x8(%ebp)
    memmove(name, s, DIRSIZ);
  else {
    memmove(name, s, len);
    name[len] = 0;
  }
  while(*path == '/')
80102af9:	8b 45 08             	mov    0x8(%ebp),%eax
80102afc:	0f b6 00             	movzbl (%eax),%eax
80102aff:	3c 2f                	cmp    $0x2f,%al
80102b01:	74 f2                	je     80102af5 <skipelem+0x9f>
    path++;
  return path;
80102b03:	8b 45 08             	mov    0x8(%ebp),%eax
}
80102b06:	c9                   	leave  
80102b07:	c3                   	ret    

80102b08 <namex>:
// If parent != 0, return the inode for the parent and copy the final
// path element into name, which must have room for DIRSIZ bytes.
// Must be called inside a transaction since it calls iput().
static struct inode*
namex(char *path, int nameiparent, char *name)
{
80102b08:	55                   	push   %ebp
80102b09:	89 e5                	mov    %esp,%ebp
80102b0b:	83 ec 28             	sub    $0x28,%esp
  struct inode *ip, *next;

  if(*path == '/')
80102b0e:	8b 45 08             	mov    0x8(%ebp),%eax
80102b11:	0f b6 00             	movzbl (%eax),%eax
80102b14:	3c 2f                	cmp    $0x2f,%al
80102b16:	75 1c                	jne    80102b34 <namex+0x2c>
    ip = iget(ROOTDEV, ROOTINO);
80102b18:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80102b1f:	00 
80102b20:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80102b27:	e8 40 f4 ff ff       	call   80101f6c <iget>
80102b2c:	89 45 f0             	mov    %eax,-0x10(%ebp)
  else
    ip = idup(proc->cwd);

  while((path = skipelem(path, name)) != 0){
80102b2f:	e9 af 00 00 00       	jmp    80102be3 <namex+0xdb>
  struct inode *ip, *next;

  if(*path == '/')
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(proc->cwd);
80102b34:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80102b3a:	8b 40 68             	mov    0x68(%eax),%eax
80102b3d:	89 04 24             	mov    %eax,(%esp)
80102b40:	e8 fa f4 ff ff       	call   8010203f <idup>
80102b45:	89 45 f0             	mov    %eax,-0x10(%ebp)

  while((path = skipelem(path, name)) != 0){
80102b48:	e9 96 00 00 00       	jmp    80102be3 <namex+0xdb>
    ilock(ip);
80102b4d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102b50:	89 04 24             	mov    %eax,(%esp)
80102b53:	e8 19 f5 ff ff       	call   80102071 <ilock>
    if(ip->type != T_DIR){
80102b58:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102b5b:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80102b5f:	66 83 f8 01          	cmp    $0x1,%ax
80102b63:	74 15                	je     80102b7a <namex+0x72>
      iunlockput(ip);
80102b65:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102b68:	89 04 24             	mov    %eax,(%esp)
80102b6b:	e8 8e f7 ff ff       	call   801022fe <iunlockput>
      return 0;
80102b70:	b8 00 00 00 00       	mov    $0x0,%eax
80102b75:	e9 a3 00 00 00       	jmp    80102c1d <namex+0x115>
    }
    if(nameiparent && *path == '\0'){
80102b7a:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80102b7e:	74 1d                	je     80102b9d <namex+0x95>
80102b80:	8b 45 08             	mov    0x8(%ebp),%eax
80102b83:	0f b6 00             	movzbl (%eax),%eax
80102b86:	84 c0                	test   %al,%al
80102b88:	75 13                	jne    80102b9d <namex+0x95>
      // Stop one level early.
      iunlock(ip);
80102b8a:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102b8d:	89 04 24             	mov    %eax,(%esp)
80102b90:	e8 33 f6 ff ff       	call   801021c8 <iunlock>
      return ip;
80102b95:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102b98:	e9 80 00 00 00       	jmp    80102c1d <namex+0x115>
    }
    if((next = dirlookup(ip, name, 0)) == 0){
80102b9d:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80102ba4:	00 
80102ba5:	8b 45 10             	mov    0x10(%ebp),%eax
80102ba8:	89 44 24 04          	mov    %eax,0x4(%esp)
80102bac:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102baf:	89 04 24             	mov    %eax,(%esp)
80102bb2:	e8 df fc ff ff       	call   80102896 <dirlookup>
80102bb7:	89 45 f4             	mov    %eax,-0xc(%ebp)
80102bba:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80102bbe:	75 12                	jne    80102bd2 <namex+0xca>
      iunlockput(ip);
80102bc0:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102bc3:	89 04 24             	mov    %eax,(%esp)
80102bc6:	e8 33 f7 ff ff       	call   801022fe <iunlockput>
      return 0;
80102bcb:	b8 00 00 00 00       	mov    $0x0,%eax
80102bd0:	eb 4b                	jmp    80102c1d <namex+0x115>
    }
    iunlockput(ip);
80102bd2:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102bd5:	89 04 24             	mov    %eax,(%esp)
80102bd8:	e8 21 f7 ff ff       	call   801022fe <iunlockput>
    ip = next;
80102bdd:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102be0:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(*path == '/')
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(proc->cwd);

  while((path = skipelem(path, name)) != 0){
80102be3:	8b 45 10             	mov    0x10(%ebp),%eax
80102be6:	89 44 24 04          	mov    %eax,0x4(%esp)
80102bea:	8b 45 08             	mov    0x8(%ebp),%eax
80102bed:	89 04 24             	mov    %eax,(%esp)
80102bf0:	e8 61 fe ff ff       	call   80102a56 <skipelem>
80102bf5:	89 45 08             	mov    %eax,0x8(%ebp)
80102bf8:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80102bfc:	0f 85 4b ff ff ff    	jne    80102b4d <namex+0x45>
      return 0;
    }
    iunlockput(ip);
    ip = next;
  }
  if(nameiparent){
80102c02:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80102c06:	74 12                	je     80102c1a <namex+0x112>
    iput(ip);
80102c08:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102c0b:	89 04 24             	mov    %eax,(%esp)
80102c0e:	e8 1a f6 ff ff       	call   8010222d <iput>
    return 0;
80102c13:	b8 00 00 00 00       	mov    $0x0,%eax
80102c18:	eb 03                	jmp    80102c1d <namex+0x115>
  }
  return ip;
80102c1a:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
80102c1d:	c9                   	leave  
80102c1e:	c3                   	ret    

80102c1f <namei>:

struct inode*
namei(char *path)
{
80102c1f:	55                   	push   %ebp
80102c20:	89 e5                	mov    %esp,%ebp
80102c22:	83 ec 28             	sub    $0x28,%esp
  char name[DIRSIZ];
  return namex(path, 0, name);
80102c25:	8d 45 ea             	lea    -0x16(%ebp),%eax
80102c28:	89 44 24 08          	mov    %eax,0x8(%esp)
80102c2c:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80102c33:	00 
80102c34:	8b 45 08             	mov    0x8(%ebp),%eax
80102c37:	89 04 24             	mov    %eax,(%esp)
80102c3a:	e8 c9 fe ff ff       	call   80102b08 <namex>
}
80102c3f:	c9                   	leave  
80102c40:	c3                   	ret    

80102c41 <nameiparent>:

struct inode*
nameiparent(char *path, char *name)
{
80102c41:	55                   	push   %ebp
80102c42:	89 e5                	mov    %esp,%ebp
80102c44:	83 ec 18             	sub    $0x18,%esp
  return namex(path, 1, name);
80102c47:	8b 45 0c             	mov    0xc(%ebp),%eax
80102c4a:	89 44 24 08          	mov    %eax,0x8(%esp)
80102c4e:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80102c55:	00 
80102c56:	8b 45 08             	mov    0x8(%ebp),%eax
80102c59:	89 04 24             	mov    %eax,(%esp)
80102c5c:	e8 a7 fe ff ff       	call   80102b08 <namex>
}
80102c61:	c9                   	leave  
80102c62:	c3                   	ret    
	...

80102c64 <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
80102c64:	55                   	push   %ebp
80102c65:	89 e5                	mov    %esp,%ebp
80102c67:	83 ec 14             	sub    $0x14,%esp
80102c6a:	8b 45 08             	mov    0x8(%ebp),%eax
80102c6d:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102c71:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
80102c75:	89 c2                	mov    %eax,%edx
80102c77:	ec                   	in     (%dx),%al
80102c78:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80102c7b:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80102c7f:	c9                   	leave  
80102c80:	c3                   	ret    

80102c81 <insl>:

static inline void
insl(int port, void *addr, int cnt)
{
80102c81:	55                   	push   %ebp
80102c82:	89 e5                	mov    %esp,%ebp
80102c84:	57                   	push   %edi
80102c85:	53                   	push   %ebx
  asm volatile("cld; rep insl" :
80102c86:	8b 55 08             	mov    0x8(%ebp),%edx
80102c89:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80102c8c:	8b 45 10             	mov    0x10(%ebp),%eax
80102c8f:	89 cb                	mov    %ecx,%ebx
80102c91:	89 df                	mov    %ebx,%edi
80102c93:	89 c1                	mov    %eax,%ecx
80102c95:	fc                   	cld    
80102c96:	f3 6d                	rep insl (%dx),%es:(%edi)
80102c98:	89 c8                	mov    %ecx,%eax
80102c9a:	89 fb                	mov    %edi,%ebx
80102c9c:	89 5d 0c             	mov    %ebx,0xc(%ebp)
80102c9f:	89 45 10             	mov    %eax,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "d" (port), "0" (addr), "1" (cnt) :
               "memory", "cc");
}
80102ca2:	5b                   	pop    %ebx
80102ca3:	5f                   	pop    %edi
80102ca4:	5d                   	pop    %ebp
80102ca5:	c3                   	ret    

80102ca6 <outb>:

static inline void
outb(ushort port, uchar data)
{
80102ca6:	55                   	push   %ebp
80102ca7:	89 e5                	mov    %esp,%ebp
80102ca9:	83 ec 08             	sub    $0x8,%esp
80102cac:	8b 55 08             	mov    0x8(%ebp),%edx
80102caf:	8b 45 0c             	mov    0xc(%ebp),%eax
80102cb2:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80102cb6:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102cb9:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80102cbd:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80102cc1:	ee                   	out    %al,(%dx)
}
80102cc2:	c9                   	leave  
80102cc3:	c3                   	ret    

80102cc4 <outsl>:
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
}

static inline void
outsl(int port, const void *addr, int cnt)
{
80102cc4:	55                   	push   %ebp
80102cc5:	89 e5                	mov    %esp,%ebp
80102cc7:	56                   	push   %esi
80102cc8:	53                   	push   %ebx
  asm volatile("cld; rep outsl" :
80102cc9:	8b 55 08             	mov    0x8(%ebp),%edx
80102ccc:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80102ccf:	8b 45 10             	mov    0x10(%ebp),%eax
80102cd2:	89 cb                	mov    %ecx,%ebx
80102cd4:	89 de                	mov    %ebx,%esi
80102cd6:	89 c1                	mov    %eax,%ecx
80102cd8:	fc                   	cld    
80102cd9:	f3 6f                	rep outsl %ds:(%esi),(%dx)
80102cdb:	89 c8                	mov    %ecx,%eax
80102cdd:	89 f3                	mov    %esi,%ebx
80102cdf:	89 5d 0c             	mov    %ebx,0xc(%ebp)
80102ce2:	89 45 10             	mov    %eax,0x10(%ebp)
               "=S" (addr), "=c" (cnt) :
               "d" (port), "0" (addr), "1" (cnt) :
               "cc");
}
80102ce5:	5b                   	pop    %ebx
80102ce6:	5e                   	pop    %esi
80102ce7:	5d                   	pop    %ebp
80102ce8:	c3                   	ret    

80102ce9 <diskready>:
static int havedisk1;
static void idestart(struct buf*);

static int
diskready(int *status)
{
80102ce9:	55                   	push   %ebp
80102cea:	89 e5                	mov    %esp,%ebp
80102cec:	83 ec 04             	sub    $0x4,%esp
  *status = inb(IDE_DATA_PRIMARY+IDE_REG_STATUS);
80102cef:	c7 04 24 f7 01 00 00 	movl   $0x1f7,(%esp)
80102cf6:	e8 69 ff ff ff       	call   80102c64 <inb>
80102cfb:	0f b6 d0             	movzbl %al,%edx
80102cfe:	8b 45 08             	mov    0x8(%ebp),%eax
80102d01:	89 10                	mov    %edx,(%eax)
  return (*status & (IDE_BSY|IDE_DRDY)) == IDE_DRDY;
80102d03:	8b 45 08             	mov    0x8(%ebp),%eax
80102d06:	8b 00                	mov    (%eax),%eax
80102d08:	25 c0 00 00 00       	and    $0xc0,%eax
80102d0d:	83 f8 40             	cmp    $0x40,%eax
80102d10:	0f 94 c0             	sete   %al
80102d13:	0f b6 c0             	movzbl %al,%eax
}
80102d16:	c9                   	leave  
80102d17:	c3                   	ret    

80102d18 <idewait>:

// Wait for IDE disk to become ready.
static int
idewait(int checkerr)
{
80102d18:	55                   	push   %ebp
80102d19:	89 e5                	mov    %esp,%ebp
80102d1b:	83 ec 14             	sub    $0x14,%esp
  int r;

  while(!diskready(&r))
80102d1e:	8d 45 fc             	lea    -0x4(%ebp),%eax
80102d21:	89 04 24             	mov    %eax,(%esp)
80102d24:	e8 c0 ff ff ff       	call   80102ce9 <diskready>
80102d29:	85 c0                	test   %eax,%eax
80102d2b:	74 f1                	je     80102d1e <idewait+0x6>
    ;
  if(checkerr && (r & (IDE_DF|IDE_ERR)) != 0)
80102d2d:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80102d31:	74 11                	je     80102d44 <idewait+0x2c>
80102d33:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102d36:	83 e0 21             	and    $0x21,%eax
80102d39:	85 c0                	test   %eax,%eax
80102d3b:	74 07                	je     80102d44 <idewait+0x2c>
    return -1;
80102d3d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102d42:	eb 05                	jmp    80102d49 <idewait+0x31>
  return 0;
80102d44:	b8 00 00 00 00       	mov    $0x0,%eax
}
80102d49:	c9                   	leave  
80102d4a:	c3                   	ret    

80102d4b <ideinit>:

void
ideinit(void)
{
80102d4b:	55                   	push   %ebp
80102d4c:	89 e5                	mov    %esp,%ebp
80102d4e:	83 ec 28             	sub    $0x28,%esp
  int i;

  initlock(&idelock, "ide");
80102d51:	c7 44 24 04 16 8d 10 	movl   $0x80108d16,0x4(%esp)
80102d58:	80 
80102d59:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80102d60:	e8 81 27 00 00       	call   801054e6 <initlock>
  picenable(IRQ_IDE);
80102d65:	c7 04 24 0e 00 00 00 	movl   $0xe,(%esp)
80102d6c:	e8 2c 17 00 00       	call   8010449d <picenable>
  ioapicenable(IRQ_IDE, ncpu - 1);
80102d71:	a1 60 41 11 80       	mov    0x80114160,%eax
80102d76:	83 e8 01             	sub    $0x1,%eax
80102d79:	89 44 24 04          	mov    %eax,0x4(%esp)
80102d7d:	c7 04 24 0e 00 00 00 	movl   $0xe,(%esp)
80102d84:	e8 71 04 00 00       	call   801031fa <ioapicenable>
  idewait(0);
80102d89:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80102d90:	e8 83 ff ff ff       	call   80102d18 <idewait>

  // Check if disk 1 is present
  outb(IDE_DATA_PRIMARY+IDE_REG_DISK, IDE_DISK_LBA | (1<<4));
80102d95:	c7 44 24 04 f0 00 00 	movl   $0xf0,0x4(%esp)
80102d9c:	00 
80102d9d:	c7 04 24 f6 01 00 00 	movl   $0x1f6,(%esp)
80102da4:	e8 fd fe ff ff       	call   80102ca6 <outb>
  for(i=0; i<1000; i++){
80102da9:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80102db0:	eb 20                	jmp    80102dd2 <ideinit+0x87>
    if(inb(IDE_DATA_PRIMARY+IDE_REG_STATUS) != 0){
80102db2:	c7 04 24 f7 01 00 00 	movl   $0x1f7,(%esp)
80102db9:	e8 a6 fe ff ff       	call   80102c64 <inb>
80102dbe:	84 c0                	test   %al,%al
80102dc0:	74 0c                	je     80102dce <ideinit+0x83>
      havedisk1 = 1;
80102dc2:	c7 05 38 c6 10 80 01 	movl   $0x1,0x8010c638
80102dc9:	00 00 00 
      break;
80102dcc:	eb 0d                	jmp    80102ddb <ideinit+0x90>
  ioapicenable(IRQ_IDE, ncpu - 1);
  idewait(0);

  // Check if disk 1 is present
  outb(IDE_DATA_PRIMARY+IDE_REG_DISK, IDE_DISK_LBA | (1<<4));
  for(i=0; i<1000; i++){
80102dce:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80102dd2:	81 7d f4 e7 03 00 00 	cmpl   $0x3e7,-0xc(%ebp)
80102dd9:	7e d7                	jle    80102db2 <ideinit+0x67>
      break;
    }
  }

  // Switch back to disk 0.
  outb(IDE_DATA_PRIMARY+IDE_REG_DISK, IDE_DISK_LBA | (0<<4));
80102ddb:	c7 44 24 04 e0 00 00 	movl   $0xe0,0x4(%esp)
80102de2:	00 
80102de3:	c7 04 24 f6 01 00 00 	movl   $0x1f6,(%esp)
80102dea:	e8 b7 fe ff ff       	call   80102ca6 <outb>
}
80102def:	c9                   	leave  
80102df0:	c3                   	ret    

80102df1 <idestart>:

// Start the request for b.  Caller must hold idelock.
static void
idestart(struct buf *b)
{
80102df1:	55                   	push   %ebp
80102df2:	89 e5                	mov    %esp,%ebp
80102df4:	83 ec 28             	sub    $0x28,%esp
  if(b == 0)
80102df7:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80102dfb:	75 0c                	jne    80102e09 <idestart+0x18>
    panic("idestart");
80102dfd:	c7 04 24 1a 8d 10 80 	movl   $0x80108d1a,(%esp)
80102e04:	e8 60 da ff ff       	call   80100869 <panic>
  if(b->blockno >= FSSIZE)
80102e09:	8b 45 08             	mov    0x8(%ebp),%eax
80102e0c:	8b 40 08             	mov    0x8(%eax),%eax
80102e0f:	3d e7 03 00 00       	cmp    $0x3e7,%eax
80102e14:	76 0c                	jbe    80102e22 <idestart+0x31>
    panic("incorrect blockno");
80102e16:	c7 04 24 23 8d 10 80 	movl   $0x80108d23,(%esp)
80102e1d:	e8 47 da ff ff       	call   80100869 <panic>
  int sector_per_block =  BSIZE/SECTOR_SIZE;
80102e22:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
  int sector = b->blockno * sector_per_block;
80102e29:	8b 45 08             	mov    0x8(%ebp),%eax
80102e2c:	8b 50 08             	mov    0x8(%eax),%edx
80102e2f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102e32:	0f af c2             	imul   %edx,%eax
80102e35:	89 45 f4             	mov    %eax,-0xc(%ebp)

  if (sector_per_block > 7) panic("idestart");
80102e38:	83 7d f0 07          	cmpl   $0x7,-0x10(%ebp)
80102e3c:	7e 0c                	jle    80102e4a <idestart+0x59>
80102e3e:	c7 04 24 1a 8d 10 80 	movl   $0x80108d1a,(%esp)
80102e45:	e8 1f da ff ff       	call   80100869 <panic>

  idewait(0);
80102e4a:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80102e51:	e8 c2 fe ff ff       	call   80102d18 <idewait>
  outb(IDE_CTRL_PRIMARY+IDE_REG_CTRL, 0);  // generate interrupt
80102e56:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80102e5d:	00 
80102e5e:	c7 04 24 f6 03 00 00 	movl   $0x3f6,(%esp)
80102e65:	e8 3c fe ff ff       	call   80102ca6 <outb>
  outb(IDE_DATA_PRIMARY+IDE_REG_SECTORS, sector_per_block);
80102e6a:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102e6d:	0f b6 c0             	movzbl %al,%eax
80102e70:	89 44 24 04          	mov    %eax,0x4(%esp)
80102e74:	c7 04 24 f2 01 00 00 	movl   $0x1f2,(%esp)
80102e7b:	e8 26 fe ff ff       	call   80102ca6 <outb>
  outb(IDE_DATA_PRIMARY+IDE_REG_LBA0, sector & 0xff);
80102e80:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102e83:	0f b6 c0             	movzbl %al,%eax
80102e86:	89 44 24 04          	mov    %eax,0x4(%esp)
80102e8a:	c7 04 24 f3 01 00 00 	movl   $0x1f3,(%esp)
80102e91:	e8 10 fe ff ff       	call   80102ca6 <outb>
  outb(IDE_DATA_PRIMARY+IDE_REG_LBA1, (sector >> 8) & 0xff);
80102e96:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102e99:	c1 f8 08             	sar    $0x8,%eax
80102e9c:	0f b6 c0             	movzbl %al,%eax
80102e9f:	89 44 24 04          	mov    %eax,0x4(%esp)
80102ea3:	c7 04 24 f4 01 00 00 	movl   $0x1f4,(%esp)
80102eaa:	e8 f7 fd ff ff       	call   80102ca6 <outb>
  outb(IDE_DATA_PRIMARY+IDE_REG_LBA2, (sector >> 16) & 0xff);
80102eaf:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102eb2:	c1 f8 10             	sar    $0x10,%eax
80102eb5:	0f b6 c0             	movzbl %al,%eax
80102eb8:	89 44 24 04          	mov    %eax,0x4(%esp)
80102ebc:	c7 04 24 f5 01 00 00 	movl   $0x1f5,(%esp)
80102ec3:	e8 de fd ff ff       	call   80102ca6 <outb>
  outb(IDE_DATA_PRIMARY+IDE_REG_DISK,
80102ec8:	8b 45 08             	mov    0x8(%ebp),%eax
80102ecb:	8b 40 04             	mov    0x4(%eax),%eax
80102ece:	83 e0 01             	and    $0x1,%eax
80102ed1:	89 c2                	mov    %eax,%edx
80102ed3:	c1 e2 04             	shl    $0x4,%edx
    IDE_DISK_LBA | ((b->dev&1)<<4) | ((sector>>24)&0x0f));
80102ed6:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102ed9:	c1 f8 18             	sar    $0x18,%eax
  outb(IDE_CTRL_PRIMARY+IDE_REG_CTRL, 0);  // generate interrupt
  outb(IDE_DATA_PRIMARY+IDE_REG_SECTORS, sector_per_block);
  outb(IDE_DATA_PRIMARY+IDE_REG_LBA0, sector & 0xff);
  outb(IDE_DATA_PRIMARY+IDE_REG_LBA1, (sector >> 8) & 0xff);
  outb(IDE_DATA_PRIMARY+IDE_REG_LBA2, (sector >> 16) & 0xff);
  outb(IDE_DATA_PRIMARY+IDE_REG_DISK,
80102edc:	83 e0 0f             	and    $0xf,%eax
80102edf:	09 d0                	or     %edx,%eax
80102ee1:	83 c8 e0             	or     $0xffffffe0,%eax
80102ee4:	0f b6 c0             	movzbl %al,%eax
80102ee7:	89 44 24 04          	mov    %eax,0x4(%esp)
80102eeb:	c7 04 24 f6 01 00 00 	movl   $0x1f6,(%esp)
80102ef2:	e8 af fd ff ff       	call   80102ca6 <outb>
    IDE_DISK_LBA | ((b->dev&1)<<4) | ((sector>>24)&0x0f));
  if(b->flags & B_DIRTY){
80102ef7:	8b 45 08             	mov    0x8(%ebp),%eax
80102efa:	8b 00                	mov    (%eax),%eax
80102efc:	83 e0 04             	and    $0x4,%eax
80102eff:	85 c0                	test   %eax,%eax
80102f01:	74 34                	je     80102f37 <idestart+0x146>
    outb(IDE_DATA_PRIMARY+IDE_REG_COMMAND, IDE_CMD_WRITE);
80102f03:	c7 44 24 04 30 00 00 	movl   $0x30,0x4(%esp)
80102f0a:	00 
80102f0b:	c7 04 24 f7 01 00 00 	movl   $0x1f7,(%esp)
80102f12:	e8 8f fd ff ff       	call   80102ca6 <outb>
    outsl(IDE_DATA_PRIMARY+IDE_REG_DATA, b->data, BSIZE/4);
80102f17:	8b 45 08             	mov    0x8(%ebp),%eax
80102f1a:	83 c0 18             	add    $0x18,%eax
80102f1d:	c7 44 24 08 80 00 00 	movl   $0x80,0x8(%esp)
80102f24:	00 
80102f25:	89 44 24 04          	mov    %eax,0x4(%esp)
80102f29:	c7 04 24 f0 01 00 00 	movl   $0x1f0,(%esp)
80102f30:	e8 8f fd ff ff       	call   80102cc4 <outsl>
80102f35:	eb 14                	jmp    80102f4b <idestart+0x15a>
  } else {
    outb(IDE_DATA_PRIMARY+IDE_REG_COMMAND, IDE_CMD_READ);
80102f37:	c7 44 24 04 20 00 00 	movl   $0x20,0x4(%esp)
80102f3e:	00 
80102f3f:	c7 04 24 f7 01 00 00 	movl   $0x1f7,(%esp)
80102f46:	e8 5b fd ff ff       	call   80102ca6 <outb>
  }
}
80102f4b:	c9                   	leave  
80102f4c:	c3                   	ret    

80102f4d <ideintr>:

// Interrupt handler.
void
ideintr(void)
{
80102f4d:	55                   	push   %ebp
80102f4e:	89 e5                	mov    %esp,%ebp
80102f50:	83 ec 28             	sub    $0x28,%esp
  struct buf *b;

  // First queued buffer is the active request.
  acquire(&idelock);
80102f53:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80102f5a:	e8 a8 25 00 00       	call   80105507 <acquire>
  if((b = idequeue) == 0){
80102f5f:	a1 34 c6 10 80       	mov    0x8010c634,%eax
80102f64:	89 45 f4             	mov    %eax,-0xc(%ebp)
80102f67:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80102f6b:	75 11                	jne    80102f7e <ideintr+0x31>
    release(&idelock);
80102f6d:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80102f74:	e8 f7 25 00 00       	call   80105570 <release>
    // cprintf("spurious IDE interrupt\n");
    return;
80102f79:	e9 90 00 00 00       	jmp    8010300e <ideintr+0xc1>
  }
  idequeue = b->qnext;
80102f7e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102f81:	8b 40 14             	mov    0x14(%eax),%eax
80102f84:	a3 34 c6 10 80       	mov    %eax,0x8010c634

  // Read data if needed.
  if(!(b->flags & B_DIRTY) && idewait(1) >= 0)
80102f89:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102f8c:	8b 00                	mov    (%eax),%eax
80102f8e:	83 e0 04             	and    $0x4,%eax
80102f91:	85 c0                	test   %eax,%eax
80102f93:	75 2e                	jne    80102fc3 <ideintr+0x76>
80102f95:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80102f9c:	e8 77 fd ff ff       	call   80102d18 <idewait>
80102fa1:	85 c0                	test   %eax,%eax
80102fa3:	78 1e                	js     80102fc3 <ideintr+0x76>
    insl(IDE_DATA_PRIMARY+IDE_REG_DATA, b->data, BSIZE/4);
80102fa5:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102fa8:	83 c0 18             	add    $0x18,%eax
80102fab:	c7 44 24 08 80 00 00 	movl   $0x80,0x8(%esp)
80102fb2:	00 
80102fb3:	89 44 24 04          	mov    %eax,0x4(%esp)
80102fb7:	c7 04 24 f0 01 00 00 	movl   $0x1f0,(%esp)
80102fbe:	e8 be fc ff ff       	call   80102c81 <insl>

  // Wake process waiting for this buf.
  b->flags |= B_VALID;
80102fc3:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102fc6:	8b 00                	mov    (%eax),%eax
80102fc8:	89 c2                	mov    %eax,%edx
80102fca:	83 ca 02             	or     $0x2,%edx
80102fcd:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102fd0:	89 10                	mov    %edx,(%eax)
  b->flags &= ~B_DIRTY;
80102fd2:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102fd5:	8b 00                	mov    (%eax),%eax
80102fd7:	89 c2                	mov    %eax,%edx
80102fd9:	83 e2 fb             	and    $0xfffffffb,%edx
80102fdc:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102fdf:	89 10                	mov    %edx,(%eax)
  wakeup(b);
80102fe1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102fe4:	89 04 24             	mov    %eax,(%esp)
80102fe7:	e8 1c 23 00 00       	call   80105308 <wakeup>

  // Start disk on next buf in queue.
  if(idequeue != 0)
80102fec:	a1 34 c6 10 80       	mov    0x8010c634,%eax
80102ff1:	85 c0                	test   %eax,%eax
80102ff3:	74 0d                	je     80103002 <ideintr+0xb5>
    idestart(idequeue);
80102ff5:	a1 34 c6 10 80       	mov    0x8010c634,%eax
80102ffa:	89 04 24             	mov    %eax,(%esp)
80102ffd:	e8 ef fd ff ff       	call   80102df1 <idestart>

  release(&idelock);
80103002:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80103009:	e8 62 25 00 00       	call   80105570 <release>
}
8010300e:	c9                   	leave  
8010300f:	c3                   	ret    

80103010 <iderw>:
// Sync buf with disk.
// If B_DIRTY is set, write buf to disk, clear B_DIRTY, set B_VALID.
// Else if B_VALID is not set, read buf from disk, set B_VALID.
void
iderw(struct buf *b)
{
80103010:	55                   	push   %ebp
80103011:	89 e5                	mov    %esp,%ebp
80103013:	83 ec 28             	sub    $0x28,%esp
  struct buf **pp;

  if(!(b->flags & B_BUSY))
80103016:	8b 45 08             	mov    0x8(%ebp),%eax
80103019:	8b 00                	mov    (%eax),%eax
8010301b:	83 e0 01             	and    $0x1,%eax
8010301e:	85 c0                	test   %eax,%eax
80103020:	75 0c                	jne    8010302e <iderw+0x1e>
    panic("iderw: buf not busy");
80103022:	c7 04 24 35 8d 10 80 	movl   $0x80108d35,(%esp)
80103029:	e8 3b d8 ff ff       	call   80100869 <panic>
  if((b->flags & (B_VALID|B_DIRTY)) == B_VALID)
8010302e:	8b 45 08             	mov    0x8(%ebp),%eax
80103031:	8b 00                	mov    (%eax),%eax
80103033:	83 e0 06             	and    $0x6,%eax
80103036:	83 f8 02             	cmp    $0x2,%eax
80103039:	75 0c                	jne    80103047 <iderw+0x37>
    panic("iderw: nothing to do");
8010303b:	c7 04 24 49 8d 10 80 	movl   $0x80108d49,(%esp)
80103042:	e8 22 d8 ff ff       	call   80100869 <panic>
  if(b->dev != 0 && !havedisk1)
80103047:	8b 45 08             	mov    0x8(%ebp),%eax
8010304a:	8b 40 04             	mov    0x4(%eax),%eax
8010304d:	85 c0                	test   %eax,%eax
8010304f:	74 15                	je     80103066 <iderw+0x56>
80103051:	a1 38 c6 10 80       	mov    0x8010c638,%eax
80103056:	85 c0                	test   %eax,%eax
80103058:	75 0c                	jne    80103066 <iderw+0x56>
    panic("iderw: ide disk 1 not present");
8010305a:	c7 04 24 5e 8d 10 80 	movl   $0x80108d5e,(%esp)
80103061:	e8 03 d8 ff ff       	call   80100869 <panic>

  acquire(&idelock);  //DOC:acquire-lock
80103066:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
8010306d:	e8 95 24 00 00       	call   80105507 <acquire>

  // Append b to idequeue.
  b->qnext = 0;
80103072:	8b 45 08             	mov    0x8(%ebp),%eax
80103075:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
8010307c:	c7 45 f4 34 c6 10 80 	movl   $0x8010c634,-0xc(%ebp)
80103083:	eb 0b                	jmp    80103090 <iderw+0x80>
80103085:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103088:	8b 00                	mov    (%eax),%eax
8010308a:	83 c0 14             	add    $0x14,%eax
8010308d:	89 45 f4             	mov    %eax,-0xc(%ebp)
80103090:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103093:	8b 00                	mov    (%eax),%eax
80103095:	85 c0                	test   %eax,%eax
80103097:	75 ec                	jne    80103085 <iderw+0x75>
    ;
  *pp = b;
80103099:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010309c:	8b 55 08             	mov    0x8(%ebp),%edx
8010309f:	89 10                	mov    %edx,(%eax)

  // Start disk if necessary.
  if(idequeue == b)
801030a1:	a1 34 c6 10 80       	mov    0x8010c634,%eax
801030a6:	3b 45 08             	cmp    0x8(%ebp),%eax
801030a9:	75 22                	jne    801030cd <iderw+0xbd>
    idestart(b);
801030ab:	8b 45 08             	mov    0x8(%ebp),%eax
801030ae:	89 04 24             	mov    %eax,(%esp)
801030b1:	e8 3b fd ff ff       	call   80102df1 <idestart>

  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
801030b6:	eb 16                	jmp    801030ce <iderw+0xbe>
    sleep(b, &idelock);
801030b8:	c7 44 24 04 00 c6 10 	movl   $0x8010c600,0x4(%esp)
801030bf:	80 
801030c0:	8b 45 08             	mov    0x8(%ebp),%eax
801030c3:	89 04 24             	mov    %eax,(%esp)
801030c6:	e8 63 21 00 00       	call   8010522e <sleep>
801030cb:	eb 01                	jmp    801030ce <iderw+0xbe>
  // Start disk if necessary.
  if(idequeue == b)
    idestart(b);

  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
801030cd:	90                   	nop
801030ce:	8b 45 08             	mov    0x8(%ebp),%eax
801030d1:	8b 00                	mov    (%eax),%eax
801030d3:	83 e0 06             	and    $0x6,%eax
801030d6:	83 f8 02             	cmp    $0x2,%eax
801030d9:	75 dd                	jne    801030b8 <iderw+0xa8>
    sleep(b, &idelock);
  }

  release(&idelock);
801030db:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
801030e2:	e8 89 24 00 00       	call   80105570 <release>
}
801030e7:	c9                   	leave  
801030e8:	c3                   	ret    
801030e9:	00 00                	add    %al,(%eax)
	...

801030ec <ioapicread>:
  uint data;    // offset  10
};

static uint
ioapicread(int reg)
{
801030ec:	55                   	push   %ebp
801030ed:	89 e5                	mov    %esp,%ebp
  ioapic->reg = (reg & 0xFF);  // bits 0..7, see datasheet
801030ef:	a1 34 3a 11 80       	mov    0x80113a34,%eax
801030f4:	8b 55 08             	mov    0x8(%ebp),%edx
801030f7:	81 e2 ff 00 00 00    	and    $0xff,%edx
801030fd:	89 10                	mov    %edx,(%eax)
  return ioapic->data;
801030ff:	a1 34 3a 11 80       	mov    0x80113a34,%eax
80103104:	8b 40 10             	mov    0x10(%eax),%eax
}
80103107:	5d                   	pop    %ebp
80103108:	c3                   	ret    

80103109 <ioapicwrite>:

static void
ioapicwrite(int reg, uint data)
{
80103109:	55                   	push   %ebp
8010310a:	89 e5                	mov    %esp,%ebp
  ioapic->reg = (reg & 0xFF);  // bits 0..7, see datasheet
8010310c:	a1 34 3a 11 80       	mov    0x80113a34,%eax
80103111:	8b 55 08             	mov    0x8(%ebp),%edx
80103114:	81 e2 ff 00 00 00    	and    $0xff,%edx
8010311a:	89 10                	mov    %edx,(%eax)
  ioapic->data = data;
8010311c:	a1 34 3a 11 80       	mov    0x80113a34,%eax
80103121:	8b 55 0c             	mov    0xc(%ebp),%edx
80103124:	89 50 10             	mov    %edx,0x10(%eax)
}
80103127:	5d                   	pop    %ebp
80103128:	c3                   	ret    

80103129 <ioapicinit>:

void
ioapicinit(void)
{
80103129:	55                   	push   %ebp
8010312a:	89 e5                	mov    %esp,%ebp
8010312c:	83 ec 28             	sub    $0x28,%esp
  int i, id;

  if(!ismp)
8010312f:	a1 64 3b 11 80       	mov    0x80113b64,%eax
80103134:	85 c0                	test   %eax,%eax
80103136:	0f 84 bb 00 00 00    	je     801031f7 <ioapicinit+0xce>
    return;

  if(ioapic == 0) {
8010313c:	a1 34 3a 11 80       	mov    0x80113a34,%eax
80103141:	85 c0                	test   %eax,%eax
80103143:	75 16                	jne    8010315b <ioapicinit+0x32>
    ioapic = (volatile struct ioapic*)IOAPIC;
80103145:	c7 05 34 3a 11 80 00 	movl   $0xfec00000,0x80113a34
8010314c:	00 c0 fe 
    cprintf("ioapicinit: falling back to default ioapic address\n");
8010314f:	c7 04 24 7c 8d 10 80 	movl   $0x80108d7c,(%esp)
80103156:	e8 6e d5 ff ff       	call   801006c9 <cprintf>
  }
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF; // bits 16..23, see datasheet
8010315b:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80103162:	e8 85 ff ff ff       	call   801030ec <ioapicread>
80103167:	c1 e8 10             	shr    $0x10,%eax
8010316a:	25 ff 00 00 00       	and    $0xff,%eax
8010316f:	a3 3c c6 10 80       	mov    %eax,0x8010c63c
  id = (ioapicread(REG_ID) >> 24) & 0x0F;  // bits 24..27, see datasheet
80103174:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
8010317b:	e8 6c ff ff ff       	call   801030ec <ioapicread>
80103180:	c1 e8 18             	shr    $0x18,%eax
80103183:	83 e0 0f             	and    $0xf,%eax
80103186:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(id != ioapicid)
80103189:	0f b6 05 60 3b 11 80 	movzbl 0x80113b60,%eax
80103190:	0f b6 c0             	movzbl %al,%eax
80103193:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103196:	74 0c                	je     801031a4 <ioapicinit+0x7b>
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");
80103198:	c7 04 24 b0 8d 10 80 	movl   $0x80108db0,(%esp)
8010319f:	e8 25 d5 ff ff       	call   801006c9 <cprintf>

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
801031a4:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
801031ab:	eb 3e                	jmp    801031eb <ioapicinit+0xc2>
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
801031ad:	8b 45 f0             	mov    -0x10(%ebp),%eax
801031b0:	83 c0 20             	add    $0x20,%eax
801031b3:	0d 00 00 01 00       	or     $0x10000,%eax
801031b8:	8b 55 f0             	mov    -0x10(%ebp),%edx
801031bb:	83 c2 08             	add    $0x8,%edx
801031be:	01 d2                	add    %edx,%edx
801031c0:	89 44 24 04          	mov    %eax,0x4(%esp)
801031c4:	89 14 24             	mov    %edx,(%esp)
801031c7:	e8 3d ff ff ff       	call   80103109 <ioapicwrite>
    ioapicwrite(REG_TABLE+2*i+1, 0);
801031cc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801031cf:	83 c0 08             	add    $0x8,%eax
801031d2:	01 c0                	add    %eax,%eax
801031d4:	83 c0 01             	add    $0x1,%eax
801031d7:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801031de:	00 
801031df:	89 04 24             	mov    %eax,(%esp)
801031e2:	e8 22 ff ff ff       	call   80103109 <ioapicwrite>
  if(id != ioapicid)
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
801031e7:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
801031eb:	a1 3c c6 10 80       	mov    0x8010c63c,%eax
801031f0:	39 45 f0             	cmp    %eax,-0x10(%ebp)
801031f3:	7e b8                	jle    801031ad <ioapicinit+0x84>
801031f5:	eb 01                	jmp    801031f8 <ioapicinit+0xcf>
ioapicinit(void)
{
  int i, id;

  if(!ismp)
    return;
801031f7:	90                   	nop
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
    ioapicwrite(REG_TABLE+2*i+1, 0);
  }
}
801031f8:	c9                   	leave  
801031f9:	c3                   	ret    

801031fa <ioapicenable>:

void
ioapicenable(int irq, int cpunum)
{
801031fa:	55                   	push   %ebp
801031fb:	89 e5                	mov    %esp,%ebp
801031fd:	83 ec 18             	sub    $0x18,%esp
  if(!ismp)
80103200:	a1 64 3b 11 80       	mov    0x80113b64,%eax
80103205:	85 c0                	test   %eax,%eax
80103207:	74 63                	je     8010326c <ioapicenable+0x72>
    return;

  if(irq > maxintr)
80103209:	a1 3c c6 10 80       	mov    0x8010c63c,%eax
8010320e:	39 45 08             	cmp    %eax,0x8(%ebp)
80103211:	7e 1c                	jle    8010322f <ioapicenable+0x35>
    cprintf("ioapicenable: no irq %d, maximum is %d\n", irq, maxintr);
80103213:	a1 3c c6 10 80       	mov    0x8010c63c,%eax
80103218:	89 44 24 08          	mov    %eax,0x8(%esp)
8010321c:	8b 45 08             	mov    0x8(%ebp),%eax
8010321f:	89 44 24 04          	mov    %eax,0x4(%esp)
80103223:	c7 04 24 e4 8d 10 80 	movl   $0x80108de4,(%esp)
8010322a:	e8 9a d4 ff ff       	call   801006c9 <cprintf>

  cpunum = cpunum & 0x0F; // used as APIC id below, 4 bits, see datasheet
8010322f:	83 65 0c 0f          	andl   $0xf,0xc(%ebp)

  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
80103233:	8b 45 08             	mov    0x8(%ebp),%eax
80103236:	83 c0 20             	add    $0x20,%eax
80103239:	8b 55 08             	mov    0x8(%ebp),%edx
8010323c:	83 c2 08             	add    $0x8,%edx
8010323f:	01 d2                	add    %edx,%edx
80103241:	89 44 24 04          	mov    %eax,0x4(%esp)
80103245:	89 14 24             	mov    %edx,(%esp)
80103248:	e8 bc fe ff ff       	call   80103109 <ioapicwrite>
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24); // bits 56..59, see datasheet
8010324d:	8b 45 0c             	mov    0xc(%ebp),%eax
80103250:	c1 e0 18             	shl    $0x18,%eax
80103253:	8b 55 08             	mov    0x8(%ebp),%edx
80103256:	83 c2 08             	add    $0x8,%edx
80103259:	01 d2                	add    %edx,%edx
8010325b:	83 c2 01             	add    $0x1,%edx
8010325e:	89 44 24 04          	mov    %eax,0x4(%esp)
80103262:	89 14 24             	mov    %edx,(%esp)
80103265:	e8 9f fe ff ff       	call   80103109 <ioapicwrite>
8010326a:	eb 01                	jmp    8010326d <ioapicenable+0x73>

void
ioapicenable(int irq, int cpunum)
{
  if(!ismp)
    return;
8010326c:	90                   	nop
  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24); // bits 56..59, see datasheet
}
8010326d:	c9                   	leave  
8010326e:	c3                   	ret    
	...

80103270 <kinit1>:
// the pages mapped by entrypgdir on free list.
// 2. main() calls kinit2() with the rest of the physical pages
// after installing a full page table that maps them on all cores.
void
kinit1(void *vstart, void *vend)
{
80103270:	55                   	push   %ebp
80103271:	89 e5                	mov    %esp,%ebp
80103273:	83 ec 18             	sub    $0x18,%esp
  initlock(&kmem.lock, "kmem");
80103276:	c7 44 24 04 0c 8e 10 	movl   $0x80108e0c,0x4(%esp)
8010327d:	80 
8010327e:	c7 04 24 40 3a 11 80 	movl   $0x80113a40,(%esp)
80103285:	e8 5c 22 00 00       	call   801054e6 <initlock>
  kmem.use_lock = 0;
8010328a:	c7 05 74 3a 11 80 00 	movl   $0x0,0x80113a74
80103291:	00 00 00 
  freerange(vstart, vend);
80103294:	8b 45 0c             	mov    0xc(%ebp),%eax
80103297:	89 44 24 04          	mov    %eax,0x4(%esp)
8010329b:	8b 45 08             	mov    0x8(%ebp),%eax
8010329e:	89 04 24             	mov    %eax,(%esp)
801032a1:	e8 26 00 00 00       	call   801032cc <freerange>
}
801032a6:	c9                   	leave  
801032a7:	c3                   	ret    

801032a8 <kinit2>:

void
kinit2(void *vstart, void *vend)
{
801032a8:	55                   	push   %ebp
801032a9:	89 e5                	mov    %esp,%ebp
801032ab:	83 ec 18             	sub    $0x18,%esp
  freerange(vstart, vend);
801032ae:	8b 45 0c             	mov    0xc(%ebp),%eax
801032b1:	89 44 24 04          	mov    %eax,0x4(%esp)
801032b5:	8b 45 08             	mov    0x8(%ebp),%eax
801032b8:	89 04 24             	mov    %eax,(%esp)
801032bb:	e8 0c 00 00 00       	call   801032cc <freerange>
  kmem.use_lock = 1;
801032c0:	c7 05 74 3a 11 80 01 	movl   $0x1,0x80113a74
801032c7:	00 00 00 
}
801032ca:	c9                   	leave  
801032cb:	c3                   	ret    

801032cc <freerange>:

void
freerange(void *vstart, void *vend)
{
801032cc:	55                   	push   %ebp
801032cd:	89 e5                	mov    %esp,%ebp
801032cf:	83 ec 28             	sub    $0x28,%esp
  char *p;
  p = (char*)PGROUNDUP((uint)vstart);
801032d2:	8b 45 08             	mov    0x8(%ebp),%eax
801032d5:	05 ff 0f 00 00       	add    $0xfff,%eax
801032da:	25 00 f0 ff ff       	and    $0xfffff000,%eax
801032df:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801032e2:	eb 12                	jmp    801032f6 <freerange+0x2a>
    kfree(p);
801032e4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801032e7:	89 04 24             	mov    %eax,(%esp)
801032ea:	e8 19 00 00 00       	call   80103308 <kfree>
void
freerange(void *vstart, void *vend)
{
  char *p;
  p = (char*)PGROUNDUP((uint)vstart);
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801032ef:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
801032f6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801032f9:	8d 90 00 10 00 00    	lea    0x1000(%eax),%edx
801032ff:	8b 45 0c             	mov    0xc(%ebp),%eax
80103302:	39 c2                	cmp    %eax,%edx
80103304:	76 de                	jbe    801032e4 <freerange+0x18>
    kfree(p);
}
80103306:	c9                   	leave  
80103307:	c3                   	ret    

80103308 <kfree>:
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void
kfree(char *v)
{
80103308:	55                   	push   %ebp
80103309:	89 e5                	mov    %esp,%ebp
8010330b:	83 ec 28             	sub    $0x28,%esp
  struct run *r;

  if((uint)v % PGSIZE || v < end || V2P(v) >= PHYSTOP)
8010330e:	8b 45 08             	mov    0x8(%ebp),%eax
80103311:	25 ff 0f 00 00       	and    $0xfff,%eax
80103316:	85 c0                	test   %eax,%eax
80103318:	75 18                	jne    80103332 <kfree+0x2a>
8010331a:	81 7d 08 f8 60 11 80 	cmpl   $0x801160f8,0x8(%ebp)
80103321:	72 0f                	jb     80103332 <kfree+0x2a>
80103323:	8b 45 08             	mov    0x8(%ebp),%eax
80103326:	2d 00 00 00 80       	sub    $0x80000000,%eax
8010332b:	3d ff ff ff 0d       	cmp    $0xdffffff,%eax
80103330:	76 0c                	jbe    8010333e <kfree+0x36>
    panic("kfree");
80103332:	c7 04 24 11 8e 10 80 	movl   $0x80108e11,(%esp)
80103339:	e8 2b d5 ff ff       	call   80100869 <panic>

  // Fill with junk to catch dangling refs.
  memset(v, 1, PGSIZE);
8010333e:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80103345:	00 
80103346:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
8010334d:	00 
8010334e:	8b 45 08             	mov    0x8(%ebp),%eax
80103351:	89 04 24             	mov    %eax,(%esp)
80103354:	e8 11 24 00 00       	call   8010576a <memset>

  if(kmem.use_lock)
80103359:	a1 74 3a 11 80       	mov    0x80113a74,%eax
8010335e:	85 c0                	test   %eax,%eax
80103360:	74 0c                	je     8010336e <kfree+0x66>
    acquire(&kmem.lock);
80103362:	c7 04 24 40 3a 11 80 	movl   $0x80113a40,(%esp)
80103369:	e8 99 21 00 00       	call   80105507 <acquire>
  r = (struct run*)v;
8010336e:	8b 45 08             	mov    0x8(%ebp),%eax
80103371:	89 45 f4             	mov    %eax,-0xc(%ebp)
  r->next = kmem.freelist;
80103374:	8b 15 78 3a 11 80    	mov    0x80113a78,%edx
8010337a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010337d:	89 10                	mov    %edx,(%eax)
  kmem.freelist = r;
8010337f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103382:	a3 78 3a 11 80       	mov    %eax,0x80113a78
  if(kmem.use_lock)
80103387:	a1 74 3a 11 80       	mov    0x80113a74,%eax
8010338c:	85 c0                	test   %eax,%eax
8010338e:	74 0c                	je     8010339c <kfree+0x94>
    release(&kmem.lock);
80103390:	c7 04 24 40 3a 11 80 	movl   $0x80113a40,(%esp)
80103397:	e8 d4 21 00 00       	call   80105570 <release>
}
8010339c:	c9                   	leave  
8010339d:	c3                   	ret    

8010339e <kalloc>:
// Allocate one 4096-byte page of physical memory.
// Returns a pointer that the kernel can use.
// Returns 0 if the memory cannot be allocated.
char*
kalloc(void)
{
8010339e:	55                   	push   %ebp
8010339f:	89 e5                	mov    %esp,%ebp
801033a1:	83 ec 28             	sub    $0x28,%esp
  struct run *r;

  if(kmem.use_lock)
801033a4:	a1 74 3a 11 80       	mov    0x80113a74,%eax
801033a9:	85 c0                	test   %eax,%eax
801033ab:	74 0c                	je     801033b9 <kalloc+0x1b>
    acquire(&kmem.lock);
801033ad:	c7 04 24 40 3a 11 80 	movl   $0x80113a40,(%esp)
801033b4:	e8 4e 21 00 00       	call   80105507 <acquire>
  r = kmem.freelist;
801033b9:	a1 78 3a 11 80       	mov    0x80113a78,%eax
801033be:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(r)
801033c1:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801033c5:	74 0a                	je     801033d1 <kalloc+0x33>
    kmem.freelist = r->next;
801033c7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801033ca:	8b 00                	mov    (%eax),%eax
801033cc:	a3 78 3a 11 80       	mov    %eax,0x80113a78
  if(kmem.use_lock)
801033d1:	a1 74 3a 11 80       	mov    0x80113a74,%eax
801033d6:	85 c0                	test   %eax,%eax
801033d8:	74 0c                	je     801033e6 <kalloc+0x48>
    release(&kmem.lock);
801033da:	c7 04 24 40 3a 11 80 	movl   $0x80113a40,(%esp)
801033e1:	e8 8a 21 00 00       	call   80105570 <release>
  return (char*)r;
801033e6:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801033e9:	c9                   	leave  
801033ea:	c3                   	ret    
	...

801033ec <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
801033ec:	55                   	push   %ebp
801033ed:	89 e5                	mov    %esp,%ebp
801033ef:	83 ec 14             	sub    $0x14,%esp
801033f2:	8b 45 08             	mov    0x8(%ebp),%eax
801033f5:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801033f9:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
801033fd:	89 c2                	mov    %eax,%edx
801033ff:	ec                   	in     (%dx),%al
80103400:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80103403:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80103407:	c9                   	leave  
80103408:	c3                   	ret    

80103409 <kbdgetc>:
#include "defs.h"
#include "kbd.h"

int
kbdgetc(void)
{
80103409:	55                   	push   %ebp
8010340a:	89 e5                	mov    %esp,%ebp
8010340c:	83 ec 14             	sub    $0x14,%esp
  static uchar *charcode[4] = {
    normalmap, shiftmap, ctlmap, ctlmap
  };
  uint st, data, c;

  st = inb(KBSTATP);
8010340f:	c7 04 24 64 00 00 00 	movl   $0x64,(%esp)
80103416:	e8 d1 ff ff ff       	call   801033ec <inb>
8010341b:	0f b6 c0             	movzbl %al,%eax
8010341e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((st & KBS_DIB) == 0)
80103421:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103424:	83 e0 01             	and    $0x1,%eax
80103427:	85 c0                	test   %eax,%eax
80103429:	75 0a                	jne    80103435 <kbdgetc+0x2c>
    return -1;
8010342b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80103430:	e9 20 01 00 00       	jmp    80103555 <kbdgetc+0x14c>
  data = inb(KBDATAP);
80103435:	c7 04 24 60 00 00 00 	movl   $0x60,(%esp)
8010343c:	e8 ab ff ff ff       	call   801033ec <inb>
80103441:	0f b6 c0             	movzbl %al,%eax
80103444:	89 45 f8             	mov    %eax,-0x8(%ebp)

  if(data == 0xE0){
80103447:	81 7d f8 e0 00 00 00 	cmpl   $0xe0,-0x8(%ebp)
8010344e:	75 17                	jne    80103467 <kbdgetc+0x5e>
    shift |= E0ESC;
80103450:	a1 40 c6 10 80       	mov    0x8010c640,%eax
80103455:	83 c8 40             	or     $0x40,%eax
80103458:	a3 40 c6 10 80       	mov    %eax,0x8010c640
    return 0;
8010345d:	b8 00 00 00 00       	mov    $0x0,%eax
80103462:	e9 ee 00 00 00       	jmp    80103555 <kbdgetc+0x14c>
  } else if(data & 0x80){
80103467:	8b 45 f8             	mov    -0x8(%ebp),%eax
8010346a:	25 80 00 00 00       	and    $0x80,%eax
8010346f:	85 c0                	test   %eax,%eax
80103471:	74 44                	je     801034b7 <kbdgetc+0xae>
    // Key released
    data = ((shift & E0ESC) ? data : data & 0x7F);
80103473:	a1 40 c6 10 80       	mov    0x8010c640,%eax
80103478:	83 e0 40             	and    $0x40,%eax
8010347b:	85 c0                	test   %eax,%eax
8010347d:	75 08                	jne    80103487 <kbdgetc+0x7e>
8010347f:	8b 45 f8             	mov    -0x8(%ebp),%eax
80103482:	83 e0 7f             	and    $0x7f,%eax
80103485:	eb 03                	jmp    8010348a <kbdgetc+0x81>
80103487:	8b 45 f8             	mov    -0x8(%ebp),%eax
8010348a:	89 45 f8             	mov    %eax,-0x8(%ebp)
    shift &= ~(shiftcode[data] | E0ESC);
8010348d:	8b 45 f8             	mov    -0x8(%ebp),%eax
80103490:	0f b6 80 40 a0 10 80 	movzbl -0x7fef5fc0(%eax),%eax
80103497:	83 c8 40             	or     $0x40,%eax
8010349a:	0f b6 c0             	movzbl %al,%eax
8010349d:	f7 d0                	not    %eax
8010349f:	89 c2                	mov    %eax,%edx
801034a1:	a1 40 c6 10 80       	mov    0x8010c640,%eax
801034a6:	21 d0                	and    %edx,%eax
801034a8:	a3 40 c6 10 80       	mov    %eax,0x8010c640
    return 0;
801034ad:	b8 00 00 00 00       	mov    $0x0,%eax
801034b2:	e9 9e 00 00 00       	jmp    80103555 <kbdgetc+0x14c>
  } else if(shift & E0ESC){
801034b7:	a1 40 c6 10 80       	mov    0x8010c640,%eax
801034bc:	83 e0 40             	and    $0x40,%eax
801034bf:	85 c0                	test   %eax,%eax
801034c1:	74 14                	je     801034d7 <kbdgetc+0xce>
    // Last character was an E0 escape; or with 0x80
    data |= 0x80;
801034c3:	81 4d f8 80 00 00 00 	orl    $0x80,-0x8(%ebp)
    shift &= ~E0ESC;
801034ca:	a1 40 c6 10 80       	mov    0x8010c640,%eax
801034cf:	83 e0 bf             	and    $0xffffffbf,%eax
801034d2:	a3 40 c6 10 80       	mov    %eax,0x8010c640
  }

  shift |= shiftcode[data];
801034d7:	8b 45 f8             	mov    -0x8(%ebp),%eax
801034da:	0f b6 80 40 a0 10 80 	movzbl -0x7fef5fc0(%eax),%eax
801034e1:	0f b6 d0             	movzbl %al,%edx
801034e4:	a1 40 c6 10 80       	mov    0x8010c640,%eax
801034e9:	09 d0                	or     %edx,%eax
801034eb:	a3 40 c6 10 80       	mov    %eax,0x8010c640
  shift ^= togglecode[data];
801034f0:	8b 45 f8             	mov    -0x8(%ebp),%eax
801034f3:	0f b6 80 40 a1 10 80 	movzbl -0x7fef5ec0(%eax),%eax
801034fa:	0f b6 d0             	movzbl %al,%edx
801034fd:	a1 40 c6 10 80       	mov    0x8010c640,%eax
80103502:	31 d0                	xor    %edx,%eax
80103504:	a3 40 c6 10 80       	mov    %eax,0x8010c640
  c = charcode[shift & (CTL | SHIFT)][data];
80103509:	a1 40 c6 10 80       	mov    0x8010c640,%eax
8010350e:	83 e0 03             	and    $0x3,%eax
80103511:	8b 04 85 40 a5 10 80 	mov    -0x7fef5ac0(,%eax,4),%eax
80103518:	03 45 f8             	add    -0x8(%ebp),%eax
8010351b:	0f b6 00             	movzbl (%eax),%eax
8010351e:	0f b6 c0             	movzbl %al,%eax
80103521:	89 45 fc             	mov    %eax,-0x4(%ebp)
  if(shift & CAPSLOCK){
80103524:	a1 40 c6 10 80       	mov    0x8010c640,%eax
80103529:	83 e0 08             	and    $0x8,%eax
8010352c:	85 c0                	test   %eax,%eax
8010352e:	74 22                	je     80103552 <kbdgetc+0x149>
    if('a' <= c && c <= 'z')
80103530:	83 7d fc 60          	cmpl   $0x60,-0x4(%ebp)
80103534:	76 0c                	jbe    80103542 <kbdgetc+0x139>
80103536:	83 7d fc 7a          	cmpl   $0x7a,-0x4(%ebp)
8010353a:	77 06                	ja     80103542 <kbdgetc+0x139>
      c += 'A' - 'a';
8010353c:	83 6d fc 20          	subl   $0x20,-0x4(%ebp)

  shift |= shiftcode[data];
  shift ^= togglecode[data];
  c = charcode[shift & (CTL | SHIFT)][data];
  if(shift & CAPSLOCK){
    if('a' <= c && c <= 'z')
80103540:	eb 10                	jmp    80103552 <kbdgetc+0x149>
      c += 'A' - 'a';
    else if('A' <= c && c <= 'Z')
80103542:	83 7d fc 40          	cmpl   $0x40,-0x4(%ebp)
80103546:	76 0a                	jbe    80103552 <kbdgetc+0x149>
80103548:	83 7d fc 5a          	cmpl   $0x5a,-0x4(%ebp)
8010354c:	77 04                	ja     80103552 <kbdgetc+0x149>
      c += 'a' - 'A';
8010354e:	83 45 fc 20          	addl   $0x20,-0x4(%ebp)
  }
  return c;
80103552:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80103555:	c9                   	leave  
80103556:	c3                   	ret    

80103557 <kbdintr>:

void
kbdintr(void)
{
80103557:	55                   	push   %ebp
80103558:	89 e5                	mov    %esp,%ebp
8010355a:	83 ec 18             	sub    $0x18,%esp
  consoleintr(kbdgetc);
8010355d:	c7 04 24 09 34 10 80 	movl   $0x80103409,(%esp)
80103564:	e8 94 d5 ff ff       	call   80100afd <consoleintr>
}
80103569:	c9                   	leave  
8010356a:	c3                   	ret    
	...

8010356c <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
8010356c:	55                   	push   %ebp
8010356d:	89 e5                	mov    %esp,%ebp
8010356f:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80103572:	9c                   	pushf  
80103573:	58                   	pop    %eax
80103574:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return eflags;
80103577:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
8010357a:	c9                   	leave  
8010357b:	c3                   	ret    

8010357c <lapicw>:

volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
8010357c:	55                   	push   %ebp
8010357d:	89 e5                	mov    %esp,%ebp
  lapic[index] = value;
8010357f:	a1 7c 3a 11 80       	mov    0x80113a7c,%eax
80103584:	8b 55 08             	mov    0x8(%ebp),%edx
80103587:	c1 e2 02             	shl    $0x2,%edx
8010358a:	8d 14 10             	lea    (%eax,%edx,1),%edx
8010358d:	8b 45 0c             	mov    0xc(%ebp),%eax
80103590:	89 02                	mov    %eax,(%edx)
  lapic[ID];  // wait for write to finish, by reading
80103592:	a1 7c 3a 11 80       	mov    0x80113a7c,%eax
80103597:	83 c0 20             	add    $0x20,%eax
8010359a:	8b 00                	mov    (%eax),%eax
}
8010359c:	5d                   	pop    %ebp
8010359d:	c3                   	ret    

8010359e <lapicinit>:
//PAGEBREAK!

void
lapicinit(void)
{
8010359e:	55                   	push   %ebp
8010359f:	89 e5                	mov    %esp,%ebp
801035a1:	83 ec 08             	sub    $0x8,%esp
  if(!lapic)
801035a4:	a1 7c 3a 11 80       	mov    0x80113a7c,%eax
801035a9:	85 c0                	test   %eax,%eax
801035ab:	0f 84 46 01 00 00    	je     801036f7 <lapicinit+0x159>
    return;

  // Enable local APIC; set spurious interrupt vector.
  lapicw(SVR, ENABLE | (T_IRQ0 + IRQ_SPURIOUS));
801035b1:	c7 44 24 04 3f 01 00 	movl   $0x13f,0x4(%esp)
801035b8:	00 
801035b9:	c7 04 24 3c 00 00 00 	movl   $0x3c,(%esp)
801035c0:	e8 b7 ff ff ff       	call   8010357c <lapicw>

  // The timer repeatedly counts down at bus frequency
  // from lapic[TICR] and then issues an interrupt.
  // If xv6 cared more about precise timekeeping,
  // TICR would be calibrated using an external time source.
  lapicw(TDCR, X1);
801035c5:	c7 44 24 04 0b 00 00 	movl   $0xb,0x4(%esp)
801035cc:	00 
801035cd:	c7 04 24 f8 00 00 00 	movl   $0xf8,(%esp)
801035d4:	e8 a3 ff ff ff       	call   8010357c <lapicw>
  lapicw(TIMER, PERIODIC | (T_IRQ0 + IRQ_TIMER));
801035d9:	c7 44 24 04 20 00 02 	movl   $0x20020,0x4(%esp)
801035e0:	00 
801035e1:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
801035e8:	e8 8f ff ff ff       	call   8010357c <lapicw>
  lapicw(TICR, 10000000);
801035ed:	c7 44 24 04 80 96 98 	movl   $0x989680,0x4(%esp)
801035f4:	00 
801035f5:	c7 04 24 e0 00 00 00 	movl   $0xe0,(%esp)
801035fc:	e8 7b ff ff ff       	call   8010357c <lapicw>

  // Disable logical interrupt lines.
  lapicw(LINT0, MASKED);
80103601:	c7 44 24 04 00 00 01 	movl   $0x10000,0x4(%esp)
80103608:	00 
80103609:	c7 04 24 d4 00 00 00 	movl   $0xd4,(%esp)
80103610:	e8 67 ff ff ff       	call   8010357c <lapicw>
  lapicw(LINT1, MASKED);
80103615:	c7 44 24 04 00 00 01 	movl   $0x10000,0x4(%esp)
8010361c:	00 
8010361d:	c7 04 24 d8 00 00 00 	movl   $0xd8,(%esp)
80103624:	e8 53 ff ff ff       	call   8010357c <lapicw>

  // Disable performance counter overflow interrupts
  // on machines that provide that interrupt entry.
  if(((lapic[VER]>>16) & 0xFF) >= 4)
80103629:	a1 7c 3a 11 80       	mov    0x80113a7c,%eax
8010362e:	83 c0 30             	add    $0x30,%eax
80103631:	8b 00                	mov    (%eax),%eax
80103633:	c1 e8 10             	shr    $0x10,%eax
80103636:	25 ff 00 00 00       	and    $0xff,%eax
8010363b:	83 f8 03             	cmp    $0x3,%eax
8010363e:	76 14                	jbe    80103654 <lapicinit+0xb6>
    lapicw(PCINT, MASKED);
80103640:	c7 44 24 04 00 00 01 	movl   $0x10000,0x4(%esp)
80103647:	00 
80103648:	c7 04 24 d0 00 00 00 	movl   $0xd0,(%esp)
8010364f:	e8 28 ff ff ff       	call   8010357c <lapicw>

  // Map error interrupt to IRQ_ERROR.
  lapicw(ERROR, T_IRQ0 + IRQ_ERROR);
80103654:	c7 44 24 04 33 00 00 	movl   $0x33,0x4(%esp)
8010365b:	00 
8010365c:	c7 04 24 dc 00 00 00 	movl   $0xdc,(%esp)
80103663:	e8 14 ff ff ff       	call   8010357c <lapicw>

  // Clear error status register (requires back-to-back writes).
  lapicw(ESR, 0);
80103668:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
8010366f:	00 
80103670:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
80103677:	e8 00 ff ff ff       	call   8010357c <lapicw>
  lapicw(ESR, 0);
8010367c:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80103683:	00 
80103684:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
8010368b:	e8 ec fe ff ff       	call   8010357c <lapicw>

  // Ack any outstanding interrupts.
  lapicw(EOI, 0);
80103690:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80103697:	00 
80103698:	c7 04 24 2c 00 00 00 	movl   $0x2c,(%esp)
8010369f:	e8 d8 fe ff ff       	call   8010357c <lapicw>

  // Send an Init Level De-Assert to synchronise arbitration ID's.
  lapicw(ICRHI, 0);
801036a4:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801036ab:	00 
801036ac:	c7 04 24 c4 00 00 00 	movl   $0xc4,(%esp)
801036b3:	e8 c4 fe ff ff       	call   8010357c <lapicw>
  lapicw(ICRLO, BCAST | INIT | LEVEL);
801036b8:	c7 44 24 04 00 85 08 	movl   $0x88500,0x4(%esp)
801036bf:	00 
801036c0:	c7 04 24 c0 00 00 00 	movl   $0xc0,(%esp)
801036c7:	e8 b0 fe ff ff       	call   8010357c <lapicw>
  while(lapic[ICRLO] & DELIVS)
801036cc:	a1 7c 3a 11 80       	mov    0x80113a7c,%eax
801036d1:	05 00 03 00 00       	add    $0x300,%eax
801036d6:	8b 00                	mov    (%eax),%eax
801036d8:	25 00 10 00 00       	and    $0x1000,%eax
801036dd:	85 c0                	test   %eax,%eax
801036df:	75 eb                	jne    801036cc <lapicinit+0x12e>
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
801036e1:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801036e8:	00 
801036e9:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
801036f0:	e8 87 fe ff ff       	call   8010357c <lapicw>
801036f5:	eb 01                	jmp    801036f8 <lapicinit+0x15a>

void
lapicinit(void)
{
  if(!lapic)
    return;
801036f7:	90                   	nop
  while(lapic[ICRLO] & DELIVS)
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
}
801036f8:	c9                   	leave  
801036f9:	c3                   	ret    

801036fa <cpunum>:

int
cpunum(void)
{
801036fa:	55                   	push   %ebp
801036fb:	89 e5                	mov    %esp,%ebp
801036fd:	83 ec 18             	sub    $0x18,%esp
  // Cannot call cpu when interrupts are enabled:
  // result not guaranteed to last long enough to be used!
  // Would prefer to panic but even printing is chancy here:
  // almost everything, including cprintf and panic, calls cpu,
  // often indirectly through acquire and release.
  if(readeflags()&FL_IF){
80103700:	e8 67 fe ff ff       	call   8010356c <readeflags>
80103705:	25 00 02 00 00       	and    $0x200,%eax
8010370a:	85 c0                	test   %eax,%eax
8010370c:	74 29                	je     80103737 <cpunum+0x3d>
    static int n;
    if(n++ == 0)
8010370e:	a1 44 c6 10 80       	mov    0x8010c644,%eax
80103713:	85 c0                	test   %eax,%eax
80103715:	0f 94 c2             	sete   %dl
80103718:	83 c0 01             	add    $0x1,%eax
8010371b:	a3 44 c6 10 80       	mov    %eax,0x8010c644
80103720:	84 d2                	test   %dl,%dl
80103722:	74 13                	je     80103737 <cpunum+0x3d>
      cprintf("cpu called from %x with interrupts enabled\n",
80103724:	8b 45 04             	mov    0x4(%ebp),%eax
80103727:	89 44 24 04          	mov    %eax,0x4(%esp)
8010372b:	c7 04 24 18 8e 10 80 	movl   $0x80108e18,(%esp)
80103732:	e8 92 cf ff ff       	call   801006c9 <cprintf>
        __builtin_return_address(0));
  }

  if(lapic)
80103737:	a1 7c 3a 11 80       	mov    0x80113a7c,%eax
8010373c:	85 c0                	test   %eax,%eax
8010373e:	74 0f                	je     8010374f <cpunum+0x55>
    return lapic[ID]>>24;
80103740:	a1 7c 3a 11 80       	mov    0x80113a7c,%eax
80103745:	83 c0 20             	add    $0x20,%eax
80103748:	8b 00                	mov    (%eax),%eax
8010374a:	c1 e8 18             	shr    $0x18,%eax
8010374d:	eb 05                	jmp    80103754 <cpunum+0x5a>
  return 0;
8010374f:	b8 00 00 00 00       	mov    $0x0,%eax
}
80103754:	c9                   	leave  
80103755:	c3                   	ret    

80103756 <lapiceoi>:

// Acknowledge interrupt.
void
lapiceoi(void)
{
80103756:	55                   	push   %ebp
80103757:	89 e5                	mov    %esp,%ebp
80103759:	83 ec 08             	sub    $0x8,%esp
  if(lapic)
8010375c:	a1 7c 3a 11 80       	mov    0x80113a7c,%eax
80103761:	85 c0                	test   %eax,%eax
80103763:	74 14                	je     80103779 <lapiceoi+0x23>
    lapicw(EOI, 0);
80103765:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
8010376c:	00 
8010376d:	c7 04 24 2c 00 00 00 	movl   $0x2c,(%esp)
80103774:	e8 03 fe ff ff       	call   8010357c <lapicw>
}
80103779:	c9                   	leave  
8010377a:	c3                   	ret    

8010377b <microdelay>:

// Spin for a given number of microseconds.
// On real hardware would want to tune this dynamically.
void
microdelay(int us)
{
8010377b:	55                   	push   %ebp
8010377c:	89 e5                	mov    %esp,%ebp
}
8010377e:	5d                   	pop    %ebp
8010377f:	c3                   	ret    

80103780 <lapicstartap>:

// Start additional processor running entry code at addr.
// See Appendix B of MultiProcessor Specification.
void
lapicstartap(uchar apicid, uint addr)
{
80103780:	55                   	push   %ebp
80103781:	89 e5                	mov    %esp,%ebp
80103783:	83 ec 38             	sub    $0x38,%esp
80103786:	8b 45 08             	mov    0x8(%ebp),%eax
80103789:	88 45 e4             	mov    %al,-0x1c(%ebp)
  ushort *wrv;

  // "The BSP must initialize CMOS shutdown code to 0AH
  // and the warm reset vector (DWORD based at 40:67) to point at
  // the AP startup code prior to the [universal startup algorithm]."
  cmoswrite(CMOS_SHUTDOWN_STAT, CMOS_JMP_DWORD_NO_EOI);
8010378c:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
80103793:	00 
80103794:	c7 04 24 0f 00 00 00 	movl   $0xf,(%esp)
8010379b:	e8 9a cb ff ff       	call   8010033a <cmoswrite>
  wrv = (ushort*)P2V((0x40<<4 | 0x67));  // Warm reset vector
801037a0:	c7 45 f4 67 04 00 80 	movl   $0x80000467,-0xc(%ebp)
  wrv[0] = 0;
801037a7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801037aa:	66 c7 00 00 00       	movw   $0x0,(%eax)
  wrv[1] = addr >> 4;
801037af:	8b 45 f4             	mov    -0xc(%ebp),%eax
801037b2:	8d 50 02             	lea    0x2(%eax),%edx
801037b5:	8b 45 0c             	mov    0xc(%ebp),%eax
801037b8:	c1 e8 04             	shr    $0x4,%eax
801037bb:	66 89 02             	mov    %ax,(%edx)

  // "Universal startup algorithm."
  // Send INIT (level-triggered) interrupt to reset other CPU.
  lapicw(ICRHI, apicid<<24);
801037be:	0f b6 45 e4          	movzbl -0x1c(%ebp),%eax
801037c2:	c1 e0 18             	shl    $0x18,%eax
801037c5:	89 44 24 04          	mov    %eax,0x4(%esp)
801037c9:	c7 04 24 c4 00 00 00 	movl   $0xc4,(%esp)
801037d0:	e8 a7 fd ff ff       	call   8010357c <lapicw>
  lapicw(ICRLO, INIT | LEVEL | ASSERT);
801037d5:	c7 44 24 04 00 c5 00 	movl   $0xc500,0x4(%esp)
801037dc:	00 
801037dd:	c7 04 24 c0 00 00 00 	movl   $0xc0,(%esp)
801037e4:	e8 93 fd ff ff       	call   8010357c <lapicw>
  microdelay(200);
801037e9:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
801037f0:	e8 86 ff ff ff       	call   8010377b <microdelay>
  lapicw(ICRLO, INIT | LEVEL);
801037f5:	c7 44 24 04 00 85 00 	movl   $0x8500,0x4(%esp)
801037fc:	00 
801037fd:	c7 04 24 c0 00 00 00 	movl   $0xc0,(%esp)
80103804:	e8 73 fd ff ff       	call   8010357c <lapicw>
  microdelay(100);    // should be 10ms, but too slow in Bochs!
80103809:	c7 04 24 64 00 00 00 	movl   $0x64,(%esp)
80103810:	e8 66 ff ff ff       	call   8010377b <microdelay>
  // Send startup IPI (twice!) to enter code.
  // Regular hardware is supposed to only accept a STARTUP
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
80103815:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
8010381c:	eb 40                	jmp    8010385e <lapicstartap+0xde>
    lapicw(ICRHI, apicid<<24);
8010381e:	0f b6 45 e4          	movzbl -0x1c(%ebp),%eax
80103822:	c1 e0 18             	shl    $0x18,%eax
80103825:	89 44 24 04          	mov    %eax,0x4(%esp)
80103829:	c7 04 24 c4 00 00 00 	movl   $0xc4,(%esp)
80103830:	e8 47 fd ff ff       	call   8010357c <lapicw>
    lapicw(ICRLO, STARTUP | (addr>>12));
80103835:	8b 45 0c             	mov    0xc(%ebp),%eax
80103838:	c1 e8 0c             	shr    $0xc,%eax
8010383b:	80 cc 06             	or     $0x6,%ah
8010383e:	89 44 24 04          	mov    %eax,0x4(%esp)
80103842:	c7 04 24 c0 00 00 00 	movl   $0xc0,(%esp)
80103849:	e8 2e fd ff ff       	call   8010357c <lapicw>
    microdelay(200);
8010384e:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
80103855:	e8 21 ff ff ff       	call   8010377b <microdelay>
  // Send startup IPI (twice!) to enter code.
  // Regular hardware is supposed to only accept a STARTUP
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
8010385a:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
8010385e:	83 7d f0 01          	cmpl   $0x1,-0x10(%ebp)
80103862:	7e ba                	jle    8010381e <lapicstartap+0x9e>
    lapicw(ICRHI, apicid<<24);
    lapicw(ICRLO, STARTUP | (addr>>12));
    microdelay(200);
  }
}
80103864:	c9                   	leave  
80103865:	c3                   	ret    
	...

80103868 <initlog>:
static void recover_from_log(void);
static void commit();

void
initlog(int dev)
{
80103868:	55                   	push   %ebp
80103869:	89 e5                	mov    %esp,%ebp
8010386b:	83 ec 38             	sub    $0x38,%esp
  if (sizeof(struct logheader) >= BSIZE)
8010386e:	90                   	nop
    panic("initlog: too big logheader");

  struct superblock sb;
  initlock(&log.lock, "log");
8010386f:	c7 44 24 04 44 8e 10 	movl   $0x80108e44,0x4(%esp)
80103876:	80 
80103877:	c7 04 24 80 3a 11 80 	movl   $0x80113a80,(%esp)
8010387e:	e8 63 1c 00 00       	call   801054e6 <initlock>
  readsb(dev, &sb);
80103883:	8d 45 dc             	lea    -0x24(%ebp),%eax
80103886:	89 44 24 04          	mov    %eax,0x4(%esp)
8010388a:	8b 45 08             	mov    0x8(%ebp),%eax
8010388d:	89 04 24             	mov    %eax,(%esp)
80103890:	e8 f3 e1 ff ff       	call   80101a88 <readsb>
  log.start = sb.logstart;
80103895:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103898:	a3 b4 3a 11 80       	mov    %eax,0x80113ab4
  log.size = sb.nlog;
8010389d:	8b 45 e8             	mov    -0x18(%ebp),%eax
801038a0:	a3 b8 3a 11 80       	mov    %eax,0x80113ab8
  log.dev = dev;
801038a5:	8b 45 08             	mov    0x8(%ebp),%eax
801038a8:	a3 c4 3a 11 80       	mov    %eax,0x80113ac4
  recover_from_log();
801038ad:	e8 97 01 00 00       	call   80103a49 <recover_from_log>
}
801038b2:	c9                   	leave  
801038b3:	c3                   	ret    

801038b4 <install_trans>:

// Copy committed blocks from log to their home location
static void 
install_trans(void)
{
801038b4:	55                   	push   %ebp
801038b5:	89 e5                	mov    %esp,%ebp
801038b7:	83 ec 28             	sub    $0x28,%esp
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
801038ba:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
801038c1:	e9 89 00 00 00       	jmp    8010394f <install_trans+0x9b>
    struct buf *lbuf = bread(log.dev, log.start+tail+1); // read log block
801038c6:	a1 b4 3a 11 80       	mov    0x80113ab4,%eax
801038cb:	03 45 ec             	add    -0x14(%ebp),%eax
801038ce:	83 c0 01             	add    $0x1,%eax
801038d1:	89 c2                	mov    %eax,%edx
801038d3:	a1 c4 3a 11 80       	mov    0x80113ac4,%eax
801038d8:	89 54 24 04          	mov    %edx,0x4(%esp)
801038dc:	89 04 24             	mov    %eax,(%esp)
801038df:	e8 c3 c8 ff ff       	call   801001a7 <bread>
801038e4:	89 45 f0             	mov    %eax,-0x10(%ebp)
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
801038e7:	8b 45 ec             	mov    -0x14(%ebp),%eax
801038ea:	83 c0 10             	add    $0x10,%eax
801038ed:	8b 04 85 8c 3a 11 80 	mov    -0x7feec574(,%eax,4),%eax
801038f4:	89 c2                	mov    %eax,%edx
801038f6:	a1 c4 3a 11 80       	mov    0x80113ac4,%eax
801038fb:	89 54 24 04          	mov    %edx,0x4(%esp)
801038ff:	89 04 24             	mov    %eax,(%esp)
80103902:	e8 a0 c8 ff ff       	call   801001a7 <bread>
80103907:	89 45 f4             	mov    %eax,-0xc(%ebp)
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
8010390a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010390d:	8d 50 18             	lea    0x18(%eax),%edx
80103910:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103913:	83 c0 18             	add    $0x18,%eax
80103916:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
8010391d:	00 
8010391e:	89 54 24 04          	mov    %edx,0x4(%esp)
80103922:	89 04 24             	mov    %eax,(%esp)
80103925:	e8 13 1f 00 00       	call   8010583d <memmove>
    bwrite(dbuf);  // write dst to disk
8010392a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010392d:	89 04 24             	mov    %eax,(%esp)
80103930:	e8 a9 c8 ff ff       	call   801001de <bwrite>
    brelse(lbuf); 
80103935:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103938:	89 04 24             	mov    %eax,(%esp)
8010393b:	e8 d8 c8 ff ff       	call   80100218 <brelse>
    brelse(dbuf);
80103940:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103943:	89 04 24             	mov    %eax,(%esp)
80103946:	e8 cd c8 ff ff       	call   80100218 <brelse>
static void 
install_trans(void)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
8010394b:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
8010394f:	a1 c8 3a 11 80       	mov    0x80113ac8,%eax
80103954:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80103957:	0f 8f 69 ff ff ff    	jg     801038c6 <install_trans+0x12>
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
    bwrite(dbuf);  // write dst to disk
    brelse(lbuf); 
    brelse(dbuf);
  }
}
8010395d:	c9                   	leave  
8010395e:	c3                   	ret    

8010395f <read_head>:

// Read the log header from disk into the in-memory log header
static void
read_head(void)
{
8010395f:	55                   	push   %ebp
80103960:	89 e5                	mov    %esp,%ebp
80103962:	83 ec 28             	sub    $0x28,%esp
  struct buf *buf = bread(log.dev, log.start);
80103965:	a1 b4 3a 11 80       	mov    0x80113ab4,%eax
8010396a:	89 c2                	mov    %eax,%edx
8010396c:	a1 c4 3a 11 80       	mov    0x80113ac4,%eax
80103971:	89 54 24 04          	mov    %edx,0x4(%esp)
80103975:	89 04 24             	mov    %eax,(%esp)
80103978:	e8 2a c8 ff ff       	call   801001a7 <bread>
8010397d:	89 45 ec             	mov    %eax,-0x14(%ebp)
  struct logheader *lh = (struct logheader *) (buf->data);
80103980:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103983:	83 c0 18             	add    $0x18,%eax
80103986:	89 45 f0             	mov    %eax,-0x10(%ebp)
  int i;
  log.lh.n = lh->n;
80103989:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010398c:	8b 00                	mov    (%eax),%eax
8010398e:	a3 c8 3a 11 80       	mov    %eax,0x80113ac8
  for (i = 0; i < log.lh.n; i++) {
80103993:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010399a:	eb 1b                	jmp    801039b7 <read_head+0x58>
    log.lh.block[i] = lh->block[i];
8010399c:	8b 4d f4             	mov    -0xc(%ebp),%ecx
8010399f:	8b 55 f4             	mov    -0xc(%ebp),%edx
801039a2:	8b 45 f0             	mov    -0x10(%ebp),%eax
801039a5:	8b 44 90 04          	mov    0x4(%eax,%edx,4),%eax
801039a9:	8d 51 10             	lea    0x10(%ecx),%edx
801039ac:	89 04 95 8c 3a 11 80 	mov    %eax,-0x7feec574(,%edx,4)
{
  struct buf *buf = bread(log.dev, log.start);
  struct logheader *lh = (struct logheader *) (buf->data);
  int i;
  log.lh.n = lh->n;
  for (i = 0; i < log.lh.n; i++) {
801039b3:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801039b7:	a1 c8 3a 11 80       	mov    0x80113ac8,%eax
801039bc:	3b 45 f4             	cmp    -0xc(%ebp),%eax
801039bf:	7f db                	jg     8010399c <read_head+0x3d>
    log.lh.block[i] = lh->block[i];
  }
  brelse(buf);
801039c1:	8b 45 ec             	mov    -0x14(%ebp),%eax
801039c4:	89 04 24             	mov    %eax,(%esp)
801039c7:	e8 4c c8 ff ff       	call   80100218 <brelse>
}
801039cc:	c9                   	leave  
801039cd:	c3                   	ret    

801039ce <write_head>:
// Write in-memory log header to disk.
// This is the true point at which the
// current transaction commits.
static void
write_head(void)
{
801039ce:	55                   	push   %ebp
801039cf:	89 e5                	mov    %esp,%ebp
801039d1:	83 ec 28             	sub    $0x28,%esp
  struct buf *buf = bread(log.dev, log.start);
801039d4:	a1 b4 3a 11 80       	mov    0x80113ab4,%eax
801039d9:	89 c2                	mov    %eax,%edx
801039db:	a1 c4 3a 11 80       	mov    0x80113ac4,%eax
801039e0:	89 54 24 04          	mov    %edx,0x4(%esp)
801039e4:	89 04 24             	mov    %eax,(%esp)
801039e7:	e8 bb c7 ff ff       	call   801001a7 <bread>
801039ec:	89 45 ec             	mov    %eax,-0x14(%ebp)
  struct logheader *hb = (struct logheader *) (buf->data);
801039ef:	8b 45 ec             	mov    -0x14(%ebp),%eax
801039f2:	83 c0 18             	add    $0x18,%eax
801039f5:	89 45 f0             	mov    %eax,-0x10(%ebp)
  int i;
  hb->n = log.lh.n;
801039f8:	8b 15 c8 3a 11 80    	mov    0x80113ac8,%edx
801039fe:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103a01:	89 10                	mov    %edx,(%eax)
  for (i = 0; i < log.lh.n; i++) {
80103a03:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80103a0a:	eb 1b                	jmp    80103a27 <write_head+0x59>
    hb->block[i] = log.lh.block[i];
80103a0c:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103a0f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103a12:	83 c0 10             	add    $0x10,%eax
80103a15:	8b 0c 85 8c 3a 11 80 	mov    -0x7feec574(,%eax,4),%ecx
80103a1c:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103a1f:	89 4c 90 04          	mov    %ecx,0x4(%eax,%edx,4)
{
  struct buf *buf = bread(log.dev, log.start);
  struct logheader *hb = (struct logheader *) (buf->data);
  int i;
  hb->n = log.lh.n;
  for (i = 0; i < log.lh.n; i++) {
80103a23:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80103a27:	a1 c8 3a 11 80       	mov    0x80113ac8,%eax
80103a2c:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103a2f:	7f db                	jg     80103a0c <write_head+0x3e>
    hb->block[i] = log.lh.block[i];
  }
  bwrite(buf);
80103a31:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103a34:	89 04 24             	mov    %eax,(%esp)
80103a37:	e8 a2 c7 ff ff       	call   801001de <bwrite>
  brelse(buf);
80103a3c:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103a3f:	89 04 24             	mov    %eax,(%esp)
80103a42:	e8 d1 c7 ff ff       	call   80100218 <brelse>
}
80103a47:	c9                   	leave  
80103a48:	c3                   	ret    

80103a49 <recover_from_log>:

static void
recover_from_log(void)
{
80103a49:	55                   	push   %ebp
80103a4a:	89 e5                	mov    %esp,%ebp
80103a4c:	83 ec 08             	sub    $0x8,%esp
  read_head();      
80103a4f:	e8 0b ff ff ff       	call   8010395f <read_head>
  install_trans(); // if committed, copy from log to disk
80103a54:	e8 5b fe ff ff       	call   801038b4 <install_trans>
  log.lh.n = 0;
80103a59:	c7 05 c8 3a 11 80 00 	movl   $0x0,0x80113ac8
80103a60:	00 00 00 
  write_head(); // clear the log
80103a63:	e8 66 ff ff ff       	call   801039ce <write_head>
}
80103a68:	c9                   	leave  
80103a69:	c3                   	ret    

80103a6a <begin_op>:

// called at the start of each FS system call.
void
begin_op(void)
{
80103a6a:	55                   	push   %ebp
80103a6b:	89 e5                	mov    %esp,%ebp
80103a6d:	83 ec 18             	sub    $0x18,%esp
  acquire(&log.lock);
80103a70:	c7 04 24 80 3a 11 80 	movl   $0x80113a80,(%esp)
80103a77:	e8 8b 1a 00 00       	call   80105507 <acquire>
  while(1){
    if(log.committing){
80103a7c:	a1 c0 3a 11 80       	mov    0x80113ac0,%eax
80103a81:	85 c0                	test   %eax,%eax
80103a83:	74 16                	je     80103a9b <begin_op+0x31>
      sleep(&log, &log.lock);
80103a85:	c7 44 24 04 80 3a 11 	movl   $0x80113a80,0x4(%esp)
80103a8c:	80 
80103a8d:	c7 04 24 80 3a 11 80 	movl   $0x80113a80,(%esp)
80103a94:	e8 95 17 00 00       	call   8010522e <sleep>
    } else {
      log.outstanding += 1;
      release(&log.lock);
      break;
    }
  }
80103a99:	eb e1                	jmp    80103a7c <begin_op+0x12>
{
  acquire(&log.lock);
  while(1){
    if(log.committing){
      sleep(&log, &log.lock);
    } else if(log.lh.n + (log.outstanding+1)*MAXOPBLOCKS > LOGSIZE){
80103a9b:	8b 0d c8 3a 11 80    	mov    0x80113ac8,%ecx
80103aa1:	a1 bc 3a 11 80       	mov    0x80113abc,%eax
80103aa6:	8d 50 01             	lea    0x1(%eax),%edx
80103aa9:	89 d0                	mov    %edx,%eax
80103aab:	c1 e0 02             	shl    $0x2,%eax
80103aae:	01 d0                	add    %edx,%eax
80103ab0:	01 c0                	add    %eax,%eax
80103ab2:	8d 04 01             	lea    (%ecx,%eax,1),%eax
80103ab5:	83 f8 1e             	cmp    $0x1e,%eax
80103ab8:	7e 16                	jle    80103ad0 <begin_op+0x66>
      // this op might exhaust log space; wait for commit.
      sleep(&log, &log.lock);
80103aba:	c7 44 24 04 80 3a 11 	movl   $0x80113a80,0x4(%esp)
80103ac1:	80 
80103ac2:	c7 04 24 80 3a 11 80 	movl   $0x80113a80,(%esp)
80103ac9:	e8 60 17 00 00       	call   8010522e <sleep>
    } else {
      log.outstanding += 1;
      release(&log.lock);
      break;
    }
  }
80103ace:	eb ac                	jmp    80103a7c <begin_op+0x12>
      sleep(&log, &log.lock);
    } else if(log.lh.n + (log.outstanding+1)*MAXOPBLOCKS > LOGSIZE){
      // this op might exhaust log space; wait for commit.
      sleep(&log, &log.lock);
    } else {
      log.outstanding += 1;
80103ad0:	a1 bc 3a 11 80       	mov    0x80113abc,%eax
80103ad5:	83 c0 01             	add    $0x1,%eax
80103ad8:	a3 bc 3a 11 80       	mov    %eax,0x80113abc
      release(&log.lock);
80103add:	c7 04 24 80 3a 11 80 	movl   $0x80113a80,(%esp)
80103ae4:	e8 87 1a 00 00       	call   80105570 <release>
      break;
80103ae9:	90                   	nop
    }
  }
}
80103aea:	c9                   	leave  
80103aeb:	c3                   	ret    

80103aec <end_op>:

// called at the end of each FS system call.
// commits if this was the last outstanding operation.
void
end_op(void)
{
80103aec:	55                   	push   %ebp
80103aed:	89 e5                	mov    %esp,%ebp
80103aef:	83 ec 28             	sub    $0x28,%esp
  int do_commit = 0;
80103af2:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

  acquire(&log.lock);
80103af9:	c7 04 24 80 3a 11 80 	movl   $0x80113a80,(%esp)
80103b00:	e8 02 1a 00 00       	call   80105507 <acquire>
  log.outstanding -= 1;
80103b05:	a1 bc 3a 11 80       	mov    0x80113abc,%eax
80103b0a:	83 e8 01             	sub    $0x1,%eax
80103b0d:	a3 bc 3a 11 80       	mov    %eax,0x80113abc
  if(log.committing)
80103b12:	a1 c0 3a 11 80       	mov    0x80113ac0,%eax
80103b17:	85 c0                	test   %eax,%eax
80103b19:	74 0c                	je     80103b27 <end_op+0x3b>
    panic("log.committing");
80103b1b:	c7 04 24 48 8e 10 80 	movl   $0x80108e48,(%esp)
80103b22:	e8 42 cd ff ff       	call   80100869 <panic>
  if(log.outstanding == 0){
80103b27:	a1 bc 3a 11 80       	mov    0x80113abc,%eax
80103b2c:	85 c0                	test   %eax,%eax
80103b2e:	75 13                	jne    80103b43 <end_op+0x57>
    do_commit = 1;
80103b30:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
    log.committing = 1;
80103b37:	c7 05 c0 3a 11 80 01 	movl   $0x1,0x80113ac0
80103b3e:	00 00 00 
80103b41:	eb 0c                	jmp    80103b4f <end_op+0x63>
  } else {
    // begin_op() may be waiting for log space.
    wakeup(&log);
80103b43:	c7 04 24 80 3a 11 80 	movl   $0x80113a80,(%esp)
80103b4a:	e8 b9 17 00 00       	call   80105308 <wakeup>
  }
  release(&log.lock);
80103b4f:	c7 04 24 80 3a 11 80 	movl   $0x80113a80,(%esp)
80103b56:	e8 15 1a 00 00       	call   80105570 <release>

  if(do_commit){
80103b5b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80103b5f:	74 33                	je     80103b94 <end_op+0xa8>
    // call commit w/o holding locks, since not allowed
    // to sleep with locks.
    commit();
80103b61:	e8 db 00 00 00       	call   80103c41 <commit>
    acquire(&log.lock);
80103b66:	c7 04 24 80 3a 11 80 	movl   $0x80113a80,(%esp)
80103b6d:	e8 95 19 00 00       	call   80105507 <acquire>
    log.committing = 0;
80103b72:	c7 05 c0 3a 11 80 00 	movl   $0x0,0x80113ac0
80103b79:	00 00 00 
    wakeup(&log);
80103b7c:	c7 04 24 80 3a 11 80 	movl   $0x80113a80,(%esp)
80103b83:	e8 80 17 00 00       	call   80105308 <wakeup>
    release(&log.lock);
80103b88:	c7 04 24 80 3a 11 80 	movl   $0x80113a80,(%esp)
80103b8f:	e8 dc 19 00 00       	call   80105570 <release>
  }
}
80103b94:	c9                   	leave  
80103b95:	c3                   	ret    

80103b96 <write_log>:

// Copy modified blocks from cache to log.
static void 
write_log(void)
{
80103b96:	55                   	push   %ebp
80103b97:	89 e5                	mov    %esp,%ebp
80103b99:	83 ec 28             	sub    $0x28,%esp
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
80103b9c:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80103ba3:	e9 89 00 00 00       	jmp    80103c31 <write_log+0x9b>
    struct buf *to = bread(log.dev, log.start+tail+1); // log block
80103ba8:	a1 b4 3a 11 80       	mov    0x80113ab4,%eax
80103bad:	03 45 ec             	add    -0x14(%ebp),%eax
80103bb0:	83 c0 01             	add    $0x1,%eax
80103bb3:	89 c2                	mov    %eax,%edx
80103bb5:	a1 c4 3a 11 80       	mov    0x80113ac4,%eax
80103bba:	89 54 24 04          	mov    %edx,0x4(%esp)
80103bbe:	89 04 24             	mov    %eax,(%esp)
80103bc1:	e8 e1 c5 ff ff       	call   801001a7 <bread>
80103bc6:	89 45 f0             	mov    %eax,-0x10(%ebp)
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
80103bc9:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103bcc:	83 c0 10             	add    $0x10,%eax
80103bcf:	8b 04 85 8c 3a 11 80 	mov    -0x7feec574(,%eax,4),%eax
80103bd6:	89 c2                	mov    %eax,%edx
80103bd8:	a1 c4 3a 11 80       	mov    0x80113ac4,%eax
80103bdd:	89 54 24 04          	mov    %edx,0x4(%esp)
80103be1:	89 04 24             	mov    %eax,(%esp)
80103be4:	e8 be c5 ff ff       	call   801001a7 <bread>
80103be9:	89 45 f4             	mov    %eax,-0xc(%ebp)
    memmove(to->data, from->data, BSIZE);
80103bec:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103bef:	8d 50 18             	lea    0x18(%eax),%edx
80103bf2:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103bf5:	83 c0 18             	add    $0x18,%eax
80103bf8:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
80103bff:	00 
80103c00:	89 54 24 04          	mov    %edx,0x4(%esp)
80103c04:	89 04 24             	mov    %eax,(%esp)
80103c07:	e8 31 1c 00 00       	call   8010583d <memmove>
    bwrite(to);  // write the log
80103c0c:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103c0f:	89 04 24             	mov    %eax,(%esp)
80103c12:	e8 c7 c5 ff ff       	call   801001de <bwrite>
    brelse(from); 
80103c17:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103c1a:	89 04 24             	mov    %eax,(%esp)
80103c1d:	e8 f6 c5 ff ff       	call   80100218 <brelse>
    brelse(to);
80103c22:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103c25:	89 04 24             	mov    %eax,(%esp)
80103c28:	e8 eb c5 ff ff       	call   80100218 <brelse>
static void 
write_log(void)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
80103c2d:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
80103c31:	a1 c8 3a 11 80       	mov    0x80113ac8,%eax
80103c36:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80103c39:	0f 8f 69 ff ff ff    	jg     80103ba8 <write_log+0x12>
    memmove(to->data, from->data, BSIZE);
    bwrite(to);  // write the log
    brelse(from); 
    brelse(to);
  }
}
80103c3f:	c9                   	leave  
80103c40:	c3                   	ret    

80103c41 <commit>:

static void
commit()
{
80103c41:	55                   	push   %ebp
80103c42:	89 e5                	mov    %esp,%ebp
80103c44:	83 ec 08             	sub    $0x8,%esp
  if (log.lh.n > 0) {
80103c47:	a1 c8 3a 11 80       	mov    0x80113ac8,%eax
80103c4c:	85 c0                	test   %eax,%eax
80103c4e:	7e 1e                	jle    80103c6e <commit+0x2d>
    write_log();     // Write modified blocks from cache to log
80103c50:	e8 41 ff ff ff       	call   80103b96 <write_log>
    write_head();    // Write header to disk -- the real commit
80103c55:	e8 74 fd ff ff       	call   801039ce <write_head>
    install_trans(); // Now install writes to home locations
80103c5a:	e8 55 fc ff ff       	call   801038b4 <install_trans>
    log.lh.n = 0; 
80103c5f:	c7 05 c8 3a 11 80 00 	movl   $0x0,0x80113ac8
80103c66:	00 00 00 
    write_head();    // Erase the transaction from the log
80103c69:	e8 60 fd ff ff       	call   801039ce <write_head>
  }
}
80103c6e:	c9                   	leave  
80103c6f:	c3                   	ret    

80103c70 <log_write>:
//   modify bp->data[]
//   log_write(bp)
//   brelse(bp)
void
log_write(struct buf *b)
{
80103c70:	55                   	push   %ebp
80103c71:	89 e5                	mov    %esp,%ebp
80103c73:	83 ec 28             	sub    $0x28,%esp
  int i;

  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
80103c76:	a1 c8 3a 11 80       	mov    0x80113ac8,%eax
80103c7b:	83 f8 1d             	cmp    $0x1d,%eax
80103c7e:	7f 12                	jg     80103c92 <log_write+0x22>
80103c80:	a1 c8 3a 11 80       	mov    0x80113ac8,%eax
80103c85:	8b 15 b8 3a 11 80    	mov    0x80113ab8,%edx
80103c8b:	83 ea 01             	sub    $0x1,%edx
80103c8e:	39 d0                	cmp    %edx,%eax
80103c90:	7c 0c                	jl     80103c9e <log_write+0x2e>
    panic("too big a transaction");
80103c92:	c7 04 24 57 8e 10 80 	movl   $0x80108e57,(%esp)
80103c99:	e8 cb cb ff ff       	call   80100869 <panic>
  if (log.outstanding < 1)
80103c9e:	a1 bc 3a 11 80       	mov    0x80113abc,%eax
80103ca3:	85 c0                	test   %eax,%eax
80103ca5:	7f 0c                	jg     80103cb3 <log_write+0x43>
    panic("log_write outside of trans");
80103ca7:	c7 04 24 6d 8e 10 80 	movl   $0x80108e6d,(%esp)
80103cae:	e8 b6 cb ff ff       	call   80100869 <panic>

  acquire(&log.lock);
80103cb3:	c7 04 24 80 3a 11 80 	movl   $0x80113a80,(%esp)
80103cba:	e8 48 18 00 00       	call   80105507 <acquire>
  for (i = 0; i < log.lh.n; i++) {
80103cbf:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80103cc6:	eb 1d                	jmp    80103ce5 <log_write+0x75>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
80103cc8:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103ccb:	83 c0 10             	add    $0x10,%eax
80103cce:	8b 04 85 8c 3a 11 80 	mov    -0x7feec574(,%eax,4),%eax
80103cd5:	89 c2                	mov    %eax,%edx
80103cd7:	8b 45 08             	mov    0x8(%ebp),%eax
80103cda:	8b 40 08             	mov    0x8(%eax),%eax
80103cdd:	39 c2                	cmp    %eax,%edx
80103cdf:	74 10                	je     80103cf1 <log_write+0x81>
    panic("too big a transaction");
  if (log.outstanding < 1)
    panic("log_write outside of trans");

  acquire(&log.lock);
  for (i = 0; i < log.lh.n; i++) {
80103ce1:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80103ce5:	a1 c8 3a 11 80       	mov    0x80113ac8,%eax
80103cea:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103ced:	7f d9                	jg     80103cc8 <log_write+0x58>
80103cef:	eb 01                	jmp    80103cf2 <log_write+0x82>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
      break;
80103cf1:	90                   	nop
  }
  log.lh.block[i] = b->blockno;
80103cf2:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103cf5:	8b 45 08             	mov    0x8(%ebp),%eax
80103cf8:	8b 40 08             	mov    0x8(%eax),%eax
80103cfb:	83 c2 10             	add    $0x10,%edx
80103cfe:	89 04 95 8c 3a 11 80 	mov    %eax,-0x7feec574(,%edx,4)
  if (i == log.lh.n)
80103d05:	a1 c8 3a 11 80       	mov    0x80113ac8,%eax
80103d0a:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103d0d:	75 0d                	jne    80103d1c <log_write+0xac>
    log.lh.n++;
80103d0f:	a1 c8 3a 11 80       	mov    0x80113ac8,%eax
80103d14:	83 c0 01             	add    $0x1,%eax
80103d17:	a3 c8 3a 11 80       	mov    %eax,0x80113ac8
  b->flags |= B_DIRTY; // prevent eviction
80103d1c:	8b 45 08             	mov    0x8(%ebp),%eax
80103d1f:	8b 00                	mov    (%eax),%eax
80103d21:	89 c2                	mov    %eax,%edx
80103d23:	83 ca 04             	or     $0x4,%edx
80103d26:	8b 45 08             	mov    0x8(%ebp),%eax
80103d29:	89 10                	mov    %edx,(%eax)
  release(&log.lock);
80103d2b:	c7 04 24 80 3a 11 80 	movl   $0x80113a80,(%esp)
80103d32:	e8 39 18 00 00       	call   80105570 <release>
}
80103d37:	c9                   	leave  
80103d38:	c3                   	ret    
80103d39:	00 00                	add    %al,(%eax)
	...

80103d3c <xchg>:
  asm volatile("sti");
}

static inline uint
xchg(volatile uint *addr, uint newval)
{
80103d3c:	55                   	push   %ebp
80103d3d:	89 e5                	mov    %esp,%ebp
80103d3f:	83 ec 10             	sub    $0x10,%esp
  uint result;

  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
80103d42:	8b 55 08             	mov    0x8(%ebp),%edx
80103d45:	8b 45 0c             	mov    0xc(%ebp),%eax
80103d48:	8b 4d 08             	mov    0x8(%ebp),%ecx
80103d4b:	f0 87 02             	lock xchg %eax,(%edx)
80103d4e:	89 45 fc             	mov    %eax,-0x4(%ebp)
               "+m" (*addr), "=a" (result) :
               "1" (newval) :
               "cc");
  return result;
80103d51:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80103d54:	c9                   	leave  
80103d55:	c3                   	ret    

80103d56 <main>:
// Bootstrap processor starts running C code here.
// Allocate a real stack and switch to it, first
// doing some setup required for memory allocator to work.
int
main(void)
{
80103d56:	55                   	push   %ebp
80103d57:	89 e5                	mov    %esp,%ebp
80103d59:	83 e4 f0             	and    $0xfffffff0,%esp
80103d5c:	83 ec 10             	sub    $0x10,%esp
  debuginit();     // enable debug output first
80103d5f:	e8 1f d2 ff ff       	call   80100f83 <debuginit>
  kinit1(end, P2V(4*1024*1024)); // phys page allocator
80103d64:	c7 44 24 04 00 00 40 	movl   $0x80400000,0x4(%esp)
80103d6b:	80 
80103d6c:	c7 04 24 f8 60 11 80 	movl   $0x801160f8,(%esp)
80103d73:	e8 f8 f4 ff ff       	call   80103270 <kinit1>
  kvmalloc();      // kernel page table
80103d78:	e8 bd 45 00 00       	call   8010833a <kvmalloc>
  mpinit();        // collect info about this machine
80103d7d:	e8 0f 04 00 00       	call   80104191 <mpinit>
  lapicinit();
80103d82:	e8 17 f8 ff ff       	call   8010359e <lapicinit>
  seginit();       // set up segments
80103d87:	e8 77 3f 00 00       	call   80107d03 <seginit>
  cprintf("\ncpu%d: starting xv6\n\n", cpu->id);
80103d8c:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80103d92:	0f b6 00             	movzbl (%eax),%eax
80103d95:	0f b6 c0             	movzbl %al,%eax
80103d98:	89 44 24 04          	mov    %eax,0x4(%esp)
80103d9c:	c7 04 24 88 8e 10 80 	movl   $0x80108e88,(%esp)
80103da3:	e8 21 c9 ff ff       	call   801006c9 <cprintf>
  picinit();       // interrupt controller
80103da8:	e8 25 07 00 00       	call   801044d2 <picinit>
  ioapicinit();    // another interrupt controller
80103dad:	e8 77 f3 ff ff       	call   80103129 <ioapicinit>
  consoleinit();   // I/O devices & their interrupts
80103db2:	e8 37 d0 ff ff       	call   80100dee <consoleinit>
  uartinit();      // serial port
80103db7:	e8 8f 32 00 00       	call   8010704b <uartinit>
  pinit();         // process table
80103dbc:	e8 21 0c 00 00       	call   801049e2 <pinit>
  tvinit();        // trap vectors
80103dc1:	e8 26 2e 00 00       	call   80106bec <tvinit>
  binit();         // buffer cache
80103dc6:	e8 69 c2 ff ff       	call   80100034 <binit>
  fileinit();      // file table
80103dcb:	e8 cc d8 ff ff       	call   8010169c <fileinit>
  ideinit();       // disk
80103dd0:	e8 76 ef ff ff       	call   80102d4b <ideinit>
  if(!ismp)
80103dd5:	a1 64 3b 11 80       	mov    0x80113b64,%eax
80103dda:	85 c0                	test   %eax,%eax
80103ddc:	75 05                	jne    80103de3 <main+0x8d>
    timerinit();   // uniprocessor timer
80103dde:	e8 4b 2d 00 00       	call   80106b2e <timerinit>
  startothers();   // start other processors
80103de3:	e8 7f 00 00 00       	call   80103e67 <startothers>
  kinit2(P2V(4*1024*1024), P2V(PHYSTOP)); // must come after startothers()
80103de8:	c7 44 24 04 00 00 00 	movl   $0x8e000000,0x4(%esp)
80103def:	8e 
80103df0:	c7 04 24 00 00 40 80 	movl   $0x80400000,(%esp)
80103df7:	e8 ac f4 ff ff       	call   801032a8 <kinit2>
  userinit();      // first user process
80103dfc:	e8 fd 0c 00 00       	call   80104afe <userinit>
  // Finish setting up this processor in mpmain.
  mpmain();
80103e01:	e8 1a 00 00 00       	call   80103e20 <mpmain>

80103e06 <mpenter>:
}

// Other CPUs jump here from entryother.S.
static void
mpenter(void)
{
80103e06:	55                   	push   %ebp
80103e07:	89 e5                	mov    %esp,%ebp
80103e09:	83 ec 08             	sub    $0x8,%esp
  switchkvm();
80103e0c:	e8 55 45 00 00       	call   80108366 <switchkvm>
  seginit();
80103e11:	e8 ed 3e 00 00       	call   80107d03 <seginit>
  lapicinit();
80103e16:	e8 83 f7 ff ff       	call   8010359e <lapicinit>
  mpmain();
80103e1b:	e8 00 00 00 00       	call   80103e20 <mpmain>

80103e20 <mpmain>:
}

// Common CPU setup code.
static void
mpmain(void)
{
80103e20:	55                   	push   %ebp
80103e21:	89 e5                	mov    %esp,%ebp
80103e23:	83 ec 18             	sub    $0x18,%esp
  cprintf("cpu%d: starting\n", cpu->id);
80103e26:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80103e2c:	0f b6 00             	movzbl (%eax),%eax
80103e2f:	0f b6 c0             	movzbl %al,%eax
80103e32:	89 44 24 04          	mov    %eax,0x4(%esp)
80103e36:	c7 04 24 9f 8e 10 80 	movl   $0x80108e9f,(%esp)
80103e3d:	e8 87 c8 ff ff       	call   801006c9 <cprintf>
  idtinit();       // load idt register
80103e42:	e8 15 2f 00 00       	call   80106d5c <idtinit>
  xchg(&cpu->started, 1); // tell startothers() we're up
80103e47:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80103e4d:	05 a8 00 00 00       	add    $0xa8,%eax
80103e52:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80103e59:	00 
80103e5a:	89 04 24             	mov    %eax,(%esp)
80103e5d:	e8 da fe ff ff       	call   80103d3c <xchg>
  scheduler();     // start running processes
80103e62:	e8 0a 12 00 00       	call   80105071 <scheduler>

80103e67 <startothers>:
pde_t entrypgdir[];  // For entry.S

// Start the non-boot (AP) processors.
static void
startothers(void)
{
80103e67:	55                   	push   %ebp
80103e68:	89 e5                	mov    %esp,%ebp
80103e6a:	83 ec 28             	sub    $0x28,%esp
  char *stack;

  // Write entry code to unused memory at 0x7000.
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = P2V(0x7000);
80103e6d:	c7 45 ec 00 70 00 80 	movl   $0x80007000,-0x14(%ebp)
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);
80103e74:	b8 86 00 00 00       	mov    $0x86,%eax
80103e79:	89 44 24 08          	mov    %eax,0x8(%esp)
80103e7d:	c7 44 24 04 0c c5 10 	movl   $0x8010c50c,0x4(%esp)
80103e84:	80 
80103e85:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103e88:	89 04 24             	mov    %eax,(%esp)
80103e8b:	e8 ad 19 00 00       	call   8010583d <memmove>

  for(c = cpus; c < cpus+ncpu; c++){
80103e90:	c7 45 f0 80 3b 11 80 	movl   $0x80113b80,-0x10(%ebp)
80103e97:	e9 93 00 00 00       	jmp    80103f2f <startothers+0xc8>
    if(c == cpus+cpunum())  // We've started already.
80103e9c:	e8 59 f8 ff ff       	call   801036fa <cpunum>
80103ea1:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
80103ea7:	05 80 3b 11 80       	add    $0x80113b80,%eax
80103eac:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80103eaf:	74 76                	je     80103f27 <startothers+0xc0>
      continue;

    // Tell entryother.S what stack to use, where to enter, and what
    // pgdir to use. We cannot use kpgdir yet, because the AP processor
    // is running in low  memory, so we use entrypgdir for the APs too.
    if((stack = kalloc()) == 0)
80103eb1:	e8 e8 f4 ff ff       	call   8010339e <kalloc>
80103eb6:	89 45 f4             	mov    %eax,-0xc(%ebp)
80103eb9:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80103ebd:	75 0c                	jne    80103ecb <startothers+0x64>
      panic("startothers: failed to allocate stack");
80103ebf:	c7 04 24 b0 8e 10 80 	movl   $0x80108eb0,(%esp)
80103ec6:	e8 9e c9 ff ff       	call   80100869 <panic>
    *(void**)(code-4) = stack + KSTACKSIZE;
80103ecb:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103ece:	83 e8 04             	sub    $0x4,%eax
80103ed1:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103ed4:	81 c2 00 10 00 00    	add    $0x1000,%edx
80103eda:	89 10                	mov    %edx,(%eax)
    *(void**)(code-8) = mpenter;
80103edc:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103edf:	83 e8 08             	sub    $0x8,%eax
80103ee2:	c7 00 06 3e 10 80    	movl   $0x80103e06,(%eax)
    *(int**)(code-12) = (void *) V2P(entrypgdir);
80103ee8:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103eeb:	8d 50 f4             	lea    -0xc(%eax),%edx
80103eee:	b8 00 b0 10 80       	mov    $0x8010b000,%eax
80103ef3:	2d 00 00 00 80       	sub    $0x80000000,%eax
80103ef8:	89 02                	mov    %eax,(%edx)

    lapicstartap(c->id, V2P(code));
80103efa:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103efd:	8d 90 00 00 00 80    	lea    -0x80000000(%eax),%edx
80103f03:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103f06:	0f b6 00             	movzbl (%eax),%eax
80103f09:	0f b6 c0             	movzbl %al,%eax
80103f0c:	89 54 24 04          	mov    %edx,0x4(%esp)
80103f10:	89 04 24             	mov    %eax,(%esp)
80103f13:	e8 68 f8 ff ff       	call   80103780 <lapicstartap>

    // wait for cpu to finish mpmain()
    while(c->started == 0)
80103f18:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103f1b:	8b 80 a8 00 00 00    	mov    0xa8(%eax),%eax
80103f21:	85 c0                	test   %eax,%eax
80103f23:	74 f3                	je     80103f18 <startothers+0xb1>
80103f25:	eb 01                	jmp    80103f28 <startothers+0xc1>
  code = P2V(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);

  for(c = cpus; c < cpus+ncpu; c++){
    if(c == cpus+cpunum())  // We've started already.
      continue;
80103f27:	90                   	nop
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = P2V(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);

  for(c = cpus; c < cpus+ncpu; c++){
80103f28:	81 45 f0 bc 00 00 00 	addl   $0xbc,-0x10(%ebp)
80103f2f:	a1 60 41 11 80       	mov    0x80114160,%eax
80103f34:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
80103f3a:	05 80 3b 11 80       	add    $0x80113b80,%eax
80103f3f:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80103f42:	0f 87 54 ff ff ff    	ja     80103e9c <startothers+0x35>

    // wait for cpu to finish mpmain()
    while(c->started == 0)
      ;
  }
}
80103f48:	c9                   	leave  
80103f49:	c3                   	ret    
	...

80103f4c <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
80103f4c:	55                   	push   %ebp
80103f4d:	89 e5                	mov    %esp,%ebp
80103f4f:	83 ec 14             	sub    $0x14,%esp
80103f52:	8b 45 08             	mov    0x8(%ebp),%eax
80103f55:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80103f59:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
80103f5d:	89 c2                	mov    %eax,%edx
80103f5f:	ec                   	in     (%dx),%al
80103f60:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80103f63:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80103f67:	c9                   	leave  
80103f68:	c3                   	ret    

80103f69 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80103f69:	55                   	push   %ebp
80103f6a:	89 e5                	mov    %esp,%ebp
80103f6c:	83 ec 08             	sub    $0x8,%esp
80103f6f:	8b 55 08             	mov    0x8(%ebp),%edx
80103f72:	8b 45 0c             	mov    0xc(%ebp),%eax
80103f75:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80103f79:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80103f7c:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80103f80:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80103f84:	ee                   	out    %al,(%dx)
}
80103f85:	c9                   	leave  
80103f86:	c3                   	ret    

80103f87 <sum>:
int ncpu;
uchar ioapicid;

static uchar
sum(uchar *addr, int len)
{
80103f87:	55                   	push   %ebp
80103f88:	89 e5                	mov    %esp,%ebp
80103f8a:	83 ec 10             	sub    $0x10,%esp
  int i, sum;

  sum = 0;
80103f8d:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  for(i=0; i<len; i++)
80103f94:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)
80103f9b:	eb 13                	jmp    80103fb0 <sum+0x29>
    sum += addr[i];
80103f9d:	8b 45 f8             	mov    -0x8(%ebp),%eax
80103fa0:	03 45 08             	add    0x8(%ebp),%eax
80103fa3:	0f b6 00             	movzbl (%eax),%eax
80103fa6:	0f b6 c0             	movzbl %al,%eax
80103fa9:	01 45 fc             	add    %eax,-0x4(%ebp)
sum(uchar *addr, int len)
{
  int i, sum;

  sum = 0;
  for(i=0; i<len; i++)
80103fac:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
80103fb0:	8b 45 f8             	mov    -0x8(%ebp),%eax
80103fb3:	3b 45 0c             	cmp    0xc(%ebp),%eax
80103fb6:	7c e5                	jl     80103f9d <sum+0x16>
    sum += addr[i];
  return sum;
80103fb8:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80103fbb:	c9                   	leave  
80103fbc:	c3                   	ret    

80103fbd <mpsearch1>:

// Look for an MP structure in the len bytes at addr.
static struct mp*
mpsearch1(uint a, int len)
{
80103fbd:	55                   	push   %ebp
80103fbe:	89 e5                	mov    %esp,%ebp
80103fc0:	83 ec 28             	sub    $0x28,%esp
  uchar *e, *p, *addr;

  addr = P2V(a);
80103fc3:	8b 45 08             	mov    0x8(%ebp),%eax
80103fc6:	2d 00 00 00 80       	sub    $0x80000000,%eax
80103fcb:	89 45 f4             	mov    %eax,-0xc(%ebp)
  e = addr+len;
80103fce:	8b 45 0c             	mov    0xc(%ebp),%eax
80103fd1:	03 45 f4             	add    -0xc(%ebp),%eax
80103fd4:	89 45 ec             	mov    %eax,-0x14(%ebp)
  for(p = addr; p < e; p += sizeof(struct mp))
80103fd7:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103fda:	89 45 f0             	mov    %eax,-0x10(%ebp)
80103fdd:	eb 3f                	jmp    8010401e <mpsearch1+0x61>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
80103fdf:	c7 44 24 08 04 00 00 	movl   $0x4,0x8(%esp)
80103fe6:	00 
80103fe7:	c7 44 24 04 d8 8e 10 	movl   $0x80108ed8,0x4(%esp)
80103fee:	80 
80103fef:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103ff2:	89 04 24             	mov    %eax,(%esp)
80103ff5:	e8 e7 17 00 00       	call   801057e1 <memcmp>
80103ffa:	85 c0                	test   %eax,%eax
80103ffc:	75 1c                	jne    8010401a <mpsearch1+0x5d>
80103ffe:	c7 44 24 04 10 00 00 	movl   $0x10,0x4(%esp)
80104005:	00 
80104006:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104009:	89 04 24             	mov    %eax,(%esp)
8010400c:	e8 76 ff ff ff       	call   80103f87 <sum>
80104011:	84 c0                	test   %al,%al
80104013:	75 05                	jne    8010401a <mpsearch1+0x5d>
      return (struct mp*)p;
80104015:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104018:	eb 11                	jmp    8010402b <mpsearch1+0x6e>
{
  uchar *e, *p, *addr;

  addr = P2V(a);
  e = addr+len;
  for(p = addr; p < e; p += sizeof(struct mp))
8010401a:	83 45 f0 10          	addl   $0x10,-0x10(%ebp)
8010401e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104021:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80104024:	72 b9                	jb     80103fdf <mpsearch1+0x22>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
      return (struct mp*)p;
  return 0;
80104026:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010402b:	c9                   	leave  
8010402c:	c3                   	ret    

8010402d <mpsearch>:
// 1) in the first KB of the EBDA;
// 2) in the last KB of system base memory;
// 3) in the BIOS ROM between 0xF0000 and 0xFFFFF.
static struct mp*
mpsearch(void)
{
8010402d:	55                   	push   %ebp
8010402e:	89 e5                	mov    %esp,%ebp
80104030:	83 ec 28             	sub    $0x28,%esp
  uchar *bda;
  uint p;
  struct mp *mp;

  bda = (uchar *) P2V(0x400);
80104033:	c7 45 ec 00 04 00 80 	movl   $0x80000400,-0x14(%ebp)
  if((p = ((bda[0x0F]<<8)| bda[0x0E]) << 4)){
8010403a:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010403d:	83 c0 0f             	add    $0xf,%eax
80104040:	0f b6 00             	movzbl (%eax),%eax
80104043:	0f b6 c0             	movzbl %al,%eax
80104046:	89 c2                	mov    %eax,%edx
80104048:	c1 e2 08             	shl    $0x8,%edx
8010404b:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010404e:	83 c0 0e             	add    $0xe,%eax
80104051:	0f b6 00             	movzbl (%eax),%eax
80104054:	0f b6 c0             	movzbl %al,%eax
80104057:	09 d0                	or     %edx,%eax
80104059:	c1 e0 04             	shl    $0x4,%eax
8010405c:	89 45 f0             	mov    %eax,-0x10(%ebp)
8010405f:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80104063:	74 21                	je     80104086 <mpsearch+0x59>
    if((mp = mpsearch1(p, 1024)))
80104065:	c7 44 24 04 00 04 00 	movl   $0x400,0x4(%esp)
8010406c:	00 
8010406d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104070:	89 04 24             	mov    %eax,(%esp)
80104073:	e8 45 ff ff ff       	call   80103fbd <mpsearch1>
80104078:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010407b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010407f:	74 50                	je     801040d1 <mpsearch+0xa4>
      return mp;
80104081:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104084:	eb 5f                	jmp    801040e5 <mpsearch+0xb8>
  } else {
    p = ((bda[0x14]<<8)|bda[0x13])*1024;
80104086:	8b 45 ec             	mov    -0x14(%ebp),%eax
80104089:	83 c0 14             	add    $0x14,%eax
8010408c:	0f b6 00             	movzbl (%eax),%eax
8010408f:	0f b6 c0             	movzbl %al,%eax
80104092:	89 c2                	mov    %eax,%edx
80104094:	c1 e2 08             	shl    $0x8,%edx
80104097:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010409a:	83 c0 13             	add    $0x13,%eax
8010409d:	0f b6 00             	movzbl (%eax),%eax
801040a0:	0f b6 c0             	movzbl %al,%eax
801040a3:	09 d0                	or     %edx,%eax
801040a5:	c1 e0 0a             	shl    $0xa,%eax
801040a8:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if((mp = mpsearch1(p-1024, 1024)))
801040ab:	8b 45 f0             	mov    -0x10(%ebp),%eax
801040ae:	2d 00 04 00 00       	sub    $0x400,%eax
801040b3:	c7 44 24 04 00 04 00 	movl   $0x400,0x4(%esp)
801040ba:	00 
801040bb:	89 04 24             	mov    %eax,(%esp)
801040be:	e8 fa fe ff ff       	call   80103fbd <mpsearch1>
801040c3:	89 45 f4             	mov    %eax,-0xc(%ebp)
801040c6:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801040ca:	74 05                	je     801040d1 <mpsearch+0xa4>
      return mp;
801040cc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801040cf:	eb 14                	jmp    801040e5 <mpsearch+0xb8>
  }
  return mpsearch1(0xF0000, 0x10000);
801040d1:	c7 44 24 04 00 00 01 	movl   $0x10000,0x4(%esp)
801040d8:	00 
801040d9:	c7 04 24 00 00 0f 00 	movl   $0xf0000,(%esp)
801040e0:	e8 d8 fe ff ff       	call   80103fbd <mpsearch1>
}
801040e5:	c9                   	leave  
801040e6:	c3                   	ret    

801040e7 <mpconfig>:
// Check for correct signature, calculate the checksum and,
// if correct, check the version.
// To do: check extended table checksum.
static struct mpconf*
mpconfig(struct mp **pmp)
{
801040e7:	55                   	push   %ebp
801040e8:	89 e5                	mov    %esp,%ebp
801040ea:	83 ec 28             	sub    $0x28,%esp
  struct mpconf *conf;
  struct mp *mp;

  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
801040ed:	e8 3b ff ff ff       	call   8010402d <mpsearch>
801040f2:	89 45 f4             	mov    %eax,-0xc(%ebp)
801040f5:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801040f9:	74 0a                	je     80104105 <mpconfig+0x1e>
801040fb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801040fe:	8b 40 04             	mov    0x4(%eax),%eax
80104101:	85 c0                	test   %eax,%eax
80104103:	75 0a                	jne    8010410f <mpconfig+0x28>
    return 0;
80104105:	b8 00 00 00 00       	mov    $0x0,%eax
8010410a:	e9 80 00 00 00       	jmp    8010418f <mpconfig+0xa8>
  conf = (struct mpconf*) P2V((uint) mp->physaddr);
8010410f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104112:	8b 40 04             	mov    0x4(%eax),%eax
80104115:	2d 00 00 00 80       	sub    $0x80000000,%eax
8010411a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(memcmp(conf, "PCMP", 4) != 0)
8010411d:	c7 44 24 08 04 00 00 	movl   $0x4,0x8(%esp)
80104124:	00 
80104125:	c7 44 24 04 dd 8e 10 	movl   $0x80108edd,0x4(%esp)
8010412c:	80 
8010412d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104130:	89 04 24             	mov    %eax,(%esp)
80104133:	e8 a9 16 00 00       	call   801057e1 <memcmp>
80104138:	85 c0                	test   %eax,%eax
8010413a:	74 07                	je     80104143 <mpconfig+0x5c>
    return 0;
8010413c:	b8 00 00 00 00       	mov    $0x0,%eax
80104141:	eb 4c                	jmp    8010418f <mpconfig+0xa8>
  if(conf->specrev != 1 && conf->specrev != 4)
80104143:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104146:	0f b6 40 06          	movzbl 0x6(%eax),%eax
8010414a:	3c 01                	cmp    $0x1,%al
8010414c:	74 12                	je     80104160 <mpconfig+0x79>
8010414e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104151:	0f b6 40 06          	movzbl 0x6(%eax),%eax
80104155:	3c 04                	cmp    $0x4,%al
80104157:	74 07                	je     80104160 <mpconfig+0x79>
    return 0;
80104159:	b8 00 00 00 00       	mov    $0x0,%eax
8010415e:	eb 2f                	jmp    8010418f <mpconfig+0xa8>
  if(sum((uchar*)conf, conf->length) != 0)
80104160:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104163:	0f b7 40 04          	movzwl 0x4(%eax),%eax
80104167:	0f b7 d0             	movzwl %ax,%edx
8010416a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010416d:	89 54 24 04          	mov    %edx,0x4(%esp)
80104171:	89 04 24             	mov    %eax,(%esp)
80104174:	e8 0e fe ff ff       	call   80103f87 <sum>
80104179:	84 c0                	test   %al,%al
8010417b:	74 07                	je     80104184 <mpconfig+0x9d>
    return 0;
8010417d:	b8 00 00 00 00       	mov    $0x0,%eax
80104182:	eb 0b                	jmp    8010418f <mpconfig+0xa8>
  *pmp = mp;
80104184:	8b 45 08             	mov    0x8(%ebp),%eax
80104187:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010418a:	89 10                	mov    %edx,(%eax)
  return conf;
8010418c:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
8010418f:	c9                   	leave  
80104190:	c3                   	ret    

80104191 <mpinit>:

void
mpinit(void)
{
80104191:	55                   	push   %ebp
80104192:	89 e5                	mov    %esp,%ebp
80104194:	83 ec 38             	sub    $0x38,%esp
  struct mp *mp;
  struct mpconf *conf;
  struct mpproc *proc;
  struct mpioapic *mioapic;

  if((conf = mpconfig(&mp)) == 0)
80104197:	8d 45 e0             	lea    -0x20(%ebp),%eax
8010419a:	89 04 24             	mov    %eax,(%esp)
8010419d:	e8 45 ff ff ff       	call   801040e7 <mpconfig>
801041a2:	89 45 ec             	mov    %eax,-0x14(%ebp)
801041a5:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
801041a9:	0f 84 80 02 00 00    	je     8010442f <mpinit+0x29e>
    return;
  ismp = 1;
801041af:	c7 05 64 3b 11 80 01 	movl   $0x1,0x80113b64
801041b6:	00 00 00 
  lapic = (uint*)conf->lapicaddr;
801041b9:	8b 45 ec             	mov    -0x14(%ebp),%eax
801041bc:	8b 40 24             	mov    0x24(%eax),%eax
801041bf:	a3 7c 3a 11 80       	mov    %eax,0x80113a7c
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
801041c4:	8b 45 ec             	mov    -0x14(%ebp),%eax
801041c7:	83 c0 2c             	add    $0x2c,%eax
801041ca:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801041cd:	8b 55 ec             	mov    -0x14(%ebp),%edx
801041d0:	8b 45 ec             	mov    -0x14(%ebp),%eax
801041d3:	0f b7 40 04          	movzwl 0x4(%eax),%eax
801041d7:	0f b7 c0             	movzwl %ax,%eax
801041da:	8d 04 02             	lea    (%edx,%eax,1),%eax
801041dd:	89 45 e8             	mov    %eax,-0x18(%ebp)
801041e0:	e9 cb 01 00 00       	jmp    801043b0 <mpinit+0x21f>
    switch(*p){
801041e5:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801041e8:	0f b6 00             	movzbl (%eax),%eax
801041eb:	0f b6 c0             	movzbl %al,%eax
801041ee:	83 f8 04             	cmp    $0x4,%eax
801041f1:	0f 87 96 01 00 00    	ja     8010438d <mpinit+0x1fc>
801041f7:	8b 04 85 f0 8f 10 80 	mov    -0x7fef7010(,%eax,4),%eax
801041fe:	ff e0                	jmp    *%eax
    case MPPROC:
      proc = (struct mpproc*)p;
80104200:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104203:	89 45 f0             	mov    %eax,-0x10(%ebp)
      if(!(proc->flags & MP_PROC_ENABLED)){
80104206:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104209:	0f b6 40 03          	movzbl 0x3(%eax),%eax
8010420d:	0f b6 c0             	movzbl %al,%eax
80104210:	83 e0 01             	and    $0x1,%eax
80104213:	85 c0                	test   %eax,%eax
80104215:	75 23                	jne    8010423a <mpinit+0xa9>
        cprintf("mpinit: ignoring cpu %d, not enabled\n", proc->apicid);
80104217:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010421a:	0f b6 40 01          	movzbl 0x1(%eax),%eax
8010421e:	0f b6 c0             	movzbl %al,%eax
80104221:	89 44 24 04          	mov    %eax,0x4(%esp)
80104225:	c7 04 24 e4 8e 10 80 	movl   $0x80108ee4,(%esp)
8010422c:	e8 98 c4 ff ff       	call   801006c9 <cprintf>
        p += sizeof(struct mpproc);
80104231:	83 45 e4 14          	addl   $0x14,-0x1c(%ebp)
        continue;
80104235:	e9 76 01 00 00       	jmp    801043b0 <mpinit+0x21f>
      }
      if(!(proc->feature & MP_PROC_APIC)){
8010423a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010423d:	8b 40 08             	mov    0x8(%eax),%eax
80104240:	25 00 02 00 00       	and    $0x200,%eax
80104245:	85 c0                	test   %eax,%eax
80104247:	75 23                	jne    8010426c <mpinit+0xdb>
        cprintf("mpinit: cpu %d has no working APIC, ignored\n", proc->apicid);
80104249:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010424c:	0f b6 40 01          	movzbl 0x1(%eax),%eax
80104250:	0f b6 c0             	movzbl %al,%eax
80104253:	89 44 24 04          	mov    %eax,0x4(%esp)
80104257:	c7 04 24 0c 8f 10 80 	movl   $0x80108f0c,(%esp)
8010425e:	e8 66 c4 ff ff       	call   801006c9 <cprintf>
        p += sizeof(struct mpproc);
80104263:	83 45 e4 14          	addl   $0x14,-0x1c(%ebp)
        continue;
80104267:	e9 44 01 00 00       	jmp    801043b0 <mpinit+0x21f>
      }
      if(ncpu >= NCPU){
8010426c:	a1 60 41 11 80       	mov    0x80114160,%eax
80104271:	83 f8 07             	cmp    $0x7,%eax
80104274:	7e 2b                	jle    801042a1 <mpinit+0x110>
        cprintf("mpinit: more than %d cpus, ignoring cpu%d\n",
          NCPU, proc->apicid);
80104276:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104279:	0f b6 40 01          	movzbl 0x1(%eax),%eax
        cprintf("mpinit: cpu %d has no working APIC, ignored\n", proc->apicid);
        p += sizeof(struct mpproc);
        continue;
      }
      if(ncpu >= NCPU){
        cprintf("mpinit: more than %d cpus, ignoring cpu%d\n",
8010427d:	0f b6 c0             	movzbl %al,%eax
80104280:	89 44 24 08          	mov    %eax,0x8(%esp)
80104284:	c7 44 24 04 08 00 00 	movl   $0x8,0x4(%esp)
8010428b:	00 
8010428c:	c7 04 24 3c 8f 10 80 	movl   $0x80108f3c,(%esp)
80104293:	e8 31 c4 ff ff       	call   801006c9 <cprintf>
          NCPU, proc->apicid);
        p += sizeof(struct mpproc);
80104298:	83 45 e4 14          	addl   $0x14,-0x1c(%ebp)
        continue;
8010429c:	e9 0f 01 00 00       	jmp    801043b0 <mpinit+0x21f>
      // says that they DON'T have to be. As long as QEMU is used to simulate
      // multiple SOCKETS everything seems fine, but as soon as we add CORES
      // the code here breaks (and fails to identify them). The confusion
      // between the id we assign here and the actual APIC id continues in
      // ioapic.c for example. A rewrite is in order.
      if(ncpu != proc->apicid){
801042a1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801042a4:	0f b6 40 01          	movzbl 0x1(%eax),%eax
801042a8:	0f b6 d0             	movzbl %al,%edx
801042ab:	a1 60 41 11 80       	mov    0x80114160,%eax
801042b0:	39 c2                	cmp    %eax,%edx
801042b2:	74 2d                	je     801042e1 <mpinit+0x150>
        cprintf("mpinit: ncpu=%d apicid=%d\n", ncpu, proc->apicid);
801042b4:	8b 45 f0             	mov    -0x10(%ebp),%eax
801042b7:	0f b6 40 01          	movzbl 0x1(%eax),%eax
801042bb:	0f b6 d0             	movzbl %al,%edx
801042be:	a1 60 41 11 80       	mov    0x80114160,%eax
801042c3:	89 54 24 08          	mov    %edx,0x8(%esp)
801042c7:	89 44 24 04          	mov    %eax,0x4(%esp)
801042cb:	c7 04 24 67 8f 10 80 	movl   $0x80108f67,(%esp)
801042d2:	e8 f2 c3 ff ff       	call   801006c9 <cprintf>
        ismp = 0;
801042d7:	c7 05 64 3b 11 80 00 	movl   $0x0,0x80113b64
801042de:	00 00 00 
      }
      cpus[ncpu].id = ncpu;
801042e1:	a1 60 41 11 80       	mov    0x80114160,%eax
801042e6:	8b 15 60 41 11 80    	mov    0x80114160,%edx
801042ec:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
801042f2:	88 90 80 3b 11 80    	mov    %dl,-0x7feec480(%eax)
      ncpu++;
801042f8:	a1 60 41 11 80       	mov    0x80114160,%eax
801042fd:	83 c0 01             	add    $0x1,%eax
80104300:	a3 60 41 11 80       	mov    %eax,0x80114160
      p += sizeof(struct mpproc);
80104305:	83 45 e4 14          	addl   $0x14,-0x1c(%ebp)
      continue;
80104309:	e9 a2 00 00 00       	jmp    801043b0 <mpinit+0x21f>
    case MPIOAPIC:
      mioapic = (struct mpioapic*)p;
8010430e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104311:	89 45 f4             	mov    %eax,-0xc(%ebp)
      if(!(mioapic->flags & MP_APIC_ENABLED)){
80104314:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104317:	0f b6 40 03          	movzbl 0x3(%eax),%eax
8010431b:	0f b6 c0             	movzbl %al,%eax
8010431e:	83 e0 01             	and    $0x1,%eax
80104321:	85 c0                	test   %eax,%eax
80104323:	75 20                	jne    80104345 <mpinit+0x1b4>
        cprintf("mpinit: ioapic %d disabled, ignored\n", mioapic->apicid);
80104325:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104328:	0f b6 40 01          	movzbl 0x1(%eax),%eax
8010432c:	0f b6 c0             	movzbl %al,%eax
8010432f:	89 44 24 04          	mov    %eax,0x4(%esp)
80104333:	c7 04 24 84 8f 10 80 	movl   $0x80108f84,(%esp)
8010433a:	e8 8a c3 ff ff       	call   801006c9 <cprintf>
        p += sizeof(struct mpioapic);
8010433f:	83 45 e4 08          	addl   $0x8,-0x1c(%ebp)
        continue;
80104343:	eb 6b                	jmp    801043b0 <mpinit+0x21f>
      }
      if(ioapic == 0){
80104345:	a1 34 3a 11 80       	mov    0x80113a34,%eax
8010434a:	85 c0                	test   %eax,%eax
8010434c:	75 19                	jne    80104367 <mpinit+0x1d6>
        ioapic = (volatile struct ioapic*)mioapic->addr;
8010434e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104351:	8b 40 04             	mov    0x4(%eax),%eax
80104354:	a3 34 3a 11 80       	mov    %eax,0x80113a34
        ioapicid = mioapic->apicid;
80104359:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010435c:	0f b6 40 01          	movzbl 0x1(%eax),%eax
80104360:	a2 60 3b 11 80       	mov    %al,0x80113b60
80104365:	eb 1a                	jmp    80104381 <mpinit+0x1f0>
      } else {
        cprintf("mpinit: ignored extra ioapic %d\n", mioapic->apicid);
80104367:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010436a:	0f b6 40 01          	movzbl 0x1(%eax),%eax
8010436e:	0f b6 c0             	movzbl %al,%eax
80104371:	89 44 24 04          	mov    %eax,0x4(%esp)
80104375:	c7 04 24 ac 8f 10 80 	movl   $0x80108fac,(%esp)
8010437c:	e8 48 c3 ff ff       	call   801006c9 <cprintf>
      }
      p += sizeof(struct mpioapic);
80104381:	83 45 e4 08          	addl   $0x8,-0x1c(%ebp)
      continue;
80104385:	eb 29                	jmp    801043b0 <mpinit+0x21f>
    case MPBUS:
    case MPIOINTR:
    case MPLINTR:
      p += 8;
80104387:	83 45 e4 08          	addl   $0x8,-0x1c(%ebp)
      continue;
8010438b:	eb 23                	jmp    801043b0 <mpinit+0x21f>
    default:
      cprintf("mpinit: unknown config type %x\n", *p);
8010438d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104390:	0f b6 00             	movzbl (%eax),%eax
80104393:	0f b6 c0             	movzbl %al,%eax
80104396:	89 44 24 04          	mov    %eax,0x4(%esp)
8010439a:	c7 04 24 d0 8f 10 80 	movl   $0x80108fd0,(%esp)
801043a1:	e8 23 c3 ff ff       	call   801006c9 <cprintf>
      ismp = 0;
801043a6:	c7 05 64 3b 11 80 00 	movl   $0x0,0x80113b64
801043ad:	00 00 00 

  if((conf = mpconfig(&mp)) == 0)
    return;
  ismp = 1;
  lapic = (uint*)conf->lapicaddr;
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
801043b0:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801043b3:	3b 45 e8             	cmp    -0x18(%ebp),%eax
801043b6:	0f 82 29 fe ff ff    	jb     801041e5 <mpinit+0x54>
    default:
      cprintf("mpinit: unknown config type %x\n", *p);
      ismp = 0;
    }
  }
  if(!ismp){
801043bc:	a1 64 3b 11 80       	mov    0x80113b64,%eax
801043c1:	85 c0                	test   %eax,%eax
801043c3:	75 27                	jne    801043ec <mpinit+0x25b>
    // Didn't like what we found; fall back to no MP.
    ncpu = 1;
801043c5:	c7 05 60 41 11 80 01 	movl   $0x1,0x80114160
801043cc:	00 00 00 
    lapic = 0;
801043cf:	c7 05 7c 3a 11 80 00 	movl   $0x0,0x80113a7c
801043d6:	00 00 00 
    ioapic = 0;
801043d9:	c7 05 34 3a 11 80 00 	movl   $0x0,0x80113a34
801043e0:	00 00 00 
    ioapicid = 0;
801043e3:	c6 05 60 3b 11 80 00 	movb   $0x0,0x80113b60
    return;
801043ea:	eb 44                	jmp    80104430 <mpinit+0x29f>
  }

  if(mp->imcrp){
801043ec:	8b 45 e0             	mov    -0x20(%ebp),%eax
801043ef:	0f b6 40 0c          	movzbl 0xc(%eax),%eax
801043f3:	84 c0                	test   %al,%al
801043f5:	74 39                	je     80104430 <mpinit+0x29f>
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
801043f7:	c7 44 24 04 70 00 00 	movl   $0x70,0x4(%esp)
801043fe:	00 
801043ff:	c7 04 24 22 00 00 00 	movl   $0x22,(%esp)
80104406:	e8 5e fb ff ff       	call   80103f69 <outb>
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
8010440b:	c7 04 24 23 00 00 00 	movl   $0x23,(%esp)
80104412:	e8 35 fb ff ff       	call   80103f4c <inb>
80104417:	83 c8 01             	or     $0x1,%eax
8010441a:	0f b6 c0             	movzbl %al,%eax
8010441d:	89 44 24 04          	mov    %eax,0x4(%esp)
80104421:	c7 04 24 23 00 00 00 	movl   $0x23,(%esp)
80104428:	e8 3c fb ff ff       	call   80103f69 <outb>
8010442d:	eb 01                	jmp    80104430 <mpinit+0x29f>
  struct mpconf *conf;
  struct mpproc *proc;
  struct mpioapic *mioapic;

  if((conf = mpconfig(&mp)) == 0)
    return;
8010442f:	90                   	nop
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
  }
}
80104430:	c9                   	leave  
80104431:	c3                   	ret    
	...

80104434 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80104434:	55                   	push   %ebp
80104435:	89 e5                	mov    %esp,%ebp
80104437:	83 ec 08             	sub    $0x8,%esp
8010443a:	8b 55 08             	mov    0x8(%ebp),%edx
8010443d:	8b 45 0c             	mov    0xc(%ebp),%eax
80104440:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80104444:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80104447:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
8010444b:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
8010444f:	ee                   	out    %al,(%dx)
}
80104450:	c9                   	leave  
80104451:	c3                   	ret    

80104452 <picsetmask>:
// Initial IRQ mask has interrupt 2 enabled (for slave 8259A).
static ushort irqmask = 0xFFFF & ~(1<<IRQ_SLAVE);

static void
picsetmask(ushort mask)
{
80104452:	55                   	push   %ebp
80104453:	89 e5                	mov    %esp,%ebp
80104455:	83 ec 0c             	sub    $0xc,%esp
80104458:	8b 45 08             	mov    0x8(%ebp),%eax
8010445b:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  irqmask = mask;
8010445f:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80104463:	66 a3 00 c0 10 80    	mov    %ax,0x8010c000
  outb(IO_PIC1+1, mask);
80104469:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
8010446d:	0f b6 c0             	movzbl %al,%eax
80104470:	89 44 24 04          	mov    %eax,0x4(%esp)
80104474:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
8010447b:	e8 b4 ff ff ff       	call   80104434 <outb>
  outb(IO_PIC2+1, mask >> 8);
80104480:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80104484:	66 c1 e8 08          	shr    $0x8,%ax
80104488:	0f b6 c0             	movzbl %al,%eax
8010448b:	89 44 24 04          	mov    %eax,0x4(%esp)
8010448f:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
80104496:	e8 99 ff ff ff       	call   80104434 <outb>
}
8010449b:	c9                   	leave  
8010449c:	c3                   	ret    

8010449d <picenable>:

void
picenable(int irq)
{
8010449d:	55                   	push   %ebp
8010449e:	89 e5                	mov    %esp,%ebp
801044a0:	53                   	push   %ebx
801044a1:	83 ec 04             	sub    $0x4,%esp
  picsetmask(irqmask & ~(1<<irq));
801044a4:	8b 45 08             	mov    0x8(%ebp),%eax
801044a7:	ba 01 00 00 00       	mov    $0x1,%edx
801044ac:	89 d3                	mov    %edx,%ebx
801044ae:	89 c1                	mov    %eax,%ecx
801044b0:	d3 e3                	shl    %cl,%ebx
801044b2:	89 d8                	mov    %ebx,%eax
801044b4:	89 c2                	mov    %eax,%edx
801044b6:	f7 d2                	not    %edx
801044b8:	0f b7 05 00 c0 10 80 	movzwl 0x8010c000,%eax
801044bf:	21 d0                	and    %edx,%eax
801044c1:	0f b7 c0             	movzwl %ax,%eax
801044c4:	89 04 24             	mov    %eax,(%esp)
801044c7:	e8 86 ff ff ff       	call   80104452 <picsetmask>
}
801044cc:	83 c4 04             	add    $0x4,%esp
801044cf:	5b                   	pop    %ebx
801044d0:	5d                   	pop    %ebp
801044d1:	c3                   	ret    

801044d2 <picinit>:

// Initialize the 8259A interrupt controllers.
void
picinit(void)
{
801044d2:	55                   	push   %ebp
801044d3:	89 e5                	mov    %esp,%ebp
801044d5:	83 ec 08             	sub    $0x8,%esp
  // mask all interrupts
  outb(IO_PIC1+1, 0xFF);
801044d8:	c7 44 24 04 ff 00 00 	movl   $0xff,0x4(%esp)
801044df:	00 
801044e0:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
801044e7:	e8 48 ff ff ff       	call   80104434 <outb>
  outb(IO_PIC2+1, 0xFF);
801044ec:	c7 44 24 04 ff 00 00 	movl   $0xff,0x4(%esp)
801044f3:	00 
801044f4:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
801044fb:	e8 34 ff ff ff       	call   80104434 <outb>

  // ICW1:  0001g0hi
  //    g:  0 = edge triggering, 1 = level triggering
  //    h:  0 = cascaded PICs, 1 = master only
  //    i:  0 = no ICW4, 1 = ICW4 required
  outb(IO_PIC1, 0x11);
80104500:	c7 44 24 04 11 00 00 	movl   $0x11,0x4(%esp)
80104507:	00 
80104508:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
8010450f:	e8 20 ff ff ff       	call   80104434 <outb>

  // ICW2:  Vector offset
  outb(IO_PIC1+1, T_IRQ0);
80104514:	c7 44 24 04 20 00 00 	movl   $0x20,0x4(%esp)
8010451b:	00 
8010451c:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
80104523:	e8 0c ff ff ff       	call   80104434 <outb>

  // ICW3:  (master PIC) bit mask of IR lines connected to slaves
  //        (slave PIC) 3-bit # of slave's connection to master
  outb(IO_PIC1+1, 1<<IRQ_SLAVE);
80104528:	c7 44 24 04 04 00 00 	movl   $0x4,0x4(%esp)
8010452f:	00 
80104530:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
80104537:	e8 f8 fe ff ff       	call   80104434 <outb>
  //    m:  0 = slave PIC, 1 = master PIC
  //      (ignored when b is 0, as the master/slave role
  //      can be hardwired).
  //    a:  1 = Automatic EOI mode
  //    p:  0 = MCS-80/85 mode, 1 = intel x86 mode
  outb(IO_PIC1+1, 0x3);
8010453c:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
80104543:	00 
80104544:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
8010454b:	e8 e4 fe ff ff       	call   80104434 <outb>

  // Set up slave (8259A-2)
  outb(IO_PIC2, 0x11);                  // ICW1
80104550:	c7 44 24 04 11 00 00 	movl   $0x11,0x4(%esp)
80104557:	00 
80104558:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
8010455f:	e8 d0 fe ff ff       	call   80104434 <outb>
  outb(IO_PIC2+1, T_IRQ0 + 8);      // ICW2
80104564:	c7 44 24 04 28 00 00 	movl   $0x28,0x4(%esp)
8010456b:	00 
8010456c:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
80104573:	e8 bc fe ff ff       	call   80104434 <outb>
  outb(IO_PIC2+1, IRQ_SLAVE);           // ICW3
80104578:	c7 44 24 04 02 00 00 	movl   $0x2,0x4(%esp)
8010457f:	00 
80104580:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
80104587:	e8 a8 fe ff ff       	call   80104434 <outb>
  // NB Automatic EOI mode doesn't tend to work on the slave.
  // Linux source code says it's "to be investigated".
  outb(IO_PIC2+1, 0x3);                 // ICW4
8010458c:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
80104593:	00 
80104594:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
8010459b:	e8 94 fe ff ff       	call   80104434 <outb>

  // OCW3:  0ef01prs
  //   ef:  0x = NOP, 10 = clear specific mask, 11 = set specific mask
  //    p:  0 = no polling, 1 = polling mode
  //   rs:  0x = NOP, 10 = read IRR, 11 = read ISR
  outb(IO_PIC1, 0x68);             // clear specific mask
801045a0:	c7 44 24 04 68 00 00 	movl   $0x68,0x4(%esp)
801045a7:	00 
801045a8:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
801045af:	e8 80 fe ff ff       	call   80104434 <outb>
  outb(IO_PIC1, 0x0a);             // read IRR by default
801045b4:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
801045bb:	00 
801045bc:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
801045c3:	e8 6c fe ff ff       	call   80104434 <outb>

  outb(IO_PIC2, 0x68);             // OCW3
801045c8:	c7 44 24 04 68 00 00 	movl   $0x68,0x4(%esp)
801045cf:	00 
801045d0:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
801045d7:	e8 58 fe ff ff       	call   80104434 <outb>
  outb(IO_PIC2, 0x0a);             // OCW3
801045dc:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
801045e3:	00 
801045e4:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
801045eb:	e8 44 fe ff ff       	call   80104434 <outb>

  if(irqmask != 0xFFFF)
801045f0:	0f b7 05 00 c0 10 80 	movzwl 0x8010c000,%eax
801045f7:	66 83 f8 ff          	cmp    $0xffffffff,%ax
801045fb:	74 12                	je     8010460f <picinit+0x13d>
    picsetmask(irqmask);
801045fd:	0f b7 05 00 c0 10 80 	movzwl 0x8010c000,%eax
80104604:	0f b7 c0             	movzwl %ax,%eax
80104607:	89 04 24             	mov    %eax,(%esp)
8010460a:	e8 43 fe ff ff       	call   80104452 <picsetmask>
}
8010460f:	c9                   	leave  
80104610:	c3                   	ret    
80104611:	00 00                	add    %al,(%eax)
	...

80104614 <pipealloc>:
  int writeopen;  // write fd is still open
};

int
pipealloc(struct file **f0, struct file **f1)
{
80104614:	55                   	push   %ebp
80104615:	89 e5                	mov    %esp,%ebp
80104617:	83 ec 28             	sub    $0x28,%esp
  struct pipe *p;

  p = 0;
8010461a:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  *f0 = *f1 = 0;
80104621:	8b 45 0c             	mov    0xc(%ebp),%eax
80104624:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
8010462a:	8b 45 0c             	mov    0xc(%ebp),%eax
8010462d:	8b 10                	mov    (%eax),%edx
8010462f:	8b 45 08             	mov    0x8(%ebp),%eax
80104632:	89 10                	mov    %edx,(%eax)
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
80104634:	e8 7f d0 ff ff       	call   801016b8 <filealloc>
80104639:	8b 55 08             	mov    0x8(%ebp),%edx
8010463c:	89 02                	mov    %eax,(%edx)
8010463e:	8b 45 08             	mov    0x8(%ebp),%eax
80104641:	8b 00                	mov    (%eax),%eax
80104643:	85 c0                	test   %eax,%eax
80104645:	0f 84 c8 00 00 00    	je     80104713 <pipealloc+0xff>
8010464b:	e8 68 d0 ff ff       	call   801016b8 <filealloc>
80104650:	8b 55 0c             	mov    0xc(%ebp),%edx
80104653:	89 02                	mov    %eax,(%edx)
80104655:	8b 45 0c             	mov    0xc(%ebp),%eax
80104658:	8b 00                	mov    (%eax),%eax
8010465a:	85 c0                	test   %eax,%eax
8010465c:	0f 84 b1 00 00 00    	je     80104713 <pipealloc+0xff>
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
80104662:	e8 37 ed ff ff       	call   8010339e <kalloc>
80104667:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010466a:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010466e:	0f 84 9e 00 00 00    	je     80104712 <pipealloc+0xfe>
    goto bad;
  p->readopen = 1;
80104674:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104677:	c7 80 3c 02 00 00 01 	movl   $0x1,0x23c(%eax)
8010467e:	00 00 00 
  p->writeopen = 1;
80104681:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104684:	c7 80 40 02 00 00 01 	movl   $0x1,0x240(%eax)
8010468b:	00 00 00 
  p->nwrite = 0;
8010468e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104691:	c7 80 38 02 00 00 00 	movl   $0x0,0x238(%eax)
80104698:	00 00 00 
  p->nread = 0;
8010469b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010469e:	c7 80 34 02 00 00 00 	movl   $0x0,0x234(%eax)
801046a5:	00 00 00 
  initlock(&p->lock, "pipe");
801046a8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801046ab:	c7 44 24 04 04 90 10 	movl   $0x80109004,0x4(%esp)
801046b2:	80 
801046b3:	89 04 24             	mov    %eax,(%esp)
801046b6:	e8 2b 0e 00 00       	call   801054e6 <initlock>
  (*f0)->type = FD_PIPE;
801046bb:	8b 45 08             	mov    0x8(%ebp),%eax
801046be:	8b 00                	mov    (%eax),%eax
801046c0:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f0)->readable = 1;
801046c6:	8b 45 08             	mov    0x8(%ebp),%eax
801046c9:	8b 00                	mov    (%eax),%eax
801046cb:	c6 40 08 01          	movb   $0x1,0x8(%eax)
  (*f0)->writable = 0;
801046cf:	8b 45 08             	mov    0x8(%ebp),%eax
801046d2:	8b 00                	mov    (%eax),%eax
801046d4:	c6 40 09 00          	movb   $0x0,0x9(%eax)
  (*f0)->pipe = p;
801046d8:	8b 45 08             	mov    0x8(%ebp),%eax
801046db:	8b 00                	mov    (%eax),%eax
801046dd:	8b 55 f4             	mov    -0xc(%ebp),%edx
801046e0:	89 50 0c             	mov    %edx,0xc(%eax)
  (*f1)->type = FD_PIPE;
801046e3:	8b 45 0c             	mov    0xc(%ebp),%eax
801046e6:	8b 00                	mov    (%eax),%eax
801046e8:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f1)->readable = 0;
801046ee:	8b 45 0c             	mov    0xc(%ebp),%eax
801046f1:	8b 00                	mov    (%eax),%eax
801046f3:	c6 40 08 00          	movb   $0x0,0x8(%eax)
  (*f1)->writable = 1;
801046f7:	8b 45 0c             	mov    0xc(%ebp),%eax
801046fa:	8b 00                	mov    (%eax),%eax
801046fc:	c6 40 09 01          	movb   $0x1,0x9(%eax)
  (*f1)->pipe = p;
80104700:	8b 45 0c             	mov    0xc(%ebp),%eax
80104703:	8b 00                	mov    (%eax),%eax
80104705:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104708:	89 50 0c             	mov    %edx,0xc(%eax)
  return 0;
8010470b:	b8 00 00 00 00       	mov    $0x0,%eax
80104710:	eb 43                	jmp    80104755 <pipealloc+0x141>
  p = 0;
  *f0 = *f1 = 0;
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
    goto bad;
80104712:	90                   	nop
  (*f1)->pipe = p;
  return 0;

//PAGEBREAK: 20
 bad:
  if(p)
80104713:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104717:	74 0b                	je     80104724 <pipealloc+0x110>
    kfree((char*)p);
80104719:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010471c:	89 04 24             	mov    %eax,(%esp)
8010471f:	e8 e4 eb ff ff       	call   80103308 <kfree>
  if(*f0)
80104724:	8b 45 08             	mov    0x8(%ebp),%eax
80104727:	8b 00                	mov    (%eax),%eax
80104729:	85 c0                	test   %eax,%eax
8010472b:	74 0d                	je     8010473a <pipealloc+0x126>
    fileclose(*f0);
8010472d:	8b 45 08             	mov    0x8(%ebp),%eax
80104730:	8b 00                	mov    (%eax),%eax
80104732:	89 04 24             	mov    %eax,(%esp)
80104735:	e8 27 d0 ff ff       	call   80101761 <fileclose>
  if(*f1)
8010473a:	8b 45 0c             	mov    0xc(%ebp),%eax
8010473d:	8b 00                	mov    (%eax),%eax
8010473f:	85 c0                	test   %eax,%eax
80104741:	74 0d                	je     80104750 <pipealloc+0x13c>
    fileclose(*f1);
80104743:	8b 45 0c             	mov    0xc(%ebp),%eax
80104746:	8b 00                	mov    (%eax),%eax
80104748:	89 04 24             	mov    %eax,(%esp)
8010474b:	e8 11 d0 ff ff       	call   80101761 <fileclose>
  return -1;
80104750:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104755:	c9                   	leave  
80104756:	c3                   	ret    

80104757 <pipeclose>:

void
pipeclose(struct pipe *p, int writable)
{
80104757:	55                   	push   %ebp
80104758:	89 e5                	mov    %esp,%ebp
8010475a:	83 ec 18             	sub    $0x18,%esp
  acquire(&p->lock);
8010475d:	8b 45 08             	mov    0x8(%ebp),%eax
80104760:	89 04 24             	mov    %eax,(%esp)
80104763:	e8 9f 0d 00 00       	call   80105507 <acquire>
  if(writable){
80104768:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
8010476c:	74 1f                	je     8010478d <pipeclose+0x36>
    p->writeopen = 0;
8010476e:	8b 45 08             	mov    0x8(%ebp),%eax
80104771:	c7 80 40 02 00 00 00 	movl   $0x0,0x240(%eax)
80104778:	00 00 00 
    wakeup(&p->nread);
8010477b:	8b 45 08             	mov    0x8(%ebp),%eax
8010477e:	05 34 02 00 00       	add    $0x234,%eax
80104783:	89 04 24             	mov    %eax,(%esp)
80104786:	e8 7d 0b 00 00       	call   80105308 <wakeup>
8010478b:	eb 1d                	jmp    801047aa <pipeclose+0x53>
  } else {
    p->readopen = 0;
8010478d:	8b 45 08             	mov    0x8(%ebp),%eax
80104790:	c7 80 3c 02 00 00 00 	movl   $0x0,0x23c(%eax)
80104797:	00 00 00 
    wakeup(&p->nwrite);
8010479a:	8b 45 08             	mov    0x8(%ebp),%eax
8010479d:	05 38 02 00 00       	add    $0x238,%eax
801047a2:	89 04 24             	mov    %eax,(%esp)
801047a5:	e8 5e 0b 00 00       	call   80105308 <wakeup>
  }
  if(p->readopen == 0 && p->writeopen == 0){
801047aa:	8b 45 08             	mov    0x8(%ebp),%eax
801047ad:	8b 80 3c 02 00 00    	mov    0x23c(%eax),%eax
801047b3:	85 c0                	test   %eax,%eax
801047b5:	75 25                	jne    801047dc <pipeclose+0x85>
801047b7:	8b 45 08             	mov    0x8(%ebp),%eax
801047ba:	8b 80 40 02 00 00    	mov    0x240(%eax),%eax
801047c0:	85 c0                	test   %eax,%eax
801047c2:	75 18                	jne    801047dc <pipeclose+0x85>
    release(&p->lock);
801047c4:	8b 45 08             	mov    0x8(%ebp),%eax
801047c7:	89 04 24             	mov    %eax,(%esp)
801047ca:	e8 a1 0d 00 00       	call   80105570 <release>
    kfree((char*)p);
801047cf:	8b 45 08             	mov    0x8(%ebp),%eax
801047d2:	89 04 24             	mov    %eax,(%esp)
801047d5:	e8 2e eb ff ff       	call   80103308 <kfree>
    wakeup(&p->nread);
  } else {
    p->readopen = 0;
    wakeup(&p->nwrite);
  }
  if(p->readopen == 0 && p->writeopen == 0){
801047da:	eb 0b                	jmp    801047e7 <pipeclose+0x90>
    release(&p->lock);
    kfree((char*)p);
  } else
    release(&p->lock);
801047dc:	8b 45 08             	mov    0x8(%ebp),%eax
801047df:	89 04 24             	mov    %eax,(%esp)
801047e2:	e8 89 0d 00 00       	call   80105570 <release>
}
801047e7:	c9                   	leave  
801047e8:	c3                   	ret    

801047e9 <pipewrite>:

//PAGEBREAK: 40
int
pipewrite(struct pipe *p, char *addr, int n)
{
801047e9:	55                   	push   %ebp
801047ea:	89 e5                	mov    %esp,%ebp
801047ec:	53                   	push   %ebx
801047ed:	83 ec 24             	sub    $0x24,%esp
  int i;

  acquire(&p->lock);
801047f0:	8b 45 08             	mov    0x8(%ebp),%eax
801047f3:	89 04 24             	mov    %eax,(%esp)
801047f6:	e8 0c 0d 00 00       	call   80105507 <acquire>
  for(i = 0; i < n; i++){
801047fb:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80104802:	e9 a6 00 00 00       	jmp    801048ad <pipewrite+0xc4>
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
      if(p->readopen == 0 || proc->killed){
80104807:	8b 45 08             	mov    0x8(%ebp),%eax
8010480a:	8b 80 3c 02 00 00    	mov    0x23c(%eax),%eax
80104810:	85 c0                	test   %eax,%eax
80104812:	74 0d                	je     80104821 <pipewrite+0x38>
80104814:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010481a:	8b 40 24             	mov    0x24(%eax),%eax
8010481d:	85 c0                	test   %eax,%eax
8010481f:	74 15                	je     80104836 <pipewrite+0x4d>
        release(&p->lock);
80104821:	8b 45 08             	mov    0x8(%ebp),%eax
80104824:	89 04 24             	mov    %eax,(%esp)
80104827:	e8 44 0d 00 00       	call   80105570 <release>
        return -1;
8010482c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104831:	e9 9d 00 00 00       	jmp    801048d3 <pipewrite+0xea>
      }
      wakeup(&p->nread);
80104836:	8b 45 08             	mov    0x8(%ebp),%eax
80104839:	05 34 02 00 00       	add    $0x234,%eax
8010483e:	89 04 24             	mov    %eax,(%esp)
80104841:	e8 c2 0a 00 00       	call   80105308 <wakeup>
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
80104846:	8b 45 08             	mov    0x8(%ebp),%eax
80104849:	8b 55 08             	mov    0x8(%ebp),%edx
8010484c:	81 c2 38 02 00 00    	add    $0x238,%edx
80104852:	89 44 24 04          	mov    %eax,0x4(%esp)
80104856:	89 14 24             	mov    %edx,(%esp)
80104859:	e8 d0 09 00 00       	call   8010522e <sleep>
8010485e:	eb 01                	jmp    80104861 <pipewrite+0x78>
{
  int i;

  acquire(&p->lock);
  for(i = 0; i < n; i++){
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80104860:	90                   	nop
80104861:	8b 45 08             	mov    0x8(%ebp),%eax
80104864:	8b 90 38 02 00 00    	mov    0x238(%eax),%edx
8010486a:	8b 45 08             	mov    0x8(%ebp),%eax
8010486d:	8b 80 34 02 00 00    	mov    0x234(%eax),%eax
80104873:	05 00 02 00 00       	add    $0x200,%eax
80104878:	39 c2                	cmp    %eax,%edx
8010487a:	74 8b                	je     80104807 <pipewrite+0x1e>
        return -1;
      }
      wakeup(&p->nread);
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
    }
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
8010487c:	8b 45 08             	mov    0x8(%ebp),%eax
8010487f:	8b 80 38 02 00 00    	mov    0x238(%eax),%eax
80104885:	89 c3                	mov    %eax,%ebx
80104887:	81 e3 ff 01 00 00    	and    $0x1ff,%ebx
8010488d:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104890:	03 55 0c             	add    0xc(%ebp),%edx
80104893:	0f b6 0a             	movzbl (%edx),%ecx
80104896:	8b 55 08             	mov    0x8(%ebp),%edx
80104899:	88 4c 1a 34          	mov    %cl,0x34(%edx,%ebx,1)
8010489d:	8d 50 01             	lea    0x1(%eax),%edx
801048a0:	8b 45 08             	mov    0x8(%ebp),%eax
801048a3:	89 90 38 02 00 00    	mov    %edx,0x238(%eax)
pipewrite(struct pipe *p, char *addr, int n)
{
  int i;

  acquire(&p->lock);
  for(i = 0; i < n; i++){
801048a9:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801048ad:	8b 45 f4             	mov    -0xc(%ebp),%eax
801048b0:	3b 45 10             	cmp    0x10(%ebp),%eax
801048b3:	7c ab                	jl     80104860 <pipewrite+0x77>
      wakeup(&p->nread);
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
    }
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
  }
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
801048b5:	8b 45 08             	mov    0x8(%ebp),%eax
801048b8:	05 34 02 00 00       	add    $0x234,%eax
801048bd:	89 04 24             	mov    %eax,(%esp)
801048c0:	e8 43 0a 00 00       	call   80105308 <wakeup>
  release(&p->lock);
801048c5:	8b 45 08             	mov    0x8(%ebp),%eax
801048c8:	89 04 24             	mov    %eax,(%esp)
801048cb:	e8 a0 0c 00 00       	call   80105570 <release>
  return n;
801048d0:	8b 45 10             	mov    0x10(%ebp),%eax
}
801048d3:	83 c4 24             	add    $0x24,%esp
801048d6:	5b                   	pop    %ebx
801048d7:	5d                   	pop    %ebp
801048d8:	c3                   	ret    

801048d9 <piperead>:

int
piperead(struct pipe *p, char *addr, int n)
{
801048d9:	55                   	push   %ebp
801048da:	89 e5                	mov    %esp,%ebp
801048dc:	53                   	push   %ebx
801048dd:	83 ec 24             	sub    $0x24,%esp
  int i;

  acquire(&p->lock);
801048e0:	8b 45 08             	mov    0x8(%ebp),%eax
801048e3:	89 04 24             	mov    %eax,(%esp)
801048e6:	e8 1c 0c 00 00       	call   80105507 <acquire>
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
801048eb:	eb 3a                	jmp    80104927 <piperead+0x4e>
    if(proc->killed){
801048ed:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801048f3:	8b 40 24             	mov    0x24(%eax),%eax
801048f6:	85 c0                	test   %eax,%eax
801048f8:	74 15                	je     8010490f <piperead+0x36>
      release(&p->lock);
801048fa:	8b 45 08             	mov    0x8(%ebp),%eax
801048fd:	89 04 24             	mov    %eax,(%esp)
80104900:	e8 6b 0c 00 00       	call   80105570 <release>
      return -1;
80104905:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010490a:	e9 b6 00 00 00       	jmp    801049c5 <piperead+0xec>
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
8010490f:	8b 45 08             	mov    0x8(%ebp),%eax
80104912:	8b 55 08             	mov    0x8(%ebp),%edx
80104915:	81 c2 34 02 00 00    	add    $0x234,%edx
8010491b:	89 44 24 04          	mov    %eax,0x4(%esp)
8010491f:	89 14 24             	mov    %edx,(%esp)
80104922:	e8 07 09 00 00       	call   8010522e <sleep>
piperead(struct pipe *p, char *addr, int n)
{
  int i;

  acquire(&p->lock);
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
80104927:	8b 45 08             	mov    0x8(%ebp),%eax
8010492a:	8b 90 34 02 00 00    	mov    0x234(%eax),%edx
80104930:	8b 45 08             	mov    0x8(%ebp),%eax
80104933:	8b 80 38 02 00 00    	mov    0x238(%eax),%eax
80104939:	39 c2                	cmp    %eax,%edx
8010493b:	75 0d                	jne    8010494a <piperead+0x71>
8010493d:	8b 45 08             	mov    0x8(%ebp),%eax
80104940:	8b 80 40 02 00 00    	mov    0x240(%eax),%eax
80104946:	85 c0                	test   %eax,%eax
80104948:	75 a3                	jne    801048ed <piperead+0x14>
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
8010494a:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80104951:	eb 49                	jmp    8010499c <piperead+0xc3>
    if(p->nread == p->nwrite)
80104953:	8b 45 08             	mov    0x8(%ebp),%eax
80104956:	8b 90 34 02 00 00    	mov    0x234(%eax),%edx
8010495c:	8b 45 08             	mov    0x8(%ebp),%eax
8010495f:	8b 80 38 02 00 00    	mov    0x238(%eax),%eax
80104965:	39 c2                	cmp    %eax,%edx
80104967:	74 3d                	je     801049a6 <piperead+0xcd>
      break;
    addr[i] = p->data[p->nread++ % PIPESIZE];
80104969:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010496c:	89 c2                	mov    %eax,%edx
8010496e:	03 55 0c             	add    0xc(%ebp),%edx
80104971:	8b 45 08             	mov    0x8(%ebp),%eax
80104974:	8b 80 34 02 00 00    	mov    0x234(%eax),%eax
8010497a:	89 c3                	mov    %eax,%ebx
8010497c:	81 e3 ff 01 00 00    	and    $0x1ff,%ebx
80104982:	8b 4d 08             	mov    0x8(%ebp),%ecx
80104985:	0f b6 4c 19 34       	movzbl 0x34(%ecx,%ebx,1),%ecx
8010498a:	88 0a                	mov    %cl,(%edx)
8010498c:	8d 50 01             	lea    0x1(%eax),%edx
8010498f:	8b 45 08             	mov    0x8(%ebp),%eax
80104992:	89 90 34 02 00 00    	mov    %edx,0x234(%eax)
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
80104998:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010499c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010499f:	3b 45 10             	cmp    0x10(%ebp),%eax
801049a2:	7c af                	jl     80104953 <piperead+0x7a>
801049a4:	eb 01                	jmp    801049a7 <piperead+0xce>
    if(p->nread == p->nwrite)
      break;
801049a6:	90                   	nop
    addr[i] = p->data[p->nread++ % PIPESIZE];
  }
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
801049a7:	8b 45 08             	mov    0x8(%ebp),%eax
801049aa:	05 38 02 00 00       	add    $0x238,%eax
801049af:	89 04 24             	mov    %eax,(%esp)
801049b2:	e8 51 09 00 00       	call   80105308 <wakeup>
  release(&p->lock);
801049b7:	8b 45 08             	mov    0x8(%ebp),%eax
801049ba:	89 04 24             	mov    %eax,(%esp)
801049bd:	e8 ae 0b 00 00       	call   80105570 <release>
  return i;
801049c2:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801049c5:	83 c4 24             	add    $0x24,%esp
801049c8:	5b                   	pop    %ebx
801049c9:	5d                   	pop    %ebp
801049ca:	c3                   	ret    
	...

801049cc <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
801049cc:	55                   	push   %ebp
801049cd:	89 e5                	mov    %esp,%ebp
801049cf:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
801049d2:	9c                   	pushf  
801049d3:	58                   	pop    %eax
801049d4:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return eflags;
801049d7:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
801049da:	c9                   	leave  
801049db:	c3                   	ret    

801049dc <sti>:
  asm volatile("cli");
}

static inline void
sti(void)
{
801049dc:	55                   	push   %ebp
801049dd:	89 e5                	mov    %esp,%ebp
  asm volatile("sti");
801049df:	fb                   	sti    
}
801049e0:	5d                   	pop    %ebp
801049e1:	c3                   	ret    

801049e2 <pinit>:

static void wakeup1(void *chan);

void
pinit(void)
{
801049e2:	55                   	push   %ebp
801049e3:	89 e5                	mov    %esp,%ebp
801049e5:	83 ec 18             	sub    $0x18,%esp
  initlock(&ptable.lock, "ptable");
801049e8:	c7 44 24 04 09 90 10 	movl   $0x80109009,0x4(%esp)
801049ef:	80 
801049f0:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
801049f7:	e8 ea 0a 00 00       	call   801054e6 <initlock>
}
801049fc:	c9                   	leave  
801049fd:	c3                   	ret    

801049fe <allocproc>:
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
801049fe:	55                   	push   %ebp
801049ff:	89 e5                	mov    %esp,%ebp
80104a01:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);
80104a04:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
80104a0b:	e8 f7 0a 00 00       	call   80105507 <acquire>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80104a10:	c7 45 f0 b4 41 11 80 	movl   $0x801141b4,-0x10(%ebp)
80104a17:	eb 0e                	jmp    80104a27 <allocproc+0x29>
    if(p->state == UNUSED)
80104a19:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104a1c:	8b 40 0c             	mov    0xc(%eax),%eax
80104a1f:	85 c0                	test   %eax,%eax
80104a21:	74 24                	je     80104a47 <allocproc+0x49>
{
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80104a23:	83 45 f0 7c          	addl   $0x7c,-0x10(%ebp)
80104a27:	b8 b4 60 11 80       	mov    $0x801160b4,%eax
80104a2c:	39 45 f0             	cmp    %eax,-0x10(%ebp)
80104a2f:	72 e8                	jb     80104a19 <allocproc+0x1b>
    if(p->state == UNUSED)
      goto found;
  release(&ptable.lock);
80104a31:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
80104a38:	e8 33 0b 00 00       	call   80105570 <release>
  return 0;
80104a3d:	b8 00 00 00 00       	mov    $0x0,%eax
80104a42:	e9 b5 00 00 00       	jmp    80104afc <allocproc+0xfe>
  char *sp;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == UNUSED)
      goto found;
80104a47:	90                   	nop
  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
80104a48:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104a4b:	c7 40 0c 01 00 00 00 	movl   $0x1,0xc(%eax)
  p->pid = nextpid++;
80104a52:	a1 04 c0 10 80       	mov    0x8010c004,%eax
80104a57:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104a5a:	89 42 10             	mov    %eax,0x10(%edx)
80104a5d:	83 c0 01             	add    $0x1,%eax
80104a60:	a3 04 c0 10 80       	mov    %eax,0x8010c004
  release(&ptable.lock);
80104a65:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
80104a6c:	e8 ff 0a 00 00       	call   80105570 <release>

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
80104a71:	e8 28 e9 ff ff       	call   8010339e <kalloc>
80104a76:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104a79:	89 42 08             	mov    %eax,0x8(%edx)
80104a7c:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104a7f:	8b 40 08             	mov    0x8(%eax),%eax
80104a82:	85 c0                	test   %eax,%eax
80104a84:	75 11                	jne    80104a97 <allocproc+0x99>
    p->state = UNUSED;
80104a86:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104a89:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
    return 0;
80104a90:	b8 00 00 00 00       	mov    $0x0,%eax
80104a95:	eb 65                	jmp    80104afc <allocproc+0xfe>
  }
  sp = p->kstack + KSTACKSIZE;
80104a97:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104a9a:	8b 40 08             	mov    0x8(%eax),%eax
80104a9d:	05 00 10 00 00       	add    $0x1000,%eax
80104aa2:	89 45 f4             	mov    %eax,-0xc(%ebp)

  // Leave room for trap frame.
  sp -= sizeof *p->tf;
80104aa5:	83 6d f4 4c          	subl   $0x4c,-0xc(%ebp)
  p->tf = (struct trapframe*)sp;
80104aa9:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104aac:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104aaf:	89 50 18             	mov    %edx,0x18(%eax)

  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
80104ab2:	83 6d f4 04          	subl   $0x4,-0xc(%ebp)
  *(uint*)sp = (uint)trapret;
80104ab6:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104ab9:	ba a0 6b 10 80       	mov    $0x80106ba0,%edx
80104abe:	89 10                	mov    %edx,(%eax)

  sp -= sizeof *p->context;
80104ac0:	83 6d f4 14          	subl   $0x14,-0xc(%ebp)
  p->context = (struct context*)sp;
80104ac4:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104ac7:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104aca:	89 50 1c             	mov    %edx,0x1c(%eax)
  memset(p->context, 0, sizeof *p->context);
80104acd:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104ad0:	8b 40 1c             	mov    0x1c(%eax),%eax
80104ad3:	c7 44 24 08 14 00 00 	movl   $0x14,0x8(%esp)
80104ada:	00 
80104adb:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80104ae2:	00 
80104ae3:	89 04 24             	mov    %eax,(%esp)
80104ae6:	e8 7f 0c 00 00       	call   8010576a <memset>
  p->context->eip = (uint)forkret;
80104aeb:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104aee:	8b 40 1c             	mov    0x1c(%eax),%eax
80104af1:	ba ef 51 10 80       	mov    $0x801051ef,%edx
80104af6:	89 50 10             	mov    %edx,0x10(%eax)

  return p;
80104af9:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
80104afc:	c9                   	leave  
80104afd:	c3                   	ret    

80104afe <userinit>:

//PAGEBREAK: 32
// Set up first user process.
void
userinit(void)
{
80104afe:	55                   	push   %ebp
80104aff:	89 e5                	mov    %esp,%ebp
80104b01:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;
  extern char _binary_initcode_start[], _binary_initcode_size[];

  p = allocproc();
80104b04:	e8 f5 fe ff ff       	call   801049fe <allocproc>
80104b09:	89 45 f4             	mov    %eax,-0xc(%ebp)
  initproc = p;
80104b0c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b0f:	a3 48 c6 10 80       	mov    %eax,0x8010c648
  if((p->pgdir = setupkvm()) == 0)
80104b14:	e8 84 37 00 00       	call   8010829d <setupkvm>
80104b19:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104b1c:	89 42 04             	mov    %eax,0x4(%edx)
80104b1f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b22:	8b 40 04             	mov    0x4(%eax),%eax
80104b25:	85 c0                	test   %eax,%eax
80104b27:	75 0c                	jne    80104b35 <userinit+0x37>
    panic("userinit: out of memory?");
80104b29:	c7 04 24 10 90 10 80 	movl   $0x80109010,(%esp)
80104b30:	e8 34 bd ff ff       	call   80100869 <panic>
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
80104b35:	ba 2c 00 00 00       	mov    $0x2c,%edx
80104b3a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b3d:	8b 40 04             	mov    0x4(%eax),%eax
80104b40:	89 54 24 08          	mov    %edx,0x8(%esp)
80104b44:	c7 44 24 04 e0 c4 10 	movl   $0x8010c4e0,0x4(%esp)
80104b4b:	80 
80104b4c:	89 04 24             	mov    %eax,(%esp)
80104b4f:	e8 90 39 00 00       	call   801084e4 <inituvm>
  p->sz = PGSIZE;
80104b54:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b57:	c7 00 00 10 00 00    	movl   $0x1000,(%eax)
  memset(p->tf, 0, sizeof(*p->tf));
80104b5d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b60:	8b 40 18             	mov    0x18(%eax),%eax
80104b63:	c7 44 24 08 4c 00 00 	movl   $0x4c,0x8(%esp)
80104b6a:	00 
80104b6b:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80104b72:	00 
80104b73:	89 04 24             	mov    %eax,(%esp)
80104b76:	e8 ef 0b 00 00       	call   8010576a <memset>
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
80104b7b:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b7e:	8b 40 18             	mov    0x18(%eax),%eax
80104b81:	66 c7 40 3c 23 00    	movw   $0x23,0x3c(%eax)
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
80104b87:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b8a:	8b 40 18             	mov    0x18(%eax),%eax
80104b8d:	66 c7 40 2c 2b 00    	movw   $0x2b,0x2c(%eax)
  p->tf->es = p->tf->ds;
80104b93:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b96:	8b 40 18             	mov    0x18(%eax),%eax
80104b99:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104b9c:	8b 52 18             	mov    0x18(%edx),%edx
80104b9f:	0f b7 52 2c          	movzwl 0x2c(%edx),%edx
80104ba3:	66 89 50 28          	mov    %dx,0x28(%eax)
  p->tf->ss = p->tf->ds;
80104ba7:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104baa:	8b 40 18             	mov    0x18(%eax),%eax
80104bad:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104bb0:	8b 52 18             	mov    0x18(%edx),%edx
80104bb3:	0f b7 52 2c          	movzwl 0x2c(%edx),%edx
80104bb7:	66 89 50 48          	mov    %dx,0x48(%eax)
  p->tf->eflags = FL_IF;
80104bbb:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104bbe:	8b 40 18             	mov    0x18(%eax),%eax
80104bc1:	c7 40 40 00 02 00 00 	movl   $0x200,0x40(%eax)
  p->tf->esp = PGSIZE;
80104bc8:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104bcb:	8b 40 18             	mov    0x18(%eax),%eax
80104bce:	c7 40 44 00 10 00 00 	movl   $0x1000,0x44(%eax)
  p->tf->eip = 0;  // beginning of initcode.S
80104bd5:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104bd8:	8b 40 18             	mov    0x18(%eax),%eax
80104bdb:	c7 40 38 00 00 00 00 	movl   $0x0,0x38(%eax)

  safestrcpy(p->name, "initcode", sizeof(p->name));
80104be2:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104be5:	83 c0 6c             	add    $0x6c,%eax
80104be8:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
80104bef:	00 
80104bf0:	c7 44 24 04 29 90 10 	movl   $0x80109029,0x4(%esp)
80104bf7:	80 
80104bf8:	89 04 24             	mov    %eax,(%esp)
80104bfb:	e8 9d 0d 00 00       	call   8010599d <safestrcpy>
  p->cwd = namei("/");
80104c00:	c7 04 24 32 90 10 80 	movl   $0x80109032,(%esp)
80104c07:	e8 13 e0 ff ff       	call   80102c1f <namei>
80104c0c:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104c0f:	89 42 68             	mov    %eax,0x68(%edx)

  p->state = RUNNABLE;
80104c12:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104c15:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
}
80104c1c:	c9                   	leave  
80104c1d:	c3                   	ret    

80104c1e <growproc>:

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
80104c1e:	55                   	push   %ebp
80104c1f:	89 e5                	mov    %esp,%ebp
80104c21:	83 ec 28             	sub    $0x28,%esp
  uint sz;

  sz = proc->sz;
80104c24:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104c2a:	8b 00                	mov    (%eax),%eax
80104c2c:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(n > 0){
80104c2f:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80104c33:	7e 34                	jle    80104c69 <growproc+0x4b>
    if((sz = allocuvm(proc->pgdir, sz, sz + n)) == 0)
80104c35:	8b 45 08             	mov    0x8(%ebp),%eax
80104c38:	89 c2                	mov    %eax,%edx
80104c3a:	03 55 f4             	add    -0xc(%ebp),%edx
80104c3d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104c43:	8b 40 04             	mov    0x4(%eax),%eax
80104c46:	89 54 24 08          	mov    %edx,0x8(%esp)
80104c4a:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104c4d:	89 54 24 04          	mov    %edx,0x4(%esp)
80104c51:	89 04 24             	mov    %eax,(%esp)
80104c54:	e8 1d 3a 00 00       	call   80108676 <allocuvm>
80104c59:	89 45 f4             	mov    %eax,-0xc(%ebp)
80104c5c:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104c60:	75 41                	jne    80104ca3 <growproc+0x85>
      return -1;
80104c62:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104c67:	eb 58                	jmp    80104cc1 <growproc+0xa3>
  } else if(n < 0){
80104c69:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80104c6d:	79 34                	jns    80104ca3 <growproc+0x85>
    if((sz = deallocuvm(proc->pgdir, sz, sz + n)) == 0)
80104c6f:	8b 45 08             	mov    0x8(%ebp),%eax
80104c72:	89 c2                	mov    %eax,%edx
80104c74:	03 55 f4             	add    -0xc(%ebp),%edx
80104c77:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104c7d:	8b 40 04             	mov    0x4(%eax),%eax
80104c80:	89 54 24 08          	mov    %edx,0x8(%esp)
80104c84:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104c87:	89 54 24 04          	mov    %edx,0x4(%esp)
80104c8b:	89 04 24             	mov    %eax,(%esp)
80104c8e:	e8 cb 3a 00 00       	call   8010875e <deallocuvm>
80104c93:	89 45 f4             	mov    %eax,-0xc(%ebp)
80104c96:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104c9a:	75 07                	jne    80104ca3 <growproc+0x85>
      return -1;
80104c9c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104ca1:	eb 1e                	jmp    80104cc1 <growproc+0xa3>
  }
  proc->sz = sz;
80104ca3:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104ca9:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104cac:	89 10                	mov    %edx,(%eax)
  switchuvm(proc);
80104cae:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104cb4:	89 04 24             	mov    %eax,(%esp)
80104cb7:	e8 c4 36 00 00       	call   80108380 <switchuvm>
  return 0;
80104cbc:	b8 00 00 00 00       	mov    $0x0,%eax
}
80104cc1:	c9                   	leave  
80104cc2:	c3                   	ret    

80104cc3 <fork>:
// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int
fork(void)
{
80104cc3:	55                   	push   %ebp
80104cc4:	89 e5                	mov    %esp,%ebp
80104cc6:	57                   	push   %edi
80104cc7:	56                   	push   %esi
80104cc8:	53                   	push   %ebx
80104cc9:	83 ec 2c             	sub    $0x2c,%esp
  int i, pid;
  struct proc *np;

  // Allocate process.
  if((np = allocproc()) == 0)
80104ccc:	e8 2d fd ff ff       	call   801049fe <allocproc>
80104cd1:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80104cd4:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
80104cd8:	75 0a                	jne    80104ce4 <fork+0x21>
    return -1;
80104cda:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104cdf:	e9 52 01 00 00       	jmp    80104e36 <fork+0x173>

  // Copy process state from p.
  if((np->pgdir = copyuvm(proc->pgdir, proc->sz)) == 0){
80104ce4:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104cea:	8b 10                	mov    (%eax),%edx
80104cec:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104cf2:	8b 40 04             	mov    0x4(%eax),%eax
80104cf5:	89 54 24 04          	mov    %edx,0x4(%esp)
80104cf9:	89 04 24             	mov    %eax,(%esp)
80104cfc:	e8 e4 3b 00 00       	call   801088e5 <copyuvm>
80104d01:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80104d04:	89 42 04             	mov    %eax,0x4(%edx)
80104d07:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d0a:	8b 40 04             	mov    0x4(%eax),%eax
80104d0d:	85 c0                	test   %eax,%eax
80104d0f:	75 2c                	jne    80104d3d <fork+0x7a>
    kfree(np->kstack);
80104d11:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d14:	8b 40 08             	mov    0x8(%eax),%eax
80104d17:	89 04 24             	mov    %eax,(%esp)
80104d1a:	e8 e9 e5 ff ff       	call   80103308 <kfree>
    np->kstack = 0;
80104d1f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d22:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
    np->state = UNUSED;
80104d29:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d2c:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
    return -1;
80104d33:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104d38:	e9 f9 00 00 00       	jmp    80104e36 <fork+0x173>
  }
  np->sz = proc->sz;
80104d3d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104d43:	8b 10                	mov    (%eax),%edx
80104d45:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d48:	89 10                	mov    %edx,(%eax)
  np->parent = proc;
80104d4a:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80104d51:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d54:	89 50 14             	mov    %edx,0x14(%eax)
  *np->tf = *proc->tf;
80104d57:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d5a:	8b 50 18             	mov    0x18(%eax),%edx
80104d5d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104d63:	8b 40 18             	mov    0x18(%eax),%eax
80104d66:	89 c3                	mov    %eax,%ebx
80104d68:	b8 13 00 00 00       	mov    $0x13,%eax
80104d6d:	89 d7                	mov    %edx,%edi
80104d6f:	89 de                	mov    %ebx,%esi
80104d71:	89 c1                	mov    %eax,%ecx
80104d73:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;
80104d75:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d78:	8b 40 18             	mov    0x18(%eax),%eax
80104d7b:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)

  for(i = 0; i < NOFILE; i++)
80104d82:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
80104d89:	eb 3d                	jmp    80104dc8 <fork+0x105>
    if(proc->ofile[i])
80104d8b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104d91:	8b 55 dc             	mov    -0x24(%ebp),%edx
80104d94:	83 c2 08             	add    $0x8,%edx
80104d97:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80104d9b:	85 c0                	test   %eax,%eax
80104d9d:	74 25                	je     80104dc4 <fork+0x101>
      np->ofile[i] = filedup(proc->ofile[i]);
80104d9f:	8b 5d dc             	mov    -0x24(%ebp),%ebx
80104da2:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104da8:	8b 55 dc             	mov    -0x24(%ebp),%edx
80104dab:	83 c2 08             	add    $0x8,%edx
80104dae:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80104db2:	89 04 24             	mov    %eax,(%esp)
80104db5:	e8 5f c9 ff ff       	call   80101719 <filedup>
80104dba:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80104dbd:	8d 4b 08             	lea    0x8(%ebx),%ecx
80104dc0:	89 44 8a 08          	mov    %eax,0x8(%edx,%ecx,4)
  *np->tf = *proc->tf;

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
80104dc4:	83 45 dc 01          	addl   $0x1,-0x24(%ebp)
80104dc8:	83 7d dc 0f          	cmpl   $0xf,-0x24(%ebp)
80104dcc:	7e bd                	jle    80104d8b <fork+0xc8>
    if(proc->ofile[i])
      np->ofile[i] = filedup(proc->ofile[i]);
  np->cwd = idup(proc->cwd);
80104dce:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104dd4:	8b 40 68             	mov    0x68(%eax),%eax
80104dd7:	89 04 24             	mov    %eax,(%esp)
80104dda:	e8 60 d2 ff ff       	call   8010203f <idup>
80104ddf:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80104de2:	89 42 68             	mov    %eax,0x68(%edx)

  safestrcpy(np->name, proc->name, sizeof(proc->name));
80104de5:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104deb:	8d 50 6c             	lea    0x6c(%eax),%edx
80104dee:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104df1:	83 c0 6c             	add    $0x6c,%eax
80104df4:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
80104dfb:	00 
80104dfc:	89 54 24 04          	mov    %edx,0x4(%esp)
80104e00:	89 04 24             	mov    %eax,(%esp)
80104e03:	e8 95 0b 00 00       	call   8010599d <safestrcpy>

  pid = np->pid;
80104e08:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104e0b:	8b 40 10             	mov    0x10(%eax),%eax
80104e0e:	89 45 e0             	mov    %eax,-0x20(%ebp)

  // lock to force the compiler to emit the np->state write last.
  acquire(&ptable.lock);
80104e11:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
80104e18:	e8 ea 06 00 00       	call   80105507 <acquire>
  np->state = RUNNABLE;
80104e1d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104e20:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  release(&ptable.lock);
80104e27:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
80104e2e:	e8 3d 07 00 00       	call   80105570 <release>

  return pid;
80104e33:	8b 45 e0             	mov    -0x20(%ebp),%eax
}
80104e36:	83 c4 2c             	add    $0x2c,%esp
80104e39:	5b                   	pop    %ebx
80104e3a:	5e                   	pop    %esi
80104e3b:	5f                   	pop    %edi
80104e3c:	5d                   	pop    %ebp
80104e3d:	c3                   	ret    

80104e3e <exit>:
// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
void
exit(void)
{
80104e3e:	55                   	push   %ebp
80104e3f:	89 e5                	mov    %esp,%ebp
80104e41:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;
  int fd;

  if(proc == initproc)
80104e44:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80104e4b:	a1 48 c6 10 80       	mov    0x8010c648,%eax
80104e50:	39 c2                	cmp    %eax,%edx
80104e52:	75 0c                	jne    80104e60 <exit+0x22>
    panic("init exiting");
80104e54:	c7 04 24 34 90 10 80 	movl   $0x80109034,(%esp)
80104e5b:	e8 09 ba ff ff       	call   80100869 <panic>

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
80104e60:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80104e67:	eb 44                	jmp    80104ead <exit+0x6f>
    if(proc->ofile[fd]){
80104e69:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104e6f:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104e72:	83 c2 08             	add    $0x8,%edx
80104e75:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80104e79:	85 c0                	test   %eax,%eax
80104e7b:	74 2c                	je     80104ea9 <exit+0x6b>
      fileclose(proc->ofile[fd]);
80104e7d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104e83:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104e86:	83 c2 08             	add    $0x8,%edx
80104e89:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80104e8d:	89 04 24             	mov    %eax,(%esp)
80104e90:	e8 cc c8 ff ff       	call   80101761 <fileclose>
      proc->ofile[fd] = 0;
80104e95:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104e9b:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104e9e:	83 c2 08             	add    $0x8,%edx
80104ea1:	c7 44 90 08 00 00 00 	movl   $0x0,0x8(%eax,%edx,4)
80104ea8:	00 

  if(proc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
80104ea9:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80104ead:	83 7d f4 0f          	cmpl   $0xf,-0xc(%ebp)
80104eb1:	7e b6                	jle    80104e69 <exit+0x2b>
      fileclose(proc->ofile[fd]);
      proc->ofile[fd] = 0;
    }
  }

  begin_op();
80104eb3:	e8 b2 eb ff ff       	call   80103a6a <begin_op>
  iput(proc->cwd);
80104eb8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104ebe:	8b 40 68             	mov    0x68(%eax),%eax
80104ec1:	89 04 24             	mov    %eax,(%esp)
80104ec4:	e8 64 d3 ff ff       	call   8010222d <iput>
  end_op();
80104ec9:	e8 1e ec ff ff       	call   80103aec <end_op>
  proc->cwd = 0;
80104ece:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104ed4:	c7 40 68 00 00 00 00 	movl   $0x0,0x68(%eax)

  acquire(&ptable.lock);
80104edb:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
80104ee2:	e8 20 06 00 00       	call   80105507 <acquire>

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);
80104ee7:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104eed:	8b 40 14             	mov    0x14(%eax),%eax
80104ef0:	89 04 24             	mov    %eax,(%esp)
80104ef3:	e8 d1 03 00 00       	call   801052c9 <wakeup1>

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104ef8:	c7 45 f0 b4 41 11 80 	movl   $0x801141b4,-0x10(%ebp)
80104eff:	eb 38                	jmp    80104f39 <exit+0xfb>
    if(p->parent == proc){
80104f01:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104f04:	8b 50 14             	mov    0x14(%eax),%edx
80104f07:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104f0d:	39 c2                	cmp    %eax,%edx
80104f0f:	75 24                	jne    80104f35 <exit+0xf7>
      p->parent = initproc;
80104f11:	8b 15 48 c6 10 80    	mov    0x8010c648,%edx
80104f17:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104f1a:	89 50 14             	mov    %edx,0x14(%eax)
      if(p->state == ZOMBIE)
80104f1d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104f20:	8b 40 0c             	mov    0xc(%eax),%eax
80104f23:	83 f8 05             	cmp    $0x5,%eax
80104f26:	75 0d                	jne    80104f35 <exit+0xf7>
        wakeup1(initproc);
80104f28:	a1 48 c6 10 80       	mov    0x8010c648,%eax
80104f2d:	89 04 24             	mov    %eax,(%esp)
80104f30:	e8 94 03 00 00       	call   801052c9 <wakeup1>

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104f35:	83 45 f0 7c          	addl   $0x7c,-0x10(%ebp)
80104f39:	b8 b4 60 11 80       	mov    $0x801160b4,%eax
80104f3e:	39 45 f0             	cmp    %eax,-0x10(%ebp)
80104f41:	72 be                	jb     80104f01 <exit+0xc3>
        wakeup1(initproc);
    }
  }

  // Jump into the scheduler, never to return.
  proc->state = ZOMBIE;
80104f43:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104f49:	c7 40 0c 05 00 00 00 	movl   $0x5,0xc(%eax)
  sched();
80104f50:	e8 b6 01 00 00       	call   8010510b <sched>
  panic("zombie exit");
80104f55:	c7 04 24 41 90 10 80 	movl   $0x80109041,(%esp)
80104f5c:	e8 08 b9 ff ff       	call   80100869 <panic>

80104f61 <wait>:

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
int
wait(void)
{
80104f61:	55                   	push   %ebp
80104f62:	89 e5                	mov    %esp,%ebp
80104f64:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;
  int havekids, pid;

  acquire(&ptable.lock);
80104f67:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
80104f6e:	e8 94 05 00 00       	call   80105507 <acquire>
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
80104f73:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104f7a:	c7 45 ec b4 41 11 80 	movl   $0x801141b4,-0x14(%ebp)
80104f81:	e9 9a 00 00 00       	jmp    80105020 <wait+0xbf>
      if(p->parent != proc)
80104f86:	8b 45 ec             	mov    -0x14(%ebp),%eax
80104f89:	8b 50 14             	mov    0x14(%eax),%edx
80104f8c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104f92:	39 c2                	cmp    %eax,%edx
80104f94:	0f 85 81 00 00 00    	jne    8010501b <wait+0xba>
        continue;
      havekids = 1;
80104f9a:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
      if(p->state == ZOMBIE){
80104fa1:	8b 45 ec             	mov    -0x14(%ebp),%eax
80104fa4:	8b 40 0c             	mov    0xc(%eax),%eax
80104fa7:	83 f8 05             	cmp    $0x5,%eax
80104faa:	75 70                	jne    8010501c <wait+0xbb>
        // Found one.
        pid = p->pid;
80104fac:	8b 45 ec             	mov    -0x14(%ebp),%eax
80104faf:	8b 40 10             	mov    0x10(%eax),%eax
80104fb2:	89 45 f4             	mov    %eax,-0xc(%ebp)
        kfree(p->kstack);
80104fb5:	8b 45 ec             	mov    -0x14(%ebp),%eax
80104fb8:	8b 40 08             	mov    0x8(%eax),%eax
80104fbb:	89 04 24             	mov    %eax,(%esp)
80104fbe:	e8 45 e3 ff ff       	call   80103308 <kfree>
        p->kstack = 0;
80104fc3:	8b 45 ec             	mov    -0x14(%ebp),%eax
80104fc6:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
        freevm(p->pgdir);
80104fcd:	8b 45 ec             	mov    -0x14(%ebp),%eax
80104fd0:	8b 40 04             	mov    0x4(%eax),%eax
80104fd3:	89 04 24             	mov    %eax,(%esp)
80104fd6:	e8 39 38 00 00       	call   80108814 <freevm>
        p->state = UNUSED;
80104fdb:	8b 45 ec             	mov    -0x14(%ebp),%eax
80104fde:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
        p->pid = 0;
80104fe5:	8b 45 ec             	mov    -0x14(%ebp),%eax
80104fe8:	c7 40 10 00 00 00 00 	movl   $0x0,0x10(%eax)
        p->parent = 0;
80104fef:	8b 45 ec             	mov    -0x14(%ebp),%eax
80104ff2:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
        p->name[0] = 0;
80104ff9:	8b 45 ec             	mov    -0x14(%ebp),%eax
80104ffc:	c6 40 6c 00          	movb   $0x0,0x6c(%eax)
        p->killed = 0;
80105000:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105003:	c7 40 24 00 00 00 00 	movl   $0x0,0x24(%eax)
        release(&ptable.lock);
8010500a:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
80105011:	e8 5a 05 00 00       	call   80105570 <release>
        return pid;
80105016:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105019:	eb 54                	jmp    8010506f <wait+0x10e>
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->parent != proc)
        continue;
8010501b:	90                   	nop

  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010501c:	83 45 ec 7c          	addl   $0x7c,-0x14(%ebp)
80105020:	b8 b4 60 11 80       	mov    $0x801160b4,%eax
80105025:	39 45 ec             	cmp    %eax,-0x14(%ebp)
80105028:	0f 82 58 ff ff ff    	jb     80104f86 <wait+0x25>
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
8010502e:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80105032:	74 0d                	je     80105041 <wait+0xe0>
80105034:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010503a:	8b 40 24             	mov    0x24(%eax),%eax
8010503d:	85 c0                	test   %eax,%eax
8010503f:	74 13                	je     80105054 <wait+0xf3>
      release(&ptable.lock);
80105041:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
80105048:	e8 23 05 00 00       	call   80105570 <release>
      return -1;
8010504d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105052:	eb 1b                	jmp    8010506f <wait+0x10e>
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
80105054:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010505a:	c7 44 24 04 80 41 11 	movl   $0x80114180,0x4(%esp)
80105061:	80 
80105062:	89 04 24             	mov    %eax,(%esp)
80105065:	e8 c4 01 00 00       	call   8010522e <sleep>
  }
8010506a:	e9 04 ff ff ff       	jmp    80104f73 <wait+0x12>
}
8010506f:	c9                   	leave  
80105070:	c3                   	ret    

80105071 <scheduler>:
//  - swtch to start running that process
//  - eventually that process transfers control
//      via swtch back to the scheduler.
void
scheduler(void)
{
80105071:	55                   	push   %ebp
80105072:	89 e5                	mov    %esp,%ebp
80105074:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;

  for(;;){
    // Enable interrupts on this processor.
    sti();
80105077:	e8 60 f9 ff ff       	call   801049dc <sti>

    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
8010507c:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
80105083:	e8 7f 04 00 00       	call   80105507 <acquire>
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80105088:	c7 45 f4 b4 41 11 80 	movl   $0x801141b4,-0xc(%ebp)
8010508f:	eb 5f                	jmp    801050f0 <scheduler+0x7f>
      if(p->state != RUNNABLE)
80105091:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105094:	8b 40 0c             	mov    0xc(%eax),%eax
80105097:	83 f8 03             	cmp    $0x3,%eax
8010509a:	75 4f                	jne    801050eb <scheduler+0x7a>
        continue;

      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      proc = p;
8010509c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010509f:	65 a3 04 00 00 00    	mov    %eax,%gs:0x4
      switchuvm(p);
801050a5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801050a8:	89 04 24             	mov    %eax,(%esp)
801050ab:	e8 d0 32 00 00       	call   80108380 <switchuvm>
      p->state = RUNNING;
801050b0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801050b3:	c7 40 0c 04 00 00 00 	movl   $0x4,0xc(%eax)
      swtch(&cpu->scheduler, proc->context);
801050ba:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801050c0:	8b 40 1c             	mov    0x1c(%eax),%eax
801050c3:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
801050ca:	83 c2 04             	add    $0x4,%edx
801050cd:	89 44 24 04          	mov    %eax,0x4(%esp)
801050d1:	89 14 24             	mov    %edx,(%esp)
801050d4:	e8 37 09 00 00       	call   80105a10 <swtch>
      switchkvm();
801050d9:	e8 88 32 00 00       	call   80108366 <switchkvm>

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      proc = 0;
801050de:	65 c7 05 04 00 00 00 	movl   $0x0,%gs:0x4
801050e5:	00 00 00 00 
801050e9:	eb 01                	jmp    801050ec <scheduler+0x7b>

    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->state != RUNNABLE)
        continue;
801050eb:	90                   	nop
    // Enable interrupts on this processor.
    sti();

    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801050ec:	83 45 f4 7c          	addl   $0x7c,-0xc(%ebp)
801050f0:	b8 b4 60 11 80       	mov    $0x801160b4,%eax
801050f5:	39 45 f4             	cmp    %eax,-0xc(%ebp)
801050f8:	72 97                	jb     80105091 <scheduler+0x20>

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      proc = 0;
    }
    release(&ptable.lock);
801050fa:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
80105101:	e8 6a 04 00 00       	call   80105570 <release>

  }
80105106:	e9 6c ff ff ff       	jmp    80105077 <scheduler+0x6>

8010510b <sched>:

// Enter scheduler.  Must hold only ptable.lock
// and have changed proc->state.
void
sched(void)
{
8010510b:	55                   	push   %ebp
8010510c:	89 e5                	mov    %esp,%ebp
8010510e:	83 ec 28             	sub    $0x28,%esp
  int intena;

  if(!holding(&ptable.lock))
80105111:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
80105118:	e8 1e 05 00 00       	call   8010563b <holding>
8010511d:	85 c0                	test   %eax,%eax
8010511f:	75 0c                	jne    8010512d <sched+0x22>
    panic("sched ptable.lock");
80105121:	c7 04 24 4d 90 10 80 	movl   $0x8010904d,(%esp)
80105128:	e8 3c b7 ff ff       	call   80100869 <panic>
  if(cpu->ncli != 1)
8010512d:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105133:	8b 80 ac 00 00 00    	mov    0xac(%eax),%eax
80105139:	83 f8 01             	cmp    $0x1,%eax
8010513c:	74 0c                	je     8010514a <sched+0x3f>
    panic("sched locks");
8010513e:	c7 04 24 5f 90 10 80 	movl   $0x8010905f,(%esp)
80105145:	e8 1f b7 ff ff       	call   80100869 <panic>
  if(proc->state == RUNNING)
8010514a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105150:	8b 40 0c             	mov    0xc(%eax),%eax
80105153:	83 f8 04             	cmp    $0x4,%eax
80105156:	75 0c                	jne    80105164 <sched+0x59>
    panic("sched running");
80105158:	c7 04 24 6b 90 10 80 	movl   $0x8010906b,(%esp)
8010515f:	e8 05 b7 ff ff       	call   80100869 <panic>
  if(readeflags()&FL_IF)
80105164:	e8 63 f8 ff ff       	call   801049cc <readeflags>
80105169:	25 00 02 00 00       	and    $0x200,%eax
8010516e:	85 c0                	test   %eax,%eax
80105170:	74 0c                	je     8010517e <sched+0x73>
    panic("sched interruptible");
80105172:	c7 04 24 79 90 10 80 	movl   $0x80109079,(%esp)
80105179:	e8 eb b6 ff ff       	call   80100869 <panic>
  intena = cpu->intena;
8010517e:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105184:	8b 80 b0 00 00 00    	mov    0xb0(%eax),%eax
8010518a:	89 45 f4             	mov    %eax,-0xc(%ebp)
  swtch(&proc->context, cpu->scheduler);
8010518d:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105193:	8b 40 04             	mov    0x4(%eax),%eax
80105196:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
8010519d:	83 c2 1c             	add    $0x1c,%edx
801051a0:	89 44 24 04          	mov    %eax,0x4(%esp)
801051a4:	89 14 24             	mov    %edx,(%esp)
801051a7:	e8 64 08 00 00       	call   80105a10 <swtch>
  cpu->intena = intena;
801051ac:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801051b2:	8b 55 f4             	mov    -0xc(%ebp),%edx
801051b5:	89 90 b0 00 00 00    	mov    %edx,0xb0(%eax)
}
801051bb:	c9                   	leave  
801051bc:	c3                   	ret    

801051bd <yield>:

// Give up the CPU for one scheduling round.
void
yield(void)
{
801051bd:	55                   	push   %ebp
801051be:	89 e5                	mov    %esp,%ebp
801051c0:	83 ec 18             	sub    $0x18,%esp
  acquire(&ptable.lock);  //DOC: yieldlock
801051c3:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
801051ca:	e8 38 03 00 00       	call   80105507 <acquire>
  proc->state = RUNNABLE;
801051cf:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801051d5:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  sched();
801051dc:	e8 2a ff ff ff       	call   8010510b <sched>
  release(&ptable.lock);
801051e1:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
801051e8:	e8 83 03 00 00       	call   80105570 <release>
}
801051ed:	c9                   	leave  
801051ee:	c3                   	ret    

801051ef <forkret>:

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
801051ef:	55                   	push   %ebp
801051f0:	89 e5                	mov    %esp,%ebp
801051f2:	83 ec 18             	sub    $0x18,%esp
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);
801051f5:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
801051fc:	e8 6f 03 00 00       	call   80105570 <release>

  if (first) {
80105201:	a1 20 c0 10 80       	mov    0x8010c020,%eax
80105206:	85 c0                	test   %eax,%eax
80105208:	74 22                	je     8010522c <forkret+0x3d>
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot
    // be run from main().
    first = 0;
8010520a:	c7 05 20 c0 10 80 00 	movl   $0x0,0x8010c020
80105211:	00 00 00 
    iinit(ROOTDEV);
80105214:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
8010521b:	e8 23 cb ff ff       	call   80101d43 <iinit>
    initlog(ROOTDEV);
80105220:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80105227:	e8 3c e6 ff ff       	call   80103868 <initlog>
  }

  // Return to "caller", actually trapret (see allocproc).
}
8010522c:	c9                   	leave  
8010522d:	c3                   	ret    

8010522e <sleep>:

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
void
sleep(void *chan, struct spinlock *lk)
{
8010522e:	55                   	push   %ebp
8010522f:	89 e5                	mov    %esp,%ebp
80105231:	83 ec 18             	sub    $0x18,%esp
  if(proc == 0)
80105234:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010523a:	85 c0                	test   %eax,%eax
8010523c:	75 0c                	jne    8010524a <sleep+0x1c>
    panic("sleep");
8010523e:	c7 04 24 8d 90 10 80 	movl   $0x8010908d,(%esp)
80105245:	e8 1f b6 ff ff       	call   80100869 <panic>

  if(lk == 0)
8010524a:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
8010524e:	75 0c                	jne    8010525c <sleep+0x2e>
    panic("sleep without lk");
80105250:	c7 04 24 93 90 10 80 	movl   $0x80109093,(%esp)
80105257:	e8 0d b6 ff ff       	call   80100869 <panic>
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.
  if(lk != &ptable.lock){  //DOC: sleeplock0
8010525c:	81 7d 0c 80 41 11 80 	cmpl   $0x80114180,0xc(%ebp)
80105263:	74 17                	je     8010527c <sleep+0x4e>
    acquire(&ptable.lock);  //DOC: sleeplock1
80105265:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
8010526c:	e8 96 02 00 00       	call   80105507 <acquire>
    release(lk);
80105271:	8b 45 0c             	mov    0xc(%ebp),%eax
80105274:	89 04 24             	mov    %eax,(%esp)
80105277:	e8 f4 02 00 00       	call   80105570 <release>
  }

  // Go to sleep.
  proc->chan = chan;
8010527c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105282:	8b 55 08             	mov    0x8(%ebp),%edx
80105285:	89 50 20             	mov    %edx,0x20(%eax)
  proc->state = SLEEPING;
80105288:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010528e:	c7 40 0c 02 00 00 00 	movl   $0x2,0xc(%eax)
  sched();
80105295:	e8 71 fe ff ff       	call   8010510b <sched>

  // Tidy up.
  proc->chan = 0;
8010529a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801052a0:	c7 40 20 00 00 00 00 	movl   $0x0,0x20(%eax)

  // Reacquire original lock.
  if(lk != &ptable.lock){  //DOC: sleeplock2
801052a7:	81 7d 0c 80 41 11 80 	cmpl   $0x80114180,0xc(%ebp)
801052ae:	74 17                	je     801052c7 <sleep+0x99>
    release(&ptable.lock);
801052b0:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
801052b7:	e8 b4 02 00 00       	call   80105570 <release>
    acquire(lk);
801052bc:	8b 45 0c             	mov    0xc(%ebp),%eax
801052bf:	89 04 24             	mov    %eax,(%esp)
801052c2:	e8 40 02 00 00       	call   80105507 <acquire>
  }
}
801052c7:	c9                   	leave  
801052c8:	c3                   	ret    

801052c9 <wakeup1>:
//PAGEBREAK!
// Wake up all processes sleeping on chan.
// The ptable lock must be held.
static void
wakeup1(void *chan)
{
801052c9:	55                   	push   %ebp
801052ca:	89 e5                	mov    %esp,%ebp
801052cc:	83 ec 10             	sub    $0x10,%esp
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801052cf:	c7 45 fc b4 41 11 80 	movl   $0x801141b4,-0x4(%ebp)
801052d6:	eb 24                	jmp    801052fc <wakeup1+0x33>
    if(p->state == SLEEPING && p->chan == chan)
801052d8:	8b 45 fc             	mov    -0x4(%ebp),%eax
801052db:	8b 40 0c             	mov    0xc(%eax),%eax
801052de:	83 f8 02             	cmp    $0x2,%eax
801052e1:	75 15                	jne    801052f8 <wakeup1+0x2f>
801052e3:	8b 45 fc             	mov    -0x4(%ebp),%eax
801052e6:	8b 40 20             	mov    0x20(%eax),%eax
801052e9:	3b 45 08             	cmp    0x8(%ebp),%eax
801052ec:	75 0a                	jne    801052f8 <wakeup1+0x2f>
      p->state = RUNNABLE;
801052ee:	8b 45 fc             	mov    -0x4(%ebp),%eax
801052f1:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801052f8:	83 45 fc 7c          	addl   $0x7c,-0x4(%ebp)
801052fc:	b8 b4 60 11 80       	mov    $0x801160b4,%eax
80105301:	39 45 fc             	cmp    %eax,-0x4(%ebp)
80105304:	72 d2                	jb     801052d8 <wakeup1+0xf>
    if(p->state == SLEEPING && p->chan == chan)
      p->state = RUNNABLE;
}
80105306:	c9                   	leave  
80105307:	c3                   	ret    

80105308 <wakeup>:

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
80105308:	55                   	push   %ebp
80105309:	89 e5                	mov    %esp,%ebp
8010530b:	83 ec 18             	sub    $0x18,%esp
  acquire(&ptable.lock);
8010530e:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
80105315:	e8 ed 01 00 00       	call   80105507 <acquire>
  wakeup1(chan);
8010531a:	8b 45 08             	mov    0x8(%ebp),%eax
8010531d:	89 04 24             	mov    %eax,(%esp)
80105320:	e8 a4 ff ff ff       	call   801052c9 <wakeup1>
  release(&ptable.lock);
80105325:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
8010532c:	e8 3f 02 00 00       	call   80105570 <release>
}
80105331:	c9                   	leave  
80105332:	c3                   	ret    

80105333 <kill>:
// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
{
80105333:	55                   	push   %ebp
80105334:	89 e5                	mov    %esp,%ebp
80105336:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;

  acquire(&ptable.lock);
80105339:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
80105340:	e8 c2 01 00 00       	call   80105507 <acquire>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80105345:	c7 45 f4 b4 41 11 80 	movl   $0x801141b4,-0xc(%ebp)
8010534c:	eb 41                	jmp    8010538f <kill+0x5c>
    if(p->pid == pid){
8010534e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105351:	8b 40 10             	mov    0x10(%eax),%eax
80105354:	3b 45 08             	cmp    0x8(%ebp),%eax
80105357:	75 32                	jne    8010538b <kill+0x58>
      p->killed = 1;
80105359:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010535c:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
80105363:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105366:	8b 40 0c             	mov    0xc(%eax),%eax
80105369:	83 f8 02             	cmp    $0x2,%eax
8010536c:	75 0a                	jne    80105378 <kill+0x45>
        p->state = RUNNABLE;
8010536e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105371:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
      release(&ptable.lock);
80105378:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
8010537f:	e8 ec 01 00 00       	call   80105570 <release>
      return 0;
80105384:	b8 00 00 00 00       	mov    $0x0,%eax
80105389:	eb 1f                	jmp    801053aa <kill+0x77>
kill(int pid)
{
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010538b:	83 45 f4 7c          	addl   $0x7c,-0xc(%ebp)
8010538f:	b8 b4 60 11 80       	mov    $0x801160b4,%eax
80105394:	39 45 f4             	cmp    %eax,-0xc(%ebp)
80105397:	72 b5                	jb     8010534e <kill+0x1b>
        p->state = RUNNABLE;
      release(&ptable.lock);
      return 0;
    }
  }
  release(&ptable.lock);
80105399:	c7 04 24 80 41 11 80 	movl   $0x80114180,(%esp)
801053a0:	e8 cb 01 00 00       	call   80105570 <release>
  return -1;
801053a5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801053aa:	c9                   	leave  
801053ab:	c3                   	ret    

801053ac <procdump>:
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
801053ac:	55                   	push   %ebp
801053ad:	89 e5                	mov    %esp,%ebp
801053af:	83 ec 58             	sub    $0x58,%esp
  int i;
  struct proc *p;
  char *state;
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801053b2:	c7 45 f0 b4 41 11 80 	movl   $0x801141b4,-0x10(%ebp)
801053b9:	e9 e0 00 00 00       	jmp    8010549e <procdump+0xf2>
    if(p->state == UNUSED)
801053be:	8b 45 f0             	mov    -0x10(%ebp),%eax
801053c1:	8b 40 0c             	mov    0xc(%eax),%eax
801053c4:	85 c0                	test   %eax,%eax
801053c6:	0f 84 cd 00 00 00    	je     80105499 <procdump+0xed>
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
801053cc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801053cf:	8b 40 0c             	mov    0xc(%eax),%eax
801053d2:	83 f8 05             	cmp    $0x5,%eax
801053d5:	77 23                	ja     801053fa <procdump+0x4e>
801053d7:	8b 45 f0             	mov    -0x10(%ebp),%eax
801053da:	8b 40 0c             	mov    0xc(%eax),%eax
801053dd:	8b 04 85 08 c0 10 80 	mov    -0x7fef3ff8(,%eax,4),%eax
801053e4:	85 c0                	test   %eax,%eax
801053e6:	74 12                	je     801053fa <procdump+0x4e>
      state = states[p->state];
801053e8:	8b 45 f0             	mov    -0x10(%ebp),%eax
801053eb:	8b 40 0c             	mov    0xc(%eax),%eax
801053ee:	8b 04 85 08 c0 10 80 	mov    -0x7fef3ff8(,%eax,4),%eax
801053f5:	89 45 f4             	mov    %eax,-0xc(%ebp)
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
801053f8:	eb 07                	jmp    80105401 <procdump+0x55>
      state = states[p->state];
    else
      state = "???";
801053fa:	c7 45 f4 a4 90 10 80 	movl   $0x801090a4,-0xc(%ebp)
    cprintf("%d %s %s", p->pid, state, p->name);
80105401:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105404:	8d 50 6c             	lea    0x6c(%eax),%edx
80105407:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010540a:	8b 40 10             	mov    0x10(%eax),%eax
8010540d:	89 54 24 0c          	mov    %edx,0xc(%esp)
80105411:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105414:	89 54 24 08          	mov    %edx,0x8(%esp)
80105418:	89 44 24 04          	mov    %eax,0x4(%esp)
8010541c:	c7 04 24 a8 90 10 80 	movl   $0x801090a8,(%esp)
80105423:	e8 a1 b2 ff ff       	call   801006c9 <cprintf>
    if(p->state == SLEEPING){
80105428:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010542b:	8b 40 0c             	mov    0xc(%eax),%eax
8010542e:	83 f8 02             	cmp    $0x2,%eax
80105431:	75 58                	jne    8010548b <procdump+0xdf>
      getcallerpcs((uint*)p->context->ebp+2, NELEM(pc), pc);
80105433:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105436:	8b 40 1c             	mov    0x1c(%eax),%eax
80105439:	8b 40 0c             	mov    0xc(%eax),%eax
8010543c:	83 c0 08             	add    $0x8,%eax
8010543f:	8d 55 c4             	lea    -0x3c(%ebp),%edx
80105442:	89 54 24 08          	mov    %edx,0x8(%esp)
80105446:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
8010544d:	00 
8010544e:	89 04 24             	mov    %eax,(%esp)
80105451:	e8 69 01 00 00       	call   801055bf <getcallerpcs>
      for(i=0; i<10 && pc[i] != 0; i++)
80105456:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
8010545d:	eb 1b                	jmp    8010547a <procdump+0xce>
        cprintf(" %p", pc[i]);
8010545f:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105462:	8b 44 85 c4          	mov    -0x3c(%ebp,%eax,4),%eax
80105466:	89 44 24 04          	mov    %eax,0x4(%esp)
8010546a:	c7 04 24 b1 90 10 80 	movl   $0x801090b1,(%esp)
80105471:	e8 53 b2 ff ff       	call   801006c9 <cprintf>
    else
      state = "???";
    cprintf("%d %s %s", p->pid, state, p->name);
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, NELEM(pc), pc);
      for(i=0; i<10 && pc[i] != 0; i++)
80105476:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
8010547a:	83 7d ec 09          	cmpl   $0x9,-0x14(%ebp)
8010547e:	7f 0b                	jg     8010548b <procdump+0xdf>
80105480:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105483:	8b 44 85 c4          	mov    -0x3c(%ebp,%eax,4),%eax
80105487:	85 c0                	test   %eax,%eax
80105489:	75 d4                	jne    8010545f <procdump+0xb3>
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
8010548b:	c7 04 24 b5 90 10 80 	movl   $0x801090b5,(%esp)
80105492:	e8 32 b2 ff ff       	call   801006c9 <cprintf>
80105497:	eb 01                	jmp    8010549a <procdump+0xee>
  char *state;
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
80105499:	90                   	nop
  int i;
  struct proc *p;
  char *state;
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010549a:	83 45 f0 7c          	addl   $0x7c,-0x10(%ebp)
8010549e:	b8 b4 60 11 80       	mov    $0x801160b4,%eax
801054a3:	39 45 f0             	cmp    %eax,-0x10(%ebp)
801054a6:	0f 82 12 ff ff ff    	jb     801053be <procdump+0x12>
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
  }
}
801054ac:	c9                   	leave  
801054ad:	c3                   	ret    
	...

801054b0 <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
801054b0:	55                   	push   %ebp
801054b1:	89 e5                	mov    %esp,%ebp
801054b3:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
801054b6:	9c                   	pushf  
801054b7:	58                   	pop    %eax
801054b8:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return eflags;
801054bb:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
801054be:	c9                   	leave  
801054bf:	c3                   	ret    

801054c0 <cli>:
  asm volatile("movw %0, %%gs" : : "r" (v));
}

static inline void
cli(void)
{
801054c0:	55                   	push   %ebp
801054c1:	89 e5                	mov    %esp,%ebp
  asm volatile("cli");
801054c3:	fa                   	cli    
}
801054c4:	5d                   	pop    %ebp
801054c5:	c3                   	ret    

801054c6 <sti>:

static inline void
sti(void)
{
801054c6:	55                   	push   %ebp
801054c7:	89 e5                	mov    %esp,%ebp
  asm volatile("sti");
801054c9:	fb                   	sti    
}
801054ca:	5d                   	pop    %ebp
801054cb:	c3                   	ret    

801054cc <xchg>:

static inline uint
xchg(volatile uint *addr, uint newval)
{
801054cc:	55                   	push   %ebp
801054cd:	89 e5                	mov    %esp,%ebp
801054cf:	83 ec 10             	sub    $0x10,%esp
  uint result;

  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
801054d2:	8b 55 08             	mov    0x8(%ebp),%edx
801054d5:	8b 45 0c             	mov    0xc(%ebp),%eax
801054d8:	8b 4d 08             	mov    0x8(%ebp),%ecx
801054db:	f0 87 02             	lock xchg %eax,(%edx)
801054de:	89 45 fc             	mov    %eax,-0x4(%ebp)
               "+m" (*addr), "=a" (result) :
               "1" (newval) :
               "cc");
  return result;
801054e1:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
801054e4:	c9                   	leave  
801054e5:	c3                   	ret    

801054e6 <initlock>:
#include "proc.h"
#include "spinlock.h"

void
initlock(struct spinlock *lk, char *name)
{
801054e6:	55                   	push   %ebp
801054e7:	89 e5                	mov    %esp,%ebp
  lk->name = name;
801054e9:	8b 45 08             	mov    0x8(%ebp),%eax
801054ec:	8b 55 0c             	mov    0xc(%ebp),%edx
801054ef:	89 50 04             	mov    %edx,0x4(%eax)
  lk->locked = 0;
801054f2:	8b 45 08             	mov    0x8(%ebp),%eax
801054f5:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  lk->cpu = 0;
801054fb:	8b 45 08             	mov    0x8(%ebp),%eax
801054fe:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
}
80105505:	5d                   	pop    %ebp
80105506:	c3                   	ret    

80105507 <acquire>:
// Loops (spins) until the lock is acquired.
// Holding a lock for a long time may cause
// other CPUs to waste time spinning to acquire it.
void
acquire(struct spinlock *lk)
{
80105507:	55                   	push   %ebp
80105508:	89 e5                	mov    %esp,%ebp
8010550a:	83 ec 18             	sub    $0x18,%esp
  pushcli(); // disable interrupts to avoid deadlock.
8010550d:	e8 53 01 00 00       	call   80105665 <pushcli>
  if(holding(lk))
80105512:	8b 45 08             	mov    0x8(%ebp),%eax
80105515:	89 04 24             	mov    %eax,(%esp)
80105518:	e8 1e 01 00 00       	call   8010563b <holding>
8010551d:	85 c0                	test   %eax,%eax
8010551f:	74 0c                	je     8010552d <acquire+0x26>
    panic("acquire");
80105521:	c7 04 24 e1 90 10 80 	movl   $0x801090e1,(%esp)
80105528:	e8 3c b3 ff ff       	call   80100869 <panic>

  // The xchg is atomic.
  // It also serializes, so that reads after acquire are not
  // reordered before it. 
  while(xchg(&lk->locked, 1) != 0)
8010552d:	8b 45 08             	mov    0x8(%ebp),%eax
80105530:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80105537:	00 
80105538:	89 04 24             	mov    %eax,(%esp)
8010553b:	e8 8c ff ff ff       	call   801054cc <xchg>
80105540:	85 c0                	test   %eax,%eax
80105542:	75 e9                	jne    8010552d <acquire+0x26>
    ;

  // Record info about lock acquisition for debugging.
  lk->cpu = cpu;
80105544:	8b 45 08             	mov    0x8(%ebp),%eax
80105547:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
8010554e:	89 50 08             	mov    %edx,0x8(%eax)
  getcallerpcs(&lk, NELEM(lk->pcs), lk->pcs);
80105551:	8b 45 08             	mov    0x8(%ebp),%eax
80105554:	83 c0 0c             	add    $0xc,%eax
80105557:	89 44 24 08          	mov    %eax,0x8(%esp)
8010555b:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
80105562:	00 
80105563:	8d 45 08             	lea    0x8(%ebp),%eax
80105566:	89 04 24             	mov    %eax,(%esp)
80105569:	e8 51 00 00 00       	call   801055bf <getcallerpcs>
}
8010556e:	c9                   	leave  
8010556f:	c3                   	ret    

80105570 <release>:

// Release the lock.
void
release(struct spinlock *lk)
{
80105570:	55                   	push   %ebp
80105571:	89 e5                	mov    %esp,%ebp
80105573:	83 ec 18             	sub    $0x18,%esp
  if(!holding(lk))
80105576:	8b 45 08             	mov    0x8(%ebp),%eax
80105579:	89 04 24             	mov    %eax,(%esp)
8010557c:	e8 ba 00 00 00       	call   8010563b <holding>
80105581:	85 c0                	test   %eax,%eax
80105583:	75 0c                	jne    80105591 <release+0x21>
    panic("release");
80105585:	c7 04 24 e9 90 10 80 	movl   $0x801090e9,(%esp)
8010558c:	e8 d8 b2 ff ff       	call   80100869 <panic>

  lk->pcs[0] = 0;
80105591:	8b 45 08             	mov    0x8(%ebp),%eax
80105594:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
  lk->cpu = 0;
8010559b:	8b 45 08             	mov    0x8(%ebp),%eax
8010559e:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
  // But the 2007 Intel 64 Architecture Memory Ordering White
  // Paper says that Intel 64 and IA-32 will not move a load
  // after a store. So lock->locked = 0 would work here.
  // The xchg being asm volatile ensures gcc emits it after
  // the above assignments (and after the critical section).
  xchg(&lk->locked, 0);
801055a5:	8b 45 08             	mov    0x8(%ebp),%eax
801055a8:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801055af:	00 
801055b0:	89 04 24             	mov    %eax,(%esp)
801055b3:	e8 14 ff ff ff       	call   801054cc <xchg>

  popcli();
801055b8:	e8 f0 00 00 00       	call   801056ad <popcli>
}
801055bd:	c9                   	leave  
801055be:	c3                   	ret    

801055bf <getcallerpcs>:

// Record the current call stack in pcs[] by following the %ebp chain.
void
getcallerpcs(void *v, uint n, uint pcs[])
{
801055bf:	55                   	push   %ebp
801055c0:	89 e5                	mov    %esp,%ebp
801055c2:	83 ec 10             	sub    $0x10,%esp
  uint *ebp;
  int i;
  
  ebp = (uint*)v - 2;
801055c5:	8b 45 08             	mov    0x8(%ebp),%eax
801055c8:	83 e8 08             	sub    $0x8,%eax
801055cb:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(i = 0; i < n; i++){
801055ce:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
801055d5:	eb 34                	jmp    8010560b <getcallerpcs+0x4c>
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
801055d7:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
801055db:	74 4d                	je     8010562a <getcallerpcs+0x6b>
801055dd:	81 7d f8 ff ff ff 7f 	cmpl   $0x7fffffff,-0x8(%ebp)
801055e4:	76 47                	jbe    8010562d <getcallerpcs+0x6e>
801055e6:	83 7d f8 ff          	cmpl   $0xffffffff,-0x8(%ebp)
801055ea:	74 44                	je     80105630 <getcallerpcs+0x71>
      break;
    pcs[i] = ebp[1];     // saved %eip
801055ec:	8b 45 fc             	mov    -0x4(%ebp),%eax
801055ef:	c1 e0 02             	shl    $0x2,%eax
801055f2:	03 45 10             	add    0x10(%ebp),%eax
801055f5:	8b 55 f8             	mov    -0x8(%ebp),%edx
801055f8:	83 c2 04             	add    $0x4,%edx
801055fb:	8b 12                	mov    (%edx),%edx
801055fd:	89 10                	mov    %edx,(%eax)
    ebp = (uint*)ebp[0]; // saved %ebp
801055ff:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105602:	8b 00                	mov    (%eax),%eax
80105604:	89 45 f8             	mov    %eax,-0x8(%ebp)
{
  uint *ebp;
  int i;
  
  ebp = (uint*)v - 2;
  for(i = 0; i < n; i++){
80105607:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
8010560b:	8b 45 fc             	mov    -0x4(%ebp),%eax
8010560e:	3b 45 0c             	cmp    0xc(%ebp),%eax
80105611:	72 c4                	jb     801055d7 <getcallerpcs+0x18>
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
      break;
    pcs[i] = ebp[1];     // saved %eip
    ebp = (uint*)ebp[0]; // saved %ebp
  }
  for(; i < n; i++)
80105613:	eb 1c                	jmp    80105631 <getcallerpcs+0x72>
    pcs[i] = 0;
80105615:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105618:	c1 e0 02             	shl    $0x2,%eax
8010561b:	03 45 10             	add    0x10(%ebp),%eax
8010561e:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
      break;
    pcs[i] = ebp[1];     // saved %eip
    ebp = (uint*)ebp[0]; // saved %ebp
  }
  for(; i < n; i++)
80105624:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80105628:	eb 07                	jmp    80105631 <getcallerpcs+0x72>
8010562a:	90                   	nop
8010562b:	eb 04                	jmp    80105631 <getcallerpcs+0x72>
8010562d:	90                   	nop
8010562e:	eb 01                	jmp    80105631 <getcallerpcs+0x72>
80105630:	90                   	nop
80105631:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105634:	3b 45 0c             	cmp    0xc(%ebp),%eax
80105637:	72 dc                	jb     80105615 <getcallerpcs+0x56>
    pcs[i] = 0;
}
80105639:	c9                   	leave  
8010563a:	c3                   	ret    

8010563b <holding>:

// Check whether this cpu is holding the lock.
int
holding(struct spinlock *lock)
{
8010563b:	55                   	push   %ebp
8010563c:	89 e5                	mov    %esp,%ebp
  return lock->locked && lock->cpu == cpu;
8010563e:	8b 45 08             	mov    0x8(%ebp),%eax
80105641:	8b 00                	mov    (%eax),%eax
80105643:	85 c0                	test   %eax,%eax
80105645:	74 17                	je     8010565e <holding+0x23>
80105647:	8b 45 08             	mov    0x8(%ebp),%eax
8010564a:	8b 50 08             	mov    0x8(%eax),%edx
8010564d:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105653:	39 c2                	cmp    %eax,%edx
80105655:	75 07                	jne    8010565e <holding+0x23>
80105657:	b8 01 00 00 00       	mov    $0x1,%eax
8010565c:	eb 05                	jmp    80105663 <holding+0x28>
8010565e:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105663:	5d                   	pop    %ebp
80105664:	c3                   	ret    

80105665 <pushcli>:
// it takes two popcli to undo two pushcli.  Also, if interrupts
// are off, then pushcli, popcli leaves them off.

void
pushcli(void)
{
80105665:	55                   	push   %ebp
80105666:	89 e5                	mov    %esp,%ebp
80105668:	83 ec 10             	sub    $0x10,%esp
  int eflags;
  
  eflags = readeflags();
8010566b:	e8 40 fe ff ff       	call   801054b0 <readeflags>
80105670:	89 45 fc             	mov    %eax,-0x4(%ebp)
  cli();
80105673:	e8 48 fe ff ff       	call   801054c0 <cli>
  if(cpu->ncli++ == 0)
80105678:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010567e:	8b 90 ac 00 00 00    	mov    0xac(%eax),%edx
80105684:	85 d2                	test   %edx,%edx
80105686:	0f 94 c1             	sete   %cl
80105689:	83 c2 01             	add    $0x1,%edx
8010568c:	89 90 ac 00 00 00    	mov    %edx,0xac(%eax)
80105692:	84 c9                	test   %cl,%cl
80105694:	74 15                	je     801056ab <pushcli+0x46>
    cpu->intena = eflags & FL_IF;
80105696:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010569c:	8b 55 fc             	mov    -0x4(%ebp),%edx
8010569f:	81 e2 00 02 00 00    	and    $0x200,%edx
801056a5:	89 90 b0 00 00 00    	mov    %edx,0xb0(%eax)
}
801056ab:	c9                   	leave  
801056ac:	c3                   	ret    

801056ad <popcli>:

void
popcli(void)
{
801056ad:	55                   	push   %ebp
801056ae:	89 e5                	mov    %esp,%ebp
801056b0:	83 ec 18             	sub    $0x18,%esp
  if(readeflags()&FL_IF)
801056b3:	e8 f8 fd ff ff       	call   801054b0 <readeflags>
801056b8:	25 00 02 00 00       	and    $0x200,%eax
801056bd:	85 c0                	test   %eax,%eax
801056bf:	74 0c                	je     801056cd <popcli+0x20>
    panic("popcli - interruptible");
801056c1:	c7 04 24 f1 90 10 80 	movl   $0x801090f1,(%esp)
801056c8:	e8 9c b1 ff ff       	call   80100869 <panic>
  if(--cpu->ncli < 0)
801056cd:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801056d3:	8b 90 ac 00 00 00    	mov    0xac(%eax),%edx
801056d9:	83 ea 01             	sub    $0x1,%edx
801056dc:	89 90 ac 00 00 00    	mov    %edx,0xac(%eax)
801056e2:	8b 80 ac 00 00 00    	mov    0xac(%eax),%eax
801056e8:	85 c0                	test   %eax,%eax
801056ea:	79 0c                	jns    801056f8 <popcli+0x4b>
    panic("popcli");
801056ec:	c7 04 24 08 91 10 80 	movl   $0x80109108,(%esp)
801056f3:	e8 71 b1 ff ff       	call   80100869 <panic>
  if(cpu->ncli == 0 && cpu->intena)
801056f8:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801056fe:	8b 80 ac 00 00 00    	mov    0xac(%eax),%eax
80105704:	85 c0                	test   %eax,%eax
80105706:	75 15                	jne    8010571d <popcli+0x70>
80105708:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010570e:	8b 80 b0 00 00 00    	mov    0xb0(%eax),%eax
80105714:	85 c0                	test   %eax,%eax
80105716:	74 05                	je     8010571d <popcli+0x70>
    sti();
80105718:	e8 a9 fd ff ff       	call   801054c6 <sti>
}
8010571d:	c9                   	leave  
8010571e:	c3                   	ret    
	...

80105720 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
80105720:	55                   	push   %ebp
80105721:	89 e5                	mov    %esp,%ebp
80105723:	57                   	push   %edi
80105724:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
80105725:	8b 4d 08             	mov    0x8(%ebp),%ecx
80105728:	8b 55 10             	mov    0x10(%ebp),%edx
8010572b:	8b 45 0c             	mov    0xc(%ebp),%eax
8010572e:	89 cb                	mov    %ecx,%ebx
80105730:	89 df                	mov    %ebx,%edi
80105732:	89 d1                	mov    %edx,%ecx
80105734:	fc                   	cld    
80105735:	f3 aa                	rep stos %al,%es:(%edi)
80105737:	89 ca                	mov    %ecx,%edx
80105739:	89 fb                	mov    %edi,%ebx
8010573b:	89 5d 08             	mov    %ebx,0x8(%ebp)
8010573e:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
80105741:	5b                   	pop    %ebx
80105742:	5f                   	pop    %edi
80105743:	5d                   	pop    %ebp
80105744:	c3                   	ret    

80105745 <stosl>:

static inline void
stosl(void *addr, int data, int cnt)
{
80105745:	55                   	push   %ebp
80105746:	89 e5                	mov    %esp,%ebp
80105748:	57                   	push   %edi
80105749:	53                   	push   %ebx
  asm volatile("cld; rep stosl" :
8010574a:	8b 4d 08             	mov    0x8(%ebp),%ecx
8010574d:	8b 55 10             	mov    0x10(%ebp),%edx
80105750:	8b 45 0c             	mov    0xc(%ebp),%eax
80105753:	89 cb                	mov    %ecx,%ebx
80105755:	89 df                	mov    %ebx,%edi
80105757:	89 d1                	mov    %edx,%ecx
80105759:	fc                   	cld    
8010575a:	f3 ab                	rep stos %eax,%es:(%edi)
8010575c:	89 ca                	mov    %ecx,%edx
8010575e:	89 fb                	mov    %edi,%ebx
80105760:	89 5d 08             	mov    %ebx,0x8(%ebp)
80105763:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
80105766:	5b                   	pop    %ebx
80105767:	5f                   	pop    %edi
80105768:	5d                   	pop    %ebp
80105769:	c3                   	ret    

8010576a <memset>:
#include "types.h"
#include "x86.h"

void*
memset(void *dst, int c, uint n)
{
8010576a:	55                   	push   %ebp
8010576b:	89 e5                	mov    %esp,%ebp
8010576d:	83 ec 0c             	sub    $0xc,%esp
  if ((int)dst%4 == 0 && n%4 == 0){
80105770:	8b 45 08             	mov    0x8(%ebp),%eax
80105773:	83 e0 03             	and    $0x3,%eax
80105776:	85 c0                	test   %eax,%eax
80105778:	75 49                	jne    801057c3 <memset+0x59>
8010577a:	8b 45 10             	mov    0x10(%ebp),%eax
8010577d:	83 e0 03             	and    $0x3,%eax
80105780:	85 c0                	test   %eax,%eax
80105782:	75 3f                	jne    801057c3 <memset+0x59>
    c &= 0xFF;
80105784:	81 65 0c ff 00 00 00 	andl   $0xff,0xc(%ebp)
    stosl(dst, (c<<24)|(c<<16)|(c<<8)|c, n/4);
8010578b:	8b 45 10             	mov    0x10(%ebp),%eax
8010578e:	c1 e8 02             	shr    $0x2,%eax
80105791:	89 c2                	mov    %eax,%edx
80105793:	8b 45 0c             	mov    0xc(%ebp),%eax
80105796:	89 c1                	mov    %eax,%ecx
80105798:	c1 e1 18             	shl    $0x18,%ecx
8010579b:	8b 45 0c             	mov    0xc(%ebp),%eax
8010579e:	c1 e0 10             	shl    $0x10,%eax
801057a1:	09 c1                	or     %eax,%ecx
801057a3:	8b 45 0c             	mov    0xc(%ebp),%eax
801057a6:	c1 e0 08             	shl    $0x8,%eax
801057a9:	09 c8                	or     %ecx,%eax
801057ab:	0b 45 0c             	or     0xc(%ebp),%eax
801057ae:	89 54 24 08          	mov    %edx,0x8(%esp)
801057b2:	89 44 24 04          	mov    %eax,0x4(%esp)
801057b6:	8b 45 08             	mov    0x8(%ebp),%eax
801057b9:	89 04 24             	mov    %eax,(%esp)
801057bc:	e8 84 ff ff ff       	call   80105745 <stosl>
#include "x86.h"

void*
memset(void *dst, int c, uint n)
{
  if ((int)dst%4 == 0 && n%4 == 0){
801057c1:	eb 19                	jmp    801057dc <memset+0x72>
    c &= 0xFF;
    stosl(dst, (c<<24)|(c<<16)|(c<<8)|c, n/4);
  } else
    stosb(dst, c, n);
801057c3:	8b 45 10             	mov    0x10(%ebp),%eax
801057c6:	89 44 24 08          	mov    %eax,0x8(%esp)
801057ca:	8b 45 0c             	mov    0xc(%ebp),%eax
801057cd:	89 44 24 04          	mov    %eax,0x4(%esp)
801057d1:	8b 45 08             	mov    0x8(%ebp),%eax
801057d4:	89 04 24             	mov    %eax,(%esp)
801057d7:	e8 44 ff ff ff       	call   80105720 <stosb>
  return dst;
801057dc:	8b 45 08             	mov    0x8(%ebp),%eax
}
801057df:	c9                   	leave  
801057e0:	c3                   	ret    

801057e1 <memcmp>:

int
memcmp(const void *v1, const void *v2, uint n)
{
801057e1:	55                   	push   %ebp
801057e2:	89 e5                	mov    %esp,%ebp
801057e4:	83 ec 10             	sub    $0x10,%esp
  const uchar *s1, *s2;
  
  s1 = v1;
801057e7:	8b 45 08             	mov    0x8(%ebp),%eax
801057ea:	89 45 f8             	mov    %eax,-0x8(%ebp)
  s2 = v2;
801057ed:	8b 45 0c             	mov    0xc(%ebp),%eax
801057f0:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0){
801057f3:	eb 32                	jmp    80105827 <memcmp+0x46>
    if(*s1 != *s2)
801057f5:	8b 45 f8             	mov    -0x8(%ebp),%eax
801057f8:	0f b6 10             	movzbl (%eax),%edx
801057fb:	8b 45 fc             	mov    -0x4(%ebp),%eax
801057fe:	0f b6 00             	movzbl (%eax),%eax
80105801:	38 c2                	cmp    %al,%dl
80105803:	74 1a                	je     8010581f <memcmp+0x3e>
      return *s1 - *s2;
80105805:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105808:	0f b6 00             	movzbl (%eax),%eax
8010580b:	0f b6 d0             	movzbl %al,%edx
8010580e:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105811:	0f b6 00             	movzbl (%eax),%eax
80105814:	0f b6 c0             	movzbl %al,%eax
80105817:	89 d1                	mov    %edx,%ecx
80105819:	29 c1                	sub    %eax,%ecx
8010581b:	89 c8                	mov    %ecx,%eax
8010581d:	eb 1c                	jmp    8010583b <memcmp+0x5a>
    s1++, s2++;
8010581f:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
80105823:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
{
  const uchar *s1, *s2;
  
  s1 = v1;
  s2 = v2;
  while(n-- > 0){
80105827:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
8010582b:	0f 95 c0             	setne  %al
8010582e:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105832:	84 c0                	test   %al,%al
80105834:	75 bf                	jne    801057f5 <memcmp+0x14>
    if(*s1 != *s2)
      return *s1 - *s2;
    s1++, s2++;
  }

  return 0;
80105836:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010583b:	c9                   	leave  
8010583c:	c3                   	ret    

8010583d <memmove>:

void*
memmove(void *dst, const void *src, uint n)
{
8010583d:	55                   	push   %ebp
8010583e:	89 e5                	mov    %esp,%ebp
80105840:	83 ec 10             	sub    $0x10,%esp
  const char *s;
  char *d;

  s = src;
80105843:	8b 45 0c             	mov    0xc(%ebp),%eax
80105846:	89 45 f8             	mov    %eax,-0x8(%ebp)
  d = dst;
80105849:	8b 45 08             	mov    0x8(%ebp),%eax
8010584c:	89 45 fc             	mov    %eax,-0x4(%ebp)
  if(s < d && s + n > d){
8010584f:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105852:	3b 45 fc             	cmp    -0x4(%ebp),%eax
80105855:	73 55                	jae    801058ac <memmove+0x6f>
80105857:	8b 45 10             	mov    0x10(%ebp),%eax
8010585a:	8b 55 f8             	mov    -0x8(%ebp),%edx
8010585d:	8d 04 02             	lea    (%edx,%eax,1),%eax
80105860:	3b 45 fc             	cmp    -0x4(%ebp),%eax
80105863:	76 4a                	jbe    801058af <memmove+0x72>
    s += n;
80105865:	8b 45 10             	mov    0x10(%ebp),%eax
80105868:	01 45 f8             	add    %eax,-0x8(%ebp)
    d += n;
8010586b:	8b 45 10             	mov    0x10(%ebp),%eax
8010586e:	01 45 fc             	add    %eax,-0x4(%ebp)
    while(n-- > 0)
80105871:	eb 13                	jmp    80105886 <memmove+0x49>
      *--d = *--s;
80105873:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
80105877:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
8010587b:	8b 45 f8             	mov    -0x8(%ebp),%eax
8010587e:	0f b6 10             	movzbl (%eax),%edx
80105881:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105884:	88 10                	mov    %dl,(%eax)
  s = src;
  d = dst;
  if(s < d && s + n > d){
    s += n;
    d += n;
    while(n-- > 0)
80105886:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
8010588a:	0f 95 c0             	setne  %al
8010588d:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105891:	84 c0                	test   %al,%al
80105893:	75 de                	jne    80105873 <memmove+0x36>
  const char *s;
  char *d;

  s = src;
  d = dst;
  if(s < d && s + n > d){
80105895:	eb 28                	jmp    801058bf <memmove+0x82>
    d += n;
    while(n-- > 0)
      *--d = *--s;
  } else
    while(n-- > 0)
      *d++ = *s++;
80105897:	8b 45 f8             	mov    -0x8(%ebp),%eax
8010589a:	0f b6 10             	movzbl (%eax),%edx
8010589d:	8b 45 fc             	mov    -0x4(%ebp),%eax
801058a0:	88 10                	mov    %dl,(%eax)
801058a2:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
801058a6:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
801058aa:	eb 04                	jmp    801058b0 <memmove+0x73>
    s += n;
    d += n;
    while(n-- > 0)
      *--d = *--s;
  } else
    while(n-- > 0)
801058ac:	90                   	nop
801058ad:	eb 01                	jmp    801058b0 <memmove+0x73>
801058af:	90                   	nop
801058b0:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
801058b4:	0f 95 c0             	setne  %al
801058b7:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
801058bb:	84 c0                	test   %al,%al
801058bd:	75 d8                	jne    80105897 <memmove+0x5a>
      *d++ = *s++;

  return dst;
801058bf:	8b 45 08             	mov    0x8(%ebp),%eax
}
801058c2:	c9                   	leave  
801058c3:	c3                   	ret    

801058c4 <memcpy>:

// memcpy exists to placate GCC.  Use memmove.
void*
memcpy(void *dst, const void *src, uint n)
{
801058c4:	55                   	push   %ebp
801058c5:	89 e5                	mov    %esp,%ebp
801058c7:	83 ec 0c             	sub    $0xc,%esp
  return memmove(dst, src, n);
801058ca:	8b 45 10             	mov    0x10(%ebp),%eax
801058cd:	89 44 24 08          	mov    %eax,0x8(%esp)
801058d1:	8b 45 0c             	mov    0xc(%ebp),%eax
801058d4:	89 44 24 04          	mov    %eax,0x4(%esp)
801058d8:	8b 45 08             	mov    0x8(%ebp),%eax
801058db:	89 04 24             	mov    %eax,(%esp)
801058de:	e8 5a ff ff ff       	call   8010583d <memmove>
}
801058e3:	c9                   	leave  
801058e4:	c3                   	ret    

801058e5 <strncmp>:

int
strncmp(const char *p, const char *q, uint n)
{
801058e5:	55                   	push   %ebp
801058e6:	89 e5                	mov    %esp,%ebp
  while(n > 0 && *p && *p == *q)
801058e8:	eb 0c                	jmp    801058f6 <strncmp+0x11>
    n--, p++, q++;
801058ea:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
801058ee:	83 45 08 01          	addl   $0x1,0x8(%ebp)
801058f2:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strncmp(const char *p, const char *q, uint n)
{
  while(n > 0 && *p && *p == *q)
801058f6:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
801058fa:	74 1a                	je     80105916 <strncmp+0x31>
801058fc:	8b 45 08             	mov    0x8(%ebp),%eax
801058ff:	0f b6 00             	movzbl (%eax),%eax
80105902:	84 c0                	test   %al,%al
80105904:	74 10                	je     80105916 <strncmp+0x31>
80105906:	8b 45 08             	mov    0x8(%ebp),%eax
80105909:	0f b6 10             	movzbl (%eax),%edx
8010590c:	8b 45 0c             	mov    0xc(%ebp),%eax
8010590f:	0f b6 00             	movzbl (%eax),%eax
80105912:	38 c2                	cmp    %al,%dl
80105914:	74 d4                	je     801058ea <strncmp+0x5>
    n--, p++, q++;
  if(n == 0)
80105916:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
8010591a:	75 07                	jne    80105923 <strncmp+0x3e>
    return 0;
8010591c:	b8 00 00 00 00       	mov    $0x0,%eax
80105921:	eb 18                	jmp    8010593b <strncmp+0x56>
  return (uchar)*p - (uchar)*q;
80105923:	8b 45 08             	mov    0x8(%ebp),%eax
80105926:	0f b6 00             	movzbl (%eax),%eax
80105929:	0f b6 d0             	movzbl %al,%edx
8010592c:	8b 45 0c             	mov    0xc(%ebp),%eax
8010592f:	0f b6 00             	movzbl (%eax),%eax
80105932:	0f b6 c0             	movzbl %al,%eax
80105935:	89 d1                	mov    %edx,%ecx
80105937:	29 c1                	sub    %eax,%ecx
80105939:	89 c8                	mov    %ecx,%eax
}
8010593b:	5d                   	pop    %ebp
8010593c:	c3                   	ret    

8010593d <strncpy>:

char*
strncpy(char *s, const char *t, int n)
{
8010593d:	55                   	push   %ebp
8010593e:	89 e5                	mov    %esp,%ebp
80105940:	83 ec 10             	sub    $0x10,%esp
  char *os;
  
  os = s;
80105943:	8b 45 08             	mov    0x8(%ebp),%eax
80105946:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0 && (*s++ = *t++) != 0)
80105949:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
8010594d:	0f 9f c0             	setg   %al
80105950:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105954:	84 c0                	test   %al,%al
80105956:	74 30                	je     80105988 <strncpy+0x4b>
80105958:	8b 45 0c             	mov    0xc(%ebp),%eax
8010595b:	0f b6 10             	movzbl (%eax),%edx
8010595e:	8b 45 08             	mov    0x8(%ebp),%eax
80105961:	88 10                	mov    %dl,(%eax)
80105963:	8b 45 08             	mov    0x8(%ebp),%eax
80105966:	0f b6 00             	movzbl (%eax),%eax
80105969:	84 c0                	test   %al,%al
8010596b:	0f 95 c0             	setne  %al
8010596e:	83 45 08 01          	addl   $0x1,0x8(%ebp)
80105972:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
80105976:	84 c0                	test   %al,%al
80105978:	75 cf                	jne    80105949 <strncpy+0xc>
    ;
  while(n-- > 0)
8010597a:	eb 0d                	jmp    80105989 <strncpy+0x4c>
    *s++ = 0;
8010597c:	8b 45 08             	mov    0x8(%ebp),%eax
8010597f:	c6 00 00             	movb   $0x0,(%eax)
80105982:	83 45 08 01          	addl   $0x1,0x8(%ebp)
80105986:	eb 01                	jmp    80105989 <strncpy+0x4c>
  char *os;
  
  os = s;
  while(n-- > 0 && (*s++ = *t++) != 0)
    ;
  while(n-- > 0)
80105988:	90                   	nop
80105989:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
8010598d:	0f 9f c0             	setg   %al
80105990:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105994:	84 c0                	test   %al,%al
80105996:	75 e4                	jne    8010597c <strncpy+0x3f>
    *s++ = 0;
  return os;
80105998:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
8010599b:	c9                   	leave  
8010599c:	c3                   	ret    

8010599d <safestrcpy>:

// Like strncpy but guaranteed to NUL-terminate.
char*
safestrcpy(char *s, const char *t, int n)
{
8010599d:	55                   	push   %ebp
8010599e:	89 e5                	mov    %esp,%ebp
801059a0:	83 ec 10             	sub    $0x10,%esp
  char *os;
  
  os = s;
801059a3:	8b 45 08             	mov    0x8(%ebp),%eax
801059a6:	89 45 fc             	mov    %eax,-0x4(%ebp)
  if(n <= 0)
801059a9:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
801059ad:	7f 05                	jg     801059b4 <safestrcpy+0x17>
    return os;
801059af:	8b 45 fc             	mov    -0x4(%ebp),%eax
801059b2:	eb 35                	jmp    801059e9 <safestrcpy+0x4c>
  while(--n > 0 && (*s++ = *t++) != 0)
801059b4:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
801059b8:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
801059bc:	7e 22                	jle    801059e0 <safestrcpy+0x43>
801059be:	8b 45 0c             	mov    0xc(%ebp),%eax
801059c1:	0f b6 10             	movzbl (%eax),%edx
801059c4:	8b 45 08             	mov    0x8(%ebp),%eax
801059c7:	88 10                	mov    %dl,(%eax)
801059c9:	8b 45 08             	mov    0x8(%ebp),%eax
801059cc:	0f b6 00             	movzbl (%eax),%eax
801059cf:	84 c0                	test   %al,%al
801059d1:	0f 95 c0             	setne  %al
801059d4:	83 45 08 01          	addl   $0x1,0x8(%ebp)
801059d8:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
801059dc:	84 c0                	test   %al,%al
801059de:	75 d4                	jne    801059b4 <safestrcpy+0x17>
    ;
  *s = 0;
801059e0:	8b 45 08             	mov    0x8(%ebp),%eax
801059e3:	c6 00 00             	movb   $0x0,(%eax)
  return os;
801059e6:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
801059e9:	c9                   	leave  
801059ea:	c3                   	ret    

801059eb <strlen>:

int
strlen(const char *s)
{
801059eb:	55                   	push   %ebp
801059ec:	89 e5                	mov    %esp,%ebp
801059ee:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
801059f1:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
801059f8:	eb 04                	jmp    801059fe <strlen+0x13>
801059fa:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
801059fe:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105a01:	03 45 08             	add    0x8(%ebp),%eax
80105a04:	0f b6 00             	movzbl (%eax),%eax
80105a07:	84 c0                	test   %al,%al
80105a09:	75 ef                	jne    801059fa <strlen+0xf>
    ;
  return n;
80105a0b:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80105a0e:	c9                   	leave  
80105a0f:	c3                   	ret    

80105a10 <swtch>:
# Save current register context in old
# and then load register context from new.

.globl swtch
swtch:
  movl 4(%esp), %eax
80105a10:	8b 44 24 04          	mov    0x4(%esp),%eax
  movl 8(%esp), %edx
80105a14:	8b 54 24 08          	mov    0x8(%esp),%edx

  # Save old callee-save registers
  pushl %ebp
80105a18:	55                   	push   %ebp
  pushl %ebx
80105a19:	53                   	push   %ebx
  pushl %esi
80105a1a:	56                   	push   %esi
  pushl %edi
80105a1b:	57                   	push   %edi

  # Switch stacks
  movl %esp, (%eax)
80105a1c:	89 20                	mov    %esp,(%eax)
  movl %edx, %esp
80105a1e:	89 d4                	mov    %edx,%esp

  # Load new callee-save registers
  popl %edi
80105a20:	5f                   	pop    %edi
  popl %esi
80105a21:	5e                   	pop    %esi
  popl %ebx
80105a22:	5b                   	pop    %ebx
  popl %ebp
80105a23:	5d                   	pop    %ebp
  ret
80105a24:	c3                   	ret    
80105a25:	00 00                	add    %al,(%eax)
	...

80105a28 <fetchint>:
// to a saved program counter, and then the first argument.

// Fetch the int at addr from the current process.
int
fetchint(uint addr, int *ip)
{
80105a28:	55                   	push   %ebp
80105a29:	89 e5                	mov    %esp,%ebp
  if(addr >= proc->sz || addr+4 > proc->sz)
80105a2b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105a31:	8b 00                	mov    (%eax),%eax
80105a33:	3b 45 08             	cmp    0x8(%ebp),%eax
80105a36:	76 12                	jbe    80105a4a <fetchint+0x22>
80105a38:	8b 45 08             	mov    0x8(%ebp),%eax
80105a3b:	8d 50 04             	lea    0x4(%eax),%edx
80105a3e:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105a44:	8b 00                	mov    (%eax),%eax
80105a46:	39 c2                	cmp    %eax,%edx
80105a48:	76 07                	jbe    80105a51 <fetchint+0x29>
    return -1;
80105a4a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105a4f:	eb 0f                	jmp    80105a60 <fetchint+0x38>
  *ip = *(int*)(addr);
80105a51:	8b 45 08             	mov    0x8(%ebp),%eax
80105a54:	8b 10                	mov    (%eax),%edx
80105a56:	8b 45 0c             	mov    0xc(%ebp),%eax
80105a59:	89 10                	mov    %edx,(%eax)
  return 0;
80105a5b:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105a60:	5d                   	pop    %ebp
80105a61:	c3                   	ret    

80105a62 <fetchstr>:
// Fetch the nul-terminated string at addr from the current process.
// Doesn't actually copy the string - just sets *pp to point at it.
// Returns length of string, not including nul.
int
fetchstr(uint addr, char **pp)
{
80105a62:	55                   	push   %ebp
80105a63:	89 e5                	mov    %esp,%ebp
80105a65:	83 ec 10             	sub    $0x10,%esp
  char *s, *ep;

  if(addr >= proc->sz)
80105a68:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105a6e:	8b 00                	mov    (%eax),%eax
80105a70:	3b 45 08             	cmp    0x8(%ebp),%eax
80105a73:	77 07                	ja     80105a7c <fetchstr+0x1a>
    return -1;
80105a75:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105a7a:	eb 48                	jmp    80105ac4 <fetchstr+0x62>
  *pp = (char*)addr;
80105a7c:	8b 55 08             	mov    0x8(%ebp),%edx
80105a7f:	8b 45 0c             	mov    0xc(%ebp),%eax
80105a82:	89 10                	mov    %edx,(%eax)
  ep = (char*)proc->sz;
80105a84:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105a8a:	8b 00                	mov    (%eax),%eax
80105a8c:	89 45 fc             	mov    %eax,-0x4(%ebp)
  for(s = *pp; s < ep; s++)
80105a8f:	8b 45 0c             	mov    0xc(%ebp),%eax
80105a92:	8b 00                	mov    (%eax),%eax
80105a94:	89 45 f8             	mov    %eax,-0x8(%ebp)
80105a97:	eb 1e                	jmp    80105ab7 <fetchstr+0x55>
    if(*s == 0)
80105a99:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105a9c:	0f b6 00             	movzbl (%eax),%eax
80105a9f:	84 c0                	test   %al,%al
80105aa1:	75 10                	jne    80105ab3 <fetchstr+0x51>
      return s - *pp;
80105aa3:	8b 55 f8             	mov    -0x8(%ebp),%edx
80105aa6:	8b 45 0c             	mov    0xc(%ebp),%eax
80105aa9:	8b 00                	mov    (%eax),%eax
80105aab:	89 d1                	mov    %edx,%ecx
80105aad:	29 c1                	sub    %eax,%ecx
80105aaf:	89 c8                	mov    %ecx,%eax
80105ab1:	eb 11                	jmp    80105ac4 <fetchstr+0x62>

  if(addr >= proc->sz)
    return -1;
  *pp = (char*)addr;
  ep = (char*)proc->sz;
  for(s = *pp; s < ep; s++)
80105ab3:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
80105ab7:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105aba:	3b 45 fc             	cmp    -0x4(%ebp),%eax
80105abd:	72 da                	jb     80105a99 <fetchstr+0x37>
    if(*s == 0)
      return s - *pp;
  return -1;
80105abf:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105ac4:	c9                   	leave  
80105ac5:	c3                   	ret    

80105ac6 <argint>:

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
80105ac6:	55                   	push   %ebp
80105ac7:	89 e5                	mov    %esp,%ebp
80105ac9:	83 ec 08             	sub    $0x8,%esp
  return fetchint(proc->tf->esp + 4 + 4*n, ip);
80105acc:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105ad2:	8b 40 18             	mov    0x18(%eax),%eax
80105ad5:	8b 50 44             	mov    0x44(%eax),%edx
80105ad8:	8b 45 08             	mov    0x8(%ebp),%eax
80105adb:	c1 e0 02             	shl    $0x2,%eax
80105ade:	8d 04 02             	lea    (%edx,%eax,1),%eax
80105ae1:	8d 50 04             	lea    0x4(%eax),%edx
80105ae4:	8b 45 0c             	mov    0xc(%ebp),%eax
80105ae7:	89 44 24 04          	mov    %eax,0x4(%esp)
80105aeb:	89 14 24             	mov    %edx,(%esp)
80105aee:	e8 35 ff ff ff       	call   80105a28 <fetchint>
}
80105af3:	c9                   	leave  
80105af4:	c3                   	ret    

80105af5 <argptr>:
// Fetch the nth word-sized system call argument as a pointer
// to a block of memory of size n bytes.  Check that the pointer
// lies within the process address space.
int
argptr(int n, char **pp, int size)
{
80105af5:	55                   	push   %ebp
80105af6:	89 e5                	mov    %esp,%ebp
80105af8:	83 ec 18             	sub    $0x18,%esp
  int i;

  if(argint(n, &i) < 0)
80105afb:	8d 45 fc             	lea    -0x4(%ebp),%eax
80105afe:	89 44 24 04          	mov    %eax,0x4(%esp)
80105b02:	8b 45 08             	mov    0x8(%ebp),%eax
80105b05:	89 04 24             	mov    %eax,(%esp)
80105b08:	e8 b9 ff ff ff       	call   80105ac6 <argint>
80105b0d:	85 c0                	test   %eax,%eax
80105b0f:	79 07                	jns    80105b18 <argptr+0x23>
    return -1;
80105b11:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105b16:	eb 3d                	jmp    80105b55 <argptr+0x60>
  if((uint)i >= proc->sz || (uint)i+size > proc->sz)
80105b18:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105b1b:	89 c2                	mov    %eax,%edx
80105b1d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105b23:	8b 00                	mov    (%eax),%eax
80105b25:	39 c2                	cmp    %eax,%edx
80105b27:	73 16                	jae    80105b3f <argptr+0x4a>
80105b29:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105b2c:	89 c2                	mov    %eax,%edx
80105b2e:	8b 45 10             	mov    0x10(%ebp),%eax
80105b31:	01 c2                	add    %eax,%edx
80105b33:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105b39:	8b 00                	mov    (%eax),%eax
80105b3b:	39 c2                	cmp    %eax,%edx
80105b3d:	76 07                	jbe    80105b46 <argptr+0x51>
    return -1;
80105b3f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105b44:	eb 0f                	jmp    80105b55 <argptr+0x60>
  *pp = (char*)i;
80105b46:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105b49:	89 c2                	mov    %eax,%edx
80105b4b:	8b 45 0c             	mov    0xc(%ebp),%eax
80105b4e:	89 10                	mov    %edx,(%eax)
  return 0;
80105b50:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105b55:	c9                   	leave  
80105b56:	c3                   	ret    

80105b57 <argstr>:
// Check that the pointer is valid and the string is nul-terminated.
// (There is no shared writable memory, so the string can't change
// between this check and being used by the kernel.)
int
argstr(int n, char **pp)
{
80105b57:	55                   	push   %ebp
80105b58:	89 e5                	mov    %esp,%ebp
80105b5a:	83 ec 18             	sub    $0x18,%esp
  int addr;
  if(argint(n, &addr) < 0)
80105b5d:	8d 45 fc             	lea    -0x4(%ebp),%eax
80105b60:	89 44 24 04          	mov    %eax,0x4(%esp)
80105b64:	8b 45 08             	mov    0x8(%ebp),%eax
80105b67:	89 04 24             	mov    %eax,(%esp)
80105b6a:	e8 57 ff ff ff       	call   80105ac6 <argint>
80105b6f:	85 c0                	test   %eax,%eax
80105b71:	79 07                	jns    80105b7a <argstr+0x23>
    return -1;
80105b73:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105b78:	eb 12                	jmp    80105b8c <argstr+0x35>
  return fetchstr(addr, pp);
80105b7a:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105b7d:	8b 55 0c             	mov    0xc(%ebp),%edx
80105b80:	89 54 24 04          	mov    %edx,0x4(%esp)
80105b84:	89 04 24             	mov    %eax,(%esp)
80105b87:	e8 d6 fe ff ff       	call   80105a62 <fetchstr>
}
80105b8c:	c9                   	leave  
80105b8d:	c3                   	ret    

80105b8e <syscall>:
  [SYS_close]  = sys_close,
};

void
syscall(void)
{
80105b8e:	55                   	push   %ebp
80105b8f:	89 e5                	mov    %esp,%ebp
80105b91:	53                   	push   %ebx
80105b92:	83 ec 24             	sub    $0x24,%esp
  int num;

  num = proc->tf->eax;
80105b95:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105b9b:	8b 40 18             	mov    0x18(%eax),%eax
80105b9e:	8b 40 1c             	mov    0x1c(%eax),%eax
80105ba1:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
80105ba4:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105ba8:	7e 30                	jle    80105bda <syscall+0x4c>
80105baa:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105bad:	83 f8 15             	cmp    $0x15,%eax
80105bb0:	77 28                	ja     80105bda <syscall+0x4c>
80105bb2:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105bb5:	8b 04 85 40 c0 10 80 	mov    -0x7fef3fc0(,%eax,4),%eax
80105bbc:	85 c0                	test   %eax,%eax
80105bbe:	74 1a                	je     80105bda <syscall+0x4c>
    proc->tf->eax = syscalls[num]();
80105bc0:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105bc6:	8b 58 18             	mov    0x18(%eax),%ebx
80105bc9:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105bcc:	8b 04 85 40 c0 10 80 	mov    -0x7fef3fc0(,%eax,4),%eax
80105bd3:	ff d0                	call   *%eax
80105bd5:	89 43 1c             	mov    %eax,0x1c(%ebx)
syscall(void)
{
  int num;

  num = proc->tf->eax;
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
80105bd8:	eb 3d                	jmp    80105c17 <syscall+0x89>
    proc->tf->eax = syscalls[num]();
  } else {
    cprintf("%d %s: unknown sys call %d\n",
            proc->pid, proc->name, num);
80105bda:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax

  num = proc->tf->eax;
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
    proc->tf->eax = syscalls[num]();
  } else {
    cprintf("%d %s: unknown sys call %d\n",
80105be0:	8d 48 6c             	lea    0x6c(%eax),%ecx
            proc->pid, proc->name, num);
80105be3:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax

  num = proc->tf->eax;
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
    proc->tf->eax = syscalls[num]();
  } else {
    cprintf("%d %s: unknown sys call %d\n",
80105be9:	8b 40 10             	mov    0x10(%eax),%eax
80105bec:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105bef:	89 54 24 0c          	mov    %edx,0xc(%esp)
80105bf3:	89 4c 24 08          	mov    %ecx,0x8(%esp)
80105bf7:	89 44 24 04          	mov    %eax,0x4(%esp)
80105bfb:	c7 04 24 0f 91 10 80 	movl   $0x8010910f,(%esp)
80105c02:	e8 c2 aa ff ff       	call   801006c9 <cprintf>
            proc->pid, proc->name, num);
    proc->tf->eax = -1;
80105c07:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105c0d:	8b 40 18             	mov    0x18(%eax),%eax
80105c10:	c7 40 1c ff ff ff ff 	movl   $0xffffffff,0x1c(%eax)
  }
}
80105c17:	83 c4 24             	add    $0x24,%esp
80105c1a:	5b                   	pop    %ebx
80105c1b:	5d                   	pop    %ebp
80105c1c:	c3                   	ret    
80105c1d:	00 00                	add    %al,(%eax)
	...

80105c20 <argfd>:

// Fetch the nth word-sized system call argument as a file descriptor
// and return both the descriptor and the corresponding struct file.
static int
argfd(int n, int *pfd, struct file **pf)
{
80105c20:	55                   	push   %ebp
80105c21:	89 e5                	mov    %esp,%ebp
80105c23:	83 ec 28             	sub    $0x28,%esp
  int fd;
  struct file *f;

  if(argint(n, &fd) < 0)
80105c26:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105c29:	89 44 24 04          	mov    %eax,0x4(%esp)
80105c2d:	8b 45 08             	mov    0x8(%ebp),%eax
80105c30:	89 04 24             	mov    %eax,(%esp)
80105c33:	e8 8e fe ff ff       	call   80105ac6 <argint>
80105c38:	85 c0                	test   %eax,%eax
80105c3a:	79 07                	jns    80105c43 <argfd+0x23>
    return -1;
80105c3c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105c41:	eb 50                	jmp    80105c93 <argfd+0x73>
  if(fd < 0 || fd >= NOFILE || (f=proc->ofile[fd]) == 0)
80105c43:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105c46:	85 c0                	test   %eax,%eax
80105c48:	78 21                	js     80105c6b <argfd+0x4b>
80105c4a:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105c4d:	83 f8 0f             	cmp    $0xf,%eax
80105c50:	7f 19                	jg     80105c6b <argfd+0x4b>
80105c52:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105c58:	8b 55 f0             	mov    -0x10(%ebp),%edx
80105c5b:	83 c2 08             	add    $0x8,%edx
80105c5e:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80105c62:	89 45 f4             	mov    %eax,-0xc(%ebp)
80105c65:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105c69:	75 07                	jne    80105c72 <argfd+0x52>
    return -1;
80105c6b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105c70:	eb 21                	jmp    80105c93 <argfd+0x73>
  if(pfd)
80105c72:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80105c76:	74 08                	je     80105c80 <argfd+0x60>
    *pfd = fd;
80105c78:	8b 55 f0             	mov    -0x10(%ebp),%edx
80105c7b:	8b 45 0c             	mov    0xc(%ebp),%eax
80105c7e:	89 10                	mov    %edx,(%eax)
  if(pf)
80105c80:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105c84:	74 08                	je     80105c8e <argfd+0x6e>
    *pf = f;
80105c86:	8b 45 10             	mov    0x10(%ebp),%eax
80105c89:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105c8c:	89 10                	mov    %edx,(%eax)
  return 0;
80105c8e:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105c93:	c9                   	leave  
80105c94:	c3                   	ret    

80105c95 <fdalloc>:

// Allocate a file descriptor for the given file.
// Takes over file reference from caller on success.
static int
fdalloc(struct file *f)
{
80105c95:	55                   	push   %ebp
80105c96:	89 e5                	mov    %esp,%ebp
80105c98:	83 ec 10             	sub    $0x10,%esp
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
80105c9b:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
80105ca2:	eb 30                	jmp    80105cd4 <fdalloc+0x3f>
    if(proc->ofile[fd] == 0){
80105ca4:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105caa:	8b 55 fc             	mov    -0x4(%ebp),%edx
80105cad:	83 c2 08             	add    $0x8,%edx
80105cb0:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80105cb4:	85 c0                	test   %eax,%eax
80105cb6:	75 18                	jne    80105cd0 <fdalloc+0x3b>
      proc->ofile[fd] = f;
80105cb8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105cbe:	8b 55 fc             	mov    -0x4(%ebp),%edx
80105cc1:	8d 4a 08             	lea    0x8(%edx),%ecx
80105cc4:	8b 55 08             	mov    0x8(%ebp),%edx
80105cc7:	89 54 88 08          	mov    %edx,0x8(%eax,%ecx,4)
      return fd;
80105ccb:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105cce:	eb 0f                	jmp    80105cdf <fdalloc+0x4a>
static int
fdalloc(struct file *f)
{
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
80105cd0:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80105cd4:	83 7d fc 0f          	cmpl   $0xf,-0x4(%ebp)
80105cd8:	7e ca                	jle    80105ca4 <fdalloc+0xf>
    if(proc->ofile[fd] == 0){
      proc->ofile[fd] = f;
      return fd;
    }
  }
  return -1;
80105cda:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105cdf:	c9                   	leave  
80105ce0:	c3                   	ret    

80105ce1 <sys_dup>:

int
sys_dup(void)
{
80105ce1:	55                   	push   %ebp
80105ce2:	89 e5                	mov    %esp,%ebp
80105ce4:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  int fd;

  if(argfd(0, 0, &f) < 0)
80105ce7:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105cea:	89 44 24 08          	mov    %eax,0x8(%esp)
80105cee:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80105cf5:	00 
80105cf6:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80105cfd:	e8 1e ff ff ff       	call   80105c20 <argfd>
80105d02:	85 c0                	test   %eax,%eax
80105d04:	79 07                	jns    80105d0d <sys_dup+0x2c>
    return -1;
80105d06:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105d0b:	eb 29                	jmp    80105d36 <sys_dup+0x55>
  if((fd=fdalloc(f)) < 0)
80105d0d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105d10:	89 04 24             	mov    %eax,(%esp)
80105d13:	e8 7d ff ff ff       	call   80105c95 <fdalloc>
80105d18:	89 45 f4             	mov    %eax,-0xc(%ebp)
80105d1b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105d1f:	79 07                	jns    80105d28 <sys_dup+0x47>
    return -1;
80105d21:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105d26:	eb 0e                	jmp    80105d36 <sys_dup+0x55>
  filedup(f);
80105d28:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105d2b:	89 04 24             	mov    %eax,(%esp)
80105d2e:	e8 e6 b9 ff ff       	call   80101719 <filedup>
  return fd;
80105d33:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
80105d36:	c9                   	leave  
80105d37:	c3                   	ret    

80105d38 <sys_read>:

int
sys_read(void)
{
80105d38:	55                   	push   %ebp
80105d39:	89 e5                	mov    %esp,%ebp
80105d3b:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80105d3e:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105d41:	89 44 24 08          	mov    %eax,0x8(%esp)
80105d45:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80105d4c:	00 
80105d4d:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80105d54:	e8 c7 fe ff ff       	call   80105c20 <argfd>
80105d59:	85 c0                	test   %eax,%eax
80105d5b:	78 35                	js     80105d92 <sys_read+0x5a>
80105d5d:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105d60:	89 44 24 04          	mov    %eax,0x4(%esp)
80105d64:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
80105d6b:	e8 56 fd ff ff       	call   80105ac6 <argint>
80105d70:	85 c0                	test   %eax,%eax
80105d72:	78 1e                	js     80105d92 <sys_read+0x5a>
80105d74:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105d77:	89 44 24 08          	mov    %eax,0x8(%esp)
80105d7b:	8d 45 ec             	lea    -0x14(%ebp),%eax
80105d7e:	89 44 24 04          	mov    %eax,0x4(%esp)
80105d82:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80105d89:	e8 67 fd ff ff       	call   80105af5 <argptr>
80105d8e:	85 c0                	test   %eax,%eax
80105d90:	79 07                	jns    80105d99 <sys_read+0x61>
    return -1;
80105d92:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105d97:	eb 19                	jmp    80105db2 <sys_read+0x7a>
  return fileread(f, p, n);
80105d99:	8b 4d f0             	mov    -0x10(%ebp),%ecx
80105d9c:	8b 55 ec             	mov    -0x14(%ebp),%edx
80105d9f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105da2:	89 4c 24 08          	mov    %ecx,0x8(%esp)
80105da6:	89 54 24 04          	mov    %edx,0x4(%esp)
80105daa:	89 04 24             	mov    %eax,(%esp)
80105dad:	e8 d4 ba ff ff       	call   80101886 <fileread>
}
80105db2:	c9                   	leave  
80105db3:	c3                   	ret    

80105db4 <sys_write>:

int
sys_write(void)
{
80105db4:	55                   	push   %ebp
80105db5:	89 e5                	mov    %esp,%ebp
80105db7:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80105dba:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105dbd:	89 44 24 08          	mov    %eax,0x8(%esp)
80105dc1:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80105dc8:	00 
80105dc9:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80105dd0:	e8 4b fe ff ff       	call   80105c20 <argfd>
80105dd5:	85 c0                	test   %eax,%eax
80105dd7:	78 35                	js     80105e0e <sys_write+0x5a>
80105dd9:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105ddc:	89 44 24 04          	mov    %eax,0x4(%esp)
80105de0:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
80105de7:	e8 da fc ff ff       	call   80105ac6 <argint>
80105dec:	85 c0                	test   %eax,%eax
80105dee:	78 1e                	js     80105e0e <sys_write+0x5a>
80105df0:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105df3:	89 44 24 08          	mov    %eax,0x8(%esp)
80105df7:	8d 45 ec             	lea    -0x14(%ebp),%eax
80105dfa:	89 44 24 04          	mov    %eax,0x4(%esp)
80105dfe:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80105e05:	e8 eb fc ff ff       	call   80105af5 <argptr>
80105e0a:	85 c0                	test   %eax,%eax
80105e0c:	79 07                	jns    80105e15 <sys_write+0x61>
    return -1;
80105e0e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105e13:	eb 19                	jmp    80105e2e <sys_write+0x7a>
  return filewrite(f, p, n);
80105e15:	8b 4d f0             	mov    -0x10(%ebp),%ecx
80105e18:	8b 55 ec             	mov    -0x14(%ebp),%edx
80105e1b:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105e1e:	89 4c 24 08          	mov    %ecx,0x8(%esp)
80105e22:	89 54 24 04          	mov    %edx,0x4(%esp)
80105e26:	89 04 24             	mov    %eax,(%esp)
80105e29:	e8 14 bb ff ff       	call   80101942 <filewrite>
}
80105e2e:	c9                   	leave  
80105e2f:	c3                   	ret    

80105e30 <sys_close>:

int
sys_close(void)
{
80105e30:	55                   	push   %ebp
80105e31:	89 e5                	mov    %esp,%ebp
80105e33:	83 ec 28             	sub    $0x28,%esp
  int fd;
  struct file *f;

  if(argfd(0, &fd, &f) < 0)
80105e36:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105e39:	89 44 24 08          	mov    %eax,0x8(%esp)
80105e3d:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105e40:	89 44 24 04          	mov    %eax,0x4(%esp)
80105e44:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80105e4b:	e8 d0 fd ff ff       	call   80105c20 <argfd>
80105e50:	85 c0                	test   %eax,%eax
80105e52:	79 07                	jns    80105e5b <sys_close+0x2b>
    return -1;
80105e54:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105e59:	eb 24                	jmp    80105e7f <sys_close+0x4f>
  proc->ofile[fd] = 0;
80105e5b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105e61:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105e64:	83 c2 08             	add    $0x8,%edx
80105e67:	c7 44 90 08 00 00 00 	movl   $0x0,0x8(%eax,%edx,4)
80105e6e:	00 
  fileclose(f);
80105e6f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105e72:	89 04 24             	mov    %eax,(%esp)
80105e75:	e8 e7 b8 ff ff       	call   80101761 <fileclose>
  return 0;
80105e7a:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105e7f:	c9                   	leave  
80105e80:	c3                   	ret    

80105e81 <sys_fstat>:

int
sys_fstat(void)
{
80105e81:	55                   	push   %ebp
80105e82:	89 e5                	mov    %esp,%ebp
80105e84:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  struct stat *st;

  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
80105e87:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105e8a:	89 44 24 08          	mov    %eax,0x8(%esp)
80105e8e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80105e95:	00 
80105e96:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80105e9d:	e8 7e fd ff ff       	call   80105c20 <argfd>
80105ea2:	85 c0                	test   %eax,%eax
80105ea4:	78 1f                	js     80105ec5 <sys_fstat+0x44>
80105ea6:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105ea9:	c7 44 24 08 14 00 00 	movl   $0x14,0x8(%esp)
80105eb0:	00 
80105eb1:	89 44 24 04          	mov    %eax,0x4(%esp)
80105eb5:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80105ebc:	e8 34 fc ff ff       	call   80105af5 <argptr>
80105ec1:	85 c0                	test   %eax,%eax
80105ec3:	79 07                	jns    80105ecc <sys_fstat+0x4b>
    return -1;
80105ec5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105eca:	eb 12                	jmp    80105ede <sys_fstat+0x5d>
  return filestat(f, st);
80105ecc:	8b 55 f0             	mov    -0x10(%ebp),%edx
80105ecf:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105ed2:	89 54 24 04          	mov    %edx,0x4(%esp)
80105ed6:	89 04 24             	mov    %eax,(%esp)
80105ed9:	e8 59 b9 ff ff       	call   80101837 <filestat>
}
80105ede:	c9                   	leave  
80105edf:	c3                   	ret    

80105ee0 <sys_link>:

// Create the path new as a link to the same inode as old.
int
sys_link(void)
{
80105ee0:	55                   	push   %ebp
80105ee1:	89 e5                	mov    %esp,%ebp
80105ee3:	83 ec 38             	sub    $0x38,%esp
  char name[DIRSIZ], *new, *old;
  struct inode *dp, *ip;

  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
80105ee6:	8d 45 d8             	lea    -0x28(%ebp),%eax
80105ee9:	89 44 24 04          	mov    %eax,0x4(%esp)
80105eed:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80105ef4:	e8 5e fc ff ff       	call   80105b57 <argstr>
80105ef9:	85 c0                	test   %eax,%eax
80105efb:	78 17                	js     80105f14 <sys_link+0x34>
80105efd:	8d 45 dc             	lea    -0x24(%ebp),%eax
80105f00:	89 44 24 04          	mov    %eax,0x4(%esp)
80105f04:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80105f0b:	e8 47 fc ff ff       	call   80105b57 <argstr>
80105f10:	85 c0                	test   %eax,%eax
80105f12:	79 0a                	jns    80105f1e <sys_link+0x3e>
    return -1;
80105f14:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105f19:	e9 41 01 00 00       	jmp    8010605f <sys_link+0x17f>

  begin_op();
80105f1e:	e8 47 db ff ff       	call   80103a6a <begin_op>
  if((ip = namei(old)) == 0){
80105f23:	8b 45 d8             	mov    -0x28(%ebp),%eax
80105f26:	89 04 24             	mov    %eax,(%esp)
80105f29:	e8 f1 cc ff ff       	call   80102c1f <namei>
80105f2e:	89 45 f4             	mov    %eax,-0xc(%ebp)
80105f31:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105f35:	75 0f                	jne    80105f46 <sys_link+0x66>
    end_op();
80105f37:	e8 b0 db ff ff       	call   80103aec <end_op>
    return -1;
80105f3c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105f41:	e9 19 01 00 00       	jmp    8010605f <sys_link+0x17f>
  }

  ilock(ip);
80105f46:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105f49:	89 04 24             	mov    %eax,(%esp)
80105f4c:	e8 20 c1 ff ff       	call   80102071 <ilock>
  if(ip->type == T_DIR){
80105f51:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105f54:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80105f58:	66 83 f8 01          	cmp    $0x1,%ax
80105f5c:	75 1a                	jne    80105f78 <sys_link+0x98>
    iunlockput(ip);
80105f5e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105f61:	89 04 24             	mov    %eax,(%esp)
80105f64:	e8 95 c3 ff ff       	call   801022fe <iunlockput>
    end_op();
80105f69:	e8 7e db ff ff       	call   80103aec <end_op>
    return -1;
80105f6e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105f73:	e9 e7 00 00 00       	jmp    8010605f <sys_link+0x17f>
  }

  ip->nlink++;
80105f78:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105f7b:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80105f7f:	8d 50 01             	lea    0x1(%eax),%edx
80105f82:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105f85:	66 89 50 16          	mov    %dx,0x16(%eax)
  iupdate(ip);
80105f89:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105f8c:	89 04 24             	mov    %eax,(%esp)
80105f8f:	e8 17 bf ff ff       	call   80101eab <iupdate>
  iunlock(ip);
80105f94:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105f97:	89 04 24             	mov    %eax,(%esp)
80105f9a:	e8 29 c2 ff ff       	call   801021c8 <iunlock>

  if((dp = nameiparent(new, name)) == 0)
80105f9f:	8b 45 dc             	mov    -0x24(%ebp),%eax
80105fa2:	8d 55 e2             	lea    -0x1e(%ebp),%edx
80105fa5:	89 54 24 04          	mov    %edx,0x4(%esp)
80105fa9:	89 04 24             	mov    %eax,(%esp)
80105fac:	e8 90 cc ff ff       	call   80102c41 <nameiparent>
80105fb1:	89 45 f0             	mov    %eax,-0x10(%ebp)
80105fb4:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80105fb8:	74 68                	je     80106022 <sys_link+0x142>
    goto bad;
  ilock(dp);
80105fba:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105fbd:	89 04 24             	mov    %eax,(%esp)
80105fc0:	e8 ac c0 ff ff       	call   80102071 <ilock>
  if(dp->dev != ip->dev || dirlink(dp, name, ip->inum) < 0){
80105fc5:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105fc8:	8b 10                	mov    (%eax),%edx
80105fca:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105fcd:	8b 00                	mov    (%eax),%eax
80105fcf:	39 c2                	cmp    %eax,%edx
80105fd1:	75 20                	jne    80105ff3 <sys_link+0x113>
80105fd3:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105fd6:	8b 40 04             	mov    0x4(%eax),%eax
80105fd9:	89 44 24 08          	mov    %eax,0x8(%esp)
80105fdd:	8d 45 e2             	lea    -0x1e(%ebp),%eax
80105fe0:	89 44 24 04          	mov    %eax,0x4(%esp)
80105fe4:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105fe7:	89 04 24             	mov    %eax,(%esp)
80105fea:	e8 6f c9 ff ff       	call   8010295e <dirlink>
80105fef:	85 c0                	test   %eax,%eax
80105ff1:	79 0d                	jns    80106000 <sys_link+0x120>
    iunlockput(dp);
80105ff3:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105ff6:	89 04 24             	mov    %eax,(%esp)
80105ff9:	e8 00 c3 ff ff       	call   801022fe <iunlockput>
    goto bad;
80105ffe:	eb 23                	jmp    80106023 <sys_link+0x143>
  }
  iunlockput(dp);
80106000:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106003:	89 04 24             	mov    %eax,(%esp)
80106006:	e8 f3 c2 ff ff       	call   801022fe <iunlockput>
  iput(ip);
8010600b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010600e:	89 04 24             	mov    %eax,(%esp)
80106011:	e8 17 c2 ff ff       	call   8010222d <iput>

  end_op();
80106016:	e8 d1 da ff ff       	call   80103aec <end_op>

  return 0;
8010601b:	b8 00 00 00 00       	mov    $0x0,%eax
80106020:	eb 3d                	jmp    8010605f <sys_link+0x17f>
  ip->nlink++;
  iupdate(ip);
  iunlock(ip);

  if((dp = nameiparent(new, name)) == 0)
    goto bad;
80106022:	90                   	nop
  end_op();

  return 0;

bad:
  ilock(ip);
80106023:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106026:	89 04 24             	mov    %eax,(%esp)
80106029:	e8 43 c0 ff ff       	call   80102071 <ilock>
  ip->nlink--;
8010602e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106031:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80106035:	8d 50 ff             	lea    -0x1(%eax),%edx
80106038:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010603b:	66 89 50 16          	mov    %dx,0x16(%eax)
  iupdate(ip);
8010603f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106042:	89 04 24             	mov    %eax,(%esp)
80106045:	e8 61 be ff ff       	call   80101eab <iupdate>
  iunlockput(ip);
8010604a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010604d:	89 04 24             	mov    %eax,(%esp)
80106050:	e8 a9 c2 ff ff       	call   801022fe <iunlockput>
  end_op();
80106055:	e8 92 da ff ff       	call   80103aec <end_op>
  return -1;
8010605a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
8010605f:	c9                   	leave  
80106060:	c3                   	ret    

80106061 <isdirempty>:

// Is the directory dp empty except for "." and ".." ?
static int
isdirempty(struct inode *dp)
{
80106061:	55                   	push   %ebp
80106062:	89 e5                	mov    %esp,%ebp
80106064:	83 ec 38             	sub    $0x38,%esp
  int off;
  struct dirent de;

  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
80106067:	c7 45 f4 20 00 00 00 	movl   $0x20,-0xc(%ebp)
8010606e:	eb 4b                	jmp    801060bb <isdirempty+0x5a>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80106070:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106073:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80106076:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
8010607d:	00 
8010607e:	89 54 24 08          	mov    %edx,0x8(%esp)
80106082:	89 44 24 04          	mov    %eax,0x4(%esp)
80106086:	8b 45 08             	mov    0x8(%ebp),%eax
80106089:	89 04 24             	mov    %eax,(%esp)
8010608c:	e8 df c4 ff ff       	call   80102570 <readi>
80106091:	83 f8 10             	cmp    $0x10,%eax
80106094:	74 0c                	je     801060a2 <isdirempty+0x41>
      panic("isdirempty: readi");
80106096:	c7 04 24 2b 91 10 80 	movl   $0x8010912b,(%esp)
8010609d:	e8 c7 a7 ff ff       	call   80100869 <panic>
    if(de.inum != 0)
801060a2:	0f b7 45 e4          	movzwl -0x1c(%ebp),%eax
801060a6:	66 85 c0             	test   %ax,%ax
801060a9:	74 07                	je     801060b2 <isdirempty+0x51>
      return 0;
801060ab:	b8 00 00 00 00       	mov    $0x0,%eax
801060b0:	eb 1b                	jmp    801060cd <isdirempty+0x6c>
isdirempty(struct inode *dp)
{
  int off;
  struct dirent de;

  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
801060b2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801060b5:	83 c0 10             	add    $0x10,%eax
801060b8:	89 45 f4             	mov    %eax,-0xc(%ebp)
801060bb:	8b 55 f4             	mov    -0xc(%ebp),%edx
801060be:	8b 45 08             	mov    0x8(%ebp),%eax
801060c1:	8b 40 18             	mov    0x18(%eax),%eax
801060c4:	39 c2                	cmp    %eax,%edx
801060c6:	72 a8                	jb     80106070 <isdirempty+0xf>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("isdirempty: readi");
    if(de.inum != 0)
      return 0;
  }
  return 1;
801060c8:	b8 01 00 00 00       	mov    $0x1,%eax
}
801060cd:	c9                   	leave  
801060ce:	c3                   	ret    

801060cf <sys_unlink>:

//PAGEBREAK!
int
sys_unlink(void)
{
801060cf:	55                   	push   %ebp
801060d0:	89 e5                	mov    %esp,%ebp
801060d2:	83 ec 48             	sub    $0x48,%esp
  struct inode *ip, *dp;
  struct dirent de;
  char name[DIRSIZ], *path;
  uint off;

  if(argstr(0, &path) < 0)
801060d5:	8d 45 cc             	lea    -0x34(%ebp),%eax
801060d8:	89 44 24 04          	mov    %eax,0x4(%esp)
801060dc:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801060e3:	e8 6f fa ff ff       	call   80105b57 <argstr>
801060e8:	85 c0                	test   %eax,%eax
801060ea:	79 0a                	jns    801060f6 <sys_unlink+0x27>
    return -1;
801060ec:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801060f1:	e9 af 01 00 00       	jmp    801062a5 <sys_unlink+0x1d6>

  begin_op();
801060f6:	e8 6f d9 ff ff       	call   80103a6a <begin_op>
  if((dp = nameiparent(path, name)) == 0){
801060fb:	8b 45 cc             	mov    -0x34(%ebp),%eax
801060fe:	8d 55 d2             	lea    -0x2e(%ebp),%edx
80106101:	89 54 24 04          	mov    %edx,0x4(%esp)
80106105:	89 04 24             	mov    %eax,(%esp)
80106108:	e8 34 cb ff ff       	call   80102c41 <nameiparent>
8010610d:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106110:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106114:	75 0f                	jne    80106125 <sys_unlink+0x56>
    end_op();
80106116:	e8 d1 d9 ff ff       	call   80103aec <end_op>
    return -1;
8010611b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106120:	e9 80 01 00 00       	jmp    801062a5 <sys_unlink+0x1d6>
  }

  ilock(dp);
80106125:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106128:	89 04 24             	mov    %eax,(%esp)
8010612b:	e8 41 bf ff ff       	call   80102071 <ilock>

  // Cannot unlink "." or "..".
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
80106130:	c7 44 24 04 3d 91 10 	movl   $0x8010913d,0x4(%esp)
80106137:	80 
80106138:	8d 45 d2             	lea    -0x2e(%ebp),%eax
8010613b:	89 04 24             	mov    %eax,(%esp)
8010613e:	e8 31 c7 ff ff       	call   80102874 <namecmp>
80106143:	85 c0                	test   %eax,%eax
80106145:	0f 84 45 01 00 00    	je     80106290 <sys_unlink+0x1c1>
8010614b:	c7 44 24 04 3f 91 10 	movl   $0x8010913f,0x4(%esp)
80106152:	80 
80106153:	8d 45 d2             	lea    -0x2e(%ebp),%eax
80106156:	89 04 24             	mov    %eax,(%esp)
80106159:	e8 16 c7 ff ff       	call   80102874 <namecmp>
8010615e:	85 c0                	test   %eax,%eax
80106160:	0f 84 2a 01 00 00    	je     80106290 <sys_unlink+0x1c1>
    goto bad;

  if((ip = dirlookup(dp, name, &off)) == 0)
80106166:	8d 45 c8             	lea    -0x38(%ebp),%eax
80106169:	89 44 24 08          	mov    %eax,0x8(%esp)
8010616d:	8d 45 d2             	lea    -0x2e(%ebp),%eax
80106170:	89 44 24 04          	mov    %eax,0x4(%esp)
80106174:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106177:	89 04 24             	mov    %eax,(%esp)
8010617a:	e8 17 c7 ff ff       	call   80102896 <dirlookup>
8010617f:	89 45 f0             	mov    %eax,-0x10(%ebp)
80106182:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106186:	0f 84 03 01 00 00    	je     8010628f <sys_unlink+0x1c0>
    goto bad;
  ilock(ip);
8010618c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010618f:	89 04 24             	mov    %eax,(%esp)
80106192:	e8 da be ff ff       	call   80102071 <ilock>

  if(ip->nlink < 1)
80106197:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010619a:	0f b7 40 16          	movzwl 0x16(%eax),%eax
8010619e:	66 85 c0             	test   %ax,%ax
801061a1:	7f 0c                	jg     801061af <sys_unlink+0xe0>
    panic("unlink: nlink < 1");
801061a3:	c7 04 24 42 91 10 80 	movl   $0x80109142,(%esp)
801061aa:	e8 ba a6 ff ff       	call   80100869 <panic>
  if(ip->type == T_DIR && !isdirempty(ip)){
801061af:	8b 45 f0             	mov    -0x10(%ebp),%eax
801061b2:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801061b6:	66 83 f8 01          	cmp    $0x1,%ax
801061ba:	75 1f                	jne    801061db <sys_unlink+0x10c>
801061bc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801061bf:	89 04 24             	mov    %eax,(%esp)
801061c2:	e8 9a fe ff ff       	call   80106061 <isdirempty>
801061c7:	85 c0                	test   %eax,%eax
801061c9:	75 10                	jne    801061db <sys_unlink+0x10c>
    iunlockput(ip);
801061cb:	8b 45 f0             	mov    -0x10(%ebp),%eax
801061ce:	89 04 24             	mov    %eax,(%esp)
801061d1:	e8 28 c1 ff ff       	call   801022fe <iunlockput>
    goto bad;
801061d6:	e9 b5 00 00 00       	jmp    80106290 <sys_unlink+0x1c1>
  }

  memset(&de, 0, sizeof(de));
801061db:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
801061e2:	00 
801061e3:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801061ea:	00 
801061eb:	8d 45 e0             	lea    -0x20(%ebp),%eax
801061ee:	89 04 24             	mov    %eax,(%esp)
801061f1:	e8 74 f5 ff ff       	call   8010576a <memset>
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801061f6:	8b 55 c8             	mov    -0x38(%ebp),%edx
801061f9:	8d 45 e0             	lea    -0x20(%ebp),%eax
801061fc:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
80106203:	00 
80106204:	89 54 24 08          	mov    %edx,0x8(%esp)
80106208:	89 44 24 04          	mov    %eax,0x4(%esp)
8010620c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010620f:	89 04 24             	mov    %eax,(%esp)
80106212:	e8 c5 c4 ff ff       	call   801026dc <writei>
80106217:	83 f8 10             	cmp    $0x10,%eax
8010621a:	74 0c                	je     80106228 <sys_unlink+0x159>
    panic("unlink: writei");
8010621c:	c7 04 24 54 91 10 80 	movl   $0x80109154,(%esp)
80106223:	e8 41 a6 ff ff       	call   80100869 <panic>
  if(ip->type == T_DIR){
80106228:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010622b:	0f b7 40 10          	movzwl 0x10(%eax),%eax
8010622f:	66 83 f8 01          	cmp    $0x1,%ax
80106233:	75 1c                	jne    80106251 <sys_unlink+0x182>
    dp->nlink--;
80106235:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106238:	0f b7 40 16          	movzwl 0x16(%eax),%eax
8010623c:	8d 50 ff             	lea    -0x1(%eax),%edx
8010623f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106242:	66 89 50 16          	mov    %dx,0x16(%eax)
    iupdate(dp);
80106246:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106249:	89 04 24             	mov    %eax,(%esp)
8010624c:	e8 5a bc ff ff       	call   80101eab <iupdate>
  }
  iunlockput(dp);
80106251:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106254:	89 04 24             	mov    %eax,(%esp)
80106257:	e8 a2 c0 ff ff       	call   801022fe <iunlockput>

  ip->nlink--;
8010625c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010625f:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80106263:	8d 50 ff             	lea    -0x1(%eax),%edx
80106266:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106269:	66 89 50 16          	mov    %dx,0x16(%eax)
  iupdate(ip);
8010626d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106270:	89 04 24             	mov    %eax,(%esp)
80106273:	e8 33 bc ff ff       	call   80101eab <iupdate>
  iunlockput(ip);
80106278:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010627b:	89 04 24             	mov    %eax,(%esp)
8010627e:	e8 7b c0 ff ff       	call   801022fe <iunlockput>

  end_op();
80106283:	e8 64 d8 ff ff       	call   80103aec <end_op>

  return 0;
80106288:	b8 00 00 00 00       	mov    $0x0,%eax
8010628d:	eb 16                	jmp    801062a5 <sys_unlink+0x1d6>
  // Cannot unlink "." or "..".
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
    goto bad;

  if((ip = dirlookup(dp, name, &off)) == 0)
    goto bad;
8010628f:	90                   	nop
  end_op();

  return 0;

bad:
  iunlockput(dp);
80106290:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106293:	89 04 24             	mov    %eax,(%esp)
80106296:	e8 63 c0 ff ff       	call   801022fe <iunlockput>
  end_op();
8010629b:	e8 4c d8 ff ff       	call   80103aec <end_op>
  return -1;
801062a0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801062a5:	c9                   	leave  
801062a6:	c3                   	ret    

801062a7 <create>:

static struct inode*
create(char *path, short type, short major, short minor)
{
801062a7:	55                   	push   %ebp
801062a8:	89 e5                	mov    %esp,%ebp
801062aa:	83 ec 48             	sub    $0x48,%esp
801062ad:	8b 4d 0c             	mov    0xc(%ebp),%ecx
801062b0:	8b 55 10             	mov    0x10(%ebp),%edx
801062b3:	8b 45 14             	mov    0x14(%ebp),%eax
801062b6:	66 89 4d d4          	mov    %cx,-0x2c(%ebp)
801062ba:	66 89 55 d0          	mov    %dx,-0x30(%ebp)
801062be:	66 89 45 cc          	mov    %ax,-0x34(%ebp)
  uint off;
  struct inode *ip, *dp;
  char name[DIRSIZ];

  if((dp = nameiparent(path, name)) == 0)
801062c2:	8d 45 de             	lea    -0x22(%ebp),%eax
801062c5:	89 44 24 04          	mov    %eax,0x4(%esp)
801062c9:	8b 45 08             	mov    0x8(%ebp),%eax
801062cc:	89 04 24             	mov    %eax,(%esp)
801062cf:	e8 6d c9 ff ff       	call   80102c41 <nameiparent>
801062d4:	89 45 f4             	mov    %eax,-0xc(%ebp)
801062d7:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801062db:	75 0a                	jne    801062e7 <create+0x40>
    return 0;
801062dd:	b8 00 00 00 00       	mov    $0x0,%eax
801062e2:	e9 7e 01 00 00       	jmp    80106465 <create+0x1be>
  ilock(dp);
801062e7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801062ea:	89 04 24             	mov    %eax,(%esp)
801062ed:	e8 7f bd ff ff       	call   80102071 <ilock>

  if((ip = dirlookup(dp, name, &off)) != 0){
801062f2:	8d 45 ec             	lea    -0x14(%ebp),%eax
801062f5:	89 44 24 08          	mov    %eax,0x8(%esp)
801062f9:	8d 45 de             	lea    -0x22(%ebp),%eax
801062fc:	89 44 24 04          	mov    %eax,0x4(%esp)
80106300:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106303:	89 04 24             	mov    %eax,(%esp)
80106306:	e8 8b c5 ff ff       	call   80102896 <dirlookup>
8010630b:	89 45 f0             	mov    %eax,-0x10(%ebp)
8010630e:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106312:	74 47                	je     8010635b <create+0xb4>
    iunlockput(dp);
80106314:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106317:	89 04 24             	mov    %eax,(%esp)
8010631a:	e8 df bf ff ff       	call   801022fe <iunlockput>
    ilock(ip);
8010631f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106322:	89 04 24             	mov    %eax,(%esp)
80106325:	e8 47 bd ff ff       	call   80102071 <ilock>
    if(type == T_FILE && ip->type == T_FILE)
8010632a:	66 83 7d d4 02       	cmpw   $0x2,-0x2c(%ebp)
8010632f:	75 15                	jne    80106346 <create+0x9f>
80106331:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106334:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80106338:	66 83 f8 02          	cmp    $0x2,%ax
8010633c:	75 08                	jne    80106346 <create+0x9f>
      return ip;
8010633e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106341:	e9 1f 01 00 00       	jmp    80106465 <create+0x1be>
    iunlockput(ip);
80106346:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106349:	89 04 24             	mov    %eax,(%esp)
8010634c:	e8 ad bf ff ff       	call   801022fe <iunlockput>
    return 0;
80106351:	b8 00 00 00 00       	mov    $0x0,%eax
80106356:	e9 0a 01 00 00       	jmp    80106465 <create+0x1be>
  }

  if((ip = ialloc(dp->dev, type)) == 0)
8010635b:	0f bf 55 d4          	movswl -0x2c(%ebp),%edx
8010635f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106362:	8b 00                	mov    (%eax),%eax
80106364:	89 54 24 04          	mov    %edx,0x4(%esp)
80106368:	89 04 24             	mov    %eax,(%esp)
8010636b:	e8 67 ba ff ff       	call   80101dd7 <ialloc>
80106370:	89 45 f0             	mov    %eax,-0x10(%ebp)
80106373:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106377:	75 0c                	jne    80106385 <create+0xde>
    panic("create: ialloc");
80106379:	c7 04 24 63 91 10 80 	movl   $0x80109163,(%esp)
80106380:	e8 e4 a4 ff ff       	call   80100869 <panic>

  ilock(ip);
80106385:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106388:	89 04 24             	mov    %eax,(%esp)
8010638b:	e8 e1 bc ff ff       	call   80102071 <ilock>
  ip->major = major;
80106390:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106393:	0f b7 55 d0          	movzwl -0x30(%ebp),%edx
80106397:	66 89 50 12          	mov    %dx,0x12(%eax)
  ip->minor = minor;
8010639b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010639e:	0f b7 55 cc          	movzwl -0x34(%ebp),%edx
801063a2:	66 89 50 14          	mov    %dx,0x14(%eax)
  ip->nlink = 1;
801063a6:	8b 45 f0             	mov    -0x10(%ebp),%eax
801063a9:	66 c7 40 16 01 00    	movw   $0x1,0x16(%eax)
  iupdate(ip);
801063af:	8b 45 f0             	mov    -0x10(%ebp),%eax
801063b2:	89 04 24             	mov    %eax,(%esp)
801063b5:	e8 f1 ba ff ff       	call   80101eab <iupdate>

  if(type == T_DIR){  // Create . and .. entries.
801063ba:	66 83 7d d4 01       	cmpw   $0x1,-0x2c(%ebp)
801063bf:	75 6a                	jne    8010642b <create+0x184>
    dp->nlink++;  // for ".."
801063c1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801063c4:	0f b7 40 16          	movzwl 0x16(%eax),%eax
801063c8:	8d 50 01             	lea    0x1(%eax),%edx
801063cb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801063ce:	66 89 50 16          	mov    %dx,0x16(%eax)
    iupdate(dp);
801063d2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801063d5:	89 04 24             	mov    %eax,(%esp)
801063d8:	e8 ce ba ff ff       	call   80101eab <iupdate>
    // No ip->nlink++ for ".": avoid cyclic ref count.
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
801063dd:	8b 45 f0             	mov    -0x10(%ebp),%eax
801063e0:	8b 40 04             	mov    0x4(%eax),%eax
801063e3:	89 44 24 08          	mov    %eax,0x8(%esp)
801063e7:	c7 44 24 04 3d 91 10 	movl   $0x8010913d,0x4(%esp)
801063ee:	80 
801063ef:	8b 45 f0             	mov    -0x10(%ebp),%eax
801063f2:	89 04 24             	mov    %eax,(%esp)
801063f5:	e8 64 c5 ff ff       	call   8010295e <dirlink>
801063fa:	85 c0                	test   %eax,%eax
801063fc:	78 21                	js     8010641f <create+0x178>
801063fe:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106401:	8b 40 04             	mov    0x4(%eax),%eax
80106404:	89 44 24 08          	mov    %eax,0x8(%esp)
80106408:	c7 44 24 04 3f 91 10 	movl   $0x8010913f,0x4(%esp)
8010640f:	80 
80106410:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106413:	89 04 24             	mov    %eax,(%esp)
80106416:	e8 43 c5 ff ff       	call   8010295e <dirlink>
8010641b:	85 c0                	test   %eax,%eax
8010641d:	79 0c                	jns    8010642b <create+0x184>
      panic("create dots");
8010641f:	c7 04 24 72 91 10 80 	movl   $0x80109172,(%esp)
80106426:	e8 3e a4 ff ff       	call   80100869 <panic>
  }

  if(dirlink(dp, name, ip->inum) < 0)
8010642b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010642e:	8b 40 04             	mov    0x4(%eax),%eax
80106431:	89 44 24 08          	mov    %eax,0x8(%esp)
80106435:	8d 45 de             	lea    -0x22(%ebp),%eax
80106438:	89 44 24 04          	mov    %eax,0x4(%esp)
8010643c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010643f:	89 04 24             	mov    %eax,(%esp)
80106442:	e8 17 c5 ff ff       	call   8010295e <dirlink>
80106447:	85 c0                	test   %eax,%eax
80106449:	79 0c                	jns    80106457 <create+0x1b0>
    panic("create: dirlink");
8010644b:	c7 04 24 7e 91 10 80 	movl   $0x8010917e,(%esp)
80106452:	e8 12 a4 ff ff       	call   80100869 <panic>

  iunlockput(dp);
80106457:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010645a:	89 04 24             	mov    %eax,(%esp)
8010645d:	e8 9c be ff ff       	call   801022fe <iunlockput>

  return ip;
80106462:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
80106465:	c9                   	leave  
80106466:	c3                   	ret    

80106467 <sys_open>:

int
sys_open(void)
{
80106467:	55                   	push   %ebp
80106468:	89 e5                	mov    %esp,%ebp
8010646a:	83 ec 38             	sub    $0x38,%esp
  char *path;
  int fd, omode;
  struct file *f;
  struct inode *ip;

  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
8010646d:	8d 45 e8             	lea    -0x18(%ebp),%eax
80106470:	89 44 24 04          	mov    %eax,0x4(%esp)
80106474:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
8010647b:	e8 d7 f6 ff ff       	call   80105b57 <argstr>
80106480:	85 c0                	test   %eax,%eax
80106482:	78 17                	js     8010649b <sys_open+0x34>
80106484:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80106487:	89 44 24 04          	mov    %eax,0x4(%esp)
8010648b:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80106492:	e8 2f f6 ff ff       	call   80105ac6 <argint>
80106497:	85 c0                	test   %eax,%eax
80106499:	79 0a                	jns    801064a5 <sys_open+0x3e>
    return -1;
8010649b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801064a0:	e9 5a 01 00 00       	jmp    801065ff <sys_open+0x198>

  begin_op();
801064a5:	e8 c0 d5 ff ff       	call   80103a6a <begin_op>

  if(omode & O_CREATE){
801064aa:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801064ad:	25 00 02 00 00       	and    $0x200,%eax
801064b2:	85 c0                	test   %eax,%eax
801064b4:	74 3b                	je     801064f1 <sys_open+0x8a>
    ip = create(path, T_FILE, 0, 0);
801064b6:	8b 45 e8             	mov    -0x18(%ebp),%eax
801064b9:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
801064c0:	00 
801064c1:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
801064c8:	00 
801064c9:	c7 44 24 04 02 00 00 	movl   $0x2,0x4(%esp)
801064d0:	00 
801064d1:	89 04 24             	mov    %eax,(%esp)
801064d4:	e8 ce fd ff ff       	call   801062a7 <create>
801064d9:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(ip == 0){
801064dc:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801064e0:	75 6b                	jne    8010654d <sys_open+0xe6>
      end_op();
801064e2:	e8 05 d6 ff ff       	call   80103aec <end_op>
      return -1;
801064e7:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801064ec:	e9 0e 01 00 00       	jmp    801065ff <sys_open+0x198>
    }
  } else {
    if((ip = namei(path)) == 0){
801064f1:	8b 45 e8             	mov    -0x18(%ebp),%eax
801064f4:	89 04 24             	mov    %eax,(%esp)
801064f7:	e8 23 c7 ff ff       	call   80102c1f <namei>
801064fc:	89 45 f4             	mov    %eax,-0xc(%ebp)
801064ff:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106503:	75 0f                	jne    80106514 <sys_open+0xad>
      end_op();
80106505:	e8 e2 d5 ff ff       	call   80103aec <end_op>
      return -1;
8010650a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010650f:	e9 eb 00 00 00       	jmp    801065ff <sys_open+0x198>
    }
    ilock(ip);
80106514:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106517:	89 04 24             	mov    %eax,(%esp)
8010651a:	e8 52 bb ff ff       	call   80102071 <ilock>
    if(ip->type == T_DIR && omode != O_RDONLY){
8010651f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106522:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80106526:	66 83 f8 01          	cmp    $0x1,%ax
8010652a:	75 21                	jne    8010654d <sys_open+0xe6>
8010652c:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010652f:	85 c0                	test   %eax,%eax
80106531:	74 1a                	je     8010654d <sys_open+0xe6>
      iunlockput(ip);
80106533:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106536:	89 04 24             	mov    %eax,(%esp)
80106539:	e8 c0 bd ff ff       	call   801022fe <iunlockput>
      end_op();
8010653e:	e8 a9 d5 ff ff       	call   80103aec <end_op>
      return -1;
80106543:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106548:	e9 b2 00 00 00       	jmp    801065ff <sys_open+0x198>
    }
  }

  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
8010654d:	e8 66 b1 ff ff       	call   801016b8 <filealloc>
80106552:	89 45 f0             	mov    %eax,-0x10(%ebp)
80106555:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106559:	74 14                	je     8010656f <sys_open+0x108>
8010655b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010655e:	89 04 24             	mov    %eax,(%esp)
80106561:	e8 2f f7 ff ff       	call   80105c95 <fdalloc>
80106566:	89 45 ec             	mov    %eax,-0x14(%ebp)
80106569:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
8010656d:	79 28                	jns    80106597 <sys_open+0x130>
    if(f)
8010656f:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106573:	74 0b                	je     80106580 <sys_open+0x119>
      fileclose(f);
80106575:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106578:	89 04 24             	mov    %eax,(%esp)
8010657b:	e8 e1 b1 ff ff       	call   80101761 <fileclose>
    iunlockput(ip);
80106580:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106583:	89 04 24             	mov    %eax,(%esp)
80106586:	e8 73 bd ff ff       	call   801022fe <iunlockput>
    end_op();
8010658b:	e8 5c d5 ff ff       	call   80103aec <end_op>
    return -1;
80106590:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106595:	eb 68                	jmp    801065ff <sys_open+0x198>
  }
  iunlock(ip);
80106597:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010659a:	89 04 24             	mov    %eax,(%esp)
8010659d:	e8 26 bc ff ff       	call   801021c8 <iunlock>
  end_op();
801065a2:	e8 45 d5 ff ff       	call   80103aec <end_op>

  f->type = FD_INODE;
801065a7:	8b 45 f0             	mov    -0x10(%ebp),%eax
801065aa:	c7 00 02 00 00 00    	movl   $0x2,(%eax)
  f->ip = ip;
801065b0:	8b 45 f0             	mov    -0x10(%ebp),%eax
801065b3:	8b 55 f4             	mov    -0xc(%ebp),%edx
801065b6:	89 50 10             	mov    %edx,0x10(%eax)
  f->off = 0;
801065b9:	8b 45 f0             	mov    -0x10(%ebp),%eax
801065bc:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
  f->readable = !(omode & O_WRONLY);
801065c3:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801065c6:	83 e0 01             	and    $0x1,%eax
801065c9:	85 c0                	test   %eax,%eax
801065cb:	0f 94 c2             	sete   %dl
801065ce:	8b 45 f0             	mov    -0x10(%ebp),%eax
801065d1:	88 50 08             	mov    %dl,0x8(%eax)
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
801065d4:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801065d7:	83 e0 01             	and    $0x1,%eax
801065da:	84 c0                	test   %al,%al
801065dc:	75 0a                	jne    801065e8 <sys_open+0x181>
801065de:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801065e1:	83 e0 02             	and    $0x2,%eax
801065e4:	85 c0                	test   %eax,%eax
801065e6:	74 07                	je     801065ef <sys_open+0x188>
801065e8:	b8 01 00 00 00       	mov    $0x1,%eax
801065ed:	eb 05                	jmp    801065f4 <sys_open+0x18d>
801065ef:	b8 00 00 00 00       	mov    $0x0,%eax
801065f4:	89 c2                	mov    %eax,%edx
801065f6:	8b 45 f0             	mov    -0x10(%ebp),%eax
801065f9:	88 50 09             	mov    %dl,0x9(%eax)
  return fd;
801065fc:	8b 45 ec             	mov    -0x14(%ebp),%eax
}
801065ff:	c9                   	leave  
80106600:	c3                   	ret    

80106601 <sys_mkdir>:

int
sys_mkdir(void)
{
80106601:	55                   	push   %ebp
80106602:	89 e5                	mov    %esp,%ebp
80106604:	83 ec 28             	sub    $0x28,%esp
  char *path;
  struct inode *ip;

  begin_op();
80106607:	e8 5e d4 ff ff       	call   80103a6a <begin_op>
  if(argstr(0, &path) < 0 || (ip = create(path, T_DIR, 0, 0)) == 0){
8010660c:	8d 45 f0             	lea    -0x10(%ebp),%eax
8010660f:	89 44 24 04          	mov    %eax,0x4(%esp)
80106613:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
8010661a:	e8 38 f5 ff ff       	call   80105b57 <argstr>
8010661f:	85 c0                	test   %eax,%eax
80106621:	78 2c                	js     8010664f <sys_mkdir+0x4e>
80106623:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106626:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
8010662d:	00 
8010662e:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80106635:	00 
80106636:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
8010663d:	00 
8010663e:	89 04 24             	mov    %eax,(%esp)
80106641:	e8 61 fc ff ff       	call   801062a7 <create>
80106646:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106649:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010664d:	75 0c                	jne    8010665b <sys_mkdir+0x5a>
    end_op();
8010664f:	e8 98 d4 ff ff       	call   80103aec <end_op>
    return -1;
80106654:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106659:	eb 15                	jmp    80106670 <sys_mkdir+0x6f>
  }
  iunlockput(ip);
8010665b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010665e:	89 04 24             	mov    %eax,(%esp)
80106661:	e8 98 bc ff ff       	call   801022fe <iunlockput>
  end_op();
80106666:	e8 81 d4 ff ff       	call   80103aec <end_op>
  return 0;
8010666b:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106670:	c9                   	leave  
80106671:	c3                   	ret    

80106672 <sys_mknod>:

int
sys_mknod(void)
{
80106672:	55                   	push   %ebp
80106673:	89 e5                	mov    %esp,%ebp
80106675:	83 ec 28             	sub    $0x28,%esp
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
80106678:	e8 ed d3 ff ff       	call   80103a6a <begin_op>
  if((argstr(0, &path)) < 0 ||
8010667d:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106680:	89 44 24 04          	mov    %eax,0x4(%esp)
80106684:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
8010668b:	e8 c7 f4 ff ff       	call   80105b57 <argstr>
80106690:	85 c0                	test   %eax,%eax
80106692:	78 5e                	js     801066f2 <sys_mknod+0x80>
     argint(1, &major) < 0 ||
80106694:	8d 45 ec             	lea    -0x14(%ebp),%eax
80106697:	89 44 24 04          	mov    %eax,0x4(%esp)
8010669b:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
801066a2:	e8 1f f4 ff ff       	call   80105ac6 <argint>
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
  if((argstr(0, &path)) < 0 ||
801066a7:	85 c0                	test   %eax,%eax
801066a9:	78 47                	js     801066f2 <sys_mknod+0x80>
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
801066ab:	8d 45 e8             	lea    -0x18(%ebp),%eax
801066ae:	89 44 24 04          	mov    %eax,0x4(%esp)
801066b2:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
801066b9:	e8 08 f4 ff ff       	call   80105ac6 <argint>
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
  if((argstr(0, &path)) < 0 ||
801066be:	85 c0                	test   %eax,%eax
801066c0:	78 30                	js     801066f2 <sys_mknod+0x80>
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
     (ip = create(path, T_DEV, major, minor)) == 0){
801066c2:	8b 45 e8             	mov    -0x18(%ebp),%eax
801066c5:	0f bf c8             	movswl %ax,%ecx
801066c8:	8b 45 ec             	mov    -0x14(%ebp),%eax
801066cb:	0f bf d0             	movswl %ax,%edx
801066ce:	8b 45 f0             	mov    -0x10(%ebp),%eax
801066d1:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
801066d5:	89 54 24 08          	mov    %edx,0x8(%esp)
801066d9:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
801066e0:	00 
801066e1:	89 04 24             	mov    %eax,(%esp)
801066e4:	e8 be fb ff ff       	call   801062a7 <create>
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
  if((argstr(0, &path)) < 0 ||
801066e9:	89 45 f4             	mov    %eax,-0xc(%ebp)
801066ec:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801066f0:	75 0c                	jne    801066fe <sys_mknod+0x8c>
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
     (ip = create(path, T_DEV, major, minor)) == 0){
    end_op();
801066f2:	e8 f5 d3 ff ff       	call   80103aec <end_op>
    return -1;
801066f7:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801066fc:	eb 15                	jmp    80106713 <sys_mknod+0xa1>
  }
  iunlockput(ip);
801066fe:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106701:	89 04 24             	mov    %eax,(%esp)
80106704:	e8 f5 bb ff ff       	call   801022fe <iunlockput>
  end_op();
80106709:	e8 de d3 ff ff       	call   80103aec <end_op>
  return 0;
8010670e:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106713:	c9                   	leave  
80106714:	c3                   	ret    

80106715 <sys_chdir>:

int
sys_chdir(void)
{
80106715:	55                   	push   %ebp
80106716:	89 e5                	mov    %esp,%ebp
80106718:	83 ec 28             	sub    $0x28,%esp
  char *path;
  struct inode *ip;

  begin_op();
8010671b:	e8 4a d3 ff ff       	call   80103a6a <begin_op>
  if(argstr(0, &path) < 0 || (ip = namei(path)) == 0){
80106720:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106723:	89 44 24 04          	mov    %eax,0x4(%esp)
80106727:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
8010672e:	e8 24 f4 ff ff       	call   80105b57 <argstr>
80106733:	85 c0                	test   %eax,%eax
80106735:	78 14                	js     8010674b <sys_chdir+0x36>
80106737:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010673a:	89 04 24             	mov    %eax,(%esp)
8010673d:	e8 dd c4 ff ff       	call   80102c1f <namei>
80106742:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106745:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106749:	75 0c                	jne    80106757 <sys_chdir+0x42>
    end_op();
8010674b:	e8 9c d3 ff ff       	call   80103aec <end_op>
    return -1;
80106750:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106755:	eb 61                	jmp    801067b8 <sys_chdir+0xa3>
  }
  ilock(ip);
80106757:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010675a:	89 04 24             	mov    %eax,(%esp)
8010675d:	e8 0f b9 ff ff       	call   80102071 <ilock>
  if(ip->type != T_DIR){
80106762:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106765:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80106769:	66 83 f8 01          	cmp    $0x1,%ax
8010676d:	74 17                	je     80106786 <sys_chdir+0x71>
    iunlockput(ip);
8010676f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106772:	89 04 24             	mov    %eax,(%esp)
80106775:	e8 84 bb ff ff       	call   801022fe <iunlockput>
    end_op();
8010677a:	e8 6d d3 ff ff       	call   80103aec <end_op>
    return -1;
8010677f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106784:	eb 32                	jmp    801067b8 <sys_chdir+0xa3>
  }
  iunlock(ip);
80106786:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106789:	89 04 24             	mov    %eax,(%esp)
8010678c:	e8 37 ba ff ff       	call   801021c8 <iunlock>
  iput(proc->cwd);
80106791:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106797:	8b 40 68             	mov    0x68(%eax),%eax
8010679a:	89 04 24             	mov    %eax,(%esp)
8010679d:	e8 8b ba ff ff       	call   8010222d <iput>
  end_op();
801067a2:	e8 45 d3 ff ff       	call   80103aec <end_op>
  proc->cwd = ip;
801067a7:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801067ad:	8b 55 f4             	mov    -0xc(%ebp),%edx
801067b0:	89 50 68             	mov    %edx,0x68(%eax)
  return 0;
801067b3:	b8 00 00 00 00       	mov    $0x0,%eax
}
801067b8:	c9                   	leave  
801067b9:	c3                   	ret    

801067ba <sys_exec>:

int
sys_exec(void)
{
801067ba:	55                   	push   %ebp
801067bb:	89 e5                	mov    %esp,%ebp
801067bd:	81 ec a8 00 00 00    	sub    $0xa8,%esp
  char *path, *argv[MAXARG];
  int i;
  uint uargv, uarg;

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
801067c3:	8d 45 f0             	lea    -0x10(%ebp),%eax
801067c6:	89 44 24 04          	mov    %eax,0x4(%esp)
801067ca:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801067d1:	e8 81 f3 ff ff       	call   80105b57 <argstr>
801067d6:	85 c0                	test   %eax,%eax
801067d8:	78 1a                	js     801067f4 <sys_exec+0x3a>
801067da:	8d 85 6c ff ff ff    	lea    -0x94(%ebp),%eax
801067e0:	89 44 24 04          	mov    %eax,0x4(%esp)
801067e4:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
801067eb:	e8 d6 f2 ff ff       	call   80105ac6 <argint>
801067f0:	85 c0                	test   %eax,%eax
801067f2:	79 0a                	jns    801067fe <sys_exec+0x44>
    return -1;
801067f4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801067f9:	e9 cd 00 00 00       	jmp    801068cb <sys_exec+0x111>
  }
  memset(argv, 0, sizeof(argv));
801067fe:	c7 44 24 08 80 00 00 	movl   $0x80,0x8(%esp)
80106805:	00 
80106806:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
8010680d:	00 
8010680e:	8d 85 70 ff ff ff    	lea    -0x90(%ebp),%eax
80106814:	89 04 24             	mov    %eax,(%esp)
80106817:	e8 4e ef ff ff       	call   8010576a <memset>
  for(i=0;; i++){
8010681c:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    if(i >= NELEM(argv))
80106823:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106826:	83 f8 1f             	cmp    $0x1f,%eax
80106829:	76 0a                	jbe    80106835 <sys_exec+0x7b>
      return -1;
8010682b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106830:	e9 96 00 00 00       	jmp    801068cb <sys_exec+0x111>
    if(fetchint(uargv+4*i, (int*)&uarg) < 0)
80106835:	8d 85 68 ff ff ff    	lea    -0x98(%ebp),%eax
8010683b:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010683e:	c1 e2 02             	shl    $0x2,%edx
80106841:	89 d1                	mov    %edx,%ecx
80106843:	8b 95 6c ff ff ff    	mov    -0x94(%ebp),%edx
80106849:	8d 14 11             	lea    (%ecx,%edx,1),%edx
8010684c:	89 44 24 04          	mov    %eax,0x4(%esp)
80106850:	89 14 24             	mov    %edx,(%esp)
80106853:	e8 d0 f1 ff ff       	call   80105a28 <fetchint>
80106858:	85 c0                	test   %eax,%eax
8010685a:	79 07                	jns    80106863 <sys_exec+0xa9>
      return -1;
8010685c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106861:	eb 68                	jmp    801068cb <sys_exec+0x111>
    if(uarg == 0){
80106863:	8b 85 68 ff ff ff    	mov    -0x98(%ebp),%eax
80106869:	85 c0                	test   %eax,%eax
8010686b:	75 26                	jne    80106893 <sys_exec+0xd9>
      argv[i] = 0;
8010686d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106870:	c7 84 85 70 ff ff ff 	movl   $0x0,-0x90(%ebp,%eax,4)
80106877:	00 00 00 00 
      break;
8010687b:	90                   	nop
    }
    if(fetchstr(uarg, &argv[i]) < 0)
      return -1;
  }
  return exec(path, argv);
8010687c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010687f:	8d 95 70 ff ff ff    	lea    -0x90(%ebp),%edx
80106885:	89 54 24 04          	mov    %edx,0x4(%esp)
80106889:	89 04 24             	mov    %eax,(%esp)
8010688c:	e8 f3 a9 ff ff       	call   80101284 <exec>
80106891:	eb 38                	jmp    801068cb <sys_exec+0x111>
      return -1;
    if(uarg == 0){
      argv[i] = 0;
      break;
    }
    if(fetchstr(uarg, &argv[i]) < 0)
80106893:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106896:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
8010689d:	8d 85 70 ff ff ff    	lea    -0x90(%ebp),%eax
801068a3:	01 d0                	add    %edx,%eax
801068a5:	8b 95 68 ff ff ff    	mov    -0x98(%ebp),%edx
801068ab:	89 44 24 04          	mov    %eax,0x4(%esp)
801068af:	89 14 24             	mov    %edx,(%esp)
801068b2:	e8 ab f1 ff ff       	call   80105a62 <fetchstr>
801068b7:	85 c0                	test   %eax,%eax
801068b9:	79 07                	jns    801068c2 <sys_exec+0x108>
      return -1;
801068bb:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801068c0:	eb 09                	jmp    801068cb <sys_exec+0x111>

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
    return -1;
  }
  memset(argv, 0, sizeof(argv));
  for(i=0;; i++){
801068c2:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      argv[i] = 0;
      break;
    }
    if(fetchstr(uarg, &argv[i]) < 0)
      return -1;
  }
801068c6:	e9 58 ff ff ff       	jmp    80106823 <sys_exec+0x69>
  return exec(path, argv);
}
801068cb:	c9                   	leave  
801068cc:	c3                   	ret    

801068cd <sys_pipe>:

int
sys_pipe(void)
{
801068cd:	55                   	push   %ebp
801068ce:	89 e5                	mov    %esp,%ebp
801068d0:	83 ec 38             	sub    $0x38,%esp
  int *fd;
  struct file *rf, *wf;
  int fd0, fd1;

  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
801068d3:	8d 45 ec             	lea    -0x14(%ebp),%eax
801068d6:	c7 44 24 08 08 00 00 	movl   $0x8,0x8(%esp)
801068dd:	00 
801068de:	89 44 24 04          	mov    %eax,0x4(%esp)
801068e2:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801068e9:	e8 07 f2 ff ff       	call   80105af5 <argptr>
801068ee:	85 c0                	test   %eax,%eax
801068f0:	79 0a                	jns    801068fc <sys_pipe+0x2f>
    return -1;
801068f2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801068f7:	e9 9b 00 00 00       	jmp    80106997 <sys_pipe+0xca>
  if(pipealloc(&rf, &wf) < 0)
801068fc:	8d 45 e4             	lea    -0x1c(%ebp),%eax
801068ff:	89 44 24 04          	mov    %eax,0x4(%esp)
80106903:	8d 45 e8             	lea    -0x18(%ebp),%eax
80106906:	89 04 24             	mov    %eax,(%esp)
80106909:	e8 06 dd ff ff       	call   80104614 <pipealloc>
8010690e:	85 c0                	test   %eax,%eax
80106910:	79 07                	jns    80106919 <sys_pipe+0x4c>
    return -1;
80106912:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106917:	eb 7e                	jmp    80106997 <sys_pipe+0xca>
  fd0 = -1;
80106919:	c7 45 f0 ff ff ff ff 	movl   $0xffffffff,-0x10(%ebp)
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
80106920:	8b 45 e8             	mov    -0x18(%ebp),%eax
80106923:	89 04 24             	mov    %eax,(%esp)
80106926:	e8 6a f3 ff ff       	call   80105c95 <fdalloc>
8010692b:	89 45 f0             	mov    %eax,-0x10(%ebp)
8010692e:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106932:	78 14                	js     80106948 <sys_pipe+0x7b>
80106934:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106937:	89 04 24             	mov    %eax,(%esp)
8010693a:	e8 56 f3 ff ff       	call   80105c95 <fdalloc>
8010693f:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106942:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106946:	79 37                	jns    8010697f <sys_pipe+0xb2>
    if(fd0 >= 0)
80106948:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010694c:	78 14                	js     80106962 <sys_pipe+0x95>
      proc->ofile[fd0] = 0;
8010694e:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106954:	8b 55 f0             	mov    -0x10(%ebp),%edx
80106957:	83 c2 08             	add    $0x8,%edx
8010695a:	c7 44 90 08 00 00 00 	movl   $0x0,0x8(%eax,%edx,4)
80106961:	00 
    fileclose(rf);
80106962:	8b 45 e8             	mov    -0x18(%ebp),%eax
80106965:	89 04 24             	mov    %eax,(%esp)
80106968:	e8 f4 ad ff ff       	call   80101761 <fileclose>
    fileclose(wf);
8010696d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106970:	89 04 24             	mov    %eax,(%esp)
80106973:	e8 e9 ad ff ff       	call   80101761 <fileclose>
    return -1;
80106978:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010697d:	eb 18                	jmp    80106997 <sys_pipe+0xca>
  }
  fd[0] = fd0;
8010697f:	8b 45 ec             	mov    -0x14(%ebp),%eax
80106982:	8b 55 f0             	mov    -0x10(%ebp),%edx
80106985:	89 10                	mov    %edx,(%eax)
  fd[1] = fd1;
80106987:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010698a:	8d 50 04             	lea    0x4(%eax),%edx
8010698d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106990:	89 02                	mov    %eax,(%edx)
  return 0;
80106992:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106997:	c9                   	leave  
80106998:	c3                   	ret    
80106999:	00 00                	add    %al,(%eax)
	...

8010699c <sys_fork>:
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
8010699c:	55                   	push   %ebp
8010699d:	89 e5                	mov    %esp,%ebp
8010699f:	83 ec 08             	sub    $0x8,%esp
  return fork();
801069a2:	e8 1c e3 ff ff       	call   80104cc3 <fork>
}
801069a7:	c9                   	leave  
801069a8:	c3                   	ret    

801069a9 <sys_exit>:

int
sys_exit(void)
{
801069a9:	55                   	push   %ebp
801069aa:	89 e5                	mov    %esp,%ebp
801069ac:	83 ec 08             	sub    $0x8,%esp
  exit();
801069af:	e8 8a e4 ff ff       	call   80104e3e <exit>
  return 0;  // not reached
801069b4:	b8 00 00 00 00       	mov    $0x0,%eax
}
801069b9:	c9                   	leave  
801069ba:	c3                   	ret    

801069bb <sys_wait>:

int
sys_wait(void)
{
801069bb:	55                   	push   %ebp
801069bc:	89 e5                	mov    %esp,%ebp
801069be:	83 ec 08             	sub    $0x8,%esp
  return wait();
801069c1:	e8 9b e5 ff ff       	call   80104f61 <wait>
}
801069c6:	c9                   	leave  
801069c7:	c3                   	ret    

801069c8 <sys_kill>:

int
sys_kill(void)
{
801069c8:	55                   	push   %ebp
801069c9:	89 e5                	mov    %esp,%ebp
801069cb:	83 ec 28             	sub    $0x28,%esp
  int pid;

  if(argint(0, &pid) < 0)
801069ce:	8d 45 f4             	lea    -0xc(%ebp),%eax
801069d1:	89 44 24 04          	mov    %eax,0x4(%esp)
801069d5:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801069dc:	e8 e5 f0 ff ff       	call   80105ac6 <argint>
801069e1:	85 c0                	test   %eax,%eax
801069e3:	79 07                	jns    801069ec <sys_kill+0x24>
    return -1;
801069e5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801069ea:	eb 0b                	jmp    801069f7 <sys_kill+0x2f>
  return kill(pid);
801069ec:	8b 45 f4             	mov    -0xc(%ebp),%eax
801069ef:	89 04 24             	mov    %eax,(%esp)
801069f2:	e8 3c e9 ff ff       	call   80105333 <kill>
}
801069f7:	c9                   	leave  
801069f8:	c3                   	ret    

801069f9 <sys_getpid>:

int
sys_getpid(void)
{
801069f9:	55                   	push   %ebp
801069fa:	89 e5                	mov    %esp,%ebp
  return proc->pid;
801069fc:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106a02:	8b 40 10             	mov    0x10(%eax),%eax
}
80106a05:	5d                   	pop    %ebp
80106a06:	c3                   	ret    

80106a07 <sys_sbrk>:

int
sys_sbrk(void)
{
80106a07:	55                   	push   %ebp
80106a08:	89 e5                	mov    %esp,%ebp
80106a0a:	83 ec 28             	sub    $0x28,%esp
  int addr;
  int n;

  if(argint(0, &n) < 0)
80106a0d:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106a10:	89 44 24 04          	mov    %eax,0x4(%esp)
80106a14:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106a1b:	e8 a6 f0 ff ff       	call   80105ac6 <argint>
80106a20:	85 c0                	test   %eax,%eax
80106a22:	79 07                	jns    80106a2b <sys_sbrk+0x24>
    return -1;
80106a24:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106a29:	eb 24                	jmp    80106a4f <sys_sbrk+0x48>
  addr = proc->sz;
80106a2b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106a31:	8b 00                	mov    (%eax),%eax
80106a33:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(growproc(n) < 0)
80106a36:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106a39:	89 04 24             	mov    %eax,(%esp)
80106a3c:	e8 dd e1 ff ff       	call   80104c1e <growproc>
80106a41:	85 c0                	test   %eax,%eax
80106a43:	79 07                	jns    80106a4c <sys_sbrk+0x45>
    return -1;
80106a45:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106a4a:	eb 03                	jmp    80106a4f <sys_sbrk+0x48>
  return addr;
80106a4c:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
80106a4f:	c9                   	leave  
80106a50:	c3                   	ret    

80106a51 <sys_sleep>:

int
sys_sleep(void)
{
80106a51:	55                   	push   %ebp
80106a52:	89 e5                	mov    %esp,%ebp
80106a54:	83 ec 28             	sub    $0x28,%esp
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
80106a57:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106a5a:	89 44 24 04          	mov    %eax,0x4(%esp)
80106a5e:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106a65:	e8 5c f0 ff ff       	call   80105ac6 <argint>
80106a6a:	85 c0                	test   %eax,%eax
80106a6c:	79 07                	jns    80106a75 <sys_sleep+0x24>
    return -1;
80106a6e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106a73:	eb 6c                	jmp    80106ae1 <sys_sleep+0x90>
  acquire(&tickslock);
80106a75:	c7 04 24 c0 60 11 80 	movl   $0x801160c0,(%esp)
80106a7c:	e8 86 ea ff ff       	call   80105507 <acquire>
  ticks0 = ticks;
80106a81:	a1 f4 60 11 80       	mov    0x801160f4,%eax
80106a86:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while(ticks - ticks0 < n){
80106a89:	eb 34                	jmp    80106abf <sys_sleep+0x6e>
    if(proc->killed){
80106a8b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106a91:	8b 40 24             	mov    0x24(%eax),%eax
80106a94:	85 c0                	test   %eax,%eax
80106a96:	74 13                	je     80106aab <sys_sleep+0x5a>
      release(&tickslock);
80106a98:	c7 04 24 c0 60 11 80 	movl   $0x801160c0,(%esp)
80106a9f:	e8 cc ea ff ff       	call   80105570 <release>
      return -1;
80106aa4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106aa9:	eb 36                	jmp    80106ae1 <sys_sleep+0x90>
    }
    sleep(&ticks, &tickslock);
80106aab:	c7 44 24 04 c0 60 11 	movl   $0x801160c0,0x4(%esp)
80106ab2:	80 
80106ab3:	c7 04 24 f4 60 11 80 	movl   $0x801160f4,(%esp)
80106aba:	e8 6f e7 ff ff       	call   8010522e <sleep>

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
80106abf:	a1 f4 60 11 80       	mov    0x801160f4,%eax
80106ac4:	89 c2                	mov    %eax,%edx
80106ac6:	2b 55 f4             	sub    -0xc(%ebp),%edx
80106ac9:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106acc:	39 c2                	cmp    %eax,%edx
80106ace:	72 bb                	jb     80106a8b <sys_sleep+0x3a>
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
80106ad0:	c7 04 24 c0 60 11 80 	movl   $0x801160c0,(%esp)
80106ad7:	e8 94 ea ff ff       	call   80105570 <release>
  return 0;
80106adc:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106ae1:	c9                   	leave  
80106ae2:	c3                   	ret    

80106ae3 <sys_uptime>:

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
80106ae3:	55                   	push   %ebp
80106ae4:	89 e5                	mov    %esp,%ebp
80106ae6:	83 ec 28             	sub    $0x28,%esp
  uint xticks;

  acquire(&tickslock);
80106ae9:	c7 04 24 c0 60 11 80 	movl   $0x801160c0,(%esp)
80106af0:	e8 12 ea ff ff       	call   80105507 <acquire>
  xticks = ticks;
80106af5:	a1 f4 60 11 80       	mov    0x801160f4,%eax
80106afa:	89 45 f4             	mov    %eax,-0xc(%ebp)
  release(&tickslock);
80106afd:	c7 04 24 c0 60 11 80 	movl   $0x801160c0,(%esp)
80106b04:	e8 67 ea ff ff       	call   80105570 <release>
  return xticks;
80106b09:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
80106b0c:	c9                   	leave  
80106b0d:	c3                   	ret    
	...

80106b10 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80106b10:	55                   	push   %ebp
80106b11:	89 e5                	mov    %esp,%ebp
80106b13:	83 ec 08             	sub    $0x8,%esp
80106b16:	8b 55 08             	mov    0x8(%ebp),%edx
80106b19:	8b 45 0c             	mov    0xc(%ebp),%eax
80106b1c:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80106b20:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80106b23:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80106b27:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80106b2b:	ee                   	out    %al,(%dx)
}
80106b2c:	c9                   	leave  
80106b2d:	c3                   	ret    

80106b2e <timerinit>:
#define TIMER_RATEGEN   0x04    // mode 2, rate generator
#define TIMER_16BIT     0x30    // r/w counter 16 bits, LSB first

void
timerinit(void)
{
80106b2e:	55                   	push   %ebp
80106b2f:	89 e5                	mov    %esp,%ebp
80106b31:	83 ec 18             	sub    $0x18,%esp
  // Interrupt 100 times/sec.
  outb(TIMER_MODE, TIMER_SEL0 | TIMER_RATEGEN | TIMER_16BIT);
80106b34:	c7 44 24 04 34 00 00 	movl   $0x34,0x4(%esp)
80106b3b:	00 
80106b3c:	c7 04 24 43 00 00 00 	movl   $0x43,(%esp)
80106b43:	e8 c8 ff ff ff       	call   80106b10 <outb>
  outb(IO_TIMER1, TIMER_DIV(100) % 256);
80106b48:	c7 44 24 04 9c 00 00 	movl   $0x9c,0x4(%esp)
80106b4f:	00 
80106b50:	c7 04 24 40 00 00 00 	movl   $0x40,(%esp)
80106b57:	e8 b4 ff ff ff       	call   80106b10 <outb>
  outb(IO_TIMER1, TIMER_DIV(100) / 256);
80106b5c:	c7 44 24 04 2e 00 00 	movl   $0x2e,0x4(%esp)
80106b63:	00 
80106b64:	c7 04 24 40 00 00 00 	movl   $0x40,(%esp)
80106b6b:	e8 a0 ff ff ff       	call   80106b10 <outb>
  picenable(IRQ_TIMER);
80106b70:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106b77:	e8 21 d9 ff ff       	call   8010449d <picenable>
}
80106b7c:	c9                   	leave  
80106b7d:	c3                   	ret    
	...

80106b80 <alltraps>:

  # vectors.S sends all traps here.
.globl alltraps
alltraps:
  # Build trap frame.
  pushl %ds
80106b80:	1e                   	push   %ds
  pushl %es
80106b81:	06                   	push   %es
  pushl %fs
80106b82:	0f a0                	push   %fs
  pushl %gs
80106b84:	0f a8                	push   %gs
  pushal
80106b86:	60                   	pusha  
  
  # Set up data and per-cpu segments.
  movw $(SEG_KDATA<<3), %ax
80106b87:	66 b8 10 00          	mov    $0x10,%ax
  movw %ax, %ds
80106b8b:	8e d8                	mov    %eax,%ds
  movw %ax, %es
80106b8d:	8e c0                	mov    %eax,%es
  movw $(SEG_KCPU<<3), %ax
80106b8f:	66 b8 18 00          	mov    $0x18,%ax
  movw %ax, %fs
80106b93:	8e e0                	mov    %eax,%fs
  movw %ax, %gs
80106b95:	8e e8                	mov    %eax,%gs

  # Call trap(tf), where tf=%esp
  pushl %esp
80106b97:	54                   	push   %esp
  call trap
80106b98:	e8 ed 01 00 00       	call   80106d8a <trap>
  addl $4, %esp
80106b9d:	83 c4 04             	add    $0x4,%esp

80106ba0 <trapret>:

  # Return falls through to trapret...
.globl trapret
trapret:
  popal
80106ba0:	61                   	popa   
  popl %gs
80106ba1:	0f a9                	pop    %gs
  popl %fs
80106ba3:	0f a1                	pop    %fs
  popl %es
80106ba5:	07                   	pop    %es
  popl %ds
80106ba6:	1f                   	pop    %ds
  addl $0x8, %esp  # trapno and errcode
80106ba7:	83 c4 08             	add    $0x8,%esp
  iret
80106baa:	cf                   	iret   
	...

80106bac <lidt>:

struct gatedesc;

static inline void
lidt(struct gatedesc *p, int size)
{
80106bac:	55                   	push   %ebp
80106bad:	89 e5                	mov    %esp,%ebp
80106baf:	83 ec 10             	sub    $0x10,%esp
  volatile ushort pd[3];

  pd[0] = size-1;
80106bb2:	8b 45 0c             	mov    0xc(%ebp),%eax
80106bb5:	83 e8 01             	sub    $0x1,%eax
80106bb8:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
  pd[1] = (uint)p;
80106bbc:	8b 45 08             	mov    0x8(%ebp),%eax
80106bbf:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
80106bc3:	8b 45 08             	mov    0x8(%ebp),%eax
80106bc6:	c1 e8 10             	shr    $0x10,%eax
80106bc9:	66 89 45 fe          	mov    %ax,-0x2(%ebp)

  asm volatile("lidt (%0)" : : "r" (pd));
80106bcd:	8d 45 fa             	lea    -0x6(%ebp),%eax
80106bd0:	0f 01 18             	lidtl  (%eax)
}
80106bd3:	c9                   	leave  
80106bd4:	c3                   	ret    

80106bd5 <sti>:
  asm volatile("cli");
}

static inline void
sti(void)
{
80106bd5:	55                   	push   %ebp
80106bd6:	89 e5                	mov    %esp,%ebp
  asm volatile("sti");
80106bd8:	fb                   	sti    
}
80106bd9:	5d                   	pop    %ebp
80106bda:	c3                   	ret    

80106bdb <rcr2>:
  return result;
}

static inline uint
rcr2(void)
{
80106bdb:	55                   	push   %ebp
80106bdc:	89 e5                	mov    %esp,%ebp
80106bde:	83 ec 10             	sub    $0x10,%esp
  uint val;
  asm volatile("movl %%cr2,%0" : "=r" (val));
80106be1:	0f 20 d0             	mov    %cr2,%eax
80106be4:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return val;
80106be7:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80106bea:	c9                   	leave  
80106beb:	c3                   	ret    

80106bec <tvinit>:
struct spinlock tickslock;
uint ticks;

void
tvinit(void)
{
80106bec:	55                   	push   %ebp
80106bed:	89 e5                	mov    %esp,%ebp
80106bef:	83 ec 28             	sub    $0x28,%esp
  int i;

  for(i = 0; i < 256; i++)
80106bf2:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80106bf9:	e9 bf 00 00 00       	jmp    80106cbd <tvinit+0xd1>
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
80106bfe:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106c01:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106c04:	8b 14 95 98 c0 10 80 	mov    -0x7fef3f68(,%edx,4),%edx
80106c0b:	66 89 14 c5 60 c6 10 	mov    %dx,-0x7fef39a0(,%eax,8)
80106c12:	80 
80106c13:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106c16:	66 c7 04 c5 62 c6 10 	movw   $0x8,-0x7fef399e(,%eax,8)
80106c1d:	80 08 00 
80106c20:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106c23:	0f b6 14 c5 64 c6 10 	movzbl -0x7fef399c(,%eax,8),%edx
80106c2a:	80 
80106c2b:	83 e2 e0             	and    $0xffffffe0,%edx
80106c2e:	88 14 c5 64 c6 10 80 	mov    %dl,-0x7fef399c(,%eax,8)
80106c35:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106c38:	0f b6 14 c5 64 c6 10 	movzbl -0x7fef399c(,%eax,8),%edx
80106c3f:	80 
80106c40:	83 e2 1f             	and    $0x1f,%edx
80106c43:	88 14 c5 64 c6 10 80 	mov    %dl,-0x7fef399c(,%eax,8)
80106c4a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106c4d:	0f b6 14 c5 65 c6 10 	movzbl -0x7fef399b(,%eax,8),%edx
80106c54:	80 
80106c55:	83 e2 f0             	and    $0xfffffff0,%edx
80106c58:	83 ca 0e             	or     $0xe,%edx
80106c5b:	88 14 c5 65 c6 10 80 	mov    %dl,-0x7fef399b(,%eax,8)
80106c62:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106c65:	0f b6 14 c5 65 c6 10 	movzbl -0x7fef399b(,%eax,8),%edx
80106c6c:	80 
80106c6d:	83 e2 ef             	and    $0xffffffef,%edx
80106c70:	88 14 c5 65 c6 10 80 	mov    %dl,-0x7fef399b(,%eax,8)
80106c77:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106c7a:	0f b6 14 c5 65 c6 10 	movzbl -0x7fef399b(,%eax,8),%edx
80106c81:	80 
80106c82:	83 e2 9f             	and    $0xffffff9f,%edx
80106c85:	88 14 c5 65 c6 10 80 	mov    %dl,-0x7fef399b(,%eax,8)
80106c8c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106c8f:	0f b6 14 c5 65 c6 10 	movzbl -0x7fef399b(,%eax,8),%edx
80106c96:	80 
80106c97:	83 ca 80             	or     $0xffffff80,%edx
80106c9a:	88 14 c5 65 c6 10 80 	mov    %dl,-0x7fef399b(,%eax,8)
80106ca1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106ca4:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106ca7:	8b 14 95 98 c0 10 80 	mov    -0x7fef3f68(,%edx,4),%edx
80106cae:	c1 ea 10             	shr    $0x10,%edx
80106cb1:	66 89 14 c5 66 c6 10 	mov    %dx,-0x7fef399a(,%eax,8)
80106cb8:	80 
void
tvinit(void)
{
  int i;

  for(i = 0; i < 256; i++)
80106cb9:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80106cbd:	81 7d f4 ff 00 00 00 	cmpl   $0xff,-0xc(%ebp)
80106cc4:	0f 8e 34 ff ff ff    	jle    80106bfe <tvinit+0x12>
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
80106cca:	a1 98 c1 10 80       	mov    0x8010c198,%eax
80106ccf:	66 a3 60 c8 10 80    	mov    %ax,0x8010c860
80106cd5:	66 c7 05 62 c8 10 80 	movw   $0x8,0x8010c862
80106cdc:	08 00 
80106cde:	0f b6 05 64 c8 10 80 	movzbl 0x8010c864,%eax
80106ce5:	83 e0 e0             	and    $0xffffffe0,%eax
80106ce8:	a2 64 c8 10 80       	mov    %al,0x8010c864
80106ced:	0f b6 05 64 c8 10 80 	movzbl 0x8010c864,%eax
80106cf4:	83 e0 1f             	and    $0x1f,%eax
80106cf7:	a2 64 c8 10 80       	mov    %al,0x8010c864
80106cfc:	0f b6 05 65 c8 10 80 	movzbl 0x8010c865,%eax
80106d03:	83 c8 0f             	or     $0xf,%eax
80106d06:	a2 65 c8 10 80       	mov    %al,0x8010c865
80106d0b:	0f b6 05 65 c8 10 80 	movzbl 0x8010c865,%eax
80106d12:	83 e0 ef             	and    $0xffffffef,%eax
80106d15:	a2 65 c8 10 80       	mov    %al,0x8010c865
80106d1a:	0f b6 05 65 c8 10 80 	movzbl 0x8010c865,%eax
80106d21:	83 c8 60             	or     $0x60,%eax
80106d24:	a2 65 c8 10 80       	mov    %al,0x8010c865
80106d29:	0f b6 05 65 c8 10 80 	movzbl 0x8010c865,%eax
80106d30:	83 c8 80             	or     $0xffffff80,%eax
80106d33:	a2 65 c8 10 80       	mov    %al,0x8010c865
80106d38:	a1 98 c1 10 80       	mov    0x8010c198,%eax
80106d3d:	c1 e8 10             	shr    $0x10,%eax
80106d40:	66 a3 66 c8 10 80    	mov    %ax,0x8010c866

  initlock(&tickslock, "time");
80106d46:	c7 44 24 04 90 91 10 	movl   $0x80109190,0x4(%esp)
80106d4d:	80 
80106d4e:	c7 04 24 c0 60 11 80 	movl   $0x801160c0,(%esp)
80106d55:	e8 8c e7 ff ff       	call   801054e6 <initlock>
}
80106d5a:	c9                   	leave  
80106d5b:	c3                   	ret    

80106d5c <idtinit>:

void
idtinit(void)
{
80106d5c:	55                   	push   %ebp
80106d5d:	89 e5                	mov    %esp,%ebp
80106d5f:	83 ec 08             	sub    $0x8,%esp
  lidt(idt, sizeof(idt));
80106d62:	c7 44 24 04 00 08 00 	movl   $0x800,0x4(%esp)
80106d69:	00 
80106d6a:	c7 04 24 60 c6 10 80 	movl   $0x8010c660,(%esp)
80106d71:	e8 36 fe ff ff       	call   80106bac <lidt>
}
80106d76:	c9                   	leave  
80106d77:	c3                   	ret    

80106d78 <trap_exit>:

//PAGEBREAK: 41
// This is here so you can break on it in gdb
static void
trap_exit()
{
80106d78:	55                   	push   %ebp
80106d79:	89 e5                	mov    %esp,%ebp
80106d7b:	83 ec 08             	sub    $0x8,%esp
  sti();
80106d7e:	e8 52 fe ff ff       	call   80106bd5 <sti>
  exit();
80106d83:	e8 b6 e0 ff ff       	call   80104e3e <exit>
}
80106d88:	c9                   	leave  
80106d89:	c3                   	ret    

80106d8a <trap>:
void
trap(struct trapframe *tf)
{
80106d8a:	55                   	push   %ebp
80106d8b:	89 e5                	mov    %esp,%ebp
80106d8d:	57                   	push   %edi
80106d8e:	56                   	push   %esi
80106d8f:	53                   	push   %ebx
80106d90:	83 ec 3c             	sub    $0x3c,%esp
  if(tf->trapno == T_SYSCALL){
80106d93:	8b 45 08             	mov    0x8(%ebp),%eax
80106d96:	8b 40 30             	mov    0x30(%eax),%eax
80106d99:	83 f8 40             	cmp    $0x40,%eax
80106d9c:	75 3e                	jne    80106ddc <trap+0x52>
    if(proc->killed)
80106d9e:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106da4:	8b 40 24             	mov    0x24(%eax),%eax
80106da7:	85 c0                	test   %eax,%eax
80106da9:	74 05                	je     80106db0 <trap+0x26>
      exit();
80106dab:	e8 8e e0 ff ff       	call   80104e3e <exit>
    proc->tf = tf;
80106db0:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106db6:	8b 55 08             	mov    0x8(%ebp),%edx
80106db9:	89 50 18             	mov    %edx,0x18(%eax)
    syscall();
80106dbc:	e8 cd ed ff ff       	call   80105b8e <syscall>
    if(proc->killed)
80106dc1:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106dc7:	8b 40 24             	mov    0x24(%eax),%eax
80106dca:	85 c0                	test   %eax,%eax
80106dcc:	0f 84 34 02 00 00    	je     80107006 <trap+0x27c>
      exit();
80106dd2:	e8 67 e0 ff ff       	call   80104e3e <exit>
    return;
80106dd7:	e9 2b 02 00 00       	jmp    80107007 <trap+0x27d>
  }

  switch(tf->trapno){
80106ddc:	8b 45 08             	mov    0x8(%ebp),%eax
80106ddf:	8b 40 30             	mov    0x30(%eax),%eax
80106de2:	83 e8 20             	sub    $0x20,%eax
80106de5:	83 f8 1f             	cmp    $0x1f,%eax
80106de8:	0f 87 bc 00 00 00    	ja     80106eaa <trap+0x120>
80106dee:	8b 04 85 38 92 10 80 	mov    -0x7fef6dc8(,%eax,4),%eax
80106df5:	ff e0                	jmp    *%eax
  case T_IRQ0 + IRQ_TIMER:
    if(cpu->id == 0){
80106df7:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80106dfd:	0f b6 00             	movzbl (%eax),%eax
80106e00:	84 c0                	test   %al,%al
80106e02:	75 31                	jne    80106e35 <trap+0xab>
      acquire(&tickslock);
80106e04:	c7 04 24 c0 60 11 80 	movl   $0x801160c0,(%esp)
80106e0b:	e8 f7 e6 ff ff       	call   80105507 <acquire>
      ticks++;
80106e10:	a1 f4 60 11 80       	mov    0x801160f4,%eax
80106e15:	83 c0 01             	add    $0x1,%eax
80106e18:	a3 f4 60 11 80       	mov    %eax,0x801160f4
      wakeup(&ticks);
80106e1d:	c7 04 24 f4 60 11 80 	movl   $0x801160f4,(%esp)
80106e24:	e8 df e4 ff ff       	call   80105308 <wakeup>
      release(&tickslock);
80106e29:	c7 04 24 c0 60 11 80 	movl   $0x801160c0,(%esp)
80106e30:	e8 3b e7 ff ff       	call   80105570 <release>
    }
    lapiceoi();
80106e35:	e8 1c c9 ff ff       	call   80103756 <lapiceoi>
    break;
80106e3a:	e9 41 01 00 00       	jmp    80106f80 <trap+0x1f6>
  case T_IRQ0 + IRQ_IDE:
    ideintr();
80106e3f:	e8 09 c1 ff ff       	call   80102f4d <ideintr>
    lapiceoi();
80106e44:	e8 0d c9 ff ff       	call   80103756 <lapiceoi>
    break;
80106e49:	e9 32 01 00 00       	jmp    80106f80 <trap+0x1f6>
  case T_IRQ0 + IRQ_IDE+1:
    // Bochs generates spurious IDE1 interrupts.
    break;
  case T_IRQ0 + IRQ_KBD:
    kbdintr();
80106e4e:	e8 04 c7 ff ff       	call   80103557 <kbdintr>
    lapiceoi();
80106e53:	e8 fe c8 ff ff       	call   80103756 <lapiceoi>
    break;
80106e58:	e9 23 01 00 00       	jmp    80106f80 <trap+0x1f6>
  case T_IRQ0 + IRQ_COM1:
    uartintr();
80106e5d:	e8 b7 03 00 00       	call   80107219 <uartintr>
    lapiceoi();
80106e62:	e8 ef c8 ff ff       	call   80103756 <lapiceoi>
    break;
80106e67:	e9 14 01 00 00       	jmp    80106f80 <trap+0x1f6>
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
80106e6c:	8b 45 08             	mov    0x8(%ebp),%eax
80106e6f:	8b 48 38             	mov    0x38(%eax),%ecx
            cpu->id, tf->cs, tf->eip);
80106e72:	8b 45 08             	mov    0x8(%ebp),%eax
80106e75:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
    uartintr();
    lapiceoi();
    break;
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
80106e79:	0f b7 d0             	movzwl %ax,%edx
            cpu->id, tf->cs, tf->eip);
80106e7c:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80106e82:	0f b6 00             	movzbl (%eax),%eax
    uartintr();
    lapiceoi();
    break;
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
80106e85:	0f b6 c0             	movzbl %al,%eax
80106e88:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
80106e8c:	89 54 24 08          	mov    %edx,0x8(%esp)
80106e90:	89 44 24 04          	mov    %eax,0x4(%esp)
80106e94:	c7 04 24 98 91 10 80 	movl   $0x80109198,(%esp)
80106e9b:	e8 29 98 ff ff       	call   801006c9 <cprintf>
            cpu->id, tf->cs, tf->eip);
    lapiceoi();
80106ea0:	e8 b1 c8 ff ff       	call   80103756 <lapiceoi>
    break;
80106ea5:	e9 d6 00 00 00       	jmp    80106f80 <trap+0x1f6>

  //PAGEBREAK: 13
  default:
    if(proc == 0 || (tf->cs&3) == 0){
80106eaa:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106eb0:	85 c0                	test   %eax,%eax
80106eb2:	74 11                	je     80106ec5 <trap+0x13b>
80106eb4:	8b 45 08             	mov    0x8(%ebp),%eax
80106eb7:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
80106ebb:	0f b7 c0             	movzwl %ax,%eax
80106ebe:	83 e0 03             	and    $0x3,%eax
80106ec1:	85 c0                	test   %eax,%eax
80106ec3:	75 46                	jne    80106f0b <trap+0x181>
      // In kernel, it must be our mistake.
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
80106ec5:	e8 11 fd ff ff       	call   80106bdb <rcr2>
80106eca:	8b 55 08             	mov    0x8(%ebp),%edx
80106ecd:	8b 5a 38             	mov    0x38(%edx),%ebx
              tf->trapno, cpu->id, tf->eip, rcr2());
80106ed0:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80106ed7:	0f b6 12             	movzbl (%edx),%edx

  //PAGEBREAK: 13
  default:
    if(proc == 0 || (tf->cs&3) == 0){
      // In kernel, it must be our mistake.
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
80106eda:	0f b6 ca             	movzbl %dl,%ecx
80106edd:	8b 55 08             	mov    0x8(%ebp),%edx
80106ee0:	8b 52 30             	mov    0x30(%edx),%edx
80106ee3:	89 44 24 10          	mov    %eax,0x10(%esp)
80106ee7:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
80106eeb:	89 4c 24 08          	mov    %ecx,0x8(%esp)
80106eef:	89 54 24 04          	mov    %edx,0x4(%esp)
80106ef3:	c7 04 24 bc 91 10 80 	movl   $0x801091bc,(%esp)
80106efa:	e8 ca 97 ff ff       	call   801006c9 <cprintf>
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
80106eff:	c7 04 24 ee 91 10 80 	movl   $0x801091ee,(%esp)
80106f06:	e8 5e 99 ff ff       	call   80100869 <panic>
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80106f0b:	e8 cb fc ff ff       	call   80106bdb <rcr2>
80106f10:	89 c2                	mov    %eax,%edx
80106f12:	8b 45 08             	mov    0x8(%ebp),%eax
80106f15:	8b 78 38             	mov    0x38(%eax),%edi
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip,
80106f18:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80106f1e:	0f b6 00             	movzbl (%eax),%eax
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80106f21:	0f b6 f0             	movzbl %al,%esi
80106f24:	8b 45 08             	mov    0x8(%ebp),%eax
80106f27:	8b 58 34             	mov    0x34(%eax),%ebx
80106f2a:	8b 45 08             	mov    0x8(%ebp),%eax
80106f2d:	8b 48 30             	mov    0x30(%eax),%ecx
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip,
80106f30:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80106f36:	83 c0 6c             	add    $0x6c,%eax
80106f39:	89 45 e4             	mov    %eax,-0x1c(%ebp)
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip,
80106f3c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80106f42:	8b 40 10             	mov    0x10(%eax),%eax
80106f45:	89 54 24 1c          	mov    %edx,0x1c(%esp)
80106f49:	89 7c 24 18          	mov    %edi,0x18(%esp)
80106f4d:	89 74 24 14          	mov    %esi,0x14(%esp)
80106f51:	89 5c 24 10          	mov    %ebx,0x10(%esp)
80106f55:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
80106f59:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80106f5c:	89 54 24 08          	mov    %edx,0x8(%esp)
80106f60:	89 44 24 04          	mov    %eax,0x4(%esp)
80106f64:	c7 04 24 f4 91 10 80 	movl   $0x801091f4,(%esp)
80106f6b:	e8 59 97 ff ff       	call   801006c9 <cprintf>
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip,
            rcr2());
    proc->killed = 1;
80106f70:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106f76:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
80106f7d:	eb 01                	jmp    80106f80 <trap+0x1f6>
    ideintr();
    lapiceoi();
    break;
  case T_IRQ0 + IRQ_IDE+1:
    // Bochs generates spurious IDE1 interrupts.
    break;
80106f7f:	90                   	nop
  }

  // Force process exit if it has been killed and is in user space.
  // (If it is still executing in the kernel, let it keep running
  // until it gets to the regular system call return.)
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
80106f80:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106f86:	85 c0                	test   %eax,%eax
80106f88:	74 24                	je     80106fae <trap+0x224>
80106f8a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106f90:	8b 40 24             	mov    0x24(%eax),%eax
80106f93:	85 c0                	test   %eax,%eax
80106f95:	74 17                	je     80106fae <trap+0x224>
80106f97:	8b 45 08             	mov    0x8(%ebp),%eax
80106f9a:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
80106f9e:	0f b7 c0             	movzwl %ax,%eax
80106fa1:	83 e0 03             	and    $0x3,%eax
80106fa4:	83 f8 03             	cmp    $0x3,%eax
80106fa7:	75 05                	jne    80106fae <trap+0x224>
    trap_exit();
80106fa9:	e8 ca fd ff ff       	call   80106d78 <trap_exit>

  // Force process to give up CPU on clock tick.
  // If interrupts were on while locks held, would need to check nlock.
  if(proc && proc->state == RUNNING && tf->trapno == T_IRQ0+IRQ_TIMER)
80106fae:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106fb4:	85 c0                	test   %eax,%eax
80106fb6:	74 1e                	je     80106fd6 <trap+0x24c>
80106fb8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106fbe:	8b 40 0c             	mov    0xc(%eax),%eax
80106fc1:	83 f8 04             	cmp    $0x4,%eax
80106fc4:	75 10                	jne    80106fd6 <trap+0x24c>
80106fc6:	8b 45 08             	mov    0x8(%ebp),%eax
80106fc9:	8b 40 30             	mov    0x30(%eax),%eax
80106fcc:	83 f8 20             	cmp    $0x20,%eax
80106fcf:	75 05                	jne    80106fd6 <trap+0x24c>
    yield();
80106fd1:	e8 e7 e1 ff ff       	call   801051bd <yield>

  // Check if the process has been killed since we yielded
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
80106fd6:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106fdc:	85 c0                	test   %eax,%eax
80106fde:	74 27                	je     80107007 <trap+0x27d>
80106fe0:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106fe6:	8b 40 24             	mov    0x24(%eax),%eax
80106fe9:	85 c0                	test   %eax,%eax
80106feb:	74 1a                	je     80107007 <trap+0x27d>
80106fed:	8b 45 08             	mov    0x8(%ebp),%eax
80106ff0:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
80106ff4:	0f b7 c0             	movzwl %ax,%eax
80106ff7:	83 e0 03             	and    $0x3,%eax
80106ffa:	83 f8 03             	cmp    $0x3,%eax
80106ffd:	75 08                	jne    80107007 <trap+0x27d>
    trap_exit();
80106fff:	e8 74 fd ff ff       	call   80106d78 <trap_exit>
80107004:	eb 01                	jmp    80107007 <trap+0x27d>
      exit();
    proc->tf = tf;
    syscall();
    if(proc->killed)
      exit();
    return;
80107006:	90                   	nop
    yield();

  // Check if the process has been killed since we yielded
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
    trap_exit();
}
80107007:	83 c4 3c             	add    $0x3c,%esp
8010700a:	5b                   	pop    %ebx
8010700b:	5e                   	pop    %esi
8010700c:	5f                   	pop    %edi
8010700d:	5d                   	pop    %ebp
8010700e:	c3                   	ret    
	...

80107010 <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
80107010:	55                   	push   %ebp
80107011:	89 e5                	mov    %esp,%ebp
80107013:	83 ec 14             	sub    $0x14,%esp
80107016:	8b 45 08             	mov    0x8(%ebp),%eax
80107019:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010701d:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
80107021:	89 c2                	mov    %eax,%edx
80107023:	ec                   	in     (%dx),%al
80107024:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80107027:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
8010702b:	c9                   	leave  
8010702c:	c3                   	ret    

8010702d <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
8010702d:	55                   	push   %ebp
8010702e:	89 e5                	mov    %esp,%ebp
80107030:	83 ec 08             	sub    $0x8,%esp
80107033:	8b 55 08             	mov    0x8(%ebp),%edx
80107036:	8b 45 0c             	mov    0xc(%ebp),%eax
80107039:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
8010703d:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80107040:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80107044:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80107048:	ee                   	out    %al,(%dx)
}
80107049:	c9                   	leave  
8010704a:	c3                   	ret    

8010704b <uartinit>:

static int uart;    // is there a uart?

void
uartinit(void)
{
8010704b:	55                   	push   %ebp
8010704c:	89 e5                	mov    %esp,%ebp
8010704e:	83 ec 28             	sub    $0x28,%esp
  char *p;

  // Turn off the FIFO
  outb(COM1+UART_FIFO_CONTROL, 0);
80107051:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80107058:	00 
80107059:	c7 04 24 fa 03 00 00 	movl   $0x3fa,(%esp)
80107060:	e8 c8 ff ff ff       	call   8010702d <outb>

  // 9600 baud, 8 data bits, 1 stop bit, parity off.
  outb(COM1+UART_LINE_CONTROL, UART_DIVISOR_LATCH);     // Unlock divisor
80107065:	c7 44 24 04 80 00 00 	movl   $0x80,0x4(%esp)
8010706c:	00 
8010706d:	c7 04 24 fb 03 00 00 	movl   $0x3fb,(%esp)
80107074:	e8 b4 ff ff ff       	call   8010702d <outb>
  outb(COM1+UART_DIVISOR_LOW, 115200/9600);
80107079:	c7 44 24 04 0c 00 00 	movl   $0xc,0x4(%esp)
80107080:	00 
80107081:	c7 04 24 f8 03 00 00 	movl   $0x3f8,(%esp)
80107088:	e8 a0 ff ff ff       	call   8010702d <outb>
  outb(COM1+UART_DIVISOR_HIGH, 0);
8010708d:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80107094:	00 
80107095:	c7 04 24 f9 03 00 00 	movl   $0x3f9,(%esp)
8010709c:	e8 8c ff ff ff       	call   8010702d <outb>
  outb(COM1+UART_LINE_CONTROL, 0x03);   // Lock divisor, 8 data bits.
801070a1:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
801070a8:	00 
801070a9:	c7 04 24 fb 03 00 00 	movl   $0x3fb,(%esp)
801070b0:	e8 78 ff ff ff       	call   8010702d <outb>
  outb(COM1+UART_MODEM_CONTROL, 0);
801070b5:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801070bc:	00 
801070bd:	c7 04 24 fc 03 00 00 	movl   $0x3fc,(%esp)
801070c4:	e8 64 ff ff ff       	call   8010702d <outb>

  // Enable receive interrupts.
  outb(COM1+UART_INTERRUPT_ENABLE, UART_RECEIVE_INTERRUPT);
801070c9:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
801070d0:	00 
801070d1:	c7 04 24 f9 03 00 00 	movl   $0x3f9,(%esp)
801070d8:	e8 50 ff ff ff       	call   8010702d <outb>

  // If status is 0xFF, no serial port.
  if(inb(COM1+UART_LINE_STATUS) == 0xFF)
801070dd:	c7 04 24 fd 03 00 00 	movl   $0x3fd,(%esp)
801070e4:	e8 27 ff ff ff       	call   80107010 <inb>
801070e9:	3c ff                	cmp    $0xff,%al
801070eb:	74 6c                	je     80107159 <uartinit+0x10e>
    return;
  uart = 1;
801070ed:	c7 05 60 ce 10 80 01 	movl   $0x1,0x8010ce60
801070f4:	00 00 00 

  // Acknowledge pre-existing interrupt conditions;
  // enable interrupts.
  inb(COM1+UART_INTERRUPT_ID);
801070f7:	c7 04 24 fa 03 00 00 	movl   $0x3fa,(%esp)
801070fe:	e8 0d ff ff ff       	call   80107010 <inb>
  inb(COM1+UART_RECEIVE_BUFFER);
80107103:	c7 04 24 f8 03 00 00 	movl   $0x3f8,(%esp)
8010710a:	e8 01 ff ff ff       	call   80107010 <inb>
  picenable(IRQ_COM1);
8010710f:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
80107116:	e8 82 d3 ff ff       	call   8010449d <picenable>
  ioapicenable(IRQ_COM1, 0);
8010711b:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80107122:	00 
80107123:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
8010712a:	e8 cb c0 ff ff       	call   801031fa <ioapicenable>

  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
8010712f:	c7 45 f4 b8 92 10 80 	movl   $0x801092b8,-0xc(%ebp)
80107136:	eb 15                	jmp    8010714d <uartinit+0x102>
    uartputc(*p);
80107138:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010713b:	0f b6 00             	movzbl (%eax),%eax
8010713e:	0f be c0             	movsbl %al,%eax
80107141:	89 04 24             	mov    %eax,(%esp)
80107144:	e8 47 00 00 00       	call   80107190 <uartputc>
  inb(COM1+UART_RECEIVE_BUFFER);
  picenable(IRQ_COM1);
  ioapicenable(IRQ_COM1, 0);

  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
80107149:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010714d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107150:	0f b6 00             	movzbl (%eax),%eax
80107153:	84 c0                	test   %al,%al
80107155:	75 e1                	jne    80107138 <uartinit+0xed>
80107157:	eb 01                	jmp    8010715a <uartinit+0x10f>
  // Enable receive interrupts.
  outb(COM1+UART_INTERRUPT_ENABLE, UART_RECEIVE_INTERRUPT);

  // If status is 0xFF, no serial port.
  if(inb(COM1+UART_LINE_STATUS) == 0xFF)
    return;
80107159:	90                   	nop
  ioapicenable(IRQ_COM1, 0);

  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
    uartputc(*p);
}
8010715a:	c9                   	leave  
8010715b:	c3                   	ret    

8010715c <cantransmit>:

static int
cantransmit(void)
{
8010715c:	55                   	push   %ebp
8010715d:	89 e5                	mov    %esp,%ebp
8010715f:	83 ec 04             	sub    $0x4,%esp
  return inb(COM1+UART_LINE_STATUS) & UART_TRANSMIT_READY;
80107162:	c7 04 24 fd 03 00 00 	movl   $0x3fd,(%esp)
80107169:	e8 a2 fe ff ff       	call   80107010 <inb>
8010716e:	0f b6 c0             	movzbl %al,%eax
80107171:	83 e0 20             	and    $0x20,%eax
}
80107174:	c9                   	leave  
80107175:	c3                   	ret    

80107176 <hasreceived>:

static int
hasreceived(void)
{
80107176:	55                   	push   %ebp
80107177:	89 e5                	mov    %esp,%ebp
80107179:	83 ec 04             	sub    $0x4,%esp
  return inb(COM1+UART_LINE_STATUS) & UART_RECEIVE_READY;
8010717c:	c7 04 24 fd 03 00 00 	movl   $0x3fd,(%esp)
80107183:	e8 88 fe ff ff       	call   80107010 <inb>
80107188:	0f b6 c0             	movzbl %al,%eax
8010718b:	83 e0 01             	and    $0x1,%eax
}
8010718e:	c9                   	leave  
8010718f:	c3                   	ret    

80107190 <uartputc>:

void
uartputc(int c)
{
80107190:	55                   	push   %ebp
80107191:	89 e5                	mov    %esp,%ebp
80107193:	83 ec 28             	sub    $0x28,%esp
  int i;

  if(!uart)
80107196:	a1 60 ce 10 80       	mov    0x8010ce60,%eax
8010719b:	85 c0                	test   %eax,%eax
8010719d:	74 40                	je     801071df <uartputc+0x4f>
    return;
  for(i = 0; i < 128 && !cantransmit(); i++)
8010719f:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
801071a6:	eb 10                	jmp    801071b8 <uartputc+0x28>
    microdelay(10);
801071a8:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
801071af:	e8 c7 c5 ff ff       	call   8010377b <microdelay>
{
  int i;

  if(!uart)
    return;
  for(i = 0; i < 128 && !cantransmit(); i++)
801071b4:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801071b8:	83 7d f4 7f          	cmpl   $0x7f,-0xc(%ebp)
801071bc:	7f 09                	jg     801071c7 <uartputc+0x37>
801071be:	e8 99 ff ff ff       	call   8010715c <cantransmit>
801071c3:	85 c0                	test   %eax,%eax
801071c5:	74 e1                	je     801071a8 <uartputc+0x18>
    microdelay(10);
  outb(COM1+UART_TRANSMIT_BUFFER, c);
801071c7:	8b 45 08             	mov    0x8(%ebp),%eax
801071ca:	0f b6 c0             	movzbl %al,%eax
801071cd:	89 44 24 04          	mov    %eax,0x4(%esp)
801071d1:	c7 04 24 f8 03 00 00 	movl   $0x3f8,(%esp)
801071d8:	e8 50 fe ff ff       	call   8010702d <outb>
801071dd:	eb 01                	jmp    801071e0 <uartputc+0x50>
uartputc(int c)
{
  int i;

  if(!uart)
    return;
801071df:	90                   	nop
  for(i = 0; i < 128 && !cantransmit(); i++)
    microdelay(10);
  outb(COM1+UART_TRANSMIT_BUFFER, c);
}
801071e0:	c9                   	leave  
801071e1:	c3                   	ret    

801071e2 <uartgetc>:

static int
uartgetc(void)
{
801071e2:	55                   	push   %ebp
801071e3:	89 e5                	mov    %esp,%ebp
801071e5:	83 ec 04             	sub    $0x4,%esp
  if(!uart)
801071e8:	a1 60 ce 10 80       	mov    0x8010ce60,%eax
801071ed:	85 c0                	test   %eax,%eax
801071ef:	75 07                	jne    801071f8 <uartgetc+0x16>
    return -1;
801071f1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801071f6:	eb 1f                	jmp    80107217 <uartgetc+0x35>
  if(!hasreceived())
801071f8:	e8 79 ff ff ff       	call   80107176 <hasreceived>
801071fd:	85 c0                	test   %eax,%eax
801071ff:	75 07                	jne    80107208 <uartgetc+0x26>
    return -1;
80107201:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107206:	eb 0f                	jmp    80107217 <uartgetc+0x35>
  return inb(COM1+UART_RECEIVE_BUFFER);
80107208:	c7 04 24 f8 03 00 00 	movl   $0x3f8,(%esp)
8010720f:	e8 fc fd ff ff       	call   80107010 <inb>
80107214:	0f b6 c0             	movzbl %al,%eax
}
80107217:	c9                   	leave  
80107218:	c3                   	ret    

80107219 <uartintr>:

void
uartintr(void)
{
80107219:	55                   	push   %ebp
8010721a:	89 e5                	mov    %esp,%ebp
8010721c:	83 ec 18             	sub    $0x18,%esp
  consoleintr(uartgetc);
8010721f:	c7 04 24 e2 71 10 80 	movl   $0x801071e2,(%esp)
80107226:	e8 d2 98 ff ff       	call   80100afd <consoleintr>
}
8010722b:	c9                   	leave  
8010722c:	c3                   	ret    
8010722d:	00 00                	add    %al,(%eax)
	...

80107230 <vector0>:
# generated by vectors.pl - do not edit
# handlers
.globl alltraps
.globl vector0
vector0:
  pushl $0
80107230:	6a 00                	push   $0x0
  pushl $0
80107232:	6a 00                	push   $0x0
  jmp alltraps
80107234:	e9 47 f9 ff ff       	jmp    80106b80 <alltraps>

80107239 <vector1>:
.globl vector1
vector1:
  pushl $0
80107239:	6a 00                	push   $0x0
  pushl $1
8010723b:	6a 01                	push   $0x1
  jmp alltraps
8010723d:	e9 3e f9 ff ff       	jmp    80106b80 <alltraps>

80107242 <vector2>:
.globl vector2
vector2:
  pushl $0
80107242:	6a 00                	push   $0x0
  pushl $2
80107244:	6a 02                	push   $0x2
  jmp alltraps
80107246:	e9 35 f9 ff ff       	jmp    80106b80 <alltraps>

8010724b <vector3>:
.globl vector3
vector3:
  pushl $0
8010724b:	6a 00                	push   $0x0
  pushl $3
8010724d:	6a 03                	push   $0x3
  jmp alltraps
8010724f:	e9 2c f9 ff ff       	jmp    80106b80 <alltraps>

80107254 <vector4>:
.globl vector4
vector4:
  pushl $0
80107254:	6a 00                	push   $0x0
  pushl $4
80107256:	6a 04                	push   $0x4
  jmp alltraps
80107258:	e9 23 f9 ff ff       	jmp    80106b80 <alltraps>

8010725d <vector5>:
.globl vector5
vector5:
  pushl $0
8010725d:	6a 00                	push   $0x0
  pushl $5
8010725f:	6a 05                	push   $0x5
  jmp alltraps
80107261:	e9 1a f9 ff ff       	jmp    80106b80 <alltraps>

80107266 <vector6>:
.globl vector6
vector6:
  pushl $0
80107266:	6a 00                	push   $0x0
  pushl $6
80107268:	6a 06                	push   $0x6
  jmp alltraps
8010726a:	e9 11 f9 ff ff       	jmp    80106b80 <alltraps>

8010726f <vector7>:
.globl vector7
vector7:
  pushl $0
8010726f:	6a 00                	push   $0x0
  pushl $7
80107271:	6a 07                	push   $0x7
  jmp alltraps
80107273:	e9 08 f9 ff ff       	jmp    80106b80 <alltraps>

80107278 <vector8>:
.globl vector8
vector8:
  pushl $8
80107278:	6a 08                	push   $0x8
  jmp alltraps
8010727a:	e9 01 f9 ff ff       	jmp    80106b80 <alltraps>

8010727f <vector9>:
.globl vector9
vector9:
  pushl $0
8010727f:	6a 00                	push   $0x0
  pushl $9
80107281:	6a 09                	push   $0x9
  jmp alltraps
80107283:	e9 f8 f8 ff ff       	jmp    80106b80 <alltraps>

80107288 <vector10>:
.globl vector10
vector10:
  pushl $10
80107288:	6a 0a                	push   $0xa
  jmp alltraps
8010728a:	e9 f1 f8 ff ff       	jmp    80106b80 <alltraps>

8010728f <vector11>:
.globl vector11
vector11:
  pushl $11
8010728f:	6a 0b                	push   $0xb
  jmp alltraps
80107291:	e9 ea f8 ff ff       	jmp    80106b80 <alltraps>

80107296 <vector12>:
.globl vector12
vector12:
  pushl $12
80107296:	6a 0c                	push   $0xc
  jmp alltraps
80107298:	e9 e3 f8 ff ff       	jmp    80106b80 <alltraps>

8010729d <vector13>:
.globl vector13
vector13:
  pushl $13
8010729d:	6a 0d                	push   $0xd
  jmp alltraps
8010729f:	e9 dc f8 ff ff       	jmp    80106b80 <alltraps>

801072a4 <vector14>:
.globl vector14
vector14:
  pushl $14
801072a4:	6a 0e                	push   $0xe
  jmp alltraps
801072a6:	e9 d5 f8 ff ff       	jmp    80106b80 <alltraps>

801072ab <vector15>:
.globl vector15
vector15:
  pushl $0
801072ab:	6a 00                	push   $0x0
  pushl $15
801072ad:	6a 0f                	push   $0xf
  jmp alltraps
801072af:	e9 cc f8 ff ff       	jmp    80106b80 <alltraps>

801072b4 <vector16>:
.globl vector16
vector16:
  pushl $0
801072b4:	6a 00                	push   $0x0
  pushl $16
801072b6:	6a 10                	push   $0x10
  jmp alltraps
801072b8:	e9 c3 f8 ff ff       	jmp    80106b80 <alltraps>

801072bd <vector17>:
.globl vector17
vector17:
  pushl $17
801072bd:	6a 11                	push   $0x11
  jmp alltraps
801072bf:	e9 bc f8 ff ff       	jmp    80106b80 <alltraps>

801072c4 <vector18>:
.globl vector18
vector18:
  pushl $0
801072c4:	6a 00                	push   $0x0
  pushl $18
801072c6:	6a 12                	push   $0x12
  jmp alltraps
801072c8:	e9 b3 f8 ff ff       	jmp    80106b80 <alltraps>

801072cd <vector19>:
.globl vector19
vector19:
  pushl $0
801072cd:	6a 00                	push   $0x0
  pushl $19
801072cf:	6a 13                	push   $0x13
  jmp alltraps
801072d1:	e9 aa f8 ff ff       	jmp    80106b80 <alltraps>

801072d6 <vector20>:
.globl vector20
vector20:
  pushl $0
801072d6:	6a 00                	push   $0x0
  pushl $20
801072d8:	6a 14                	push   $0x14
  jmp alltraps
801072da:	e9 a1 f8 ff ff       	jmp    80106b80 <alltraps>

801072df <vector21>:
.globl vector21
vector21:
  pushl $0
801072df:	6a 00                	push   $0x0
  pushl $21
801072e1:	6a 15                	push   $0x15
  jmp alltraps
801072e3:	e9 98 f8 ff ff       	jmp    80106b80 <alltraps>

801072e8 <vector22>:
.globl vector22
vector22:
  pushl $0
801072e8:	6a 00                	push   $0x0
  pushl $22
801072ea:	6a 16                	push   $0x16
  jmp alltraps
801072ec:	e9 8f f8 ff ff       	jmp    80106b80 <alltraps>

801072f1 <vector23>:
.globl vector23
vector23:
  pushl $0
801072f1:	6a 00                	push   $0x0
  pushl $23
801072f3:	6a 17                	push   $0x17
  jmp alltraps
801072f5:	e9 86 f8 ff ff       	jmp    80106b80 <alltraps>

801072fa <vector24>:
.globl vector24
vector24:
  pushl $0
801072fa:	6a 00                	push   $0x0
  pushl $24
801072fc:	6a 18                	push   $0x18
  jmp alltraps
801072fe:	e9 7d f8 ff ff       	jmp    80106b80 <alltraps>

80107303 <vector25>:
.globl vector25
vector25:
  pushl $0
80107303:	6a 00                	push   $0x0
  pushl $25
80107305:	6a 19                	push   $0x19
  jmp alltraps
80107307:	e9 74 f8 ff ff       	jmp    80106b80 <alltraps>

8010730c <vector26>:
.globl vector26
vector26:
  pushl $0
8010730c:	6a 00                	push   $0x0
  pushl $26
8010730e:	6a 1a                	push   $0x1a
  jmp alltraps
80107310:	e9 6b f8 ff ff       	jmp    80106b80 <alltraps>

80107315 <vector27>:
.globl vector27
vector27:
  pushl $0
80107315:	6a 00                	push   $0x0
  pushl $27
80107317:	6a 1b                	push   $0x1b
  jmp alltraps
80107319:	e9 62 f8 ff ff       	jmp    80106b80 <alltraps>

8010731e <vector28>:
.globl vector28
vector28:
  pushl $0
8010731e:	6a 00                	push   $0x0
  pushl $28
80107320:	6a 1c                	push   $0x1c
  jmp alltraps
80107322:	e9 59 f8 ff ff       	jmp    80106b80 <alltraps>

80107327 <vector29>:
.globl vector29
vector29:
  pushl $0
80107327:	6a 00                	push   $0x0
  pushl $29
80107329:	6a 1d                	push   $0x1d
  jmp alltraps
8010732b:	e9 50 f8 ff ff       	jmp    80106b80 <alltraps>

80107330 <vector30>:
.globl vector30
vector30:
  pushl $0
80107330:	6a 00                	push   $0x0
  pushl $30
80107332:	6a 1e                	push   $0x1e
  jmp alltraps
80107334:	e9 47 f8 ff ff       	jmp    80106b80 <alltraps>

80107339 <vector31>:
.globl vector31
vector31:
  pushl $0
80107339:	6a 00                	push   $0x0
  pushl $31
8010733b:	6a 1f                	push   $0x1f
  jmp alltraps
8010733d:	e9 3e f8 ff ff       	jmp    80106b80 <alltraps>

80107342 <vector32>:
.globl vector32
vector32:
  pushl $0
80107342:	6a 00                	push   $0x0
  pushl $32
80107344:	6a 20                	push   $0x20
  jmp alltraps
80107346:	e9 35 f8 ff ff       	jmp    80106b80 <alltraps>

8010734b <vector33>:
.globl vector33
vector33:
  pushl $0
8010734b:	6a 00                	push   $0x0
  pushl $33
8010734d:	6a 21                	push   $0x21
  jmp alltraps
8010734f:	e9 2c f8 ff ff       	jmp    80106b80 <alltraps>

80107354 <vector34>:
.globl vector34
vector34:
  pushl $0
80107354:	6a 00                	push   $0x0
  pushl $34
80107356:	6a 22                	push   $0x22
  jmp alltraps
80107358:	e9 23 f8 ff ff       	jmp    80106b80 <alltraps>

8010735d <vector35>:
.globl vector35
vector35:
  pushl $0
8010735d:	6a 00                	push   $0x0
  pushl $35
8010735f:	6a 23                	push   $0x23
  jmp alltraps
80107361:	e9 1a f8 ff ff       	jmp    80106b80 <alltraps>

80107366 <vector36>:
.globl vector36
vector36:
  pushl $0
80107366:	6a 00                	push   $0x0
  pushl $36
80107368:	6a 24                	push   $0x24
  jmp alltraps
8010736a:	e9 11 f8 ff ff       	jmp    80106b80 <alltraps>

8010736f <vector37>:
.globl vector37
vector37:
  pushl $0
8010736f:	6a 00                	push   $0x0
  pushl $37
80107371:	6a 25                	push   $0x25
  jmp alltraps
80107373:	e9 08 f8 ff ff       	jmp    80106b80 <alltraps>

80107378 <vector38>:
.globl vector38
vector38:
  pushl $0
80107378:	6a 00                	push   $0x0
  pushl $38
8010737a:	6a 26                	push   $0x26
  jmp alltraps
8010737c:	e9 ff f7 ff ff       	jmp    80106b80 <alltraps>

80107381 <vector39>:
.globl vector39
vector39:
  pushl $0
80107381:	6a 00                	push   $0x0
  pushl $39
80107383:	6a 27                	push   $0x27
  jmp alltraps
80107385:	e9 f6 f7 ff ff       	jmp    80106b80 <alltraps>

8010738a <vector40>:
.globl vector40
vector40:
  pushl $0
8010738a:	6a 00                	push   $0x0
  pushl $40
8010738c:	6a 28                	push   $0x28
  jmp alltraps
8010738e:	e9 ed f7 ff ff       	jmp    80106b80 <alltraps>

80107393 <vector41>:
.globl vector41
vector41:
  pushl $0
80107393:	6a 00                	push   $0x0
  pushl $41
80107395:	6a 29                	push   $0x29
  jmp alltraps
80107397:	e9 e4 f7 ff ff       	jmp    80106b80 <alltraps>

8010739c <vector42>:
.globl vector42
vector42:
  pushl $0
8010739c:	6a 00                	push   $0x0
  pushl $42
8010739e:	6a 2a                	push   $0x2a
  jmp alltraps
801073a0:	e9 db f7 ff ff       	jmp    80106b80 <alltraps>

801073a5 <vector43>:
.globl vector43
vector43:
  pushl $0
801073a5:	6a 00                	push   $0x0
  pushl $43
801073a7:	6a 2b                	push   $0x2b
  jmp alltraps
801073a9:	e9 d2 f7 ff ff       	jmp    80106b80 <alltraps>

801073ae <vector44>:
.globl vector44
vector44:
  pushl $0
801073ae:	6a 00                	push   $0x0
  pushl $44
801073b0:	6a 2c                	push   $0x2c
  jmp alltraps
801073b2:	e9 c9 f7 ff ff       	jmp    80106b80 <alltraps>

801073b7 <vector45>:
.globl vector45
vector45:
  pushl $0
801073b7:	6a 00                	push   $0x0
  pushl $45
801073b9:	6a 2d                	push   $0x2d
  jmp alltraps
801073bb:	e9 c0 f7 ff ff       	jmp    80106b80 <alltraps>

801073c0 <vector46>:
.globl vector46
vector46:
  pushl $0
801073c0:	6a 00                	push   $0x0
  pushl $46
801073c2:	6a 2e                	push   $0x2e
  jmp alltraps
801073c4:	e9 b7 f7 ff ff       	jmp    80106b80 <alltraps>

801073c9 <vector47>:
.globl vector47
vector47:
  pushl $0
801073c9:	6a 00                	push   $0x0
  pushl $47
801073cb:	6a 2f                	push   $0x2f
  jmp alltraps
801073cd:	e9 ae f7 ff ff       	jmp    80106b80 <alltraps>

801073d2 <vector48>:
.globl vector48
vector48:
  pushl $0
801073d2:	6a 00                	push   $0x0
  pushl $48
801073d4:	6a 30                	push   $0x30
  jmp alltraps
801073d6:	e9 a5 f7 ff ff       	jmp    80106b80 <alltraps>

801073db <vector49>:
.globl vector49
vector49:
  pushl $0
801073db:	6a 00                	push   $0x0
  pushl $49
801073dd:	6a 31                	push   $0x31
  jmp alltraps
801073df:	e9 9c f7 ff ff       	jmp    80106b80 <alltraps>

801073e4 <vector50>:
.globl vector50
vector50:
  pushl $0
801073e4:	6a 00                	push   $0x0
  pushl $50
801073e6:	6a 32                	push   $0x32
  jmp alltraps
801073e8:	e9 93 f7 ff ff       	jmp    80106b80 <alltraps>

801073ed <vector51>:
.globl vector51
vector51:
  pushl $0
801073ed:	6a 00                	push   $0x0
  pushl $51
801073ef:	6a 33                	push   $0x33
  jmp alltraps
801073f1:	e9 8a f7 ff ff       	jmp    80106b80 <alltraps>

801073f6 <vector52>:
.globl vector52
vector52:
  pushl $0
801073f6:	6a 00                	push   $0x0
  pushl $52
801073f8:	6a 34                	push   $0x34
  jmp alltraps
801073fa:	e9 81 f7 ff ff       	jmp    80106b80 <alltraps>

801073ff <vector53>:
.globl vector53
vector53:
  pushl $0
801073ff:	6a 00                	push   $0x0
  pushl $53
80107401:	6a 35                	push   $0x35
  jmp alltraps
80107403:	e9 78 f7 ff ff       	jmp    80106b80 <alltraps>

80107408 <vector54>:
.globl vector54
vector54:
  pushl $0
80107408:	6a 00                	push   $0x0
  pushl $54
8010740a:	6a 36                	push   $0x36
  jmp alltraps
8010740c:	e9 6f f7 ff ff       	jmp    80106b80 <alltraps>

80107411 <vector55>:
.globl vector55
vector55:
  pushl $0
80107411:	6a 00                	push   $0x0
  pushl $55
80107413:	6a 37                	push   $0x37
  jmp alltraps
80107415:	e9 66 f7 ff ff       	jmp    80106b80 <alltraps>

8010741a <vector56>:
.globl vector56
vector56:
  pushl $0
8010741a:	6a 00                	push   $0x0
  pushl $56
8010741c:	6a 38                	push   $0x38
  jmp alltraps
8010741e:	e9 5d f7 ff ff       	jmp    80106b80 <alltraps>

80107423 <vector57>:
.globl vector57
vector57:
  pushl $0
80107423:	6a 00                	push   $0x0
  pushl $57
80107425:	6a 39                	push   $0x39
  jmp alltraps
80107427:	e9 54 f7 ff ff       	jmp    80106b80 <alltraps>

8010742c <vector58>:
.globl vector58
vector58:
  pushl $0
8010742c:	6a 00                	push   $0x0
  pushl $58
8010742e:	6a 3a                	push   $0x3a
  jmp alltraps
80107430:	e9 4b f7 ff ff       	jmp    80106b80 <alltraps>

80107435 <vector59>:
.globl vector59
vector59:
  pushl $0
80107435:	6a 00                	push   $0x0
  pushl $59
80107437:	6a 3b                	push   $0x3b
  jmp alltraps
80107439:	e9 42 f7 ff ff       	jmp    80106b80 <alltraps>

8010743e <vector60>:
.globl vector60
vector60:
  pushl $0
8010743e:	6a 00                	push   $0x0
  pushl $60
80107440:	6a 3c                	push   $0x3c
  jmp alltraps
80107442:	e9 39 f7 ff ff       	jmp    80106b80 <alltraps>

80107447 <vector61>:
.globl vector61
vector61:
  pushl $0
80107447:	6a 00                	push   $0x0
  pushl $61
80107449:	6a 3d                	push   $0x3d
  jmp alltraps
8010744b:	e9 30 f7 ff ff       	jmp    80106b80 <alltraps>

80107450 <vector62>:
.globl vector62
vector62:
  pushl $0
80107450:	6a 00                	push   $0x0
  pushl $62
80107452:	6a 3e                	push   $0x3e
  jmp alltraps
80107454:	e9 27 f7 ff ff       	jmp    80106b80 <alltraps>

80107459 <vector63>:
.globl vector63
vector63:
  pushl $0
80107459:	6a 00                	push   $0x0
  pushl $63
8010745b:	6a 3f                	push   $0x3f
  jmp alltraps
8010745d:	e9 1e f7 ff ff       	jmp    80106b80 <alltraps>

80107462 <vector64>:
.globl vector64
vector64:
  pushl $0
80107462:	6a 00                	push   $0x0
  pushl $64
80107464:	6a 40                	push   $0x40
  jmp alltraps
80107466:	e9 15 f7 ff ff       	jmp    80106b80 <alltraps>

8010746b <vector65>:
.globl vector65
vector65:
  pushl $0
8010746b:	6a 00                	push   $0x0
  pushl $65
8010746d:	6a 41                	push   $0x41
  jmp alltraps
8010746f:	e9 0c f7 ff ff       	jmp    80106b80 <alltraps>

80107474 <vector66>:
.globl vector66
vector66:
  pushl $0
80107474:	6a 00                	push   $0x0
  pushl $66
80107476:	6a 42                	push   $0x42
  jmp alltraps
80107478:	e9 03 f7 ff ff       	jmp    80106b80 <alltraps>

8010747d <vector67>:
.globl vector67
vector67:
  pushl $0
8010747d:	6a 00                	push   $0x0
  pushl $67
8010747f:	6a 43                	push   $0x43
  jmp alltraps
80107481:	e9 fa f6 ff ff       	jmp    80106b80 <alltraps>

80107486 <vector68>:
.globl vector68
vector68:
  pushl $0
80107486:	6a 00                	push   $0x0
  pushl $68
80107488:	6a 44                	push   $0x44
  jmp alltraps
8010748a:	e9 f1 f6 ff ff       	jmp    80106b80 <alltraps>

8010748f <vector69>:
.globl vector69
vector69:
  pushl $0
8010748f:	6a 00                	push   $0x0
  pushl $69
80107491:	6a 45                	push   $0x45
  jmp alltraps
80107493:	e9 e8 f6 ff ff       	jmp    80106b80 <alltraps>

80107498 <vector70>:
.globl vector70
vector70:
  pushl $0
80107498:	6a 00                	push   $0x0
  pushl $70
8010749a:	6a 46                	push   $0x46
  jmp alltraps
8010749c:	e9 df f6 ff ff       	jmp    80106b80 <alltraps>

801074a1 <vector71>:
.globl vector71
vector71:
  pushl $0
801074a1:	6a 00                	push   $0x0
  pushl $71
801074a3:	6a 47                	push   $0x47
  jmp alltraps
801074a5:	e9 d6 f6 ff ff       	jmp    80106b80 <alltraps>

801074aa <vector72>:
.globl vector72
vector72:
  pushl $0
801074aa:	6a 00                	push   $0x0
  pushl $72
801074ac:	6a 48                	push   $0x48
  jmp alltraps
801074ae:	e9 cd f6 ff ff       	jmp    80106b80 <alltraps>

801074b3 <vector73>:
.globl vector73
vector73:
  pushl $0
801074b3:	6a 00                	push   $0x0
  pushl $73
801074b5:	6a 49                	push   $0x49
  jmp alltraps
801074b7:	e9 c4 f6 ff ff       	jmp    80106b80 <alltraps>

801074bc <vector74>:
.globl vector74
vector74:
  pushl $0
801074bc:	6a 00                	push   $0x0
  pushl $74
801074be:	6a 4a                	push   $0x4a
  jmp alltraps
801074c0:	e9 bb f6 ff ff       	jmp    80106b80 <alltraps>

801074c5 <vector75>:
.globl vector75
vector75:
  pushl $0
801074c5:	6a 00                	push   $0x0
  pushl $75
801074c7:	6a 4b                	push   $0x4b
  jmp alltraps
801074c9:	e9 b2 f6 ff ff       	jmp    80106b80 <alltraps>

801074ce <vector76>:
.globl vector76
vector76:
  pushl $0
801074ce:	6a 00                	push   $0x0
  pushl $76
801074d0:	6a 4c                	push   $0x4c
  jmp alltraps
801074d2:	e9 a9 f6 ff ff       	jmp    80106b80 <alltraps>

801074d7 <vector77>:
.globl vector77
vector77:
  pushl $0
801074d7:	6a 00                	push   $0x0
  pushl $77
801074d9:	6a 4d                	push   $0x4d
  jmp alltraps
801074db:	e9 a0 f6 ff ff       	jmp    80106b80 <alltraps>

801074e0 <vector78>:
.globl vector78
vector78:
  pushl $0
801074e0:	6a 00                	push   $0x0
  pushl $78
801074e2:	6a 4e                	push   $0x4e
  jmp alltraps
801074e4:	e9 97 f6 ff ff       	jmp    80106b80 <alltraps>

801074e9 <vector79>:
.globl vector79
vector79:
  pushl $0
801074e9:	6a 00                	push   $0x0
  pushl $79
801074eb:	6a 4f                	push   $0x4f
  jmp alltraps
801074ed:	e9 8e f6 ff ff       	jmp    80106b80 <alltraps>

801074f2 <vector80>:
.globl vector80
vector80:
  pushl $0
801074f2:	6a 00                	push   $0x0
  pushl $80
801074f4:	6a 50                	push   $0x50
  jmp alltraps
801074f6:	e9 85 f6 ff ff       	jmp    80106b80 <alltraps>

801074fb <vector81>:
.globl vector81
vector81:
  pushl $0
801074fb:	6a 00                	push   $0x0
  pushl $81
801074fd:	6a 51                	push   $0x51
  jmp alltraps
801074ff:	e9 7c f6 ff ff       	jmp    80106b80 <alltraps>

80107504 <vector82>:
.globl vector82
vector82:
  pushl $0
80107504:	6a 00                	push   $0x0
  pushl $82
80107506:	6a 52                	push   $0x52
  jmp alltraps
80107508:	e9 73 f6 ff ff       	jmp    80106b80 <alltraps>

8010750d <vector83>:
.globl vector83
vector83:
  pushl $0
8010750d:	6a 00                	push   $0x0
  pushl $83
8010750f:	6a 53                	push   $0x53
  jmp alltraps
80107511:	e9 6a f6 ff ff       	jmp    80106b80 <alltraps>

80107516 <vector84>:
.globl vector84
vector84:
  pushl $0
80107516:	6a 00                	push   $0x0
  pushl $84
80107518:	6a 54                	push   $0x54
  jmp alltraps
8010751a:	e9 61 f6 ff ff       	jmp    80106b80 <alltraps>

8010751f <vector85>:
.globl vector85
vector85:
  pushl $0
8010751f:	6a 00                	push   $0x0
  pushl $85
80107521:	6a 55                	push   $0x55
  jmp alltraps
80107523:	e9 58 f6 ff ff       	jmp    80106b80 <alltraps>

80107528 <vector86>:
.globl vector86
vector86:
  pushl $0
80107528:	6a 00                	push   $0x0
  pushl $86
8010752a:	6a 56                	push   $0x56
  jmp alltraps
8010752c:	e9 4f f6 ff ff       	jmp    80106b80 <alltraps>

80107531 <vector87>:
.globl vector87
vector87:
  pushl $0
80107531:	6a 00                	push   $0x0
  pushl $87
80107533:	6a 57                	push   $0x57
  jmp alltraps
80107535:	e9 46 f6 ff ff       	jmp    80106b80 <alltraps>

8010753a <vector88>:
.globl vector88
vector88:
  pushl $0
8010753a:	6a 00                	push   $0x0
  pushl $88
8010753c:	6a 58                	push   $0x58
  jmp alltraps
8010753e:	e9 3d f6 ff ff       	jmp    80106b80 <alltraps>

80107543 <vector89>:
.globl vector89
vector89:
  pushl $0
80107543:	6a 00                	push   $0x0
  pushl $89
80107545:	6a 59                	push   $0x59
  jmp alltraps
80107547:	e9 34 f6 ff ff       	jmp    80106b80 <alltraps>

8010754c <vector90>:
.globl vector90
vector90:
  pushl $0
8010754c:	6a 00                	push   $0x0
  pushl $90
8010754e:	6a 5a                	push   $0x5a
  jmp alltraps
80107550:	e9 2b f6 ff ff       	jmp    80106b80 <alltraps>

80107555 <vector91>:
.globl vector91
vector91:
  pushl $0
80107555:	6a 00                	push   $0x0
  pushl $91
80107557:	6a 5b                	push   $0x5b
  jmp alltraps
80107559:	e9 22 f6 ff ff       	jmp    80106b80 <alltraps>

8010755e <vector92>:
.globl vector92
vector92:
  pushl $0
8010755e:	6a 00                	push   $0x0
  pushl $92
80107560:	6a 5c                	push   $0x5c
  jmp alltraps
80107562:	e9 19 f6 ff ff       	jmp    80106b80 <alltraps>

80107567 <vector93>:
.globl vector93
vector93:
  pushl $0
80107567:	6a 00                	push   $0x0
  pushl $93
80107569:	6a 5d                	push   $0x5d
  jmp alltraps
8010756b:	e9 10 f6 ff ff       	jmp    80106b80 <alltraps>

80107570 <vector94>:
.globl vector94
vector94:
  pushl $0
80107570:	6a 00                	push   $0x0
  pushl $94
80107572:	6a 5e                	push   $0x5e
  jmp alltraps
80107574:	e9 07 f6 ff ff       	jmp    80106b80 <alltraps>

80107579 <vector95>:
.globl vector95
vector95:
  pushl $0
80107579:	6a 00                	push   $0x0
  pushl $95
8010757b:	6a 5f                	push   $0x5f
  jmp alltraps
8010757d:	e9 fe f5 ff ff       	jmp    80106b80 <alltraps>

80107582 <vector96>:
.globl vector96
vector96:
  pushl $0
80107582:	6a 00                	push   $0x0
  pushl $96
80107584:	6a 60                	push   $0x60
  jmp alltraps
80107586:	e9 f5 f5 ff ff       	jmp    80106b80 <alltraps>

8010758b <vector97>:
.globl vector97
vector97:
  pushl $0
8010758b:	6a 00                	push   $0x0
  pushl $97
8010758d:	6a 61                	push   $0x61
  jmp alltraps
8010758f:	e9 ec f5 ff ff       	jmp    80106b80 <alltraps>

80107594 <vector98>:
.globl vector98
vector98:
  pushl $0
80107594:	6a 00                	push   $0x0
  pushl $98
80107596:	6a 62                	push   $0x62
  jmp alltraps
80107598:	e9 e3 f5 ff ff       	jmp    80106b80 <alltraps>

8010759d <vector99>:
.globl vector99
vector99:
  pushl $0
8010759d:	6a 00                	push   $0x0
  pushl $99
8010759f:	6a 63                	push   $0x63
  jmp alltraps
801075a1:	e9 da f5 ff ff       	jmp    80106b80 <alltraps>

801075a6 <vector100>:
.globl vector100
vector100:
  pushl $0
801075a6:	6a 00                	push   $0x0
  pushl $100
801075a8:	6a 64                	push   $0x64
  jmp alltraps
801075aa:	e9 d1 f5 ff ff       	jmp    80106b80 <alltraps>

801075af <vector101>:
.globl vector101
vector101:
  pushl $0
801075af:	6a 00                	push   $0x0
  pushl $101
801075b1:	6a 65                	push   $0x65
  jmp alltraps
801075b3:	e9 c8 f5 ff ff       	jmp    80106b80 <alltraps>

801075b8 <vector102>:
.globl vector102
vector102:
  pushl $0
801075b8:	6a 00                	push   $0x0
  pushl $102
801075ba:	6a 66                	push   $0x66
  jmp alltraps
801075bc:	e9 bf f5 ff ff       	jmp    80106b80 <alltraps>

801075c1 <vector103>:
.globl vector103
vector103:
  pushl $0
801075c1:	6a 00                	push   $0x0
  pushl $103
801075c3:	6a 67                	push   $0x67
  jmp alltraps
801075c5:	e9 b6 f5 ff ff       	jmp    80106b80 <alltraps>

801075ca <vector104>:
.globl vector104
vector104:
  pushl $0
801075ca:	6a 00                	push   $0x0
  pushl $104
801075cc:	6a 68                	push   $0x68
  jmp alltraps
801075ce:	e9 ad f5 ff ff       	jmp    80106b80 <alltraps>

801075d3 <vector105>:
.globl vector105
vector105:
  pushl $0
801075d3:	6a 00                	push   $0x0
  pushl $105
801075d5:	6a 69                	push   $0x69
  jmp alltraps
801075d7:	e9 a4 f5 ff ff       	jmp    80106b80 <alltraps>

801075dc <vector106>:
.globl vector106
vector106:
  pushl $0
801075dc:	6a 00                	push   $0x0
  pushl $106
801075de:	6a 6a                	push   $0x6a
  jmp alltraps
801075e0:	e9 9b f5 ff ff       	jmp    80106b80 <alltraps>

801075e5 <vector107>:
.globl vector107
vector107:
  pushl $0
801075e5:	6a 00                	push   $0x0
  pushl $107
801075e7:	6a 6b                	push   $0x6b
  jmp alltraps
801075e9:	e9 92 f5 ff ff       	jmp    80106b80 <alltraps>

801075ee <vector108>:
.globl vector108
vector108:
  pushl $0
801075ee:	6a 00                	push   $0x0
  pushl $108
801075f0:	6a 6c                	push   $0x6c
  jmp alltraps
801075f2:	e9 89 f5 ff ff       	jmp    80106b80 <alltraps>

801075f7 <vector109>:
.globl vector109
vector109:
  pushl $0
801075f7:	6a 00                	push   $0x0
  pushl $109
801075f9:	6a 6d                	push   $0x6d
  jmp alltraps
801075fb:	e9 80 f5 ff ff       	jmp    80106b80 <alltraps>

80107600 <vector110>:
.globl vector110
vector110:
  pushl $0
80107600:	6a 00                	push   $0x0
  pushl $110
80107602:	6a 6e                	push   $0x6e
  jmp alltraps
80107604:	e9 77 f5 ff ff       	jmp    80106b80 <alltraps>

80107609 <vector111>:
.globl vector111
vector111:
  pushl $0
80107609:	6a 00                	push   $0x0
  pushl $111
8010760b:	6a 6f                	push   $0x6f
  jmp alltraps
8010760d:	e9 6e f5 ff ff       	jmp    80106b80 <alltraps>

80107612 <vector112>:
.globl vector112
vector112:
  pushl $0
80107612:	6a 00                	push   $0x0
  pushl $112
80107614:	6a 70                	push   $0x70
  jmp alltraps
80107616:	e9 65 f5 ff ff       	jmp    80106b80 <alltraps>

8010761b <vector113>:
.globl vector113
vector113:
  pushl $0
8010761b:	6a 00                	push   $0x0
  pushl $113
8010761d:	6a 71                	push   $0x71
  jmp alltraps
8010761f:	e9 5c f5 ff ff       	jmp    80106b80 <alltraps>

80107624 <vector114>:
.globl vector114
vector114:
  pushl $0
80107624:	6a 00                	push   $0x0
  pushl $114
80107626:	6a 72                	push   $0x72
  jmp alltraps
80107628:	e9 53 f5 ff ff       	jmp    80106b80 <alltraps>

8010762d <vector115>:
.globl vector115
vector115:
  pushl $0
8010762d:	6a 00                	push   $0x0
  pushl $115
8010762f:	6a 73                	push   $0x73
  jmp alltraps
80107631:	e9 4a f5 ff ff       	jmp    80106b80 <alltraps>

80107636 <vector116>:
.globl vector116
vector116:
  pushl $0
80107636:	6a 00                	push   $0x0
  pushl $116
80107638:	6a 74                	push   $0x74
  jmp alltraps
8010763a:	e9 41 f5 ff ff       	jmp    80106b80 <alltraps>

8010763f <vector117>:
.globl vector117
vector117:
  pushl $0
8010763f:	6a 00                	push   $0x0
  pushl $117
80107641:	6a 75                	push   $0x75
  jmp alltraps
80107643:	e9 38 f5 ff ff       	jmp    80106b80 <alltraps>

80107648 <vector118>:
.globl vector118
vector118:
  pushl $0
80107648:	6a 00                	push   $0x0
  pushl $118
8010764a:	6a 76                	push   $0x76
  jmp alltraps
8010764c:	e9 2f f5 ff ff       	jmp    80106b80 <alltraps>

80107651 <vector119>:
.globl vector119
vector119:
  pushl $0
80107651:	6a 00                	push   $0x0
  pushl $119
80107653:	6a 77                	push   $0x77
  jmp alltraps
80107655:	e9 26 f5 ff ff       	jmp    80106b80 <alltraps>

8010765a <vector120>:
.globl vector120
vector120:
  pushl $0
8010765a:	6a 00                	push   $0x0
  pushl $120
8010765c:	6a 78                	push   $0x78
  jmp alltraps
8010765e:	e9 1d f5 ff ff       	jmp    80106b80 <alltraps>

80107663 <vector121>:
.globl vector121
vector121:
  pushl $0
80107663:	6a 00                	push   $0x0
  pushl $121
80107665:	6a 79                	push   $0x79
  jmp alltraps
80107667:	e9 14 f5 ff ff       	jmp    80106b80 <alltraps>

8010766c <vector122>:
.globl vector122
vector122:
  pushl $0
8010766c:	6a 00                	push   $0x0
  pushl $122
8010766e:	6a 7a                	push   $0x7a
  jmp alltraps
80107670:	e9 0b f5 ff ff       	jmp    80106b80 <alltraps>

80107675 <vector123>:
.globl vector123
vector123:
  pushl $0
80107675:	6a 00                	push   $0x0
  pushl $123
80107677:	6a 7b                	push   $0x7b
  jmp alltraps
80107679:	e9 02 f5 ff ff       	jmp    80106b80 <alltraps>

8010767e <vector124>:
.globl vector124
vector124:
  pushl $0
8010767e:	6a 00                	push   $0x0
  pushl $124
80107680:	6a 7c                	push   $0x7c
  jmp alltraps
80107682:	e9 f9 f4 ff ff       	jmp    80106b80 <alltraps>

80107687 <vector125>:
.globl vector125
vector125:
  pushl $0
80107687:	6a 00                	push   $0x0
  pushl $125
80107689:	6a 7d                	push   $0x7d
  jmp alltraps
8010768b:	e9 f0 f4 ff ff       	jmp    80106b80 <alltraps>

80107690 <vector126>:
.globl vector126
vector126:
  pushl $0
80107690:	6a 00                	push   $0x0
  pushl $126
80107692:	6a 7e                	push   $0x7e
  jmp alltraps
80107694:	e9 e7 f4 ff ff       	jmp    80106b80 <alltraps>

80107699 <vector127>:
.globl vector127
vector127:
  pushl $0
80107699:	6a 00                	push   $0x0
  pushl $127
8010769b:	6a 7f                	push   $0x7f
  jmp alltraps
8010769d:	e9 de f4 ff ff       	jmp    80106b80 <alltraps>

801076a2 <vector128>:
.globl vector128
vector128:
  pushl $0
801076a2:	6a 00                	push   $0x0
  pushl $128
801076a4:	68 80 00 00 00       	push   $0x80
  jmp alltraps
801076a9:	e9 d2 f4 ff ff       	jmp    80106b80 <alltraps>

801076ae <vector129>:
.globl vector129
vector129:
  pushl $0
801076ae:	6a 00                	push   $0x0
  pushl $129
801076b0:	68 81 00 00 00       	push   $0x81
  jmp alltraps
801076b5:	e9 c6 f4 ff ff       	jmp    80106b80 <alltraps>

801076ba <vector130>:
.globl vector130
vector130:
  pushl $0
801076ba:	6a 00                	push   $0x0
  pushl $130
801076bc:	68 82 00 00 00       	push   $0x82
  jmp alltraps
801076c1:	e9 ba f4 ff ff       	jmp    80106b80 <alltraps>

801076c6 <vector131>:
.globl vector131
vector131:
  pushl $0
801076c6:	6a 00                	push   $0x0
  pushl $131
801076c8:	68 83 00 00 00       	push   $0x83
  jmp alltraps
801076cd:	e9 ae f4 ff ff       	jmp    80106b80 <alltraps>

801076d2 <vector132>:
.globl vector132
vector132:
  pushl $0
801076d2:	6a 00                	push   $0x0
  pushl $132
801076d4:	68 84 00 00 00       	push   $0x84
  jmp alltraps
801076d9:	e9 a2 f4 ff ff       	jmp    80106b80 <alltraps>

801076de <vector133>:
.globl vector133
vector133:
  pushl $0
801076de:	6a 00                	push   $0x0
  pushl $133
801076e0:	68 85 00 00 00       	push   $0x85
  jmp alltraps
801076e5:	e9 96 f4 ff ff       	jmp    80106b80 <alltraps>

801076ea <vector134>:
.globl vector134
vector134:
  pushl $0
801076ea:	6a 00                	push   $0x0
  pushl $134
801076ec:	68 86 00 00 00       	push   $0x86
  jmp alltraps
801076f1:	e9 8a f4 ff ff       	jmp    80106b80 <alltraps>

801076f6 <vector135>:
.globl vector135
vector135:
  pushl $0
801076f6:	6a 00                	push   $0x0
  pushl $135
801076f8:	68 87 00 00 00       	push   $0x87
  jmp alltraps
801076fd:	e9 7e f4 ff ff       	jmp    80106b80 <alltraps>

80107702 <vector136>:
.globl vector136
vector136:
  pushl $0
80107702:	6a 00                	push   $0x0
  pushl $136
80107704:	68 88 00 00 00       	push   $0x88
  jmp alltraps
80107709:	e9 72 f4 ff ff       	jmp    80106b80 <alltraps>

8010770e <vector137>:
.globl vector137
vector137:
  pushl $0
8010770e:	6a 00                	push   $0x0
  pushl $137
80107710:	68 89 00 00 00       	push   $0x89
  jmp alltraps
80107715:	e9 66 f4 ff ff       	jmp    80106b80 <alltraps>

8010771a <vector138>:
.globl vector138
vector138:
  pushl $0
8010771a:	6a 00                	push   $0x0
  pushl $138
8010771c:	68 8a 00 00 00       	push   $0x8a
  jmp alltraps
80107721:	e9 5a f4 ff ff       	jmp    80106b80 <alltraps>

80107726 <vector139>:
.globl vector139
vector139:
  pushl $0
80107726:	6a 00                	push   $0x0
  pushl $139
80107728:	68 8b 00 00 00       	push   $0x8b
  jmp alltraps
8010772d:	e9 4e f4 ff ff       	jmp    80106b80 <alltraps>

80107732 <vector140>:
.globl vector140
vector140:
  pushl $0
80107732:	6a 00                	push   $0x0
  pushl $140
80107734:	68 8c 00 00 00       	push   $0x8c
  jmp alltraps
80107739:	e9 42 f4 ff ff       	jmp    80106b80 <alltraps>

8010773e <vector141>:
.globl vector141
vector141:
  pushl $0
8010773e:	6a 00                	push   $0x0
  pushl $141
80107740:	68 8d 00 00 00       	push   $0x8d
  jmp alltraps
80107745:	e9 36 f4 ff ff       	jmp    80106b80 <alltraps>

8010774a <vector142>:
.globl vector142
vector142:
  pushl $0
8010774a:	6a 00                	push   $0x0
  pushl $142
8010774c:	68 8e 00 00 00       	push   $0x8e
  jmp alltraps
80107751:	e9 2a f4 ff ff       	jmp    80106b80 <alltraps>

80107756 <vector143>:
.globl vector143
vector143:
  pushl $0
80107756:	6a 00                	push   $0x0
  pushl $143
80107758:	68 8f 00 00 00       	push   $0x8f
  jmp alltraps
8010775d:	e9 1e f4 ff ff       	jmp    80106b80 <alltraps>

80107762 <vector144>:
.globl vector144
vector144:
  pushl $0
80107762:	6a 00                	push   $0x0
  pushl $144
80107764:	68 90 00 00 00       	push   $0x90
  jmp alltraps
80107769:	e9 12 f4 ff ff       	jmp    80106b80 <alltraps>

8010776e <vector145>:
.globl vector145
vector145:
  pushl $0
8010776e:	6a 00                	push   $0x0
  pushl $145
80107770:	68 91 00 00 00       	push   $0x91
  jmp alltraps
80107775:	e9 06 f4 ff ff       	jmp    80106b80 <alltraps>

8010777a <vector146>:
.globl vector146
vector146:
  pushl $0
8010777a:	6a 00                	push   $0x0
  pushl $146
8010777c:	68 92 00 00 00       	push   $0x92
  jmp alltraps
80107781:	e9 fa f3 ff ff       	jmp    80106b80 <alltraps>

80107786 <vector147>:
.globl vector147
vector147:
  pushl $0
80107786:	6a 00                	push   $0x0
  pushl $147
80107788:	68 93 00 00 00       	push   $0x93
  jmp alltraps
8010778d:	e9 ee f3 ff ff       	jmp    80106b80 <alltraps>

80107792 <vector148>:
.globl vector148
vector148:
  pushl $0
80107792:	6a 00                	push   $0x0
  pushl $148
80107794:	68 94 00 00 00       	push   $0x94
  jmp alltraps
80107799:	e9 e2 f3 ff ff       	jmp    80106b80 <alltraps>

8010779e <vector149>:
.globl vector149
vector149:
  pushl $0
8010779e:	6a 00                	push   $0x0
  pushl $149
801077a0:	68 95 00 00 00       	push   $0x95
  jmp alltraps
801077a5:	e9 d6 f3 ff ff       	jmp    80106b80 <alltraps>

801077aa <vector150>:
.globl vector150
vector150:
  pushl $0
801077aa:	6a 00                	push   $0x0
  pushl $150
801077ac:	68 96 00 00 00       	push   $0x96
  jmp alltraps
801077b1:	e9 ca f3 ff ff       	jmp    80106b80 <alltraps>

801077b6 <vector151>:
.globl vector151
vector151:
  pushl $0
801077b6:	6a 00                	push   $0x0
  pushl $151
801077b8:	68 97 00 00 00       	push   $0x97
  jmp alltraps
801077bd:	e9 be f3 ff ff       	jmp    80106b80 <alltraps>

801077c2 <vector152>:
.globl vector152
vector152:
  pushl $0
801077c2:	6a 00                	push   $0x0
  pushl $152
801077c4:	68 98 00 00 00       	push   $0x98
  jmp alltraps
801077c9:	e9 b2 f3 ff ff       	jmp    80106b80 <alltraps>

801077ce <vector153>:
.globl vector153
vector153:
  pushl $0
801077ce:	6a 00                	push   $0x0
  pushl $153
801077d0:	68 99 00 00 00       	push   $0x99
  jmp alltraps
801077d5:	e9 a6 f3 ff ff       	jmp    80106b80 <alltraps>

801077da <vector154>:
.globl vector154
vector154:
  pushl $0
801077da:	6a 00                	push   $0x0
  pushl $154
801077dc:	68 9a 00 00 00       	push   $0x9a
  jmp alltraps
801077e1:	e9 9a f3 ff ff       	jmp    80106b80 <alltraps>

801077e6 <vector155>:
.globl vector155
vector155:
  pushl $0
801077e6:	6a 00                	push   $0x0
  pushl $155
801077e8:	68 9b 00 00 00       	push   $0x9b
  jmp alltraps
801077ed:	e9 8e f3 ff ff       	jmp    80106b80 <alltraps>

801077f2 <vector156>:
.globl vector156
vector156:
  pushl $0
801077f2:	6a 00                	push   $0x0
  pushl $156
801077f4:	68 9c 00 00 00       	push   $0x9c
  jmp alltraps
801077f9:	e9 82 f3 ff ff       	jmp    80106b80 <alltraps>

801077fe <vector157>:
.globl vector157
vector157:
  pushl $0
801077fe:	6a 00                	push   $0x0
  pushl $157
80107800:	68 9d 00 00 00       	push   $0x9d
  jmp alltraps
80107805:	e9 76 f3 ff ff       	jmp    80106b80 <alltraps>

8010780a <vector158>:
.globl vector158
vector158:
  pushl $0
8010780a:	6a 00                	push   $0x0
  pushl $158
8010780c:	68 9e 00 00 00       	push   $0x9e
  jmp alltraps
80107811:	e9 6a f3 ff ff       	jmp    80106b80 <alltraps>

80107816 <vector159>:
.globl vector159
vector159:
  pushl $0
80107816:	6a 00                	push   $0x0
  pushl $159
80107818:	68 9f 00 00 00       	push   $0x9f
  jmp alltraps
8010781d:	e9 5e f3 ff ff       	jmp    80106b80 <alltraps>

80107822 <vector160>:
.globl vector160
vector160:
  pushl $0
80107822:	6a 00                	push   $0x0
  pushl $160
80107824:	68 a0 00 00 00       	push   $0xa0
  jmp alltraps
80107829:	e9 52 f3 ff ff       	jmp    80106b80 <alltraps>

8010782e <vector161>:
.globl vector161
vector161:
  pushl $0
8010782e:	6a 00                	push   $0x0
  pushl $161
80107830:	68 a1 00 00 00       	push   $0xa1
  jmp alltraps
80107835:	e9 46 f3 ff ff       	jmp    80106b80 <alltraps>

8010783a <vector162>:
.globl vector162
vector162:
  pushl $0
8010783a:	6a 00                	push   $0x0
  pushl $162
8010783c:	68 a2 00 00 00       	push   $0xa2
  jmp alltraps
80107841:	e9 3a f3 ff ff       	jmp    80106b80 <alltraps>

80107846 <vector163>:
.globl vector163
vector163:
  pushl $0
80107846:	6a 00                	push   $0x0
  pushl $163
80107848:	68 a3 00 00 00       	push   $0xa3
  jmp alltraps
8010784d:	e9 2e f3 ff ff       	jmp    80106b80 <alltraps>

80107852 <vector164>:
.globl vector164
vector164:
  pushl $0
80107852:	6a 00                	push   $0x0
  pushl $164
80107854:	68 a4 00 00 00       	push   $0xa4
  jmp alltraps
80107859:	e9 22 f3 ff ff       	jmp    80106b80 <alltraps>

8010785e <vector165>:
.globl vector165
vector165:
  pushl $0
8010785e:	6a 00                	push   $0x0
  pushl $165
80107860:	68 a5 00 00 00       	push   $0xa5
  jmp alltraps
80107865:	e9 16 f3 ff ff       	jmp    80106b80 <alltraps>

8010786a <vector166>:
.globl vector166
vector166:
  pushl $0
8010786a:	6a 00                	push   $0x0
  pushl $166
8010786c:	68 a6 00 00 00       	push   $0xa6
  jmp alltraps
80107871:	e9 0a f3 ff ff       	jmp    80106b80 <alltraps>

80107876 <vector167>:
.globl vector167
vector167:
  pushl $0
80107876:	6a 00                	push   $0x0
  pushl $167
80107878:	68 a7 00 00 00       	push   $0xa7
  jmp alltraps
8010787d:	e9 fe f2 ff ff       	jmp    80106b80 <alltraps>

80107882 <vector168>:
.globl vector168
vector168:
  pushl $0
80107882:	6a 00                	push   $0x0
  pushl $168
80107884:	68 a8 00 00 00       	push   $0xa8
  jmp alltraps
80107889:	e9 f2 f2 ff ff       	jmp    80106b80 <alltraps>

8010788e <vector169>:
.globl vector169
vector169:
  pushl $0
8010788e:	6a 00                	push   $0x0
  pushl $169
80107890:	68 a9 00 00 00       	push   $0xa9
  jmp alltraps
80107895:	e9 e6 f2 ff ff       	jmp    80106b80 <alltraps>

8010789a <vector170>:
.globl vector170
vector170:
  pushl $0
8010789a:	6a 00                	push   $0x0
  pushl $170
8010789c:	68 aa 00 00 00       	push   $0xaa
  jmp alltraps
801078a1:	e9 da f2 ff ff       	jmp    80106b80 <alltraps>

801078a6 <vector171>:
.globl vector171
vector171:
  pushl $0
801078a6:	6a 00                	push   $0x0
  pushl $171
801078a8:	68 ab 00 00 00       	push   $0xab
  jmp alltraps
801078ad:	e9 ce f2 ff ff       	jmp    80106b80 <alltraps>

801078b2 <vector172>:
.globl vector172
vector172:
  pushl $0
801078b2:	6a 00                	push   $0x0
  pushl $172
801078b4:	68 ac 00 00 00       	push   $0xac
  jmp alltraps
801078b9:	e9 c2 f2 ff ff       	jmp    80106b80 <alltraps>

801078be <vector173>:
.globl vector173
vector173:
  pushl $0
801078be:	6a 00                	push   $0x0
  pushl $173
801078c0:	68 ad 00 00 00       	push   $0xad
  jmp alltraps
801078c5:	e9 b6 f2 ff ff       	jmp    80106b80 <alltraps>

801078ca <vector174>:
.globl vector174
vector174:
  pushl $0
801078ca:	6a 00                	push   $0x0
  pushl $174
801078cc:	68 ae 00 00 00       	push   $0xae
  jmp alltraps
801078d1:	e9 aa f2 ff ff       	jmp    80106b80 <alltraps>

801078d6 <vector175>:
.globl vector175
vector175:
  pushl $0
801078d6:	6a 00                	push   $0x0
  pushl $175
801078d8:	68 af 00 00 00       	push   $0xaf
  jmp alltraps
801078dd:	e9 9e f2 ff ff       	jmp    80106b80 <alltraps>

801078e2 <vector176>:
.globl vector176
vector176:
  pushl $0
801078e2:	6a 00                	push   $0x0
  pushl $176
801078e4:	68 b0 00 00 00       	push   $0xb0
  jmp alltraps
801078e9:	e9 92 f2 ff ff       	jmp    80106b80 <alltraps>

801078ee <vector177>:
.globl vector177
vector177:
  pushl $0
801078ee:	6a 00                	push   $0x0
  pushl $177
801078f0:	68 b1 00 00 00       	push   $0xb1
  jmp alltraps
801078f5:	e9 86 f2 ff ff       	jmp    80106b80 <alltraps>

801078fa <vector178>:
.globl vector178
vector178:
  pushl $0
801078fa:	6a 00                	push   $0x0
  pushl $178
801078fc:	68 b2 00 00 00       	push   $0xb2
  jmp alltraps
80107901:	e9 7a f2 ff ff       	jmp    80106b80 <alltraps>

80107906 <vector179>:
.globl vector179
vector179:
  pushl $0
80107906:	6a 00                	push   $0x0
  pushl $179
80107908:	68 b3 00 00 00       	push   $0xb3
  jmp alltraps
8010790d:	e9 6e f2 ff ff       	jmp    80106b80 <alltraps>

80107912 <vector180>:
.globl vector180
vector180:
  pushl $0
80107912:	6a 00                	push   $0x0
  pushl $180
80107914:	68 b4 00 00 00       	push   $0xb4
  jmp alltraps
80107919:	e9 62 f2 ff ff       	jmp    80106b80 <alltraps>

8010791e <vector181>:
.globl vector181
vector181:
  pushl $0
8010791e:	6a 00                	push   $0x0
  pushl $181
80107920:	68 b5 00 00 00       	push   $0xb5
  jmp alltraps
80107925:	e9 56 f2 ff ff       	jmp    80106b80 <alltraps>

8010792a <vector182>:
.globl vector182
vector182:
  pushl $0
8010792a:	6a 00                	push   $0x0
  pushl $182
8010792c:	68 b6 00 00 00       	push   $0xb6
  jmp alltraps
80107931:	e9 4a f2 ff ff       	jmp    80106b80 <alltraps>

80107936 <vector183>:
.globl vector183
vector183:
  pushl $0
80107936:	6a 00                	push   $0x0
  pushl $183
80107938:	68 b7 00 00 00       	push   $0xb7
  jmp alltraps
8010793d:	e9 3e f2 ff ff       	jmp    80106b80 <alltraps>

80107942 <vector184>:
.globl vector184
vector184:
  pushl $0
80107942:	6a 00                	push   $0x0
  pushl $184
80107944:	68 b8 00 00 00       	push   $0xb8
  jmp alltraps
80107949:	e9 32 f2 ff ff       	jmp    80106b80 <alltraps>

8010794e <vector185>:
.globl vector185
vector185:
  pushl $0
8010794e:	6a 00                	push   $0x0
  pushl $185
80107950:	68 b9 00 00 00       	push   $0xb9
  jmp alltraps
80107955:	e9 26 f2 ff ff       	jmp    80106b80 <alltraps>

8010795a <vector186>:
.globl vector186
vector186:
  pushl $0
8010795a:	6a 00                	push   $0x0
  pushl $186
8010795c:	68 ba 00 00 00       	push   $0xba
  jmp alltraps
80107961:	e9 1a f2 ff ff       	jmp    80106b80 <alltraps>

80107966 <vector187>:
.globl vector187
vector187:
  pushl $0
80107966:	6a 00                	push   $0x0
  pushl $187
80107968:	68 bb 00 00 00       	push   $0xbb
  jmp alltraps
8010796d:	e9 0e f2 ff ff       	jmp    80106b80 <alltraps>

80107972 <vector188>:
.globl vector188
vector188:
  pushl $0
80107972:	6a 00                	push   $0x0
  pushl $188
80107974:	68 bc 00 00 00       	push   $0xbc
  jmp alltraps
80107979:	e9 02 f2 ff ff       	jmp    80106b80 <alltraps>

8010797e <vector189>:
.globl vector189
vector189:
  pushl $0
8010797e:	6a 00                	push   $0x0
  pushl $189
80107980:	68 bd 00 00 00       	push   $0xbd
  jmp alltraps
80107985:	e9 f6 f1 ff ff       	jmp    80106b80 <alltraps>

8010798a <vector190>:
.globl vector190
vector190:
  pushl $0
8010798a:	6a 00                	push   $0x0
  pushl $190
8010798c:	68 be 00 00 00       	push   $0xbe
  jmp alltraps
80107991:	e9 ea f1 ff ff       	jmp    80106b80 <alltraps>

80107996 <vector191>:
.globl vector191
vector191:
  pushl $0
80107996:	6a 00                	push   $0x0
  pushl $191
80107998:	68 bf 00 00 00       	push   $0xbf
  jmp alltraps
8010799d:	e9 de f1 ff ff       	jmp    80106b80 <alltraps>

801079a2 <vector192>:
.globl vector192
vector192:
  pushl $0
801079a2:	6a 00                	push   $0x0
  pushl $192
801079a4:	68 c0 00 00 00       	push   $0xc0
  jmp alltraps
801079a9:	e9 d2 f1 ff ff       	jmp    80106b80 <alltraps>

801079ae <vector193>:
.globl vector193
vector193:
  pushl $0
801079ae:	6a 00                	push   $0x0
  pushl $193
801079b0:	68 c1 00 00 00       	push   $0xc1
  jmp alltraps
801079b5:	e9 c6 f1 ff ff       	jmp    80106b80 <alltraps>

801079ba <vector194>:
.globl vector194
vector194:
  pushl $0
801079ba:	6a 00                	push   $0x0
  pushl $194
801079bc:	68 c2 00 00 00       	push   $0xc2
  jmp alltraps
801079c1:	e9 ba f1 ff ff       	jmp    80106b80 <alltraps>

801079c6 <vector195>:
.globl vector195
vector195:
  pushl $0
801079c6:	6a 00                	push   $0x0
  pushl $195
801079c8:	68 c3 00 00 00       	push   $0xc3
  jmp alltraps
801079cd:	e9 ae f1 ff ff       	jmp    80106b80 <alltraps>

801079d2 <vector196>:
.globl vector196
vector196:
  pushl $0
801079d2:	6a 00                	push   $0x0
  pushl $196
801079d4:	68 c4 00 00 00       	push   $0xc4
  jmp alltraps
801079d9:	e9 a2 f1 ff ff       	jmp    80106b80 <alltraps>

801079de <vector197>:
.globl vector197
vector197:
  pushl $0
801079de:	6a 00                	push   $0x0
  pushl $197
801079e0:	68 c5 00 00 00       	push   $0xc5
  jmp alltraps
801079e5:	e9 96 f1 ff ff       	jmp    80106b80 <alltraps>

801079ea <vector198>:
.globl vector198
vector198:
  pushl $0
801079ea:	6a 00                	push   $0x0
  pushl $198
801079ec:	68 c6 00 00 00       	push   $0xc6
  jmp alltraps
801079f1:	e9 8a f1 ff ff       	jmp    80106b80 <alltraps>

801079f6 <vector199>:
.globl vector199
vector199:
  pushl $0
801079f6:	6a 00                	push   $0x0
  pushl $199
801079f8:	68 c7 00 00 00       	push   $0xc7
  jmp alltraps
801079fd:	e9 7e f1 ff ff       	jmp    80106b80 <alltraps>

80107a02 <vector200>:
.globl vector200
vector200:
  pushl $0
80107a02:	6a 00                	push   $0x0
  pushl $200
80107a04:	68 c8 00 00 00       	push   $0xc8
  jmp alltraps
80107a09:	e9 72 f1 ff ff       	jmp    80106b80 <alltraps>

80107a0e <vector201>:
.globl vector201
vector201:
  pushl $0
80107a0e:	6a 00                	push   $0x0
  pushl $201
80107a10:	68 c9 00 00 00       	push   $0xc9
  jmp alltraps
80107a15:	e9 66 f1 ff ff       	jmp    80106b80 <alltraps>

80107a1a <vector202>:
.globl vector202
vector202:
  pushl $0
80107a1a:	6a 00                	push   $0x0
  pushl $202
80107a1c:	68 ca 00 00 00       	push   $0xca
  jmp alltraps
80107a21:	e9 5a f1 ff ff       	jmp    80106b80 <alltraps>

80107a26 <vector203>:
.globl vector203
vector203:
  pushl $0
80107a26:	6a 00                	push   $0x0
  pushl $203
80107a28:	68 cb 00 00 00       	push   $0xcb
  jmp alltraps
80107a2d:	e9 4e f1 ff ff       	jmp    80106b80 <alltraps>

80107a32 <vector204>:
.globl vector204
vector204:
  pushl $0
80107a32:	6a 00                	push   $0x0
  pushl $204
80107a34:	68 cc 00 00 00       	push   $0xcc
  jmp alltraps
80107a39:	e9 42 f1 ff ff       	jmp    80106b80 <alltraps>

80107a3e <vector205>:
.globl vector205
vector205:
  pushl $0
80107a3e:	6a 00                	push   $0x0
  pushl $205
80107a40:	68 cd 00 00 00       	push   $0xcd
  jmp alltraps
80107a45:	e9 36 f1 ff ff       	jmp    80106b80 <alltraps>

80107a4a <vector206>:
.globl vector206
vector206:
  pushl $0
80107a4a:	6a 00                	push   $0x0
  pushl $206
80107a4c:	68 ce 00 00 00       	push   $0xce
  jmp alltraps
80107a51:	e9 2a f1 ff ff       	jmp    80106b80 <alltraps>

80107a56 <vector207>:
.globl vector207
vector207:
  pushl $0
80107a56:	6a 00                	push   $0x0
  pushl $207
80107a58:	68 cf 00 00 00       	push   $0xcf
  jmp alltraps
80107a5d:	e9 1e f1 ff ff       	jmp    80106b80 <alltraps>

80107a62 <vector208>:
.globl vector208
vector208:
  pushl $0
80107a62:	6a 00                	push   $0x0
  pushl $208
80107a64:	68 d0 00 00 00       	push   $0xd0
  jmp alltraps
80107a69:	e9 12 f1 ff ff       	jmp    80106b80 <alltraps>

80107a6e <vector209>:
.globl vector209
vector209:
  pushl $0
80107a6e:	6a 00                	push   $0x0
  pushl $209
80107a70:	68 d1 00 00 00       	push   $0xd1
  jmp alltraps
80107a75:	e9 06 f1 ff ff       	jmp    80106b80 <alltraps>

80107a7a <vector210>:
.globl vector210
vector210:
  pushl $0
80107a7a:	6a 00                	push   $0x0
  pushl $210
80107a7c:	68 d2 00 00 00       	push   $0xd2
  jmp alltraps
80107a81:	e9 fa f0 ff ff       	jmp    80106b80 <alltraps>

80107a86 <vector211>:
.globl vector211
vector211:
  pushl $0
80107a86:	6a 00                	push   $0x0
  pushl $211
80107a88:	68 d3 00 00 00       	push   $0xd3
  jmp alltraps
80107a8d:	e9 ee f0 ff ff       	jmp    80106b80 <alltraps>

80107a92 <vector212>:
.globl vector212
vector212:
  pushl $0
80107a92:	6a 00                	push   $0x0
  pushl $212
80107a94:	68 d4 00 00 00       	push   $0xd4
  jmp alltraps
80107a99:	e9 e2 f0 ff ff       	jmp    80106b80 <alltraps>

80107a9e <vector213>:
.globl vector213
vector213:
  pushl $0
80107a9e:	6a 00                	push   $0x0
  pushl $213
80107aa0:	68 d5 00 00 00       	push   $0xd5
  jmp alltraps
80107aa5:	e9 d6 f0 ff ff       	jmp    80106b80 <alltraps>

80107aaa <vector214>:
.globl vector214
vector214:
  pushl $0
80107aaa:	6a 00                	push   $0x0
  pushl $214
80107aac:	68 d6 00 00 00       	push   $0xd6
  jmp alltraps
80107ab1:	e9 ca f0 ff ff       	jmp    80106b80 <alltraps>

80107ab6 <vector215>:
.globl vector215
vector215:
  pushl $0
80107ab6:	6a 00                	push   $0x0
  pushl $215
80107ab8:	68 d7 00 00 00       	push   $0xd7
  jmp alltraps
80107abd:	e9 be f0 ff ff       	jmp    80106b80 <alltraps>

80107ac2 <vector216>:
.globl vector216
vector216:
  pushl $0
80107ac2:	6a 00                	push   $0x0
  pushl $216
80107ac4:	68 d8 00 00 00       	push   $0xd8
  jmp alltraps
80107ac9:	e9 b2 f0 ff ff       	jmp    80106b80 <alltraps>

80107ace <vector217>:
.globl vector217
vector217:
  pushl $0
80107ace:	6a 00                	push   $0x0
  pushl $217
80107ad0:	68 d9 00 00 00       	push   $0xd9
  jmp alltraps
80107ad5:	e9 a6 f0 ff ff       	jmp    80106b80 <alltraps>

80107ada <vector218>:
.globl vector218
vector218:
  pushl $0
80107ada:	6a 00                	push   $0x0
  pushl $218
80107adc:	68 da 00 00 00       	push   $0xda
  jmp alltraps
80107ae1:	e9 9a f0 ff ff       	jmp    80106b80 <alltraps>

80107ae6 <vector219>:
.globl vector219
vector219:
  pushl $0
80107ae6:	6a 00                	push   $0x0
  pushl $219
80107ae8:	68 db 00 00 00       	push   $0xdb
  jmp alltraps
80107aed:	e9 8e f0 ff ff       	jmp    80106b80 <alltraps>

80107af2 <vector220>:
.globl vector220
vector220:
  pushl $0
80107af2:	6a 00                	push   $0x0
  pushl $220
80107af4:	68 dc 00 00 00       	push   $0xdc
  jmp alltraps
80107af9:	e9 82 f0 ff ff       	jmp    80106b80 <alltraps>

80107afe <vector221>:
.globl vector221
vector221:
  pushl $0
80107afe:	6a 00                	push   $0x0
  pushl $221
80107b00:	68 dd 00 00 00       	push   $0xdd
  jmp alltraps
80107b05:	e9 76 f0 ff ff       	jmp    80106b80 <alltraps>

80107b0a <vector222>:
.globl vector222
vector222:
  pushl $0
80107b0a:	6a 00                	push   $0x0
  pushl $222
80107b0c:	68 de 00 00 00       	push   $0xde
  jmp alltraps
80107b11:	e9 6a f0 ff ff       	jmp    80106b80 <alltraps>

80107b16 <vector223>:
.globl vector223
vector223:
  pushl $0
80107b16:	6a 00                	push   $0x0
  pushl $223
80107b18:	68 df 00 00 00       	push   $0xdf
  jmp alltraps
80107b1d:	e9 5e f0 ff ff       	jmp    80106b80 <alltraps>

80107b22 <vector224>:
.globl vector224
vector224:
  pushl $0
80107b22:	6a 00                	push   $0x0
  pushl $224
80107b24:	68 e0 00 00 00       	push   $0xe0
  jmp alltraps
80107b29:	e9 52 f0 ff ff       	jmp    80106b80 <alltraps>

80107b2e <vector225>:
.globl vector225
vector225:
  pushl $0
80107b2e:	6a 00                	push   $0x0
  pushl $225
80107b30:	68 e1 00 00 00       	push   $0xe1
  jmp alltraps
80107b35:	e9 46 f0 ff ff       	jmp    80106b80 <alltraps>

80107b3a <vector226>:
.globl vector226
vector226:
  pushl $0
80107b3a:	6a 00                	push   $0x0
  pushl $226
80107b3c:	68 e2 00 00 00       	push   $0xe2
  jmp alltraps
80107b41:	e9 3a f0 ff ff       	jmp    80106b80 <alltraps>

80107b46 <vector227>:
.globl vector227
vector227:
  pushl $0
80107b46:	6a 00                	push   $0x0
  pushl $227
80107b48:	68 e3 00 00 00       	push   $0xe3
  jmp alltraps
80107b4d:	e9 2e f0 ff ff       	jmp    80106b80 <alltraps>

80107b52 <vector228>:
.globl vector228
vector228:
  pushl $0
80107b52:	6a 00                	push   $0x0
  pushl $228
80107b54:	68 e4 00 00 00       	push   $0xe4
  jmp alltraps
80107b59:	e9 22 f0 ff ff       	jmp    80106b80 <alltraps>

80107b5e <vector229>:
.globl vector229
vector229:
  pushl $0
80107b5e:	6a 00                	push   $0x0
  pushl $229
80107b60:	68 e5 00 00 00       	push   $0xe5
  jmp alltraps
80107b65:	e9 16 f0 ff ff       	jmp    80106b80 <alltraps>

80107b6a <vector230>:
.globl vector230
vector230:
  pushl $0
80107b6a:	6a 00                	push   $0x0
  pushl $230
80107b6c:	68 e6 00 00 00       	push   $0xe6
  jmp alltraps
80107b71:	e9 0a f0 ff ff       	jmp    80106b80 <alltraps>

80107b76 <vector231>:
.globl vector231
vector231:
  pushl $0
80107b76:	6a 00                	push   $0x0
  pushl $231
80107b78:	68 e7 00 00 00       	push   $0xe7
  jmp alltraps
80107b7d:	e9 fe ef ff ff       	jmp    80106b80 <alltraps>

80107b82 <vector232>:
.globl vector232
vector232:
  pushl $0
80107b82:	6a 00                	push   $0x0
  pushl $232
80107b84:	68 e8 00 00 00       	push   $0xe8
  jmp alltraps
80107b89:	e9 f2 ef ff ff       	jmp    80106b80 <alltraps>

80107b8e <vector233>:
.globl vector233
vector233:
  pushl $0
80107b8e:	6a 00                	push   $0x0
  pushl $233
80107b90:	68 e9 00 00 00       	push   $0xe9
  jmp alltraps
80107b95:	e9 e6 ef ff ff       	jmp    80106b80 <alltraps>

80107b9a <vector234>:
.globl vector234
vector234:
  pushl $0
80107b9a:	6a 00                	push   $0x0
  pushl $234
80107b9c:	68 ea 00 00 00       	push   $0xea
  jmp alltraps
80107ba1:	e9 da ef ff ff       	jmp    80106b80 <alltraps>

80107ba6 <vector235>:
.globl vector235
vector235:
  pushl $0
80107ba6:	6a 00                	push   $0x0
  pushl $235
80107ba8:	68 eb 00 00 00       	push   $0xeb
  jmp alltraps
80107bad:	e9 ce ef ff ff       	jmp    80106b80 <alltraps>

80107bb2 <vector236>:
.globl vector236
vector236:
  pushl $0
80107bb2:	6a 00                	push   $0x0
  pushl $236
80107bb4:	68 ec 00 00 00       	push   $0xec
  jmp alltraps
80107bb9:	e9 c2 ef ff ff       	jmp    80106b80 <alltraps>

80107bbe <vector237>:
.globl vector237
vector237:
  pushl $0
80107bbe:	6a 00                	push   $0x0
  pushl $237
80107bc0:	68 ed 00 00 00       	push   $0xed
  jmp alltraps
80107bc5:	e9 b6 ef ff ff       	jmp    80106b80 <alltraps>

80107bca <vector238>:
.globl vector238
vector238:
  pushl $0
80107bca:	6a 00                	push   $0x0
  pushl $238
80107bcc:	68 ee 00 00 00       	push   $0xee
  jmp alltraps
80107bd1:	e9 aa ef ff ff       	jmp    80106b80 <alltraps>

80107bd6 <vector239>:
.globl vector239
vector239:
  pushl $0
80107bd6:	6a 00                	push   $0x0
  pushl $239
80107bd8:	68 ef 00 00 00       	push   $0xef
  jmp alltraps
80107bdd:	e9 9e ef ff ff       	jmp    80106b80 <alltraps>

80107be2 <vector240>:
.globl vector240
vector240:
  pushl $0
80107be2:	6a 00                	push   $0x0
  pushl $240
80107be4:	68 f0 00 00 00       	push   $0xf0
  jmp alltraps
80107be9:	e9 92 ef ff ff       	jmp    80106b80 <alltraps>

80107bee <vector241>:
.globl vector241
vector241:
  pushl $0
80107bee:	6a 00                	push   $0x0
  pushl $241
80107bf0:	68 f1 00 00 00       	push   $0xf1
  jmp alltraps
80107bf5:	e9 86 ef ff ff       	jmp    80106b80 <alltraps>

80107bfa <vector242>:
.globl vector242
vector242:
  pushl $0
80107bfa:	6a 00                	push   $0x0
  pushl $242
80107bfc:	68 f2 00 00 00       	push   $0xf2
  jmp alltraps
80107c01:	e9 7a ef ff ff       	jmp    80106b80 <alltraps>

80107c06 <vector243>:
.globl vector243
vector243:
  pushl $0
80107c06:	6a 00                	push   $0x0
  pushl $243
80107c08:	68 f3 00 00 00       	push   $0xf3
  jmp alltraps
80107c0d:	e9 6e ef ff ff       	jmp    80106b80 <alltraps>

80107c12 <vector244>:
.globl vector244
vector244:
  pushl $0
80107c12:	6a 00                	push   $0x0
  pushl $244
80107c14:	68 f4 00 00 00       	push   $0xf4
  jmp alltraps
80107c19:	e9 62 ef ff ff       	jmp    80106b80 <alltraps>

80107c1e <vector245>:
.globl vector245
vector245:
  pushl $0
80107c1e:	6a 00                	push   $0x0
  pushl $245
80107c20:	68 f5 00 00 00       	push   $0xf5
  jmp alltraps
80107c25:	e9 56 ef ff ff       	jmp    80106b80 <alltraps>

80107c2a <vector246>:
.globl vector246
vector246:
  pushl $0
80107c2a:	6a 00                	push   $0x0
  pushl $246
80107c2c:	68 f6 00 00 00       	push   $0xf6
  jmp alltraps
80107c31:	e9 4a ef ff ff       	jmp    80106b80 <alltraps>

80107c36 <vector247>:
.globl vector247
vector247:
  pushl $0
80107c36:	6a 00                	push   $0x0
  pushl $247
80107c38:	68 f7 00 00 00       	push   $0xf7
  jmp alltraps
80107c3d:	e9 3e ef ff ff       	jmp    80106b80 <alltraps>

80107c42 <vector248>:
.globl vector248
vector248:
  pushl $0
80107c42:	6a 00                	push   $0x0
  pushl $248
80107c44:	68 f8 00 00 00       	push   $0xf8
  jmp alltraps
80107c49:	e9 32 ef ff ff       	jmp    80106b80 <alltraps>

80107c4e <vector249>:
.globl vector249
vector249:
  pushl $0
80107c4e:	6a 00                	push   $0x0
  pushl $249
80107c50:	68 f9 00 00 00       	push   $0xf9
  jmp alltraps
80107c55:	e9 26 ef ff ff       	jmp    80106b80 <alltraps>

80107c5a <vector250>:
.globl vector250
vector250:
  pushl $0
80107c5a:	6a 00                	push   $0x0
  pushl $250
80107c5c:	68 fa 00 00 00       	push   $0xfa
  jmp alltraps
80107c61:	e9 1a ef ff ff       	jmp    80106b80 <alltraps>

80107c66 <vector251>:
.globl vector251
vector251:
  pushl $0
80107c66:	6a 00                	push   $0x0
  pushl $251
80107c68:	68 fb 00 00 00       	push   $0xfb
  jmp alltraps
80107c6d:	e9 0e ef ff ff       	jmp    80106b80 <alltraps>

80107c72 <vector252>:
.globl vector252
vector252:
  pushl $0
80107c72:	6a 00                	push   $0x0
  pushl $252
80107c74:	68 fc 00 00 00       	push   $0xfc
  jmp alltraps
80107c79:	e9 02 ef ff ff       	jmp    80106b80 <alltraps>

80107c7e <vector253>:
.globl vector253
vector253:
  pushl $0
80107c7e:	6a 00                	push   $0x0
  pushl $253
80107c80:	68 fd 00 00 00       	push   $0xfd
  jmp alltraps
80107c85:	e9 f6 ee ff ff       	jmp    80106b80 <alltraps>

80107c8a <vector254>:
.globl vector254
vector254:
  pushl $0
80107c8a:	6a 00                	push   $0x0
  pushl $254
80107c8c:	68 fe 00 00 00       	push   $0xfe
  jmp alltraps
80107c91:	e9 ea ee ff ff       	jmp    80106b80 <alltraps>

80107c96 <vector255>:
.globl vector255
vector255:
  pushl $0
80107c96:	6a 00                	push   $0x0
  pushl $255
80107c98:	68 ff 00 00 00       	push   $0xff
  jmp alltraps
80107c9d:	e9 de ee ff ff       	jmp    80106b80 <alltraps>
	...

80107ca4 <lgdt>:

struct segdesc;

static inline void
lgdt(struct segdesc *p, int size)
{
80107ca4:	55                   	push   %ebp
80107ca5:	89 e5                	mov    %esp,%ebp
80107ca7:	83 ec 10             	sub    $0x10,%esp
  volatile ushort pd[3];

  pd[0] = size-1;
80107caa:	8b 45 0c             	mov    0xc(%ebp),%eax
80107cad:	83 e8 01             	sub    $0x1,%eax
80107cb0:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
  pd[1] = (uint)p;
80107cb4:	8b 45 08             	mov    0x8(%ebp),%eax
80107cb7:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
80107cbb:	8b 45 08             	mov    0x8(%ebp),%eax
80107cbe:	c1 e8 10             	shr    $0x10,%eax
80107cc1:	66 89 45 fe          	mov    %ax,-0x2(%ebp)

  asm volatile("lgdt (%0)" : : "r" (pd));
80107cc5:	8d 45 fa             	lea    -0x6(%ebp),%eax
80107cc8:	0f 01 10             	lgdtl  (%eax)
}
80107ccb:	c9                   	leave  
80107ccc:	c3                   	ret    

80107ccd <ltr>:
  asm volatile("lidt (%0)" : : "r" (pd));
}

static inline void
ltr(ushort sel)
{
80107ccd:	55                   	push   %ebp
80107cce:	89 e5                	mov    %esp,%ebp
80107cd0:	83 ec 04             	sub    $0x4,%esp
80107cd3:	8b 45 08             	mov    0x8(%ebp),%eax
80107cd6:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  asm volatile("ltr %0" : : "r" (sel));
80107cda:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80107cde:	0f 00 d8             	ltr    %ax
}
80107ce1:	c9                   	leave  
80107ce2:	c3                   	ret    

80107ce3 <loadgs>:
  return eflags;
}

static inline void
loadgs(ushort v)
{
80107ce3:	55                   	push   %ebp
80107ce4:	89 e5                	mov    %esp,%ebp
80107ce6:	83 ec 04             	sub    $0x4,%esp
80107ce9:	8b 45 08             	mov    0x8(%ebp),%eax
80107cec:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  asm volatile("movw %0, %%gs" : : "r" (v));
80107cf0:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80107cf4:	8e e8                	mov    %eax,%gs
}
80107cf6:	c9                   	leave  
80107cf7:	c3                   	ret    

80107cf8 <lcr3>:
  return val;
}

static inline void
lcr3(uint val)
{
80107cf8:	55                   	push   %ebp
80107cf9:	89 e5                	mov    %esp,%ebp
  asm volatile("movl %0,%%cr3" : : "r" (val));
80107cfb:	8b 45 08             	mov    0x8(%ebp),%eax
80107cfe:	0f 22 d8             	mov    %eax,%cr3
}
80107d01:	5d                   	pop    %ebp
80107d02:	c3                   	ret    

80107d03 <seginit>:

// Set up CPU's kernel segment descriptors.
// Run once on entry on each CPU.
void
seginit(void)
{
80107d03:	55                   	push   %ebp
80107d04:	89 e5                	mov    %esp,%ebp
80107d06:	53                   	push   %ebx
80107d07:	83 ec 24             	sub    $0x24,%esp

  // Map "logical" addresses to virtual addresses using identity map.
  // Cannot share a CODE descriptor for both kernel and user
  // because it would have to have DPL_USR, but the CPU forbids
  // an interrupt from CPL=0 to DPL=3.
  c = &cpus[cpunum()];
80107d0a:	e8 eb b9 ff ff       	call   801036fa <cpunum>
80107d0f:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
80107d15:	05 80 3b 11 80       	add    $0x80113b80,%eax
80107d1a:	89 45 f4             	mov    %eax,-0xc(%ebp)
  c->gdt[SEG_KCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, 0);
80107d1d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107d20:	66 c7 40 78 ff ff    	movw   $0xffff,0x78(%eax)
80107d26:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107d29:	66 c7 40 7a 00 00    	movw   $0x0,0x7a(%eax)
80107d2f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107d32:	c6 40 7c 00          	movb   $0x0,0x7c(%eax)
80107d36:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107d39:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80107d3d:	83 e2 f0             	and    $0xfffffff0,%edx
80107d40:	83 ca 0a             	or     $0xa,%edx
80107d43:	88 50 7d             	mov    %dl,0x7d(%eax)
80107d46:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107d49:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80107d4d:	83 ca 10             	or     $0x10,%edx
80107d50:	88 50 7d             	mov    %dl,0x7d(%eax)
80107d53:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107d56:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80107d5a:	83 e2 9f             	and    $0xffffff9f,%edx
80107d5d:	88 50 7d             	mov    %dl,0x7d(%eax)
80107d60:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107d63:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80107d67:	83 ca 80             	or     $0xffffff80,%edx
80107d6a:	88 50 7d             	mov    %dl,0x7d(%eax)
80107d6d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107d70:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
80107d74:	83 ca 0f             	or     $0xf,%edx
80107d77:	88 50 7e             	mov    %dl,0x7e(%eax)
80107d7a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107d7d:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
80107d81:	83 e2 ef             	and    $0xffffffef,%edx
80107d84:	88 50 7e             	mov    %dl,0x7e(%eax)
80107d87:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107d8a:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
80107d8e:	83 e2 df             	and    $0xffffffdf,%edx
80107d91:	88 50 7e             	mov    %dl,0x7e(%eax)
80107d94:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107d97:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
80107d9b:	83 ca 40             	or     $0x40,%edx
80107d9e:	88 50 7e             	mov    %dl,0x7e(%eax)
80107da1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107da4:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
80107da8:	83 ca 80             	or     $0xffffff80,%edx
80107dab:	88 50 7e             	mov    %dl,0x7e(%eax)
80107dae:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107db1:	c6 40 7f 00          	movb   $0x0,0x7f(%eax)
  c->gdt[SEG_KDATA] = SEG(STA_W, 0, 0xffffffff, 0);
80107db5:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107db8:	66 c7 80 80 00 00 00 	movw   $0xffff,0x80(%eax)
80107dbf:	ff ff 
80107dc1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107dc4:	66 c7 80 82 00 00 00 	movw   $0x0,0x82(%eax)
80107dcb:	00 00 
80107dcd:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107dd0:	c6 80 84 00 00 00 00 	movb   $0x0,0x84(%eax)
80107dd7:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107dda:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
80107de1:	83 e2 f0             	and    $0xfffffff0,%edx
80107de4:	83 ca 02             	or     $0x2,%edx
80107de7:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
80107ded:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107df0:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
80107df7:	83 ca 10             	or     $0x10,%edx
80107dfa:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
80107e00:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107e03:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
80107e0a:	83 e2 9f             	and    $0xffffff9f,%edx
80107e0d:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
80107e13:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107e16:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
80107e1d:	83 ca 80             	or     $0xffffff80,%edx
80107e20:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
80107e26:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107e29:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
80107e30:	83 ca 0f             	or     $0xf,%edx
80107e33:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
80107e39:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107e3c:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
80107e43:	83 e2 ef             	and    $0xffffffef,%edx
80107e46:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
80107e4c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107e4f:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
80107e56:	83 e2 df             	and    $0xffffffdf,%edx
80107e59:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
80107e5f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107e62:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
80107e69:	83 ca 40             	or     $0x40,%edx
80107e6c:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
80107e72:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107e75:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
80107e7c:	83 ca 80             	or     $0xffffff80,%edx
80107e7f:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
80107e85:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107e88:	c6 80 87 00 00 00 00 	movb   $0x0,0x87(%eax)
  c->gdt[SEG_UCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, DPL_USER);
80107e8f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107e92:	66 c7 80 90 00 00 00 	movw   $0xffff,0x90(%eax)
80107e99:	ff ff 
80107e9b:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107e9e:	66 c7 80 92 00 00 00 	movw   $0x0,0x92(%eax)
80107ea5:	00 00 
80107ea7:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107eaa:	c6 80 94 00 00 00 00 	movb   $0x0,0x94(%eax)
80107eb1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107eb4:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
80107ebb:	83 e2 f0             	and    $0xfffffff0,%edx
80107ebe:	83 ca 0a             	or     $0xa,%edx
80107ec1:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
80107ec7:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107eca:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
80107ed1:	83 ca 10             	or     $0x10,%edx
80107ed4:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
80107eda:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107edd:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
80107ee4:	83 ca 60             	or     $0x60,%edx
80107ee7:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
80107eed:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107ef0:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
80107ef7:	83 ca 80             	or     $0xffffff80,%edx
80107efa:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
80107f00:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107f03:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80107f0a:	83 ca 0f             	or     $0xf,%edx
80107f0d:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80107f13:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107f16:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80107f1d:	83 e2 ef             	and    $0xffffffef,%edx
80107f20:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80107f26:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107f29:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80107f30:	83 e2 df             	and    $0xffffffdf,%edx
80107f33:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80107f39:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107f3c:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80107f43:	83 ca 40             	or     $0x40,%edx
80107f46:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80107f4c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107f4f:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80107f56:	83 ca 80             	or     $0xffffff80,%edx
80107f59:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80107f5f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107f62:	c6 80 97 00 00 00 00 	movb   $0x0,0x97(%eax)
  c->gdt[SEG_UDATA] = SEG(STA_W, 0, 0xffffffff, DPL_USER);
80107f69:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107f6c:	66 c7 80 98 00 00 00 	movw   $0xffff,0x98(%eax)
80107f73:	ff ff 
80107f75:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107f78:	66 c7 80 9a 00 00 00 	movw   $0x0,0x9a(%eax)
80107f7f:	00 00 
80107f81:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107f84:	c6 80 9c 00 00 00 00 	movb   $0x0,0x9c(%eax)
80107f8b:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107f8e:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
80107f95:	83 e2 f0             	and    $0xfffffff0,%edx
80107f98:	83 ca 02             	or     $0x2,%edx
80107f9b:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
80107fa1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107fa4:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
80107fab:	83 ca 10             	or     $0x10,%edx
80107fae:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
80107fb4:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107fb7:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
80107fbe:	83 ca 60             	or     $0x60,%edx
80107fc1:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
80107fc7:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107fca:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
80107fd1:	83 ca 80             	or     $0xffffff80,%edx
80107fd4:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
80107fda:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107fdd:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
80107fe4:	83 ca 0f             	or     $0xf,%edx
80107fe7:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80107fed:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107ff0:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
80107ff7:	83 e2 ef             	and    $0xffffffef,%edx
80107ffa:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80108000:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108003:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
8010800a:	83 e2 df             	and    $0xffffffdf,%edx
8010800d:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80108013:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108016:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
8010801d:	83 ca 40             	or     $0x40,%edx
80108020:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80108026:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108029:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
80108030:	83 ca 80             	or     $0xffffff80,%edx
80108033:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80108039:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010803c:	c6 80 9f 00 00 00 00 	movb   $0x0,0x9f(%eax)

  // Map cpu, and curproc
  c->gdt[SEG_KCPU] = SEG(STA_W, &c->cpu, 8, 0);
80108043:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108046:	05 b4 00 00 00       	add    $0xb4,%eax
8010804b:	89 c3                	mov    %eax,%ebx
8010804d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108050:	05 b4 00 00 00       	add    $0xb4,%eax
80108055:	c1 e8 10             	shr    $0x10,%eax
80108058:	89 c1                	mov    %eax,%ecx
8010805a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010805d:	05 b4 00 00 00       	add    $0xb4,%eax
80108062:	c1 e8 18             	shr    $0x18,%eax
80108065:	89 c2                	mov    %eax,%edx
80108067:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010806a:	66 c7 80 88 00 00 00 	movw   $0x0,0x88(%eax)
80108071:	00 00 
80108073:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108076:	66 89 98 8a 00 00 00 	mov    %bx,0x8a(%eax)
8010807d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108080:	88 88 8c 00 00 00    	mov    %cl,0x8c(%eax)
80108086:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108089:	0f b6 88 8d 00 00 00 	movzbl 0x8d(%eax),%ecx
80108090:	83 e1 f0             	and    $0xfffffff0,%ecx
80108093:	83 c9 02             	or     $0x2,%ecx
80108096:	88 88 8d 00 00 00    	mov    %cl,0x8d(%eax)
8010809c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010809f:	0f b6 88 8d 00 00 00 	movzbl 0x8d(%eax),%ecx
801080a6:	83 c9 10             	or     $0x10,%ecx
801080a9:	88 88 8d 00 00 00    	mov    %cl,0x8d(%eax)
801080af:	8b 45 f4             	mov    -0xc(%ebp),%eax
801080b2:	0f b6 88 8d 00 00 00 	movzbl 0x8d(%eax),%ecx
801080b9:	83 e1 9f             	and    $0xffffff9f,%ecx
801080bc:	88 88 8d 00 00 00    	mov    %cl,0x8d(%eax)
801080c2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801080c5:	0f b6 88 8d 00 00 00 	movzbl 0x8d(%eax),%ecx
801080cc:	83 c9 80             	or     $0xffffff80,%ecx
801080cf:	88 88 8d 00 00 00    	mov    %cl,0x8d(%eax)
801080d5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801080d8:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
801080df:	83 e1 f0             	and    $0xfffffff0,%ecx
801080e2:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
801080e8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801080eb:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
801080f2:	83 e1 ef             	and    $0xffffffef,%ecx
801080f5:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
801080fb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801080fe:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
80108105:	83 e1 df             	and    $0xffffffdf,%ecx
80108108:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
8010810e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108111:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
80108118:	83 c9 40             	or     $0x40,%ecx
8010811b:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
80108121:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108124:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
8010812b:	83 c9 80             	or     $0xffffff80,%ecx
8010812e:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
80108134:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108137:	88 90 8f 00 00 00    	mov    %dl,0x8f(%eax)

  lgdt(c->gdt, sizeof(c->gdt));
8010813d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108140:	83 c0 70             	add    $0x70,%eax
80108143:	c7 44 24 04 38 00 00 	movl   $0x38,0x4(%esp)
8010814a:	00 
8010814b:	89 04 24             	mov    %eax,(%esp)
8010814e:	e8 51 fb ff ff       	call   80107ca4 <lgdt>
  loadgs(SEG_KCPU << 3);
80108153:	c7 04 24 18 00 00 00 	movl   $0x18,(%esp)
8010815a:	e8 84 fb ff ff       	call   80107ce3 <loadgs>

  // Initialize cpu-local storage.
  cpu = c;
8010815f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108162:	65 a3 00 00 00 00    	mov    %eax,%gs:0x0
  proc = 0;
80108168:	65 c7 05 04 00 00 00 	movl   $0x0,%gs:0x4
8010816f:	00 00 00 00 
}
80108173:	83 c4 24             	add    $0x24,%esp
80108176:	5b                   	pop    %ebx
80108177:	5d                   	pop    %ebp
80108178:	c3                   	ret    

80108179 <walkpgdir>:
// Return the address of the PTE in page table pgdir
// that corresponds to virtual address va.  If alloc!=0,
// create any required page table pages.
static pte_t *
walkpgdir(pde_t *pgdir, const void *va, int alloc)
{
80108179:	55                   	push   %ebp
8010817a:	89 e5                	mov    %esp,%ebp
8010817c:	83 ec 28             	sub    $0x28,%esp
  pde_t *pde;
  pte_t *pgtab;

  pde = &pgdir[PDX(va)];
8010817f:	8b 45 0c             	mov    0xc(%ebp),%eax
80108182:	c1 e8 16             	shr    $0x16,%eax
80108185:	c1 e0 02             	shl    $0x2,%eax
80108188:	03 45 08             	add    0x8(%ebp),%eax
8010818b:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(*pde & PTE_P){
8010818e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108191:	8b 00                	mov    (%eax),%eax
80108193:	83 e0 01             	and    $0x1,%eax
80108196:	84 c0                	test   %al,%al
80108198:	74 14                	je     801081ae <walkpgdir+0x35>
    pgtab = (pte_t*)P2V(PTE_ADDR(*pde));
8010819a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010819d:	8b 00                	mov    (%eax),%eax
8010819f:	25 00 f0 ff ff       	and    $0xfffff000,%eax
801081a4:	2d 00 00 00 80       	sub    $0x80000000,%eax
801081a9:	89 45 f4             	mov    %eax,-0xc(%ebp)
801081ac:	eb 48                	jmp    801081f6 <walkpgdir+0x7d>
  } else {
    if(!alloc || (pgtab = (pte_t*)kalloc()) == 0)
801081ae:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
801081b2:	74 0e                	je     801081c2 <walkpgdir+0x49>
801081b4:	e8 e5 b1 ff ff       	call   8010339e <kalloc>
801081b9:	89 45 f4             	mov    %eax,-0xc(%ebp)
801081bc:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801081c0:	75 07                	jne    801081c9 <walkpgdir+0x50>
      return 0;
801081c2:	b8 00 00 00 00       	mov    $0x0,%eax
801081c7:	eb 3e                	jmp    80108207 <walkpgdir+0x8e>
    // Make sure all those PTE_P bits are zero.
    memset(pgtab, 0, PGSIZE);
801081c9:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
801081d0:	00 
801081d1:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801081d8:	00 
801081d9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801081dc:	89 04 24             	mov    %eax,(%esp)
801081df:	e8 86 d5 ff ff       	call   8010576a <memset>
    // The permissions here are overly generous, but they can
    // be further restricted by the permissions in the page table
    // entries, if necessary.
    *pde = V2P(pgtab) | PTE_P | PTE_W | PTE_U;
801081e4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801081e7:	2d 00 00 00 80       	sub    $0x80000000,%eax
801081ec:	89 c2                	mov    %eax,%edx
801081ee:	83 ca 07             	or     $0x7,%edx
801081f1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801081f4:	89 10                	mov    %edx,(%eax)
  }
  return &pgtab[PTX(va)];
801081f6:	8b 45 0c             	mov    0xc(%ebp),%eax
801081f9:	c1 e8 0c             	shr    $0xc,%eax
801081fc:	25 ff 03 00 00       	and    $0x3ff,%eax
80108201:	c1 e0 02             	shl    $0x2,%eax
80108204:	03 45 f4             	add    -0xc(%ebp),%eax
}
80108207:	c9                   	leave  
80108208:	c3                   	ret    

80108209 <mappages>:
// Create PTEs for virtual addresses starting at va that refer to
// physical addresses starting at pa. va and size might not
// be page-aligned.
static int
mappages(pde_t *pgdir, void *va, uint size, uint pa, int perm)
{
80108209:	55                   	push   %ebp
8010820a:	89 e5                	mov    %esp,%ebp
8010820c:	83 ec 28             	sub    $0x28,%esp
  char *a, *last;
  pte_t *pte;

  a = (char*)PGROUNDDOWN((uint)va);
8010820f:	8b 45 0c             	mov    0xc(%ebp),%eax
80108212:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108217:	89 45 ec             	mov    %eax,-0x14(%ebp)
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
8010821a:	8b 45 0c             	mov    0xc(%ebp),%eax
8010821d:	03 45 10             	add    0x10(%ebp),%eax
80108220:	83 e8 01             	sub    $0x1,%eax
80108223:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108228:	89 45 f0             	mov    %eax,-0x10(%ebp)
  for(;;){
    if((pte = walkpgdir(pgdir, a, 1)) == 0)
8010822b:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
80108232:	00 
80108233:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108236:	89 44 24 04          	mov    %eax,0x4(%esp)
8010823a:	8b 45 08             	mov    0x8(%ebp),%eax
8010823d:	89 04 24             	mov    %eax,(%esp)
80108240:	e8 34 ff ff ff       	call   80108179 <walkpgdir>
80108245:	89 45 f4             	mov    %eax,-0xc(%ebp)
80108248:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010824c:	75 07                	jne    80108255 <mappages+0x4c>
      return -1;
8010824e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80108253:	eb 46                	jmp    8010829b <mappages+0x92>
    if(*pte & PTE_P)
80108255:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108258:	8b 00                	mov    (%eax),%eax
8010825a:	83 e0 01             	and    $0x1,%eax
8010825d:	84 c0                	test   %al,%al
8010825f:	74 0c                	je     8010826d <mappages+0x64>
      panic("remap");
80108261:	c7 04 24 c0 92 10 80 	movl   $0x801092c0,(%esp)
80108268:	e8 fc 85 ff ff       	call   80100869 <panic>
    *pte = pa | perm | PTE_P;
8010826d:	8b 45 18             	mov    0x18(%ebp),%eax
80108270:	0b 45 14             	or     0x14(%ebp),%eax
80108273:	89 c2                	mov    %eax,%edx
80108275:	83 ca 01             	or     $0x1,%edx
80108278:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010827b:	89 10                	mov    %edx,(%eax)
    if(a == last)
8010827d:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108280:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80108283:	74 10                	je     80108295 <mappages+0x8c>
      break;
    a += PGSIZE;
80108285:	81 45 ec 00 10 00 00 	addl   $0x1000,-0x14(%ebp)
    pa += PGSIZE;
8010828c:	81 45 14 00 10 00 00 	addl   $0x1000,0x14(%ebp)
  }
80108293:	eb 96                	jmp    8010822b <mappages+0x22>
      return -1;
    if(*pte & PTE_P)
      panic("remap");
    *pte = pa | perm | PTE_P;
    if(a == last)
      break;
80108295:	90                   	nop
    a += PGSIZE;
    pa += PGSIZE;
  }
  return 0;
80108296:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010829b:	c9                   	leave  
8010829c:	c3                   	ret    

8010829d <setupkvm>:
};

// Set up kernel part of a page table.
pde_t*
setupkvm(void)
{
8010829d:	55                   	push   %ebp
8010829e:	89 e5                	mov    %esp,%ebp
801082a0:	53                   	push   %ebx
801082a1:	83 ec 34             	sub    $0x34,%esp
  pde_t *pgdir;
  struct kmap *k;

  if((pgdir = (pde_t*)kalloc()) == 0)
801082a4:	e8 f5 b0 ff ff       	call   8010339e <kalloc>
801082a9:	89 45 f0             	mov    %eax,-0x10(%ebp)
801082ac:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801082b0:	75 07                	jne    801082b9 <setupkvm+0x1c>
    return 0;
801082b2:	b8 00 00 00 00       	mov    $0x0,%eax
801082b7:	eb 7b                	jmp    80108334 <setupkvm+0x97>
  memset(pgdir, 0, PGSIZE);
801082b9:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
801082c0:	00 
801082c1:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801082c8:	00 
801082c9:	8b 45 f0             	mov    -0x10(%ebp),%eax
801082cc:	89 04 24             	mov    %eax,(%esp)
801082cf:	e8 96 d4 ff ff       	call   8010576a <memset>
  if(P2V(PHYSTOP) > (void*)DEVSPACE)
801082d4:	90                   	nop
    panic("setupkvm: PHYSTOP too high");
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
801082d5:	c7 45 f4 a0 c4 10 80 	movl   $0x8010c4a0,-0xc(%ebp)
801082dc:	eb 49                	jmp    80108327 <setupkvm+0x8a>
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start,
801082de:	8b 45 f4             	mov    -0xc(%ebp),%eax
801082e1:	8b 48 0c             	mov    0xc(%eax),%ecx
801082e4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801082e7:	8b 50 04             	mov    0x4(%eax),%edx
801082ea:	8b 45 f4             	mov    -0xc(%ebp),%eax
801082ed:	8b 58 08             	mov    0x8(%eax),%ebx
801082f0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801082f3:	8b 40 04             	mov    0x4(%eax),%eax
801082f6:	29 c3                	sub    %eax,%ebx
801082f8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801082fb:	8b 00                	mov    (%eax),%eax
801082fd:	89 4c 24 10          	mov    %ecx,0x10(%esp)
80108301:	89 54 24 0c          	mov    %edx,0xc(%esp)
80108305:	89 5c 24 08          	mov    %ebx,0x8(%esp)
80108309:	89 44 24 04          	mov    %eax,0x4(%esp)
8010830d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108310:	89 04 24             	mov    %eax,(%esp)
80108313:	e8 f1 fe ff ff       	call   80108209 <mappages>
80108318:	85 c0                	test   %eax,%eax
8010831a:	79 07                	jns    80108323 <setupkvm+0x86>
                (uint)k->phys_start, k->perm) < 0)
      return 0;
8010831c:	b8 00 00 00 00       	mov    $0x0,%eax
80108321:	eb 11                	jmp    80108334 <setupkvm+0x97>
  if((pgdir = (pde_t*)kalloc()) == 0)
    return 0;
  memset(pgdir, 0, PGSIZE);
  if(P2V(PHYSTOP) > (void*)DEVSPACE)
    panic("setupkvm: PHYSTOP too high");
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
80108323:	83 45 f4 10          	addl   $0x10,-0xc(%ebp)
80108327:	b8 e0 c4 10 80       	mov    $0x8010c4e0,%eax
8010832c:	39 45 f4             	cmp    %eax,-0xc(%ebp)
8010832f:	72 ad                	jb     801082de <setupkvm+0x41>
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start,
                (uint)k->phys_start, k->perm) < 0)
      return 0;
  return pgdir;
80108331:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
80108334:	83 c4 34             	add    $0x34,%esp
80108337:	5b                   	pop    %ebx
80108338:	5d                   	pop    %ebp
80108339:	c3                   	ret    

8010833a <kvmalloc>:

// Allocate one page table for the machine for the kernel address
// space for scheduler processes.
void
kvmalloc(void)
{
8010833a:	55                   	push   %ebp
8010833b:	89 e5                	mov    %esp,%ebp
8010833d:	83 ec 18             	sub    $0x18,%esp
  kpgdir = setupkvm();
80108340:	e8 58 ff ff ff       	call   8010829d <setupkvm>
80108345:	a3 64 ce 10 80       	mov    %eax,0x8010ce64
  if(kpgdir == 0)
8010834a:	a1 64 ce 10 80       	mov    0x8010ce64,%eax
8010834f:	85 c0                	test   %eax,%eax
80108351:	75 0c                	jne    8010835f <kvmalloc+0x25>
    panic("kvmalloc: could not create kernel page table");
80108353:	c7 04 24 c8 92 10 80 	movl   $0x801092c8,(%esp)
8010835a:	e8 0a 85 ff ff       	call   80100869 <panic>
  switchkvm();
8010835f:	e8 02 00 00 00       	call   80108366 <switchkvm>
}
80108364:	c9                   	leave  
80108365:	c3                   	ret    

80108366 <switchkvm>:

// Switch h/w page table register to the kernel-only page table,
// for when no process is running.
void
switchkvm(void)
{
80108366:	55                   	push   %ebp
80108367:	89 e5                	mov    %esp,%ebp
80108369:	83 ec 04             	sub    $0x4,%esp
  lcr3(V2P(kpgdir));   // switch to the kernel page table
8010836c:	a1 64 ce 10 80       	mov    0x8010ce64,%eax
80108371:	2d 00 00 00 80       	sub    $0x80000000,%eax
80108376:	89 04 24             	mov    %eax,(%esp)
80108379:	e8 7a f9 ff ff       	call   80107cf8 <lcr3>
}
8010837e:	c9                   	leave  
8010837f:	c3                   	ret    

80108380 <switchuvm>:

// Switch TSS and h/w page table to correspond to process p.
void
switchuvm(struct proc *p)
{
80108380:	55                   	push   %ebp
80108381:	89 e5                	mov    %esp,%ebp
80108383:	53                   	push   %ebx
80108384:	83 ec 14             	sub    $0x14,%esp
  pushcli();
80108387:	e8 d9 d2 ff ff       	call   80105665 <pushcli>
  cpu->gdt[SEG_TSS] = SEG16(STS_T32A, &cpu->ts, sizeof(cpu->ts)-1, 0);
8010838c:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80108392:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80108399:	83 c2 08             	add    $0x8,%edx
8010839c:	89 d3                	mov    %edx,%ebx
8010839e:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
801083a5:	83 c2 08             	add    $0x8,%edx
801083a8:	c1 ea 10             	shr    $0x10,%edx
801083ab:	89 d1                	mov    %edx,%ecx
801083ad:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
801083b4:	83 c2 08             	add    $0x8,%edx
801083b7:	c1 ea 18             	shr    $0x18,%edx
801083ba:	66 c7 80 a0 00 00 00 	movw   $0x67,0xa0(%eax)
801083c1:	67 00 
801083c3:	66 89 98 a2 00 00 00 	mov    %bx,0xa2(%eax)
801083ca:	88 88 a4 00 00 00    	mov    %cl,0xa4(%eax)
801083d0:	0f b6 88 a5 00 00 00 	movzbl 0xa5(%eax),%ecx
801083d7:	83 e1 f0             	and    $0xfffffff0,%ecx
801083da:	83 c9 09             	or     $0x9,%ecx
801083dd:	88 88 a5 00 00 00    	mov    %cl,0xa5(%eax)
801083e3:	0f b6 88 a5 00 00 00 	movzbl 0xa5(%eax),%ecx
801083ea:	83 c9 10             	or     $0x10,%ecx
801083ed:	88 88 a5 00 00 00    	mov    %cl,0xa5(%eax)
801083f3:	0f b6 88 a5 00 00 00 	movzbl 0xa5(%eax),%ecx
801083fa:	83 e1 9f             	and    $0xffffff9f,%ecx
801083fd:	88 88 a5 00 00 00    	mov    %cl,0xa5(%eax)
80108403:	0f b6 88 a5 00 00 00 	movzbl 0xa5(%eax),%ecx
8010840a:	83 c9 80             	or     $0xffffff80,%ecx
8010840d:	88 88 a5 00 00 00    	mov    %cl,0xa5(%eax)
80108413:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
8010841a:	83 e1 f0             	and    $0xfffffff0,%ecx
8010841d:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
80108423:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
8010842a:	83 e1 ef             	and    $0xffffffef,%ecx
8010842d:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
80108433:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
8010843a:	83 e1 df             	and    $0xffffffdf,%ecx
8010843d:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
80108443:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
8010844a:	83 c9 40             	or     $0x40,%ecx
8010844d:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
80108453:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
8010845a:	83 e1 7f             	and    $0x7f,%ecx
8010845d:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
80108463:	88 90 a7 00 00 00    	mov    %dl,0xa7(%eax)
  cpu->gdt[SEG_TSS].s = 0;
80108469:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010846f:	0f b6 90 a5 00 00 00 	movzbl 0xa5(%eax),%edx
80108476:	83 e2 ef             	and    $0xffffffef,%edx
80108479:	88 90 a5 00 00 00    	mov    %dl,0xa5(%eax)
  cpu->ts.ss0 = SEG_KDATA << 3;
8010847f:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80108485:	66 c7 40 10 10 00    	movw   $0x10,0x10(%eax)
  cpu->ts.esp0 = (uint)proc->kstack + KSTACKSIZE;
8010848b:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80108491:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80108498:	8b 52 08             	mov    0x8(%edx),%edx
8010849b:	81 c2 00 10 00 00    	add    $0x1000,%edx
801084a1:	89 50 0c             	mov    %edx,0xc(%eax)
  ltr(SEG_TSS << 3);
801084a4:	c7 04 24 30 00 00 00 	movl   $0x30,(%esp)
801084ab:	e8 1d f8 ff ff       	call   80107ccd <ltr>
  if(p->pgdir == 0)
801084b0:	8b 45 08             	mov    0x8(%ebp),%eax
801084b3:	8b 40 04             	mov    0x4(%eax),%eax
801084b6:	85 c0                	test   %eax,%eax
801084b8:	75 0c                	jne    801084c6 <switchuvm+0x146>
    panic("switchuvm: no pgdir");
801084ba:	c7 04 24 f5 92 10 80 	movl   $0x801092f5,(%esp)
801084c1:	e8 a3 83 ff ff       	call   80100869 <panic>
  lcr3(V2P(p->pgdir));  // switch to new address space
801084c6:	8b 45 08             	mov    0x8(%ebp),%eax
801084c9:	8b 40 04             	mov    0x4(%eax),%eax
801084cc:	2d 00 00 00 80       	sub    $0x80000000,%eax
801084d1:	89 04 24             	mov    %eax,(%esp)
801084d4:	e8 1f f8 ff ff       	call   80107cf8 <lcr3>
  popcli();
801084d9:	e8 cf d1 ff ff       	call   801056ad <popcli>
}
801084de:	83 c4 14             	add    $0x14,%esp
801084e1:	5b                   	pop    %ebx
801084e2:	5d                   	pop    %ebp
801084e3:	c3                   	ret    

801084e4 <inituvm>:

// Load the initcode into address 0 of pgdir.
// sz must be less than a page.
void
inituvm(pde_t *pgdir, char *init, uint sz)
{
801084e4:	55                   	push   %ebp
801084e5:	89 e5                	mov    %esp,%ebp
801084e7:	83 ec 38             	sub    $0x38,%esp
  char *mem;

  if(sz >= PGSIZE)
801084ea:	81 7d 10 ff 0f 00 00 	cmpl   $0xfff,0x10(%ebp)
801084f1:	76 0c                	jbe    801084ff <inituvm+0x1b>
    panic("inituvm: more than a page");
801084f3:	c7 04 24 09 93 10 80 	movl   $0x80109309,(%esp)
801084fa:	e8 6a 83 ff ff       	call   80100869 <panic>
  if ((mem = kalloc()) == 0)
801084ff:	e8 9a ae ff ff       	call   8010339e <kalloc>
80108504:	89 45 f4             	mov    %eax,-0xc(%ebp)
80108507:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010850b:	75 0c                	jne    80108519 <inituvm+0x35>
    panic("inituvm: cannot allocate memory");
8010850d:	c7 04 24 24 93 10 80 	movl   $0x80109324,(%esp)
80108514:	e8 50 83 ff ff       	call   80100869 <panic>
  memset(mem, 0, PGSIZE);
80108519:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80108520:	00 
80108521:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80108528:	00 
80108529:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010852c:	89 04 24             	mov    %eax,(%esp)
8010852f:	e8 36 d2 ff ff       	call   8010576a <memset>
  if (mappages(pgdir, 0, PGSIZE, V2P(mem), PTE_W|PTE_U) < 0)
80108534:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108537:	2d 00 00 00 80       	sub    $0x80000000,%eax
8010853c:	c7 44 24 10 06 00 00 	movl   $0x6,0x10(%esp)
80108543:	00 
80108544:	89 44 24 0c          	mov    %eax,0xc(%esp)
80108548:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
8010854f:	00 
80108550:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80108557:	00 
80108558:	8b 45 08             	mov    0x8(%ebp),%eax
8010855b:	89 04 24             	mov    %eax,(%esp)
8010855e:	e8 a6 fc ff ff       	call   80108209 <mappages>
80108563:	85 c0                	test   %eax,%eax
80108565:	79 0c                	jns    80108573 <inituvm+0x8f>
    panic("inituvm: cannot create pagetable");
80108567:	c7 04 24 44 93 10 80 	movl   $0x80109344,(%esp)
8010856e:	e8 f6 82 ff ff       	call   80100869 <panic>
  memmove(mem, init, sz);
80108573:	8b 45 10             	mov    0x10(%ebp),%eax
80108576:	89 44 24 08          	mov    %eax,0x8(%esp)
8010857a:	8b 45 0c             	mov    0xc(%ebp),%eax
8010857d:	89 44 24 04          	mov    %eax,0x4(%esp)
80108581:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108584:	89 04 24             	mov    %eax,(%esp)
80108587:	e8 b1 d2 ff ff       	call   8010583d <memmove>
}
8010858c:	c9                   	leave  
8010858d:	c3                   	ret    

8010858e <loaduvm>:

// Load a program segment into pgdir.  addr must be page-aligned
// and the pages from addr to addr+sz must already be mapped.
int
loaduvm(pde_t *pgdir, char *addr, struct inode *ip, uint offset, uint sz)
{
8010858e:	55                   	push   %ebp
8010858f:	89 e5                	mov    %esp,%ebp
80108591:	83 ec 28             	sub    $0x28,%esp
  uint i, pa, n;
  pte_t *pte;

  if((uint) addr % PGSIZE != 0)
80108594:	8b 45 0c             	mov    0xc(%ebp),%eax
80108597:	25 ff 0f 00 00       	and    $0xfff,%eax
8010859c:	85 c0                	test   %eax,%eax
8010859e:	74 0c                	je     801085ac <loaduvm+0x1e>
    panic("loaduvm: addr must be page aligned");
801085a0:	c7 04 24 68 93 10 80 	movl   $0x80109368,(%esp)
801085a7:	e8 bd 82 ff ff       	call   80100869 <panic>
  for(i = 0; i < sz; i += PGSIZE){
801085ac:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
801085b3:	e9 ab 00 00 00       	jmp    80108663 <loaduvm+0xd5>
    if((pte = walkpgdir(pgdir, addr+i, 0)) == 0)
801085b8:	8b 45 e8             	mov    -0x18(%ebp),%eax
801085bb:	8b 55 0c             	mov    0xc(%ebp),%edx
801085be:	8d 04 02             	lea    (%edx,%eax,1),%eax
801085c1:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
801085c8:	00 
801085c9:	89 44 24 04          	mov    %eax,0x4(%esp)
801085cd:	8b 45 08             	mov    0x8(%ebp),%eax
801085d0:	89 04 24             	mov    %eax,(%esp)
801085d3:	e8 a1 fb ff ff       	call   80108179 <walkpgdir>
801085d8:	89 45 f4             	mov    %eax,-0xc(%ebp)
801085db:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801085df:	75 0c                	jne    801085ed <loaduvm+0x5f>
      panic("loaduvm: address should exist");
801085e1:	c7 04 24 8b 93 10 80 	movl   $0x8010938b,(%esp)
801085e8:	e8 7c 82 ff ff       	call   80100869 <panic>
    pa = PTE_ADDR(*pte);
801085ed:	8b 45 f4             	mov    -0xc(%ebp),%eax
801085f0:	8b 00                	mov    (%eax),%eax
801085f2:	25 00 f0 ff ff       	and    $0xfffff000,%eax
801085f7:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(sz - i < PGSIZE)
801085fa:	8b 45 e8             	mov    -0x18(%ebp),%eax
801085fd:	8b 55 18             	mov    0x18(%ebp),%edx
80108600:	89 d1                	mov    %edx,%ecx
80108602:	29 c1                	sub    %eax,%ecx
80108604:	89 c8                	mov    %ecx,%eax
80108606:	3d ff 0f 00 00       	cmp    $0xfff,%eax
8010860b:	77 11                	ja     8010861e <loaduvm+0x90>
      n = sz - i;
8010860d:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108610:	8b 55 18             	mov    0x18(%ebp),%edx
80108613:	89 d1                	mov    %edx,%ecx
80108615:	29 c1                	sub    %eax,%ecx
80108617:	89 c8                	mov    %ecx,%eax
80108619:	89 45 f0             	mov    %eax,-0x10(%ebp)
8010861c:	eb 07                	jmp    80108625 <loaduvm+0x97>
    else
      n = PGSIZE;
8010861e:	c7 45 f0 00 10 00 00 	movl   $0x1000,-0x10(%ebp)
    if(readi(ip, P2V(pa), offset+i, n) != n)
80108625:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108628:	8b 55 14             	mov    0x14(%ebp),%edx
8010862b:	8d 0c 02             	lea    (%edx,%eax,1),%ecx
8010862e:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108631:	2d 00 00 00 80       	sub    $0x80000000,%eax
80108636:	8b 55 f0             	mov    -0x10(%ebp),%edx
80108639:	89 54 24 0c          	mov    %edx,0xc(%esp)
8010863d:	89 4c 24 08          	mov    %ecx,0x8(%esp)
80108641:	89 44 24 04          	mov    %eax,0x4(%esp)
80108645:	8b 45 10             	mov    0x10(%ebp),%eax
80108648:	89 04 24             	mov    %eax,(%esp)
8010864b:	e8 20 9f ff ff       	call   80102570 <readi>
80108650:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80108653:	74 07                	je     8010865c <loaduvm+0xce>
      return -1;
80108655:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010865a:	eb 18                	jmp    80108674 <loaduvm+0xe6>
  uint i, pa, n;
  pte_t *pte;

  if((uint) addr % PGSIZE != 0)
    panic("loaduvm: addr must be page aligned");
  for(i = 0; i < sz; i += PGSIZE){
8010865c:	81 45 e8 00 10 00 00 	addl   $0x1000,-0x18(%ebp)
80108663:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108666:	3b 45 18             	cmp    0x18(%ebp),%eax
80108669:	0f 82 49 ff ff ff    	jb     801085b8 <loaduvm+0x2a>
    else
      n = PGSIZE;
    if(readi(ip, P2V(pa), offset+i, n) != n)
      return -1;
  }
  return 0;
8010866f:	b8 00 00 00 00       	mov    $0x0,%eax
}
80108674:	c9                   	leave  
80108675:	c3                   	ret    

80108676 <allocuvm>:

// Allocate page tables and physical memory to grow process from oldsz to
// newsz, which need not be page aligned.  Returns new size or 0 on error.
int
allocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
80108676:	55                   	push   %ebp
80108677:	89 e5                	mov    %esp,%ebp
80108679:	83 ec 38             	sub    $0x38,%esp
  char *mem;
  uint a;

  if(newsz >= KERNBASE)
8010867c:	8b 45 10             	mov    0x10(%ebp),%eax
8010867f:	85 c0                	test   %eax,%eax
80108681:	79 0a                	jns    8010868d <allocuvm+0x17>
    return 0;
80108683:	b8 00 00 00 00       	mov    $0x0,%eax
80108688:	e9 cf 00 00 00       	jmp    8010875c <allocuvm+0xe6>
  if(newsz < oldsz)
8010868d:	8b 45 10             	mov    0x10(%ebp),%eax
80108690:	3b 45 0c             	cmp    0xc(%ebp),%eax
80108693:	73 08                	jae    8010869d <allocuvm+0x27>
    return oldsz;
80108695:	8b 45 0c             	mov    0xc(%ebp),%eax
80108698:	e9 bf 00 00 00       	jmp    8010875c <allocuvm+0xe6>

  a = PGROUNDUP(oldsz);
8010869d:	8b 45 0c             	mov    0xc(%ebp),%eax
801086a0:	05 ff 0f 00 00       	add    $0xfff,%eax
801086a5:	25 00 f0 ff ff       	and    $0xfffff000,%eax
801086aa:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(; a < newsz; a += PGSIZE){
801086ad:	e9 9b 00 00 00       	jmp    8010874d <allocuvm+0xd7>
    mem = kalloc();
801086b2:	e8 e7 ac ff ff       	call   8010339e <kalloc>
801086b7:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(mem == 0){
801086ba:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801086be:	75 2c                	jne    801086ec <allocuvm+0x76>
      cprintf("allocuvm out of memory\n");
801086c0:	c7 04 24 a9 93 10 80 	movl   $0x801093a9,(%esp)
801086c7:	e8 fd 7f ff ff       	call   801006c9 <cprintf>
      deallocuvm(pgdir, newsz, oldsz);
801086cc:	8b 45 0c             	mov    0xc(%ebp),%eax
801086cf:	89 44 24 08          	mov    %eax,0x8(%esp)
801086d3:	8b 45 10             	mov    0x10(%ebp),%eax
801086d6:	89 44 24 04          	mov    %eax,0x4(%esp)
801086da:	8b 45 08             	mov    0x8(%ebp),%eax
801086dd:	89 04 24             	mov    %eax,(%esp)
801086e0:	e8 79 00 00 00       	call   8010875e <deallocuvm>
      return 0;
801086e5:	b8 00 00 00 00       	mov    $0x0,%eax
801086ea:	eb 70                	jmp    8010875c <allocuvm+0xe6>
    }
    memset(mem, 0, PGSIZE);
801086ec:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
801086f3:	00 
801086f4:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801086fb:	00 
801086fc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801086ff:	89 04 24             	mov    %eax,(%esp)
80108702:	e8 63 d0 ff ff       	call   8010576a <memset>
    if (mappages(pgdir, (char*)a, PGSIZE, V2P(mem), PTE_W|PTE_U) < 0)
80108707:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010870a:	8d 90 00 00 00 80    	lea    -0x80000000(%eax),%edx
80108710:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108713:	c7 44 24 10 06 00 00 	movl   $0x6,0x10(%esp)
8010871a:	00 
8010871b:	89 54 24 0c          	mov    %edx,0xc(%esp)
8010871f:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80108726:	00 
80108727:	89 44 24 04          	mov    %eax,0x4(%esp)
8010872b:	8b 45 08             	mov    0x8(%ebp),%eax
8010872e:	89 04 24             	mov    %eax,(%esp)
80108731:	e8 d3 fa ff ff       	call   80108209 <mappages>
80108736:	85 c0                	test   %eax,%eax
80108738:	79 0c                	jns    80108746 <allocuvm+0xd0>
      panic("allocuvm: cannot create pagetable");
8010873a:	c7 04 24 c4 93 10 80 	movl   $0x801093c4,(%esp)
80108741:	e8 23 81 ff ff       	call   80100869 <panic>
    return 0;
  if(newsz < oldsz)
    return oldsz;

  a = PGROUNDUP(oldsz);
  for(; a < newsz; a += PGSIZE){
80108746:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
8010874d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108750:	3b 45 10             	cmp    0x10(%ebp),%eax
80108753:	0f 82 59 ff ff ff    	jb     801086b2 <allocuvm+0x3c>
    }
    memset(mem, 0, PGSIZE);
    if (mappages(pgdir, (char*)a, PGSIZE, V2P(mem), PTE_W|PTE_U) < 0)
      panic("allocuvm: cannot create pagetable");
  }
  return newsz;
80108759:	8b 45 10             	mov    0x10(%ebp),%eax
}
8010875c:	c9                   	leave  
8010875d:	c3                   	ret    

8010875e <deallocuvm>:
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
int
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
8010875e:	55                   	push   %ebp
8010875f:	89 e5                	mov    %esp,%ebp
80108761:	83 ec 28             	sub    $0x28,%esp
  pte_t *pte;
  uint a, pa;

  if(newsz >= oldsz)
80108764:	8b 45 10             	mov    0x10(%ebp),%eax
80108767:	3b 45 0c             	cmp    0xc(%ebp),%eax
8010876a:	72 08                	jb     80108774 <deallocuvm+0x16>
    return oldsz;
8010876c:	8b 45 0c             	mov    0xc(%ebp),%eax
8010876f:	e9 9e 00 00 00       	jmp    80108812 <deallocuvm+0xb4>

  a = PGROUNDUP(newsz);
80108774:	8b 45 10             	mov    0x10(%ebp),%eax
80108777:	05 ff 0f 00 00       	add    $0xfff,%eax
8010877c:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108781:	89 45 ec             	mov    %eax,-0x14(%ebp)
  for(; a  < oldsz; a += PGSIZE){
80108784:	eb 7d                	jmp    80108803 <deallocuvm+0xa5>
    pte = walkpgdir(pgdir, (char*)a, 0);
80108786:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108789:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80108790:	00 
80108791:	89 44 24 04          	mov    %eax,0x4(%esp)
80108795:	8b 45 08             	mov    0x8(%ebp),%eax
80108798:	89 04 24             	mov    %eax,(%esp)
8010879b:	e8 d9 f9 ff ff       	call   80108179 <walkpgdir>
801087a0:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(!pte)
801087a3:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
801087a7:	75 09                	jne    801087b2 <deallocuvm+0x54>
      a += (NPTENTRIES - 1) * PGSIZE;
801087a9:	81 45 ec 00 f0 3f 00 	addl   $0x3ff000,-0x14(%ebp)
801087b0:	eb 4a                	jmp    801087fc <deallocuvm+0x9e>
    else if((*pte & PTE_P) != 0){
801087b2:	8b 45 e8             	mov    -0x18(%ebp),%eax
801087b5:	8b 00                	mov    (%eax),%eax
801087b7:	83 e0 01             	and    $0x1,%eax
801087ba:	84 c0                	test   %al,%al
801087bc:	74 3e                	je     801087fc <deallocuvm+0x9e>
      pa = PTE_ADDR(*pte);
801087be:	8b 45 e8             	mov    -0x18(%ebp),%eax
801087c1:	8b 00                	mov    (%eax),%eax
801087c3:	25 00 f0 ff ff       	and    $0xfffff000,%eax
801087c8:	89 45 f0             	mov    %eax,-0x10(%ebp)
      if(pa == 0)
801087cb:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801087cf:	75 0c                	jne    801087dd <deallocuvm+0x7f>
        panic("deallocuvm");
801087d1:	c7 04 24 e6 93 10 80 	movl   $0x801093e6,(%esp)
801087d8:	e8 8c 80 ff ff       	call   80100869 <panic>
      char *v = P2V(pa);
801087dd:	8b 45 f0             	mov    -0x10(%ebp),%eax
801087e0:	2d 00 00 00 80       	sub    $0x80000000,%eax
801087e5:	89 45 f4             	mov    %eax,-0xc(%ebp)
      kfree(v);
801087e8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801087eb:	89 04 24             	mov    %eax,(%esp)
801087ee:	e8 15 ab ff ff       	call   80103308 <kfree>
      *pte = 0;
801087f3:	8b 45 e8             	mov    -0x18(%ebp),%eax
801087f6:	c7 00 00 00 00 00    	movl   $0x0,(%eax)

  if(newsz >= oldsz)
    return oldsz;

  a = PGROUNDUP(newsz);
  for(; a  < oldsz; a += PGSIZE){
801087fc:	81 45 ec 00 10 00 00 	addl   $0x1000,-0x14(%ebp)
80108803:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108806:	3b 45 0c             	cmp    0xc(%ebp),%eax
80108809:	0f 82 77 ff ff ff    	jb     80108786 <deallocuvm+0x28>
      char *v = P2V(pa);
      kfree(v);
      *pte = 0;
    }
  }
  return newsz;
8010880f:	8b 45 10             	mov    0x10(%ebp),%eax
}
80108812:	c9                   	leave  
80108813:	c3                   	ret    

80108814 <freevm>:

// Free a page table and all the physical memory pages
// in the user part.
void
freevm(pde_t *pgdir)
{
80108814:	55                   	push   %ebp
80108815:	89 e5                	mov    %esp,%ebp
80108817:	83 ec 28             	sub    $0x28,%esp
  uint i;

  if(pgdir == 0)
8010881a:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
8010881e:	75 0c                	jne    8010882c <freevm+0x18>
    panic("freevm: no pgdir");
80108820:	c7 04 24 f1 93 10 80 	movl   $0x801093f1,(%esp)
80108827:	e8 3d 80 ff ff       	call   80100869 <panic>
  deallocuvm(pgdir, KERNBASE, 0);
8010882c:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80108833:	00 
80108834:	c7 44 24 04 00 00 00 	movl   $0x80000000,0x4(%esp)
8010883b:	80 
8010883c:	8b 45 08             	mov    0x8(%ebp),%eax
8010883f:	89 04 24             	mov    %eax,(%esp)
80108842:	e8 17 ff ff ff       	call   8010875e <deallocuvm>
  for(i = 0; i < NPDENTRIES; i++){
80108847:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
8010884e:	eb 39                	jmp    80108889 <freevm+0x75>
    if(pgdir[i] & PTE_P){
80108850:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108853:	c1 e0 02             	shl    $0x2,%eax
80108856:	03 45 08             	add    0x8(%ebp),%eax
80108859:	8b 00                	mov    (%eax),%eax
8010885b:	83 e0 01             	and    $0x1,%eax
8010885e:	84 c0                	test   %al,%al
80108860:	74 23                	je     80108885 <freevm+0x71>
      char * v = P2V(PTE_ADDR(pgdir[i]));
80108862:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108865:	c1 e0 02             	shl    $0x2,%eax
80108868:	03 45 08             	add    0x8(%ebp),%eax
8010886b:	8b 00                	mov    (%eax),%eax
8010886d:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108872:	2d 00 00 00 80       	sub    $0x80000000,%eax
80108877:	89 45 f4             	mov    %eax,-0xc(%ebp)
      kfree(v);
8010887a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010887d:	89 04 24             	mov    %eax,(%esp)
80108880:	e8 83 aa ff ff       	call   80103308 <kfree>
  uint i;

  if(pgdir == 0)
    panic("freevm: no pgdir");
  deallocuvm(pgdir, KERNBASE, 0);
  for(i = 0; i < NPDENTRIES; i++){
80108885:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
80108889:	81 7d f0 ff 03 00 00 	cmpl   $0x3ff,-0x10(%ebp)
80108890:	76 be                	jbe    80108850 <freevm+0x3c>
    if(pgdir[i] & PTE_P){
      char * v = P2V(PTE_ADDR(pgdir[i]));
      kfree(v);
    }
  }
  kfree((char*)pgdir);
80108892:	8b 45 08             	mov    0x8(%ebp),%eax
80108895:	89 04 24             	mov    %eax,(%esp)
80108898:	e8 6b aa ff ff       	call   80103308 <kfree>
}
8010889d:	c9                   	leave  
8010889e:	c3                   	ret    

8010889f <clearpteu>:

// Clear PTE_U on a page. Used to create an inaccessible
// page beneath the user stack.
void
clearpteu(pde_t *pgdir, char *uva)
{
8010889f:	55                   	push   %ebp
801088a0:	89 e5                	mov    %esp,%ebp
801088a2:	83 ec 28             	sub    $0x28,%esp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
801088a5:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
801088ac:	00 
801088ad:	8b 45 0c             	mov    0xc(%ebp),%eax
801088b0:	89 44 24 04          	mov    %eax,0x4(%esp)
801088b4:	8b 45 08             	mov    0x8(%ebp),%eax
801088b7:	89 04 24             	mov    %eax,(%esp)
801088ba:	e8 ba f8 ff ff       	call   80108179 <walkpgdir>
801088bf:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(pte == 0)
801088c2:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801088c6:	75 0c                	jne    801088d4 <clearpteu+0x35>
    panic("clearpteu");
801088c8:	c7 04 24 02 94 10 80 	movl   $0x80109402,(%esp)
801088cf:	e8 95 7f ff ff       	call   80100869 <panic>
  *pte &= ~PTE_U;
801088d4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801088d7:	8b 00                	mov    (%eax),%eax
801088d9:	89 c2                	mov    %eax,%edx
801088db:	83 e2 fb             	and    $0xfffffffb,%edx
801088de:	8b 45 f4             	mov    -0xc(%ebp),%eax
801088e1:	89 10                	mov    %edx,(%eax)
}
801088e3:	c9                   	leave  
801088e4:	c3                   	ret    

801088e5 <copyuvm>:

// Given a parent process's page table, create a copy
// of it for a child.
pde_t*
copyuvm(pde_t *pgdir, uint sz)
{
801088e5:	55                   	push   %ebp
801088e6:	89 e5                	mov    %esp,%ebp
801088e8:	83 ec 48             	sub    $0x48,%esp
  pde_t *d;
  pte_t *pte;
  uint pa, i, flags;
  char *mem;

  if((d = setupkvm()) == 0)
801088eb:	e8 ad f9 ff ff       	call   8010829d <setupkvm>
801088f0:	89 45 e0             	mov    %eax,-0x20(%ebp)
801088f3:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
801088f7:	75 0a                	jne    80108903 <copyuvm+0x1e>
    return 0;
801088f9:	b8 00 00 00 00       	mov    $0x0,%eax
801088fe:	e9 f8 00 00 00       	jmp    801089fb <copyuvm+0x116>
  for(i = 0; i < sz; i += PGSIZE){
80108903:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
8010890a:	e9 c7 00 00 00       	jmp    801089d6 <copyuvm+0xf1>
    if((pte = walkpgdir(pgdir, (void *) i, 0)) == 0)
8010890f:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108912:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80108919:	00 
8010891a:	89 44 24 04          	mov    %eax,0x4(%esp)
8010891e:	8b 45 08             	mov    0x8(%ebp),%eax
80108921:	89 04 24             	mov    %eax,(%esp)
80108924:	e8 50 f8 ff ff       	call   80108179 <walkpgdir>
80108929:	89 45 e4             	mov    %eax,-0x1c(%ebp)
8010892c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
80108930:	75 0c                	jne    8010893e <copyuvm+0x59>
      panic("copyuvm: pte should exist");
80108932:	c7 04 24 0c 94 10 80 	movl   $0x8010940c,(%esp)
80108939:	e8 2b 7f ff ff       	call   80100869 <panic>
    if(!(*pte & PTE_P))
8010893e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80108941:	8b 00                	mov    (%eax),%eax
80108943:	83 e0 01             	and    $0x1,%eax
80108946:	85 c0                	test   %eax,%eax
80108948:	75 0c                	jne    80108956 <copyuvm+0x71>
      panic("copyuvm: page not present");
8010894a:	c7 04 24 26 94 10 80 	movl   $0x80109426,(%esp)
80108951:	e8 13 7f ff ff       	call   80100869 <panic>
    pa = PTE_ADDR(*pte);
80108956:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80108959:	8b 00                	mov    (%eax),%eax
8010895b:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108960:	89 45 e8             	mov    %eax,-0x18(%ebp)
    flags = PTE_FLAGS(*pte);
80108963:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80108966:	8b 00                	mov    (%eax),%eax
80108968:	25 ff 0f 00 00       	and    $0xfff,%eax
8010896d:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if((mem = kalloc()) == 0)
80108970:	e8 29 aa ff ff       	call   8010339e <kalloc>
80108975:	89 45 f4             	mov    %eax,-0xc(%ebp)
80108978:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010897c:	74 69                	je     801089e7 <copyuvm+0x102>
      goto bad;
    memmove(mem, (char*)P2V(pa), PGSIZE);
8010897e:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108981:	2d 00 00 00 80       	sub    $0x80000000,%eax
80108986:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
8010898d:	00 
8010898e:	89 44 24 04          	mov    %eax,0x4(%esp)
80108992:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108995:	89 04 24             	mov    %eax,(%esp)
80108998:	e8 a0 ce ff ff       	call   8010583d <memmove>
    if(mappages(d, (void*)i, PGSIZE, V2P(mem), flags) < 0)
8010899d:	8b 55 f0             	mov    -0x10(%ebp),%edx
801089a0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801089a3:	8d 88 00 00 00 80    	lea    -0x80000000(%eax),%ecx
801089a9:	8b 45 ec             	mov    -0x14(%ebp),%eax
801089ac:	89 54 24 10          	mov    %edx,0x10(%esp)
801089b0:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
801089b4:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
801089bb:	00 
801089bc:	89 44 24 04          	mov    %eax,0x4(%esp)
801089c0:	8b 45 e0             	mov    -0x20(%ebp),%eax
801089c3:	89 04 24             	mov    %eax,(%esp)
801089c6:	e8 3e f8 ff ff       	call   80108209 <mappages>
801089cb:	85 c0                	test   %eax,%eax
801089cd:	78 1b                	js     801089ea <copyuvm+0x105>
  uint pa, i, flags;
  char *mem;

  if((d = setupkvm()) == 0)
    return 0;
  for(i = 0; i < sz; i += PGSIZE){
801089cf:	81 45 ec 00 10 00 00 	addl   $0x1000,-0x14(%ebp)
801089d6:	8b 45 ec             	mov    -0x14(%ebp),%eax
801089d9:	3b 45 0c             	cmp    0xc(%ebp),%eax
801089dc:	0f 82 2d ff ff ff    	jb     8010890f <copyuvm+0x2a>
      goto bad;
    memmove(mem, (char*)P2V(pa), PGSIZE);
    if(mappages(d, (void*)i, PGSIZE, V2P(mem), flags) < 0)
      goto bad;
  }
  return d;
801089e2:	8b 45 e0             	mov    -0x20(%ebp),%eax
801089e5:	eb 14                	jmp    801089fb <copyuvm+0x116>
    if(!(*pte & PTE_P))
      panic("copyuvm: page not present");
    pa = PTE_ADDR(*pte);
    flags = PTE_FLAGS(*pte);
    if((mem = kalloc()) == 0)
      goto bad;
801089e7:	90                   	nop
801089e8:	eb 01                	jmp    801089eb <copyuvm+0x106>
    memmove(mem, (char*)P2V(pa), PGSIZE);
    if(mappages(d, (void*)i, PGSIZE, V2P(mem), flags) < 0)
      goto bad;
801089ea:	90                   	nop
  }
  return d;

bad:
  freevm(d);
801089eb:	8b 45 e0             	mov    -0x20(%ebp),%eax
801089ee:	89 04 24             	mov    %eax,(%esp)
801089f1:	e8 1e fe ff ff       	call   80108814 <freevm>
  return 0;
801089f6:	b8 00 00 00 00       	mov    $0x0,%eax
}
801089fb:	c9                   	leave  
801089fc:	c3                   	ret    

801089fd <uva2ka>:

//PAGEBREAK!
// Map user virtual address to kernel address.
char*
uva2ka(pde_t *pgdir, char *uva)
{
801089fd:	55                   	push   %ebp
801089fe:	89 e5                	mov    %esp,%ebp
80108a00:	83 ec 28             	sub    $0x28,%esp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80108a03:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80108a0a:	00 
80108a0b:	8b 45 0c             	mov    0xc(%ebp),%eax
80108a0e:	89 44 24 04          	mov    %eax,0x4(%esp)
80108a12:	8b 45 08             	mov    0x8(%ebp),%eax
80108a15:	89 04 24             	mov    %eax,(%esp)
80108a18:	e8 5c f7 ff ff       	call   80108179 <walkpgdir>
80108a1d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((*pte & PTE_P) == 0)
80108a20:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108a23:	8b 00                	mov    (%eax),%eax
80108a25:	83 e0 01             	and    $0x1,%eax
80108a28:	85 c0                	test   %eax,%eax
80108a2a:	75 07                	jne    80108a33 <uva2ka+0x36>
    return 0;
80108a2c:	b8 00 00 00 00       	mov    $0x0,%eax
80108a31:	eb 22                	jmp    80108a55 <uva2ka+0x58>
  if((*pte & PTE_U) == 0)
80108a33:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108a36:	8b 00                	mov    (%eax),%eax
80108a38:	83 e0 04             	and    $0x4,%eax
80108a3b:	85 c0                	test   %eax,%eax
80108a3d:	75 07                	jne    80108a46 <uva2ka+0x49>
    return 0;
80108a3f:	b8 00 00 00 00       	mov    $0x0,%eax
80108a44:	eb 0f                	jmp    80108a55 <uva2ka+0x58>
  return (char*)P2V(PTE_ADDR(*pte));
80108a46:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108a49:	8b 00                	mov    (%eax),%eax
80108a4b:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108a50:	2d 00 00 00 80       	sub    $0x80000000,%eax
}
80108a55:	c9                   	leave  
80108a56:	c3                   	ret    

80108a57 <copyout>:
// Copy len bytes from p to user address va in page table pgdir.
// Most useful when pgdir is not the current page table.
// uva2ka ensures this only works for PTE_U pages.
int
copyout(pde_t *pgdir, uint va, void *p, uint len)
{
80108a57:	55                   	push   %ebp
80108a58:	89 e5                	mov    %esp,%ebp
80108a5a:	83 ec 28             	sub    $0x28,%esp
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
80108a5d:	8b 45 10             	mov    0x10(%ebp),%eax
80108a60:	89 45 e8             	mov    %eax,-0x18(%ebp)
  while(len > 0){
80108a63:	e9 8b 00 00 00       	jmp    80108af3 <copyout+0x9c>
    va0 = (uint)PGROUNDDOWN(va);
80108a68:	8b 45 0c             	mov    0xc(%ebp),%eax
80108a6b:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108a70:	89 45 f4             	mov    %eax,-0xc(%ebp)
    pa0 = uva2ka(pgdir, (char*)va0);
80108a73:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108a76:	89 44 24 04          	mov    %eax,0x4(%esp)
80108a7a:	8b 45 08             	mov    0x8(%ebp),%eax
80108a7d:	89 04 24             	mov    %eax,(%esp)
80108a80:	e8 78 ff ff ff       	call   801089fd <uva2ka>
80108a85:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(pa0 == 0)
80108a88:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80108a8c:	75 07                	jne    80108a95 <copyout+0x3e>
      return -1;
80108a8e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80108a93:	eb 6d                	jmp    80108b02 <copyout+0xab>
    n = PGSIZE - (va - va0);
80108a95:	8b 45 0c             	mov    0xc(%ebp),%eax
80108a98:	8b 55 f4             	mov    -0xc(%ebp),%edx
80108a9b:	89 d1                	mov    %edx,%ecx
80108a9d:	29 c1                	sub    %eax,%ecx
80108a9f:	89 c8                	mov    %ecx,%eax
80108aa1:	05 00 10 00 00       	add    $0x1000,%eax
80108aa6:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(n > len)
80108aa9:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108aac:	3b 45 14             	cmp    0x14(%ebp),%eax
80108aaf:	76 06                	jbe    80108ab7 <copyout+0x60>
      n = len;
80108ab1:	8b 45 14             	mov    0x14(%ebp),%eax
80108ab4:	89 45 f0             	mov    %eax,-0x10(%ebp)
    memmove(pa0 + (va - va0), buf, n);
80108ab7:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108aba:	8b 55 0c             	mov    0xc(%ebp),%edx
80108abd:	89 d1                	mov    %edx,%ecx
80108abf:	29 c1                	sub    %eax,%ecx
80108ac1:	89 c8                	mov    %ecx,%eax
80108ac3:	03 45 ec             	add    -0x14(%ebp),%eax
80108ac6:	8b 55 f0             	mov    -0x10(%ebp),%edx
80108ac9:	89 54 24 08          	mov    %edx,0x8(%esp)
80108acd:	8b 55 e8             	mov    -0x18(%ebp),%edx
80108ad0:	89 54 24 04          	mov    %edx,0x4(%esp)
80108ad4:	89 04 24             	mov    %eax,(%esp)
80108ad7:	e8 61 cd ff ff       	call   8010583d <memmove>
    len -= n;
80108adc:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108adf:	29 45 14             	sub    %eax,0x14(%ebp)
    buf += n;
80108ae2:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108ae5:	01 45 e8             	add    %eax,-0x18(%ebp)
    va = va0 + PGSIZE;
80108ae8:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108aeb:	05 00 10 00 00       	add    $0x1000,%eax
80108af0:	89 45 0c             	mov    %eax,0xc(%ebp)
{
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
  while(len > 0){
80108af3:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
80108af7:	0f 85 6b ff ff ff    	jne    80108a68 <copyout+0x11>
    memmove(pa0 + (va - va0), buf, n);
    len -= n;
    buf += n;
    va = va0 + PGSIZE;
  }
  return 0;
80108afd:	b8 00 00 00 00       	mov    $0x0,%eax
}
80108b02:	c9                   	leave  
80108b03:	c3                   	ret    
