#include "types.h"
#include "stat.h"
#include "user.h"
char buf;

int main(int argc, char *argv[])
{
    int Fdata, NLines;
    int filename;
    int linecount = 0;
    if (argc<=1)
    {
        printf(1,"Usage: head file OR head file #oflines %sn", argv[1]);
        exit();
    }
    if((filename = open(argv[1],0)) < 0)
    {
        printf(1,"Error, cannot open %sn", argv[1]);
        exit();
    }
    if(argc ==2)
    {
        NLines = 10;
    }
    else if(argc == 3)
    {
        NLines =  (int) atoi(argv[2]);
    }

    while((Fdata=read(filename, &buf, 1))>0)
    {
	if(linecount > NLines -1){
	    exit();
	}
        if(buf == '\n')
        {
            linecount++;
        }
        printf(1,"%c",buf);
    }
    if(Fdata<0)
    {
        printf(1,"head: read errorn");
        exit();
    }

    close(Fdata);
    exit();
}
