
_mv:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
// mv.c is used for the purpose of moving  data from 1 file to another
// Usage: mv srcname destname

int
main(int argc, char *argv[])
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 e4 f0             	and    $0xfffffff0,%esp
   6:	81 ec 30 02 00 00    	sub    $0x230,%esp
  int  r, w;
  char *orig;
  char *copy;

// If not the correct ammount of arguements throw an error at the user
  if(argc != 3){
   c:	83 7d 08 03          	cmpl   $0x3,0x8(%ebp)
  10:	74 19                	je     2b <main+0x2b>
    printf(2, "Please use the format specified in the documentation: mv originalfilename copyfilename\n");
  12:	c7 44 24 04 ac 09 00 	movl   $0x9ac,0x4(%esp)
  19:	00 
  1a:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
  21:	e8 be 05 00 00       	call   5e4 <printf>
    exit();
  26:	e8 3d 04 00 00       	call   468 <exit>
  }

// Read the first and second arguements into the orig and copy "strings"
  orig = argv[1];
  2b:	8b 45 0c             	mov    0xc(%ebp),%eax
  2e:	83 c0 04             	add    $0x4,%eax
  31:	8b 00                	mov    (%eax),%eax
  33:	89 84 24 28 02 00 00 	mov    %eax,0x228(%esp)
  copy = argv[2];
  3a:	8b 45 0c             	mov    0xc(%ebp),%eax
  3d:	83 c0 08             	add    $0x8,%eax
  40:	8b 00                	mov    (%eax),%eax
  42:	89 84 24 2c 02 00 00 	mov    %eax,0x22c(%esp)

// Check to see if orig can be opened and the copy can be made
  if ((filenameF = open(orig, O_RDONLY)) < 0) {
  49:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  50:	00 
  51:	8b 84 24 28 02 00 00 	mov    0x228(%esp),%eax
  58:	89 04 24             	mov    %eax,(%esp)
  5b:	e8 48 04 00 00       	call   4a8 <open>
  60:	89 84 24 18 02 00 00 	mov    %eax,0x218(%esp)
  67:	83 bc 24 18 02 00 00 	cmpl   $0x0,0x218(%esp)
  6e:	00 
  6f:	79 2f                	jns    a0 <main+0xa0>
    printf(2, "Error, cannot move %s to %s\n", orig,copy);
  71:	8b 84 24 2c 02 00 00 	mov    0x22c(%esp),%eax
  78:	89 44 24 0c          	mov    %eax,0xc(%esp)
  7c:	8b 84 24 28 02 00 00 	mov    0x228(%esp),%eax
  83:	89 44 24 08          	mov    %eax,0x8(%esp)
  87:	c7 44 24 04 04 0a 00 	movl   $0xa04,0x4(%esp)
  8e:	00 
  8f:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
  96:	e8 49 05 00 00       	call   5e4 <printf>
    exit();
  9b:	e8 c8 03 00 00       	call   468 <exit>
  }
  if ((copynameF = open(copy, O_CREATE|O_WRONLY)) < 0) {
  a0:	c7 44 24 04 01 02 00 	movl   $0x201,0x4(%esp)
  a7:	00 
  a8:	8b 84 24 2c 02 00 00 	mov    0x22c(%esp),%eax
  af:	89 04 24             	mov    %eax,(%esp)
  b2:	e8 f1 03 00 00       	call   4a8 <open>
  b7:	89 84 24 1c 02 00 00 	mov    %eax,0x21c(%esp)
  be:	83 bc 24 1c 02 00 00 	cmpl   $0x0,0x21c(%esp)
  c5:	00 
  c6:	79 74                	jns    13c <main+0x13c>
    printf(2, "Error, cannot move %s to %s\n", orig,copy);
  c8:	8b 84 24 2c 02 00 00 	mov    0x22c(%esp),%eax
  cf:	89 44 24 0c          	mov    %eax,0xc(%esp)
  d3:	8b 84 24 28 02 00 00 	mov    0x228(%esp),%eax
  da:	89 44 24 08          	mov    %eax,0x8(%esp)
  de:	c7 44 24 04 04 0a 00 	movl   $0xa04,0x4(%esp)
  e5:	00 
  e6:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
  ed:	e8 f2 04 00 00       	call   5e4 <printf>
    exit();
  f2:	e8 71 03 00 00       	call   468 <exit>
  }
// Read from orig and write into copy simultaneously
// Breaks it into 512 bits per block or execution
  while ((r = read(filenameF, buf, sizeof(buf))) > 0) {
    w = write(copynameF, buf, r);
  f7:	8b 84 24 20 02 00 00 	mov    0x220(%esp),%eax
  fe:	89 44 24 08          	mov    %eax,0x8(%esp)
 102:	8d 44 24 18          	lea    0x18(%esp),%eax
 106:	89 44 24 04          	mov    %eax,0x4(%esp)
 10a:	8b 84 24 1c 02 00 00 	mov    0x21c(%esp),%eax
 111:	89 04 24             	mov    %eax,(%esp)
 114:	e8 6f 03 00 00       	call   488 <write>
 119:	89 84 24 24 02 00 00 	mov    %eax,0x224(%esp)
    if (w != r || w < 0) 
 120:	8b 84 24 24 02 00 00 	mov    0x224(%esp),%eax
 127:	3b 84 24 20 02 00 00 	cmp    0x220(%esp),%eax
 12e:	75 3d                	jne    16d <main+0x16d>
 130:	83 bc 24 24 02 00 00 	cmpl   $0x0,0x224(%esp)
 137:	00 
 138:	78 33                	js     16d <main+0x16d>
 13a:	eb 01                	jmp    13d <main+0x13d>
    printf(2, "Error, cannot move %s to %s\n", orig,copy);
    exit();
  }
// Read from orig and write into copy simultaneously
// Breaks it into 512 bits per block or execution
  while ((r = read(filenameF, buf, sizeof(buf))) > 0) {
 13c:	90                   	nop
 13d:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
 144:	00 
 145:	8d 44 24 18          	lea    0x18(%esp),%eax
 149:	89 44 24 04          	mov    %eax,0x4(%esp)
 14d:	8b 84 24 18 02 00 00 	mov    0x218(%esp),%eax
 154:	89 04 24             	mov    %eax,(%esp)
 157:	e8 24 03 00 00       	call   480 <read>
 15c:	89 84 24 20 02 00 00 	mov    %eax,0x220(%esp)
 163:	83 bc 24 20 02 00 00 	cmpl   $0x0,0x220(%esp)
 16a:	00 
 16b:	7f 8a                	jg     f7 <main+0xf7>
    if (w != r || w < 0) 
      break;
  }
// Check to see if we are actually reading or writing anything.
// Broad Error Catcher
  if (r < 0 || w < 0)
 16d:	83 bc 24 20 02 00 00 	cmpl   $0x0,0x220(%esp)
 174:	00 
 175:	78 0a                	js     181 <main+0x181>
 177:	83 bc 24 24 02 00 00 	cmpl   $0x0,0x224(%esp)
 17e:	00 
 17f:	79 2a                	jns    1ab <main+0x1ab>
    printf(2, "mv: error copying %s to %s\n", orig, copy);
 181:	8b 84 24 2c 02 00 00 	mov    0x22c(%esp),%eax
 188:	89 44 24 0c          	mov    %eax,0xc(%esp)
 18c:	8b 84 24 28 02 00 00 	mov    0x228(%esp),%eax
 193:	89 44 24 08          	mov    %eax,0x8(%esp)
 197:	c7 44 24 04 21 0a 00 	movl   $0xa21,0x4(%esp)
 19e:	00 
 19f:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
 1a6:	e8 39 04 00 00       	call   5e4 <printf>
// Close the files

  if(unlink(argv[1]) > 0){
 1ab:	8b 45 0c             	mov    0xc(%ebp),%eax
 1ae:	83 c0 04             	add    $0x4,%eax
 1b1:	8b 00                	mov    (%eax),%eax
 1b3:	89 04 24             	mov    %eax,(%esp)
 1b6:	e8 fd 02 00 00       	call   4b8 <unlink>
 1bb:	85 c0                	test   %eax,%eax
 1bd:	7e 1f                	jle    1de <main+0x1de>
    printf(2, "mv: Could not delete orig %s\n", orig);
 1bf:	8b 84 24 28 02 00 00 	mov    0x228(%esp),%eax
 1c6:	89 44 24 08          	mov    %eax,0x8(%esp)
 1ca:	c7 44 24 04 3d 0a 00 	movl   $0xa3d,0x4(%esp)
 1d1:	00 
 1d2:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
 1d9:	e8 06 04 00 00       	call   5e4 <printf>
  }
  close(filenameF);
 1de:	8b 84 24 18 02 00 00 	mov    0x218(%esp),%eax
 1e5:	89 04 24             	mov    %eax,(%esp)
 1e8:	e8 a3 02 00 00       	call   490 <close>
  close(copynameF);
 1ed:	8b 84 24 1c 02 00 00 	mov    0x21c(%esp),%eax
 1f4:	89 04 24             	mov    %eax,(%esp)
 1f7:	e8 94 02 00 00       	call   490 <close>

  exit();
 1fc:	e8 67 02 00 00       	call   468 <exit>
 201:	90                   	nop
 202:	90                   	nop
 203:	90                   	nop

00000204 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 204:	55                   	push   %ebp
 205:	89 e5                	mov    %esp,%ebp
 207:	57                   	push   %edi
 208:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 209:	8b 4d 08             	mov    0x8(%ebp),%ecx
 20c:	8b 55 10             	mov    0x10(%ebp),%edx
 20f:	8b 45 0c             	mov    0xc(%ebp),%eax
 212:	89 cb                	mov    %ecx,%ebx
 214:	89 df                	mov    %ebx,%edi
 216:	89 d1                	mov    %edx,%ecx
 218:	fc                   	cld    
 219:	f3 aa                	rep stos %al,%es:(%edi)
 21b:	89 ca                	mov    %ecx,%edx
 21d:	89 fb                	mov    %edi,%ebx
 21f:	89 5d 08             	mov    %ebx,0x8(%ebp)
 222:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 225:	5b                   	pop    %ebx
 226:	5f                   	pop    %edi
 227:	5d                   	pop    %ebp
 228:	c3                   	ret    

00000229 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 229:	55                   	push   %ebp
 22a:	89 e5                	mov    %esp,%ebp
 22c:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 22f:	8b 45 08             	mov    0x8(%ebp),%eax
 232:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 235:	8b 45 0c             	mov    0xc(%ebp),%eax
 238:	0f b6 10             	movzbl (%eax),%edx
 23b:	8b 45 08             	mov    0x8(%ebp),%eax
 23e:	88 10                	mov    %dl,(%eax)
 240:	8b 45 08             	mov    0x8(%ebp),%eax
 243:	0f b6 00             	movzbl (%eax),%eax
 246:	84 c0                	test   %al,%al
 248:	0f 95 c0             	setne  %al
 24b:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 24f:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
 253:	84 c0                	test   %al,%al
 255:	75 de                	jne    235 <strcpy+0xc>
    ;
  return os;
 257:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 25a:	c9                   	leave  
 25b:	c3                   	ret    

0000025c <strcmp>:

int
strcmp(const char *p, const char *q)
{
 25c:	55                   	push   %ebp
 25d:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 25f:	eb 08                	jmp    269 <strcmp+0xd>
    p++, q++;
 261:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 265:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 269:	8b 45 08             	mov    0x8(%ebp),%eax
 26c:	0f b6 00             	movzbl (%eax),%eax
 26f:	84 c0                	test   %al,%al
 271:	74 10                	je     283 <strcmp+0x27>
 273:	8b 45 08             	mov    0x8(%ebp),%eax
 276:	0f b6 10             	movzbl (%eax),%edx
 279:	8b 45 0c             	mov    0xc(%ebp),%eax
 27c:	0f b6 00             	movzbl (%eax),%eax
 27f:	38 c2                	cmp    %al,%dl
 281:	74 de                	je     261 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 283:	8b 45 08             	mov    0x8(%ebp),%eax
 286:	0f b6 00             	movzbl (%eax),%eax
 289:	0f b6 d0             	movzbl %al,%edx
 28c:	8b 45 0c             	mov    0xc(%ebp),%eax
 28f:	0f b6 00             	movzbl (%eax),%eax
 292:	0f b6 c0             	movzbl %al,%eax
 295:	89 d1                	mov    %edx,%ecx
 297:	29 c1                	sub    %eax,%ecx
 299:	89 c8                	mov    %ecx,%eax
}
 29b:	5d                   	pop    %ebp
 29c:	c3                   	ret    

0000029d <strlen>:

uint
strlen(char *s)
{
 29d:	55                   	push   %ebp
 29e:	89 e5                	mov    %esp,%ebp
 2a0:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 2a3:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 2aa:	eb 04                	jmp    2b0 <strlen+0x13>
 2ac:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 2b0:	8b 45 fc             	mov    -0x4(%ebp),%eax
 2b3:	03 45 08             	add    0x8(%ebp),%eax
 2b6:	0f b6 00             	movzbl (%eax),%eax
 2b9:	84 c0                	test   %al,%al
 2bb:	75 ef                	jne    2ac <strlen+0xf>
    ;
  return n;
 2bd:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 2c0:	c9                   	leave  
 2c1:	c3                   	ret    

000002c2 <memset>:

void*
memset(void *dst, int c, uint n)
{
 2c2:	55                   	push   %ebp
 2c3:	89 e5                	mov    %esp,%ebp
 2c5:	83 ec 0c             	sub    $0xc,%esp
  stosb(dst, c, n);
 2c8:	8b 45 10             	mov    0x10(%ebp),%eax
 2cb:	89 44 24 08          	mov    %eax,0x8(%esp)
 2cf:	8b 45 0c             	mov    0xc(%ebp),%eax
 2d2:	89 44 24 04          	mov    %eax,0x4(%esp)
 2d6:	8b 45 08             	mov    0x8(%ebp),%eax
 2d9:	89 04 24             	mov    %eax,(%esp)
 2dc:	e8 23 ff ff ff       	call   204 <stosb>
  return dst;
 2e1:	8b 45 08             	mov    0x8(%ebp),%eax
}
 2e4:	c9                   	leave  
 2e5:	c3                   	ret    

000002e6 <strchr>:

char*
strchr(const char *s, char c)
{
 2e6:	55                   	push   %ebp
 2e7:	89 e5                	mov    %esp,%ebp
 2e9:	83 ec 04             	sub    $0x4,%esp
 2ec:	8b 45 0c             	mov    0xc(%ebp),%eax
 2ef:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 2f2:	eb 14                	jmp    308 <strchr+0x22>
    if(*s == c)
 2f4:	8b 45 08             	mov    0x8(%ebp),%eax
 2f7:	0f b6 00             	movzbl (%eax),%eax
 2fa:	3a 45 fc             	cmp    -0x4(%ebp),%al
 2fd:	75 05                	jne    304 <strchr+0x1e>
      return (char*)s;
 2ff:	8b 45 08             	mov    0x8(%ebp),%eax
 302:	eb 13                	jmp    317 <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 304:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 308:	8b 45 08             	mov    0x8(%ebp),%eax
 30b:	0f b6 00             	movzbl (%eax),%eax
 30e:	84 c0                	test   %al,%al
 310:	75 e2                	jne    2f4 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 312:	b8 00 00 00 00       	mov    $0x0,%eax
}
 317:	c9                   	leave  
 318:	c3                   	ret    

00000319 <gets>:

char*
gets(char *buf, int max)
{
 319:	55                   	push   %ebp
 31a:	89 e5                	mov    %esp,%ebp
 31c:	83 ec 28             	sub    $0x28,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 31f:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 326:	eb 44                	jmp    36c <gets+0x53>
    cc = read(0, &c, 1);
 328:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 32f:	00 
 330:	8d 45 ef             	lea    -0x11(%ebp),%eax
 333:	89 44 24 04          	mov    %eax,0x4(%esp)
 337:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 33e:	e8 3d 01 00 00       	call   480 <read>
 343:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(cc < 1)
 346:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 34a:	7e 2d                	jle    379 <gets+0x60>
      break;
    buf[i++] = c;
 34c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 34f:	03 45 08             	add    0x8(%ebp),%eax
 352:	0f b6 55 ef          	movzbl -0x11(%ebp),%edx
 356:	88 10                	mov    %dl,(%eax)
 358:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
    if(c == '\n' || c == '\r')
 35c:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 360:	3c 0a                	cmp    $0xa,%al
 362:	74 16                	je     37a <gets+0x61>
 364:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 368:	3c 0d                	cmp    $0xd,%al
 36a:	74 0e                	je     37a <gets+0x61>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 36c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 36f:	83 c0 01             	add    $0x1,%eax
 372:	3b 45 0c             	cmp    0xc(%ebp),%eax
 375:	7c b1                	jl     328 <gets+0xf>
 377:	eb 01                	jmp    37a <gets+0x61>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 379:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 37a:	8b 45 f0             	mov    -0x10(%ebp),%eax
 37d:	03 45 08             	add    0x8(%ebp),%eax
 380:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 383:	8b 45 08             	mov    0x8(%ebp),%eax
}
 386:	c9                   	leave  
 387:	c3                   	ret    

00000388 <stat>:

int
stat(char *n, struct stat *st)
{
 388:	55                   	push   %ebp
 389:	89 e5                	mov    %esp,%ebp
 38b:	83 ec 28             	sub    $0x28,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 38e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 395:	00 
 396:	8b 45 08             	mov    0x8(%ebp),%eax
 399:	89 04 24             	mov    %eax,(%esp)
 39c:	e8 07 01 00 00       	call   4a8 <open>
 3a1:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(fd < 0)
 3a4:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 3a8:	79 07                	jns    3b1 <stat+0x29>
    return -1;
 3aa:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 3af:	eb 23                	jmp    3d4 <stat+0x4c>
  r = fstat(fd, st);
 3b1:	8b 45 0c             	mov    0xc(%ebp),%eax
 3b4:	89 44 24 04          	mov    %eax,0x4(%esp)
 3b8:	8b 45 f0             	mov    -0x10(%ebp),%eax
 3bb:	89 04 24             	mov    %eax,(%esp)
 3be:	e8 fd 00 00 00       	call   4c0 <fstat>
 3c3:	89 45 f4             	mov    %eax,-0xc(%ebp)
  close(fd);
 3c6:	8b 45 f0             	mov    -0x10(%ebp),%eax
 3c9:	89 04 24             	mov    %eax,(%esp)
 3cc:	e8 bf 00 00 00       	call   490 <close>
  return r;
 3d1:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
 3d4:	c9                   	leave  
 3d5:	c3                   	ret    

000003d6 <atoi>:

int
atoi(const char *s)
{
 3d6:	55                   	push   %ebp
 3d7:	89 e5                	mov    %esp,%ebp
 3d9:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 3dc:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 3e3:	eb 24                	jmp    409 <atoi+0x33>
    n = n*10 + *s++ - '0';
 3e5:	8b 55 fc             	mov    -0x4(%ebp),%edx
 3e8:	89 d0                	mov    %edx,%eax
 3ea:	c1 e0 02             	shl    $0x2,%eax
 3ed:	01 d0                	add    %edx,%eax
 3ef:	01 c0                	add    %eax,%eax
 3f1:	89 c2                	mov    %eax,%edx
 3f3:	8b 45 08             	mov    0x8(%ebp),%eax
 3f6:	0f b6 00             	movzbl (%eax),%eax
 3f9:	0f be c0             	movsbl %al,%eax
 3fc:	8d 04 02             	lea    (%edx,%eax,1),%eax
 3ff:	83 e8 30             	sub    $0x30,%eax
 402:	89 45 fc             	mov    %eax,-0x4(%ebp)
 405:	83 45 08 01          	addl   $0x1,0x8(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 409:	8b 45 08             	mov    0x8(%ebp),%eax
 40c:	0f b6 00             	movzbl (%eax),%eax
 40f:	3c 2f                	cmp    $0x2f,%al
 411:	7e 0a                	jle    41d <atoi+0x47>
 413:	8b 45 08             	mov    0x8(%ebp),%eax
 416:	0f b6 00             	movzbl (%eax),%eax
 419:	3c 39                	cmp    $0x39,%al
 41b:	7e c8                	jle    3e5 <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 41d:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 420:	c9                   	leave  
 421:	c3                   	ret    

00000422 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 422:	55                   	push   %ebp
 423:	89 e5                	mov    %esp,%ebp
 425:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 428:	8b 45 08             	mov    0x8(%ebp),%eax
 42b:	89 45 f8             	mov    %eax,-0x8(%ebp)
  src = vsrc;
 42e:	8b 45 0c             	mov    0xc(%ebp),%eax
 431:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0)
 434:	eb 13                	jmp    449 <memmove+0x27>
    *dst++ = *src++;
 436:	8b 45 fc             	mov    -0x4(%ebp),%eax
 439:	0f b6 10             	movzbl (%eax),%edx
 43c:	8b 45 f8             	mov    -0x8(%ebp),%eax
 43f:	88 10                	mov    %dl,(%eax)
 441:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
 445:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 449:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
 44d:	0f 9f c0             	setg   %al
 450:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
 454:	84 c0                	test   %al,%al
 456:	75 de                	jne    436 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 458:	8b 45 08             	mov    0x8(%ebp),%eax
}
 45b:	c9                   	leave  
 45c:	c3                   	ret    
 45d:	90                   	nop
 45e:	90                   	nop
 45f:	90                   	nop

00000460 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 460:	b8 01 00 00 00       	mov    $0x1,%eax
 465:	cd 40                	int    $0x40
 467:	c3                   	ret    

00000468 <exit>:
SYSCALL(exit)
 468:	b8 02 00 00 00       	mov    $0x2,%eax
 46d:	cd 40                	int    $0x40
 46f:	c3                   	ret    

00000470 <wait>:
SYSCALL(wait)
 470:	b8 03 00 00 00       	mov    $0x3,%eax
 475:	cd 40                	int    $0x40
 477:	c3                   	ret    

00000478 <pipe>:
SYSCALL(pipe)
 478:	b8 04 00 00 00       	mov    $0x4,%eax
 47d:	cd 40                	int    $0x40
 47f:	c3                   	ret    

00000480 <read>:
SYSCALL(read)
 480:	b8 05 00 00 00       	mov    $0x5,%eax
 485:	cd 40                	int    $0x40
 487:	c3                   	ret    

00000488 <write>:
SYSCALL(write)
 488:	b8 10 00 00 00       	mov    $0x10,%eax
 48d:	cd 40                	int    $0x40
 48f:	c3                   	ret    

00000490 <close>:
SYSCALL(close)
 490:	b8 15 00 00 00       	mov    $0x15,%eax
 495:	cd 40                	int    $0x40
 497:	c3                   	ret    

00000498 <kill>:
SYSCALL(kill)
 498:	b8 06 00 00 00       	mov    $0x6,%eax
 49d:	cd 40                	int    $0x40
 49f:	c3                   	ret    

000004a0 <exec>:
SYSCALL(exec)
 4a0:	b8 07 00 00 00       	mov    $0x7,%eax
 4a5:	cd 40                	int    $0x40
 4a7:	c3                   	ret    

000004a8 <open>:
SYSCALL(open)
 4a8:	b8 0f 00 00 00       	mov    $0xf,%eax
 4ad:	cd 40                	int    $0x40
 4af:	c3                   	ret    

000004b0 <mknod>:
SYSCALL(mknod)
 4b0:	b8 11 00 00 00       	mov    $0x11,%eax
 4b5:	cd 40                	int    $0x40
 4b7:	c3                   	ret    

000004b8 <unlink>:
SYSCALL(unlink)
 4b8:	b8 12 00 00 00       	mov    $0x12,%eax
 4bd:	cd 40                	int    $0x40
 4bf:	c3                   	ret    

000004c0 <fstat>:
SYSCALL(fstat)
 4c0:	b8 08 00 00 00       	mov    $0x8,%eax
 4c5:	cd 40                	int    $0x40
 4c7:	c3                   	ret    

000004c8 <link>:
SYSCALL(link)
 4c8:	b8 13 00 00 00       	mov    $0x13,%eax
 4cd:	cd 40                	int    $0x40
 4cf:	c3                   	ret    

000004d0 <mkdir>:
SYSCALL(mkdir)
 4d0:	b8 14 00 00 00       	mov    $0x14,%eax
 4d5:	cd 40                	int    $0x40
 4d7:	c3                   	ret    

000004d8 <chdir>:
SYSCALL(chdir)
 4d8:	b8 09 00 00 00       	mov    $0x9,%eax
 4dd:	cd 40                	int    $0x40
 4df:	c3                   	ret    

000004e0 <dup>:
SYSCALL(dup)
 4e0:	b8 0a 00 00 00       	mov    $0xa,%eax
 4e5:	cd 40                	int    $0x40
 4e7:	c3                   	ret    

000004e8 <getpid>:
SYSCALL(getpid)
 4e8:	b8 0b 00 00 00       	mov    $0xb,%eax
 4ed:	cd 40                	int    $0x40
 4ef:	c3                   	ret    

000004f0 <sbrk>:
SYSCALL(sbrk)
 4f0:	b8 0c 00 00 00       	mov    $0xc,%eax
 4f5:	cd 40                	int    $0x40
 4f7:	c3                   	ret    

000004f8 <sleep>:
SYSCALL(sleep)
 4f8:	b8 0d 00 00 00       	mov    $0xd,%eax
 4fd:	cd 40                	int    $0x40
 4ff:	c3                   	ret    

00000500 <uptime>:
SYSCALL(uptime)
 500:	b8 0e 00 00 00       	mov    $0xe,%eax
 505:	cd 40                	int    $0x40
 507:	c3                   	ret    

00000508 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 508:	55                   	push   %ebp
 509:	89 e5                	mov    %esp,%ebp
 50b:	83 ec 28             	sub    $0x28,%esp
 50e:	8b 45 0c             	mov    0xc(%ebp),%eax
 511:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 514:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 51b:	00 
 51c:	8d 45 f4             	lea    -0xc(%ebp),%eax
 51f:	89 44 24 04          	mov    %eax,0x4(%esp)
 523:	8b 45 08             	mov    0x8(%ebp),%eax
 526:	89 04 24             	mov    %eax,(%esp)
 529:	e8 5a ff ff ff       	call   488 <write>
}
 52e:	c9                   	leave  
 52f:	c3                   	ret    

00000530 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 530:	55                   	push   %ebp
 531:	89 e5                	mov    %esp,%ebp
 533:	53                   	push   %ebx
 534:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 537:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 53e:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 542:	74 17                	je     55b <printint+0x2b>
 544:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 548:	79 11                	jns    55b <printint+0x2b>
    neg = 1;
 54a:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 551:	8b 45 0c             	mov    0xc(%ebp),%eax
 554:	f7 d8                	neg    %eax
 556:	89 45 f4             	mov    %eax,-0xc(%ebp)
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 559:	eb 06                	jmp    561 <printint+0x31>
    neg = 1;
    x = -xx;
  } else {
    x = xx;
 55b:	8b 45 0c             	mov    0xc(%ebp),%eax
 55e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  }

  i = 0;
 561:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  do{
    buf[i++] = digits[x % base];
 568:	8b 4d ec             	mov    -0x14(%ebp),%ecx
 56b:	8b 5d 10             	mov    0x10(%ebp),%ebx
 56e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 571:	ba 00 00 00 00       	mov    $0x0,%edx
 576:	f7 f3                	div    %ebx
 578:	89 d0                	mov    %edx,%eax
 57a:	0f b6 80 64 0a 00 00 	movzbl 0xa64(%eax),%eax
 581:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
 585:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
  }while((x /= base) != 0);
 589:	8b 45 10             	mov    0x10(%ebp),%eax
 58c:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 58f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 592:	ba 00 00 00 00       	mov    $0x0,%edx
 597:	f7 75 d4             	divl   -0x2c(%ebp)
 59a:	89 45 f4             	mov    %eax,-0xc(%ebp)
 59d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 5a1:	75 c5                	jne    568 <printint+0x38>
  if(neg)
 5a3:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 5a7:	74 2a                	je     5d3 <printint+0xa3>
    buf[i++] = '-';
 5a9:	8b 45 ec             	mov    -0x14(%ebp),%eax
 5ac:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)
 5b1:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)

  while(--i >= 0)
 5b5:	eb 1d                	jmp    5d4 <printint+0xa4>
    putc(fd, buf[i]);
 5b7:	8b 45 ec             	mov    -0x14(%ebp),%eax
 5ba:	0f b6 44 05 dc       	movzbl -0x24(%ebp,%eax,1),%eax
 5bf:	0f be c0             	movsbl %al,%eax
 5c2:	89 44 24 04          	mov    %eax,0x4(%esp)
 5c6:	8b 45 08             	mov    0x8(%ebp),%eax
 5c9:	89 04 24             	mov    %eax,(%esp)
 5cc:	e8 37 ff ff ff       	call   508 <putc>
 5d1:	eb 01                	jmp    5d4 <printint+0xa4>
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 5d3:	90                   	nop
 5d4:	83 6d ec 01          	subl   $0x1,-0x14(%ebp)
 5d8:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 5dc:	79 d9                	jns    5b7 <printint+0x87>
    putc(fd, buf[i]);
}
 5de:	83 c4 44             	add    $0x44,%esp
 5e1:	5b                   	pop    %ebx
 5e2:	5d                   	pop    %ebp
 5e3:	c3                   	ret    

000005e4 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 5e4:	55                   	push   %ebp
 5e5:	89 e5                	mov    %esp,%ebp
 5e7:	83 ec 38             	sub    $0x38,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 5ea:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 5f1:	8d 45 0c             	lea    0xc(%ebp),%eax
 5f4:	83 c0 04             	add    $0x4,%eax
 5f7:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(i = 0; fmt[i]; i++){
 5fa:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 601:	e9 7e 01 00 00       	jmp    784 <printf+0x1a0>
    c = fmt[i] & 0xff;
 606:	8b 55 0c             	mov    0xc(%ebp),%edx
 609:	8b 45 ec             	mov    -0x14(%ebp),%eax
 60c:	8d 04 02             	lea    (%edx,%eax,1),%eax
 60f:	0f b6 00             	movzbl (%eax),%eax
 612:	0f be c0             	movsbl %al,%eax
 615:	25 ff 00 00 00       	and    $0xff,%eax
 61a:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(state == 0){
 61d:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 621:	75 2c                	jne    64f <printf+0x6b>
      if(c == '%'){
 623:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 627:	75 0c                	jne    635 <printf+0x51>
        state = '%';
 629:	c7 45 f0 25 00 00 00 	movl   $0x25,-0x10(%ebp)
 630:	e9 4b 01 00 00       	jmp    780 <printf+0x19c>
      } else {
        putc(fd, c);
 635:	8b 45 e8             	mov    -0x18(%ebp),%eax
 638:	0f be c0             	movsbl %al,%eax
 63b:	89 44 24 04          	mov    %eax,0x4(%esp)
 63f:	8b 45 08             	mov    0x8(%ebp),%eax
 642:	89 04 24             	mov    %eax,(%esp)
 645:	e8 be fe ff ff       	call   508 <putc>
 64a:	e9 31 01 00 00       	jmp    780 <printf+0x19c>
      }
    } else if(state == '%'){
 64f:	83 7d f0 25          	cmpl   $0x25,-0x10(%ebp)
 653:	0f 85 27 01 00 00    	jne    780 <printf+0x19c>
      if(c == 'd'){
 659:	83 7d e8 64          	cmpl   $0x64,-0x18(%ebp)
 65d:	75 2d                	jne    68c <printf+0xa8>
        printint(fd, *ap, 10, 1);
 65f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 662:	8b 00                	mov    (%eax),%eax
 664:	c7 44 24 0c 01 00 00 	movl   $0x1,0xc(%esp)
 66b:	00 
 66c:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
 673:	00 
 674:	89 44 24 04          	mov    %eax,0x4(%esp)
 678:	8b 45 08             	mov    0x8(%ebp),%eax
 67b:	89 04 24             	mov    %eax,(%esp)
 67e:	e8 ad fe ff ff       	call   530 <printint>
        ap++;
 683:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 687:	e9 ed 00 00 00       	jmp    779 <printf+0x195>
      } else if(c == 'x' || c == 'p'){
 68c:	83 7d e8 78          	cmpl   $0x78,-0x18(%ebp)
 690:	74 06                	je     698 <printf+0xb4>
 692:	83 7d e8 70          	cmpl   $0x70,-0x18(%ebp)
 696:	75 2d                	jne    6c5 <printf+0xe1>
        printint(fd, *ap, 16, 0);
 698:	8b 45 f4             	mov    -0xc(%ebp),%eax
 69b:	8b 00                	mov    (%eax),%eax
 69d:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
 6a4:	00 
 6a5:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
 6ac:	00 
 6ad:	89 44 24 04          	mov    %eax,0x4(%esp)
 6b1:	8b 45 08             	mov    0x8(%ebp),%eax
 6b4:	89 04 24             	mov    %eax,(%esp)
 6b7:	e8 74 fe ff ff       	call   530 <printint>
        ap++;
 6bc:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
      }
    } else if(state == '%'){
      if(c == 'd'){
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 6c0:	e9 b4 00 00 00       	jmp    779 <printf+0x195>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 6c5:	83 7d e8 73          	cmpl   $0x73,-0x18(%ebp)
 6c9:	75 46                	jne    711 <printf+0x12d>
        s = (char*)*ap;
 6cb:	8b 45 f4             	mov    -0xc(%ebp),%eax
 6ce:	8b 00                	mov    (%eax),%eax
 6d0:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        ap++;
 6d3:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
        if(s == 0)
 6d7:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 6db:	75 27                	jne    704 <printf+0x120>
          s = "(null)";
 6dd:	c7 45 e4 5b 0a 00 00 	movl   $0xa5b,-0x1c(%ebp)
        while(*s != 0){
 6e4:	eb 1f                	jmp    705 <printf+0x121>
          putc(fd, *s);
 6e6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 6e9:	0f b6 00             	movzbl (%eax),%eax
 6ec:	0f be c0             	movsbl %al,%eax
 6ef:	89 44 24 04          	mov    %eax,0x4(%esp)
 6f3:	8b 45 08             	mov    0x8(%ebp),%eax
 6f6:	89 04 24             	mov    %eax,(%esp)
 6f9:	e8 0a fe ff ff       	call   508 <putc>
          s++;
 6fe:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
 702:	eb 01                	jmp    705 <printf+0x121>
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 704:	90                   	nop
 705:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 708:	0f b6 00             	movzbl (%eax),%eax
 70b:	84 c0                	test   %al,%al
 70d:	75 d7                	jne    6e6 <printf+0x102>
 70f:	eb 68                	jmp    779 <printf+0x195>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 711:	83 7d e8 63          	cmpl   $0x63,-0x18(%ebp)
 715:	75 1d                	jne    734 <printf+0x150>
        putc(fd, *ap);
 717:	8b 45 f4             	mov    -0xc(%ebp),%eax
 71a:	8b 00                	mov    (%eax),%eax
 71c:	0f be c0             	movsbl %al,%eax
 71f:	89 44 24 04          	mov    %eax,0x4(%esp)
 723:	8b 45 08             	mov    0x8(%ebp),%eax
 726:	89 04 24             	mov    %eax,(%esp)
 729:	e8 da fd ff ff       	call   508 <putc>
        ap++;
 72e:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 732:	eb 45                	jmp    779 <printf+0x195>
      } else if(c == '%'){
 734:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 738:	75 17                	jne    751 <printf+0x16d>
        putc(fd, c);
 73a:	8b 45 e8             	mov    -0x18(%ebp),%eax
 73d:	0f be c0             	movsbl %al,%eax
 740:	89 44 24 04          	mov    %eax,0x4(%esp)
 744:	8b 45 08             	mov    0x8(%ebp),%eax
 747:	89 04 24             	mov    %eax,(%esp)
 74a:	e8 b9 fd ff ff       	call   508 <putc>
 74f:	eb 28                	jmp    779 <printf+0x195>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 751:	c7 44 24 04 25 00 00 	movl   $0x25,0x4(%esp)
 758:	00 
 759:	8b 45 08             	mov    0x8(%ebp),%eax
 75c:	89 04 24             	mov    %eax,(%esp)
 75f:	e8 a4 fd ff ff       	call   508 <putc>
        putc(fd, c);
 764:	8b 45 e8             	mov    -0x18(%ebp),%eax
 767:	0f be c0             	movsbl %al,%eax
 76a:	89 44 24 04          	mov    %eax,0x4(%esp)
 76e:	8b 45 08             	mov    0x8(%ebp),%eax
 771:	89 04 24             	mov    %eax,(%esp)
 774:	e8 8f fd ff ff       	call   508 <putc>
      }
      state = 0;
 779:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 780:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
 784:	8b 55 0c             	mov    0xc(%ebp),%edx
 787:	8b 45 ec             	mov    -0x14(%ebp),%eax
 78a:	8d 04 02             	lea    (%edx,%eax,1),%eax
 78d:	0f b6 00             	movzbl (%eax),%eax
 790:	84 c0                	test   %al,%al
 792:	0f 85 6e fe ff ff    	jne    606 <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 798:	c9                   	leave  
 799:	c3                   	ret    
 79a:	90                   	nop
 79b:	90                   	nop

0000079c <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 79c:	55                   	push   %ebp
 79d:	89 e5                	mov    %esp,%ebp
 79f:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 7a2:	8b 45 08             	mov    0x8(%ebp),%eax
 7a5:	83 e8 08             	sub    $0x8,%eax
 7a8:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 7ab:	a1 80 0a 00 00       	mov    0xa80,%eax
 7b0:	89 45 fc             	mov    %eax,-0x4(%ebp)
 7b3:	eb 24                	jmp    7d9 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 7b5:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7b8:	8b 00                	mov    (%eax),%eax
 7ba:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 7bd:	77 12                	ja     7d1 <free+0x35>
 7bf:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7c2:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 7c5:	77 24                	ja     7eb <free+0x4f>
 7c7:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7ca:	8b 00                	mov    (%eax),%eax
 7cc:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 7cf:	77 1a                	ja     7eb <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 7d1:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7d4:	8b 00                	mov    (%eax),%eax
 7d6:	89 45 fc             	mov    %eax,-0x4(%ebp)
 7d9:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7dc:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 7df:	76 d4                	jbe    7b5 <free+0x19>
 7e1:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7e4:	8b 00                	mov    (%eax),%eax
 7e6:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 7e9:	76 ca                	jbe    7b5 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 7eb:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7ee:	8b 40 04             	mov    0x4(%eax),%eax
 7f1:	c1 e0 03             	shl    $0x3,%eax
 7f4:	89 c2                	mov    %eax,%edx
 7f6:	03 55 f8             	add    -0x8(%ebp),%edx
 7f9:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7fc:	8b 00                	mov    (%eax),%eax
 7fe:	39 c2                	cmp    %eax,%edx
 800:	75 24                	jne    826 <free+0x8a>
    bp->s.size += p->s.ptr->s.size;
 802:	8b 45 f8             	mov    -0x8(%ebp),%eax
 805:	8b 50 04             	mov    0x4(%eax),%edx
 808:	8b 45 fc             	mov    -0x4(%ebp),%eax
 80b:	8b 00                	mov    (%eax),%eax
 80d:	8b 40 04             	mov    0x4(%eax),%eax
 810:	01 c2                	add    %eax,%edx
 812:	8b 45 f8             	mov    -0x8(%ebp),%eax
 815:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 818:	8b 45 fc             	mov    -0x4(%ebp),%eax
 81b:	8b 00                	mov    (%eax),%eax
 81d:	8b 10                	mov    (%eax),%edx
 81f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 822:	89 10                	mov    %edx,(%eax)
 824:	eb 0a                	jmp    830 <free+0x94>
  } else
    bp->s.ptr = p->s.ptr;
 826:	8b 45 fc             	mov    -0x4(%ebp),%eax
 829:	8b 10                	mov    (%eax),%edx
 82b:	8b 45 f8             	mov    -0x8(%ebp),%eax
 82e:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 830:	8b 45 fc             	mov    -0x4(%ebp),%eax
 833:	8b 40 04             	mov    0x4(%eax),%eax
 836:	c1 e0 03             	shl    $0x3,%eax
 839:	03 45 fc             	add    -0x4(%ebp),%eax
 83c:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 83f:	75 20                	jne    861 <free+0xc5>
    p->s.size += bp->s.size;
 841:	8b 45 fc             	mov    -0x4(%ebp),%eax
 844:	8b 50 04             	mov    0x4(%eax),%edx
 847:	8b 45 f8             	mov    -0x8(%ebp),%eax
 84a:	8b 40 04             	mov    0x4(%eax),%eax
 84d:	01 c2                	add    %eax,%edx
 84f:	8b 45 fc             	mov    -0x4(%ebp),%eax
 852:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 855:	8b 45 f8             	mov    -0x8(%ebp),%eax
 858:	8b 10                	mov    (%eax),%edx
 85a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 85d:	89 10                	mov    %edx,(%eax)
 85f:	eb 08                	jmp    869 <free+0xcd>
  } else
    p->s.ptr = bp;
 861:	8b 45 fc             	mov    -0x4(%ebp),%eax
 864:	8b 55 f8             	mov    -0x8(%ebp),%edx
 867:	89 10                	mov    %edx,(%eax)
  freep = p;
 869:	8b 45 fc             	mov    -0x4(%ebp),%eax
 86c:	a3 80 0a 00 00       	mov    %eax,0xa80
}
 871:	c9                   	leave  
 872:	c3                   	ret    

00000873 <morecore>:

static Header*
morecore(uint nu)
{
 873:	55                   	push   %ebp
 874:	89 e5                	mov    %esp,%ebp
 876:	83 ec 28             	sub    $0x28,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 879:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 880:	77 07                	ja     889 <morecore+0x16>
    nu = 4096;
 882:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 889:	8b 45 08             	mov    0x8(%ebp),%eax
 88c:	c1 e0 03             	shl    $0x3,%eax
 88f:	89 04 24             	mov    %eax,(%esp)
 892:	e8 59 fc ff ff       	call   4f0 <sbrk>
 897:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(p == (char*)-1)
 89a:	83 7d f0 ff          	cmpl   $0xffffffff,-0x10(%ebp)
 89e:	75 07                	jne    8a7 <morecore+0x34>
    return 0;
 8a0:	b8 00 00 00 00       	mov    $0x0,%eax
 8a5:	eb 22                	jmp    8c9 <morecore+0x56>
  hp = (Header*)p;
 8a7:	8b 45 f0             	mov    -0x10(%ebp),%eax
 8aa:	89 45 f4             	mov    %eax,-0xc(%ebp)
  hp->s.size = nu;
 8ad:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8b0:	8b 55 08             	mov    0x8(%ebp),%edx
 8b3:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 8b6:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8b9:	83 c0 08             	add    $0x8,%eax
 8bc:	89 04 24             	mov    %eax,(%esp)
 8bf:	e8 d8 fe ff ff       	call   79c <free>
  return freep;
 8c4:	a1 80 0a 00 00       	mov    0xa80,%eax
}
 8c9:	c9                   	leave  
 8ca:	c3                   	ret    

000008cb <malloc>:

void*
malloc(uint nbytes)
{
 8cb:	55                   	push   %ebp
 8cc:	89 e5                	mov    %esp,%ebp
 8ce:	83 ec 28             	sub    $0x28,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 8d1:	8b 45 08             	mov    0x8(%ebp),%eax
 8d4:	83 c0 07             	add    $0x7,%eax
 8d7:	c1 e8 03             	shr    $0x3,%eax
 8da:	83 c0 01             	add    $0x1,%eax
 8dd:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((prevp = freep) == 0){
 8e0:	a1 80 0a 00 00       	mov    0xa80,%eax
 8e5:	89 45 f0             	mov    %eax,-0x10(%ebp)
 8e8:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 8ec:	75 23                	jne    911 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 8ee:	c7 45 f0 78 0a 00 00 	movl   $0xa78,-0x10(%ebp)
 8f5:	8b 45 f0             	mov    -0x10(%ebp),%eax
 8f8:	a3 80 0a 00 00       	mov    %eax,0xa80
 8fd:	a1 80 0a 00 00       	mov    0xa80,%eax
 902:	a3 78 0a 00 00       	mov    %eax,0xa78
    base.s.size = 0;
 907:	c7 05 7c 0a 00 00 00 	movl   $0x0,0xa7c
 90e:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 911:	8b 45 f0             	mov    -0x10(%ebp),%eax
 914:	8b 00                	mov    (%eax),%eax
 916:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(p->s.size >= nunits){
 919:	8b 45 ec             	mov    -0x14(%ebp),%eax
 91c:	8b 40 04             	mov    0x4(%eax),%eax
 91f:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 922:	72 4d                	jb     971 <malloc+0xa6>
      if(p->s.size == nunits)
 924:	8b 45 ec             	mov    -0x14(%ebp),%eax
 927:	8b 40 04             	mov    0x4(%eax),%eax
 92a:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 92d:	75 0c                	jne    93b <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 92f:	8b 45 ec             	mov    -0x14(%ebp),%eax
 932:	8b 10                	mov    (%eax),%edx
 934:	8b 45 f0             	mov    -0x10(%ebp),%eax
 937:	89 10                	mov    %edx,(%eax)
 939:	eb 26                	jmp    961 <malloc+0x96>
      else {
        p->s.size -= nunits;
 93b:	8b 45 ec             	mov    -0x14(%ebp),%eax
 93e:	8b 40 04             	mov    0x4(%eax),%eax
 941:	89 c2                	mov    %eax,%edx
 943:	2b 55 f4             	sub    -0xc(%ebp),%edx
 946:	8b 45 ec             	mov    -0x14(%ebp),%eax
 949:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 94c:	8b 45 ec             	mov    -0x14(%ebp),%eax
 94f:	8b 40 04             	mov    0x4(%eax),%eax
 952:	c1 e0 03             	shl    $0x3,%eax
 955:	01 45 ec             	add    %eax,-0x14(%ebp)
        p->s.size = nunits;
 958:	8b 45 ec             	mov    -0x14(%ebp),%eax
 95b:	8b 55 f4             	mov    -0xc(%ebp),%edx
 95e:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 961:	8b 45 f0             	mov    -0x10(%ebp),%eax
 964:	a3 80 0a 00 00       	mov    %eax,0xa80
      return (void*)(p + 1);
 969:	8b 45 ec             	mov    -0x14(%ebp),%eax
 96c:	83 c0 08             	add    $0x8,%eax
 96f:	eb 38                	jmp    9a9 <malloc+0xde>
    }
    if(p == freep)
 971:	a1 80 0a 00 00       	mov    0xa80,%eax
 976:	39 45 ec             	cmp    %eax,-0x14(%ebp)
 979:	75 1b                	jne    996 <malloc+0xcb>
      if((p = morecore(nunits)) == 0)
 97b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 97e:	89 04 24             	mov    %eax,(%esp)
 981:	e8 ed fe ff ff       	call   873 <morecore>
 986:	89 45 ec             	mov    %eax,-0x14(%ebp)
 989:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 98d:	75 07                	jne    996 <malloc+0xcb>
        return 0;
 98f:	b8 00 00 00 00       	mov    $0x0,%eax
 994:	eb 13                	jmp    9a9 <malloc+0xde>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 996:	8b 45 ec             	mov    -0x14(%ebp),%eax
 999:	89 45 f0             	mov    %eax,-0x10(%ebp)
 99c:	8b 45 ec             	mov    -0x14(%ebp),%eax
 99f:	8b 00                	mov    (%eax),%eax
 9a1:	89 45 ec             	mov    %eax,-0x14(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 9a4:	e9 70 ff ff ff       	jmp    919 <malloc+0x4e>
}
 9a9:	c9                   	leave  
 9aa:	c3                   	ret    
