
_head:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "stat.h"
#include "user.h"
char buf;

int main(int argc, char *argv[])
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 e4 f0             	and    $0xfffffff0,%esp
   6:	83 ec 20             	sub    $0x20,%esp
    int Fdata, NLines;
    int filename;
    int linecount = 0;
   9:	c7 44 24 1c 00 00 00 	movl   $0x0,0x1c(%esp)
  10:	00 
    if (argc<=1)
  11:	83 7d 08 01          	cmpl   $0x1,0x8(%ebp)
  15:	7f 25                	jg     3c <main+0x3c>
    {
        printf(1,"Usage: head file OR head file #oflines %sn", argv[1]);
  17:	8b 45 0c             	mov    0xc(%ebp),%eax
  1a:	83 c0 04             	add    $0x4,%eax
  1d:	8b 00                	mov    (%eax),%eax
  1f:	89 44 24 08          	mov    %eax,0x8(%esp)
  23:	c7 44 24 04 f8 08 00 	movl   $0x8f8,0x4(%esp)
  2a:	00 
  2b:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  32:	e8 f9 04 00 00       	call   530 <printf>
        exit();
  37:	e8 78 03 00 00       	call   3b4 <exit>
    }
    if((filename = open(argv[1],0)) < 0)
  3c:	8b 45 0c             	mov    0xc(%ebp),%eax
  3f:	83 c0 04             	add    $0x4,%eax
  42:	8b 00                	mov    (%eax),%eax
  44:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  4b:	00 
  4c:	89 04 24             	mov    %eax,(%esp)
  4f:	e8 a0 03 00 00       	call   3f4 <open>
  54:	89 44 24 18          	mov    %eax,0x18(%esp)
  58:	83 7c 24 18 00       	cmpl   $0x0,0x18(%esp)
  5d:	79 25                	jns    84 <main+0x84>
    {
        printf(1,"Error, cannot open %sn", argv[1]);
  5f:	8b 45 0c             	mov    0xc(%ebp),%eax
  62:	83 c0 04             	add    $0x4,%eax
  65:	8b 00                	mov    (%eax),%eax
  67:	89 44 24 08          	mov    %eax,0x8(%esp)
  6b:	c7 44 24 04 23 09 00 	movl   $0x923,0x4(%esp)
  72:	00 
  73:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  7a:	e8 b1 04 00 00       	call   530 <printf>
        exit();
  7f:	e8 30 03 00 00       	call   3b4 <exit>
    }
    if(argc ==2)
  84:	83 7d 08 02          	cmpl   $0x2,0x8(%ebp)
  88:	75 0a                	jne    94 <main+0x94>
    {
        NLines = 10;
  8a:	c7 44 24 14 0a 00 00 	movl   $0xa,0x14(%esp)
  91:	00 
    else if(argc == 3)
    {
        NLines =  (int) atoi(argv[2]);
    }

    while((Fdata=read(filename, &buf, 1))>0)
  92:	eb 63                	jmp    f7 <main+0xf7>
    }
    if(argc ==2)
    {
        NLines = 10;
    }
    else if(argc == 3)
  94:	83 7d 08 03          	cmpl   $0x3,0x8(%ebp)
  98:	75 5c                	jne    f6 <main+0xf6>
    {
        NLines =  (int) atoi(argv[2]);
  9a:	8b 45 0c             	mov    0xc(%ebp),%eax
  9d:	83 c0 08             	add    $0x8,%eax
  a0:	8b 00                	mov    (%eax),%eax
  a2:	89 04 24             	mov    %eax,(%esp)
  a5:	e8 78 02 00 00       	call   322 <atoi>
  aa:	89 44 24 14          	mov    %eax,0x14(%esp)
    }

    while((Fdata=read(filename, &buf, 1))>0)
  ae:	eb 47                	jmp    f7 <main+0xf7>
    {
	if(linecount > NLines -1){
  b0:	8b 44 24 14          	mov    0x14(%esp),%eax
  b4:	83 e8 01             	sub    $0x1,%eax
  b7:	3b 44 24 1c          	cmp    0x1c(%esp),%eax
  bb:	7d 05                	jge    c2 <main+0xc2>
	    exit();
  bd:	e8 f2 02 00 00       	call   3b4 <exit>
	}
        if(buf == '\n')
  c2:	0f b6 05 78 09 00 00 	movzbl 0x978,%eax
  c9:	3c 0a                	cmp    $0xa,%al
  cb:	75 05                	jne    d2 <main+0xd2>
        {
            linecount++;
  cd:	83 44 24 1c 01       	addl   $0x1,0x1c(%esp)
        }
        printf(1,"%c",buf);
  d2:	0f b6 05 78 09 00 00 	movzbl 0x978,%eax
  d9:	0f be c0             	movsbl %al,%eax
  dc:	89 44 24 08          	mov    %eax,0x8(%esp)
  e0:	c7 44 24 04 3a 09 00 	movl   $0x93a,0x4(%esp)
  e7:	00 
  e8:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  ef:	e8 3c 04 00 00       	call   530 <printf>
  f4:	eb 01                	jmp    f7 <main+0xf7>
    else if(argc == 3)
    {
        NLines =  (int) atoi(argv[2]);
    }

    while((Fdata=read(filename, &buf, 1))>0)
  f6:	90                   	nop
  f7:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
  fe:	00 
  ff:	c7 44 24 04 78 09 00 	movl   $0x978,0x4(%esp)
 106:	00 
 107:	8b 44 24 18          	mov    0x18(%esp),%eax
 10b:	89 04 24             	mov    %eax,(%esp)
 10e:	e8 b9 02 00 00       	call   3cc <read>
 113:	89 44 24 10          	mov    %eax,0x10(%esp)
 117:	83 7c 24 10 00       	cmpl   $0x0,0x10(%esp)
 11c:	7f 92                	jg     b0 <main+0xb0>
        {
            linecount++;
        }
        printf(1,"%c",buf);
    }
    if(Fdata<0)
 11e:	83 7c 24 10 00       	cmpl   $0x0,0x10(%esp)
 123:	79 19                	jns    13e <main+0x13e>
    {
        printf(1,"head: read errorn");
 125:	c7 44 24 04 3d 09 00 	movl   $0x93d,0x4(%esp)
 12c:	00 
 12d:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 134:	e8 f7 03 00 00       	call   530 <printf>
        exit();
 139:	e8 76 02 00 00       	call   3b4 <exit>
    }

    close(Fdata);
 13e:	8b 44 24 10          	mov    0x10(%esp),%eax
 142:	89 04 24             	mov    %eax,(%esp)
 145:	e8 92 02 00 00       	call   3dc <close>
    exit();
 14a:	e8 65 02 00 00       	call   3b4 <exit>
 14f:	90                   	nop

00000150 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 150:	55                   	push   %ebp
 151:	89 e5                	mov    %esp,%ebp
 153:	57                   	push   %edi
 154:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 155:	8b 4d 08             	mov    0x8(%ebp),%ecx
 158:	8b 55 10             	mov    0x10(%ebp),%edx
 15b:	8b 45 0c             	mov    0xc(%ebp),%eax
 15e:	89 cb                	mov    %ecx,%ebx
 160:	89 df                	mov    %ebx,%edi
 162:	89 d1                	mov    %edx,%ecx
 164:	fc                   	cld    
 165:	f3 aa                	rep stos %al,%es:(%edi)
 167:	89 ca                	mov    %ecx,%edx
 169:	89 fb                	mov    %edi,%ebx
 16b:	89 5d 08             	mov    %ebx,0x8(%ebp)
 16e:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 171:	5b                   	pop    %ebx
 172:	5f                   	pop    %edi
 173:	5d                   	pop    %ebp
 174:	c3                   	ret    

00000175 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 175:	55                   	push   %ebp
 176:	89 e5                	mov    %esp,%ebp
 178:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 17b:	8b 45 08             	mov    0x8(%ebp),%eax
 17e:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 181:	8b 45 0c             	mov    0xc(%ebp),%eax
 184:	0f b6 10             	movzbl (%eax),%edx
 187:	8b 45 08             	mov    0x8(%ebp),%eax
 18a:	88 10                	mov    %dl,(%eax)
 18c:	8b 45 08             	mov    0x8(%ebp),%eax
 18f:	0f b6 00             	movzbl (%eax),%eax
 192:	84 c0                	test   %al,%al
 194:	0f 95 c0             	setne  %al
 197:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 19b:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
 19f:	84 c0                	test   %al,%al
 1a1:	75 de                	jne    181 <strcpy+0xc>
    ;
  return os;
 1a3:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 1a6:	c9                   	leave  
 1a7:	c3                   	ret    

000001a8 <strcmp>:

int
strcmp(const char *p, const char *q)
{
 1a8:	55                   	push   %ebp
 1a9:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 1ab:	eb 08                	jmp    1b5 <strcmp+0xd>
    p++, q++;
 1ad:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 1b1:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 1b5:	8b 45 08             	mov    0x8(%ebp),%eax
 1b8:	0f b6 00             	movzbl (%eax),%eax
 1bb:	84 c0                	test   %al,%al
 1bd:	74 10                	je     1cf <strcmp+0x27>
 1bf:	8b 45 08             	mov    0x8(%ebp),%eax
 1c2:	0f b6 10             	movzbl (%eax),%edx
 1c5:	8b 45 0c             	mov    0xc(%ebp),%eax
 1c8:	0f b6 00             	movzbl (%eax),%eax
 1cb:	38 c2                	cmp    %al,%dl
 1cd:	74 de                	je     1ad <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 1cf:	8b 45 08             	mov    0x8(%ebp),%eax
 1d2:	0f b6 00             	movzbl (%eax),%eax
 1d5:	0f b6 d0             	movzbl %al,%edx
 1d8:	8b 45 0c             	mov    0xc(%ebp),%eax
 1db:	0f b6 00             	movzbl (%eax),%eax
 1de:	0f b6 c0             	movzbl %al,%eax
 1e1:	89 d1                	mov    %edx,%ecx
 1e3:	29 c1                	sub    %eax,%ecx
 1e5:	89 c8                	mov    %ecx,%eax
}
 1e7:	5d                   	pop    %ebp
 1e8:	c3                   	ret    

000001e9 <strlen>:

uint
strlen(char *s)
{
 1e9:	55                   	push   %ebp
 1ea:	89 e5                	mov    %esp,%ebp
 1ec:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 1ef:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 1f6:	eb 04                	jmp    1fc <strlen+0x13>
 1f8:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 1fc:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1ff:	03 45 08             	add    0x8(%ebp),%eax
 202:	0f b6 00             	movzbl (%eax),%eax
 205:	84 c0                	test   %al,%al
 207:	75 ef                	jne    1f8 <strlen+0xf>
    ;
  return n;
 209:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 20c:	c9                   	leave  
 20d:	c3                   	ret    

0000020e <memset>:

void*
memset(void *dst, int c, uint n)
{
 20e:	55                   	push   %ebp
 20f:	89 e5                	mov    %esp,%ebp
 211:	83 ec 0c             	sub    $0xc,%esp
  stosb(dst, c, n);
 214:	8b 45 10             	mov    0x10(%ebp),%eax
 217:	89 44 24 08          	mov    %eax,0x8(%esp)
 21b:	8b 45 0c             	mov    0xc(%ebp),%eax
 21e:	89 44 24 04          	mov    %eax,0x4(%esp)
 222:	8b 45 08             	mov    0x8(%ebp),%eax
 225:	89 04 24             	mov    %eax,(%esp)
 228:	e8 23 ff ff ff       	call   150 <stosb>
  return dst;
 22d:	8b 45 08             	mov    0x8(%ebp),%eax
}
 230:	c9                   	leave  
 231:	c3                   	ret    

00000232 <strchr>:

char*
strchr(const char *s, char c)
{
 232:	55                   	push   %ebp
 233:	89 e5                	mov    %esp,%ebp
 235:	83 ec 04             	sub    $0x4,%esp
 238:	8b 45 0c             	mov    0xc(%ebp),%eax
 23b:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 23e:	eb 14                	jmp    254 <strchr+0x22>
    if(*s == c)
 240:	8b 45 08             	mov    0x8(%ebp),%eax
 243:	0f b6 00             	movzbl (%eax),%eax
 246:	3a 45 fc             	cmp    -0x4(%ebp),%al
 249:	75 05                	jne    250 <strchr+0x1e>
      return (char*)s;
 24b:	8b 45 08             	mov    0x8(%ebp),%eax
 24e:	eb 13                	jmp    263 <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 250:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 254:	8b 45 08             	mov    0x8(%ebp),%eax
 257:	0f b6 00             	movzbl (%eax),%eax
 25a:	84 c0                	test   %al,%al
 25c:	75 e2                	jne    240 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 25e:	b8 00 00 00 00       	mov    $0x0,%eax
}
 263:	c9                   	leave  
 264:	c3                   	ret    

00000265 <gets>:

char*
gets(char *buf, int max)
{
 265:	55                   	push   %ebp
 266:	89 e5                	mov    %esp,%ebp
 268:	83 ec 28             	sub    $0x28,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 26b:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 272:	eb 44                	jmp    2b8 <gets+0x53>
    cc = read(0, &c, 1);
 274:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 27b:	00 
 27c:	8d 45 ef             	lea    -0x11(%ebp),%eax
 27f:	89 44 24 04          	mov    %eax,0x4(%esp)
 283:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 28a:	e8 3d 01 00 00       	call   3cc <read>
 28f:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(cc < 1)
 292:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 296:	7e 2d                	jle    2c5 <gets+0x60>
      break;
    buf[i++] = c;
 298:	8b 45 f0             	mov    -0x10(%ebp),%eax
 29b:	03 45 08             	add    0x8(%ebp),%eax
 29e:	0f b6 55 ef          	movzbl -0x11(%ebp),%edx
 2a2:	88 10                	mov    %dl,(%eax)
 2a4:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
    if(c == '\n' || c == '\r')
 2a8:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 2ac:	3c 0a                	cmp    $0xa,%al
 2ae:	74 16                	je     2c6 <gets+0x61>
 2b0:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 2b4:	3c 0d                	cmp    $0xd,%al
 2b6:	74 0e                	je     2c6 <gets+0x61>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 2b8:	8b 45 f0             	mov    -0x10(%ebp),%eax
 2bb:	83 c0 01             	add    $0x1,%eax
 2be:	3b 45 0c             	cmp    0xc(%ebp),%eax
 2c1:	7c b1                	jl     274 <gets+0xf>
 2c3:	eb 01                	jmp    2c6 <gets+0x61>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 2c5:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 2c6:	8b 45 f0             	mov    -0x10(%ebp),%eax
 2c9:	03 45 08             	add    0x8(%ebp),%eax
 2cc:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 2cf:	8b 45 08             	mov    0x8(%ebp),%eax
}
 2d2:	c9                   	leave  
 2d3:	c3                   	ret    

000002d4 <stat>:

int
stat(char *n, struct stat *st)
{
 2d4:	55                   	push   %ebp
 2d5:	89 e5                	mov    %esp,%ebp
 2d7:	83 ec 28             	sub    $0x28,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 2da:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 2e1:	00 
 2e2:	8b 45 08             	mov    0x8(%ebp),%eax
 2e5:	89 04 24             	mov    %eax,(%esp)
 2e8:	e8 07 01 00 00       	call   3f4 <open>
 2ed:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(fd < 0)
 2f0:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 2f4:	79 07                	jns    2fd <stat+0x29>
    return -1;
 2f6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 2fb:	eb 23                	jmp    320 <stat+0x4c>
  r = fstat(fd, st);
 2fd:	8b 45 0c             	mov    0xc(%ebp),%eax
 300:	89 44 24 04          	mov    %eax,0x4(%esp)
 304:	8b 45 f0             	mov    -0x10(%ebp),%eax
 307:	89 04 24             	mov    %eax,(%esp)
 30a:	e8 fd 00 00 00       	call   40c <fstat>
 30f:	89 45 f4             	mov    %eax,-0xc(%ebp)
  close(fd);
 312:	8b 45 f0             	mov    -0x10(%ebp),%eax
 315:	89 04 24             	mov    %eax,(%esp)
 318:	e8 bf 00 00 00       	call   3dc <close>
  return r;
 31d:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
 320:	c9                   	leave  
 321:	c3                   	ret    

00000322 <atoi>:

int
atoi(const char *s)
{
 322:	55                   	push   %ebp
 323:	89 e5                	mov    %esp,%ebp
 325:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 328:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 32f:	eb 24                	jmp    355 <atoi+0x33>
    n = n*10 + *s++ - '0';
 331:	8b 55 fc             	mov    -0x4(%ebp),%edx
 334:	89 d0                	mov    %edx,%eax
 336:	c1 e0 02             	shl    $0x2,%eax
 339:	01 d0                	add    %edx,%eax
 33b:	01 c0                	add    %eax,%eax
 33d:	89 c2                	mov    %eax,%edx
 33f:	8b 45 08             	mov    0x8(%ebp),%eax
 342:	0f b6 00             	movzbl (%eax),%eax
 345:	0f be c0             	movsbl %al,%eax
 348:	8d 04 02             	lea    (%edx,%eax,1),%eax
 34b:	83 e8 30             	sub    $0x30,%eax
 34e:	89 45 fc             	mov    %eax,-0x4(%ebp)
 351:	83 45 08 01          	addl   $0x1,0x8(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 355:	8b 45 08             	mov    0x8(%ebp),%eax
 358:	0f b6 00             	movzbl (%eax),%eax
 35b:	3c 2f                	cmp    $0x2f,%al
 35d:	7e 0a                	jle    369 <atoi+0x47>
 35f:	8b 45 08             	mov    0x8(%ebp),%eax
 362:	0f b6 00             	movzbl (%eax),%eax
 365:	3c 39                	cmp    $0x39,%al
 367:	7e c8                	jle    331 <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 369:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 36c:	c9                   	leave  
 36d:	c3                   	ret    

0000036e <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 36e:	55                   	push   %ebp
 36f:	89 e5                	mov    %esp,%ebp
 371:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 374:	8b 45 08             	mov    0x8(%ebp),%eax
 377:	89 45 f8             	mov    %eax,-0x8(%ebp)
  src = vsrc;
 37a:	8b 45 0c             	mov    0xc(%ebp),%eax
 37d:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0)
 380:	eb 13                	jmp    395 <memmove+0x27>
    *dst++ = *src++;
 382:	8b 45 fc             	mov    -0x4(%ebp),%eax
 385:	0f b6 10             	movzbl (%eax),%edx
 388:	8b 45 f8             	mov    -0x8(%ebp),%eax
 38b:	88 10                	mov    %dl,(%eax)
 38d:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
 391:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 395:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
 399:	0f 9f c0             	setg   %al
 39c:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
 3a0:	84 c0                	test   %al,%al
 3a2:	75 de                	jne    382 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 3a4:	8b 45 08             	mov    0x8(%ebp),%eax
}
 3a7:	c9                   	leave  
 3a8:	c3                   	ret    
 3a9:	90                   	nop
 3aa:	90                   	nop
 3ab:	90                   	nop

000003ac <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 3ac:	b8 01 00 00 00       	mov    $0x1,%eax
 3b1:	cd 40                	int    $0x40
 3b3:	c3                   	ret    

000003b4 <exit>:
SYSCALL(exit)
 3b4:	b8 02 00 00 00       	mov    $0x2,%eax
 3b9:	cd 40                	int    $0x40
 3bb:	c3                   	ret    

000003bc <wait>:
SYSCALL(wait)
 3bc:	b8 03 00 00 00       	mov    $0x3,%eax
 3c1:	cd 40                	int    $0x40
 3c3:	c3                   	ret    

000003c4 <pipe>:
SYSCALL(pipe)
 3c4:	b8 04 00 00 00       	mov    $0x4,%eax
 3c9:	cd 40                	int    $0x40
 3cb:	c3                   	ret    

000003cc <read>:
SYSCALL(read)
 3cc:	b8 05 00 00 00       	mov    $0x5,%eax
 3d1:	cd 40                	int    $0x40
 3d3:	c3                   	ret    

000003d4 <write>:
SYSCALL(write)
 3d4:	b8 10 00 00 00       	mov    $0x10,%eax
 3d9:	cd 40                	int    $0x40
 3db:	c3                   	ret    

000003dc <close>:
SYSCALL(close)
 3dc:	b8 15 00 00 00       	mov    $0x15,%eax
 3e1:	cd 40                	int    $0x40
 3e3:	c3                   	ret    

000003e4 <kill>:
SYSCALL(kill)
 3e4:	b8 06 00 00 00       	mov    $0x6,%eax
 3e9:	cd 40                	int    $0x40
 3eb:	c3                   	ret    

000003ec <exec>:
SYSCALL(exec)
 3ec:	b8 07 00 00 00       	mov    $0x7,%eax
 3f1:	cd 40                	int    $0x40
 3f3:	c3                   	ret    

000003f4 <open>:
SYSCALL(open)
 3f4:	b8 0f 00 00 00       	mov    $0xf,%eax
 3f9:	cd 40                	int    $0x40
 3fb:	c3                   	ret    

000003fc <mknod>:
SYSCALL(mknod)
 3fc:	b8 11 00 00 00       	mov    $0x11,%eax
 401:	cd 40                	int    $0x40
 403:	c3                   	ret    

00000404 <unlink>:
SYSCALL(unlink)
 404:	b8 12 00 00 00       	mov    $0x12,%eax
 409:	cd 40                	int    $0x40
 40b:	c3                   	ret    

0000040c <fstat>:
SYSCALL(fstat)
 40c:	b8 08 00 00 00       	mov    $0x8,%eax
 411:	cd 40                	int    $0x40
 413:	c3                   	ret    

00000414 <link>:
SYSCALL(link)
 414:	b8 13 00 00 00       	mov    $0x13,%eax
 419:	cd 40                	int    $0x40
 41b:	c3                   	ret    

0000041c <mkdir>:
SYSCALL(mkdir)
 41c:	b8 14 00 00 00       	mov    $0x14,%eax
 421:	cd 40                	int    $0x40
 423:	c3                   	ret    

00000424 <chdir>:
SYSCALL(chdir)
 424:	b8 09 00 00 00       	mov    $0x9,%eax
 429:	cd 40                	int    $0x40
 42b:	c3                   	ret    

0000042c <dup>:
SYSCALL(dup)
 42c:	b8 0a 00 00 00       	mov    $0xa,%eax
 431:	cd 40                	int    $0x40
 433:	c3                   	ret    

00000434 <getpid>:
SYSCALL(getpid)
 434:	b8 0b 00 00 00       	mov    $0xb,%eax
 439:	cd 40                	int    $0x40
 43b:	c3                   	ret    

0000043c <sbrk>:
SYSCALL(sbrk)
 43c:	b8 0c 00 00 00       	mov    $0xc,%eax
 441:	cd 40                	int    $0x40
 443:	c3                   	ret    

00000444 <sleep>:
SYSCALL(sleep)
 444:	b8 0d 00 00 00       	mov    $0xd,%eax
 449:	cd 40                	int    $0x40
 44b:	c3                   	ret    

0000044c <uptime>:
SYSCALL(uptime)
 44c:	b8 0e 00 00 00       	mov    $0xe,%eax
 451:	cd 40                	int    $0x40
 453:	c3                   	ret    

00000454 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 454:	55                   	push   %ebp
 455:	89 e5                	mov    %esp,%ebp
 457:	83 ec 28             	sub    $0x28,%esp
 45a:	8b 45 0c             	mov    0xc(%ebp),%eax
 45d:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 460:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 467:	00 
 468:	8d 45 f4             	lea    -0xc(%ebp),%eax
 46b:	89 44 24 04          	mov    %eax,0x4(%esp)
 46f:	8b 45 08             	mov    0x8(%ebp),%eax
 472:	89 04 24             	mov    %eax,(%esp)
 475:	e8 5a ff ff ff       	call   3d4 <write>
}
 47a:	c9                   	leave  
 47b:	c3                   	ret    

0000047c <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 47c:	55                   	push   %ebp
 47d:	89 e5                	mov    %esp,%ebp
 47f:	53                   	push   %ebx
 480:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 483:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 48a:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 48e:	74 17                	je     4a7 <printint+0x2b>
 490:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 494:	79 11                	jns    4a7 <printint+0x2b>
    neg = 1;
 496:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 49d:	8b 45 0c             	mov    0xc(%ebp),%eax
 4a0:	f7 d8                	neg    %eax
 4a2:	89 45 f4             	mov    %eax,-0xc(%ebp)
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 4a5:	eb 06                	jmp    4ad <printint+0x31>
    neg = 1;
    x = -xx;
  } else {
    x = xx;
 4a7:	8b 45 0c             	mov    0xc(%ebp),%eax
 4aa:	89 45 f4             	mov    %eax,-0xc(%ebp)
  }

  i = 0;
 4ad:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  do{
    buf[i++] = digits[x % base];
 4b4:	8b 4d ec             	mov    -0x14(%ebp),%ecx
 4b7:	8b 5d 10             	mov    0x10(%ebp),%ebx
 4ba:	8b 45 f4             	mov    -0xc(%ebp),%eax
 4bd:	ba 00 00 00 00       	mov    $0x0,%edx
 4c2:	f7 f3                	div    %ebx
 4c4:	89 d0                	mov    %edx,%eax
 4c6:	0f b6 80 58 09 00 00 	movzbl 0x958(%eax),%eax
 4cd:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
 4d1:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
  }while((x /= base) != 0);
 4d5:	8b 45 10             	mov    0x10(%ebp),%eax
 4d8:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 4db:	8b 45 f4             	mov    -0xc(%ebp),%eax
 4de:	ba 00 00 00 00       	mov    $0x0,%edx
 4e3:	f7 75 d4             	divl   -0x2c(%ebp)
 4e6:	89 45 f4             	mov    %eax,-0xc(%ebp)
 4e9:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 4ed:	75 c5                	jne    4b4 <printint+0x38>
  if(neg)
 4ef:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 4f3:	74 2a                	je     51f <printint+0xa3>
    buf[i++] = '-';
 4f5:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4f8:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)
 4fd:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)

  while(--i >= 0)
 501:	eb 1d                	jmp    520 <printint+0xa4>
    putc(fd, buf[i]);
 503:	8b 45 ec             	mov    -0x14(%ebp),%eax
 506:	0f b6 44 05 dc       	movzbl -0x24(%ebp,%eax,1),%eax
 50b:	0f be c0             	movsbl %al,%eax
 50e:	89 44 24 04          	mov    %eax,0x4(%esp)
 512:	8b 45 08             	mov    0x8(%ebp),%eax
 515:	89 04 24             	mov    %eax,(%esp)
 518:	e8 37 ff ff ff       	call   454 <putc>
 51d:	eb 01                	jmp    520 <printint+0xa4>
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 51f:	90                   	nop
 520:	83 6d ec 01          	subl   $0x1,-0x14(%ebp)
 524:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 528:	79 d9                	jns    503 <printint+0x87>
    putc(fd, buf[i]);
}
 52a:	83 c4 44             	add    $0x44,%esp
 52d:	5b                   	pop    %ebx
 52e:	5d                   	pop    %ebp
 52f:	c3                   	ret    

00000530 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 530:	55                   	push   %ebp
 531:	89 e5                	mov    %esp,%ebp
 533:	83 ec 38             	sub    $0x38,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 536:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 53d:	8d 45 0c             	lea    0xc(%ebp),%eax
 540:	83 c0 04             	add    $0x4,%eax
 543:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(i = 0; fmt[i]; i++){
 546:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 54d:	e9 7e 01 00 00       	jmp    6d0 <printf+0x1a0>
    c = fmt[i] & 0xff;
 552:	8b 55 0c             	mov    0xc(%ebp),%edx
 555:	8b 45 ec             	mov    -0x14(%ebp),%eax
 558:	8d 04 02             	lea    (%edx,%eax,1),%eax
 55b:	0f b6 00             	movzbl (%eax),%eax
 55e:	0f be c0             	movsbl %al,%eax
 561:	25 ff 00 00 00       	and    $0xff,%eax
 566:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(state == 0){
 569:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 56d:	75 2c                	jne    59b <printf+0x6b>
      if(c == '%'){
 56f:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 573:	75 0c                	jne    581 <printf+0x51>
        state = '%';
 575:	c7 45 f0 25 00 00 00 	movl   $0x25,-0x10(%ebp)
 57c:	e9 4b 01 00 00       	jmp    6cc <printf+0x19c>
      } else {
        putc(fd, c);
 581:	8b 45 e8             	mov    -0x18(%ebp),%eax
 584:	0f be c0             	movsbl %al,%eax
 587:	89 44 24 04          	mov    %eax,0x4(%esp)
 58b:	8b 45 08             	mov    0x8(%ebp),%eax
 58e:	89 04 24             	mov    %eax,(%esp)
 591:	e8 be fe ff ff       	call   454 <putc>
 596:	e9 31 01 00 00       	jmp    6cc <printf+0x19c>
      }
    } else if(state == '%'){
 59b:	83 7d f0 25          	cmpl   $0x25,-0x10(%ebp)
 59f:	0f 85 27 01 00 00    	jne    6cc <printf+0x19c>
      if(c == 'd'){
 5a5:	83 7d e8 64          	cmpl   $0x64,-0x18(%ebp)
 5a9:	75 2d                	jne    5d8 <printf+0xa8>
        printint(fd, *ap, 10, 1);
 5ab:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5ae:	8b 00                	mov    (%eax),%eax
 5b0:	c7 44 24 0c 01 00 00 	movl   $0x1,0xc(%esp)
 5b7:	00 
 5b8:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
 5bf:	00 
 5c0:	89 44 24 04          	mov    %eax,0x4(%esp)
 5c4:	8b 45 08             	mov    0x8(%ebp),%eax
 5c7:	89 04 24             	mov    %eax,(%esp)
 5ca:	e8 ad fe ff ff       	call   47c <printint>
        ap++;
 5cf:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 5d3:	e9 ed 00 00 00       	jmp    6c5 <printf+0x195>
      } else if(c == 'x' || c == 'p'){
 5d8:	83 7d e8 78          	cmpl   $0x78,-0x18(%ebp)
 5dc:	74 06                	je     5e4 <printf+0xb4>
 5de:	83 7d e8 70          	cmpl   $0x70,-0x18(%ebp)
 5e2:	75 2d                	jne    611 <printf+0xe1>
        printint(fd, *ap, 16, 0);
 5e4:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5e7:	8b 00                	mov    (%eax),%eax
 5e9:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
 5f0:	00 
 5f1:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
 5f8:	00 
 5f9:	89 44 24 04          	mov    %eax,0x4(%esp)
 5fd:	8b 45 08             	mov    0x8(%ebp),%eax
 600:	89 04 24             	mov    %eax,(%esp)
 603:	e8 74 fe ff ff       	call   47c <printint>
        ap++;
 608:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
      }
    } else if(state == '%'){
      if(c == 'd'){
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 60c:	e9 b4 00 00 00       	jmp    6c5 <printf+0x195>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 611:	83 7d e8 73          	cmpl   $0x73,-0x18(%ebp)
 615:	75 46                	jne    65d <printf+0x12d>
        s = (char*)*ap;
 617:	8b 45 f4             	mov    -0xc(%ebp),%eax
 61a:	8b 00                	mov    (%eax),%eax
 61c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        ap++;
 61f:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
        if(s == 0)
 623:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 627:	75 27                	jne    650 <printf+0x120>
          s = "(null)";
 629:	c7 45 e4 4f 09 00 00 	movl   $0x94f,-0x1c(%ebp)
        while(*s != 0){
 630:	eb 1f                	jmp    651 <printf+0x121>
          putc(fd, *s);
 632:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 635:	0f b6 00             	movzbl (%eax),%eax
 638:	0f be c0             	movsbl %al,%eax
 63b:	89 44 24 04          	mov    %eax,0x4(%esp)
 63f:	8b 45 08             	mov    0x8(%ebp),%eax
 642:	89 04 24             	mov    %eax,(%esp)
 645:	e8 0a fe ff ff       	call   454 <putc>
          s++;
 64a:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
 64e:	eb 01                	jmp    651 <printf+0x121>
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 650:	90                   	nop
 651:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 654:	0f b6 00             	movzbl (%eax),%eax
 657:	84 c0                	test   %al,%al
 659:	75 d7                	jne    632 <printf+0x102>
 65b:	eb 68                	jmp    6c5 <printf+0x195>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 65d:	83 7d e8 63          	cmpl   $0x63,-0x18(%ebp)
 661:	75 1d                	jne    680 <printf+0x150>
        putc(fd, *ap);
 663:	8b 45 f4             	mov    -0xc(%ebp),%eax
 666:	8b 00                	mov    (%eax),%eax
 668:	0f be c0             	movsbl %al,%eax
 66b:	89 44 24 04          	mov    %eax,0x4(%esp)
 66f:	8b 45 08             	mov    0x8(%ebp),%eax
 672:	89 04 24             	mov    %eax,(%esp)
 675:	e8 da fd ff ff       	call   454 <putc>
        ap++;
 67a:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 67e:	eb 45                	jmp    6c5 <printf+0x195>
      } else if(c == '%'){
 680:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 684:	75 17                	jne    69d <printf+0x16d>
        putc(fd, c);
 686:	8b 45 e8             	mov    -0x18(%ebp),%eax
 689:	0f be c0             	movsbl %al,%eax
 68c:	89 44 24 04          	mov    %eax,0x4(%esp)
 690:	8b 45 08             	mov    0x8(%ebp),%eax
 693:	89 04 24             	mov    %eax,(%esp)
 696:	e8 b9 fd ff ff       	call   454 <putc>
 69b:	eb 28                	jmp    6c5 <printf+0x195>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 69d:	c7 44 24 04 25 00 00 	movl   $0x25,0x4(%esp)
 6a4:	00 
 6a5:	8b 45 08             	mov    0x8(%ebp),%eax
 6a8:	89 04 24             	mov    %eax,(%esp)
 6ab:	e8 a4 fd ff ff       	call   454 <putc>
        putc(fd, c);
 6b0:	8b 45 e8             	mov    -0x18(%ebp),%eax
 6b3:	0f be c0             	movsbl %al,%eax
 6b6:	89 44 24 04          	mov    %eax,0x4(%esp)
 6ba:	8b 45 08             	mov    0x8(%ebp),%eax
 6bd:	89 04 24             	mov    %eax,(%esp)
 6c0:	e8 8f fd ff ff       	call   454 <putc>
      }
      state = 0;
 6c5:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 6cc:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
 6d0:	8b 55 0c             	mov    0xc(%ebp),%edx
 6d3:	8b 45 ec             	mov    -0x14(%ebp),%eax
 6d6:	8d 04 02             	lea    (%edx,%eax,1),%eax
 6d9:	0f b6 00             	movzbl (%eax),%eax
 6dc:	84 c0                	test   %al,%al
 6de:	0f 85 6e fe ff ff    	jne    552 <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 6e4:	c9                   	leave  
 6e5:	c3                   	ret    
 6e6:	90                   	nop
 6e7:	90                   	nop

000006e8 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 6e8:	55                   	push   %ebp
 6e9:	89 e5                	mov    %esp,%ebp
 6eb:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 6ee:	8b 45 08             	mov    0x8(%ebp),%eax
 6f1:	83 e8 08             	sub    $0x8,%eax
 6f4:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 6f7:	a1 74 09 00 00       	mov    0x974,%eax
 6fc:	89 45 fc             	mov    %eax,-0x4(%ebp)
 6ff:	eb 24                	jmp    725 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 701:	8b 45 fc             	mov    -0x4(%ebp),%eax
 704:	8b 00                	mov    (%eax),%eax
 706:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 709:	77 12                	ja     71d <free+0x35>
 70b:	8b 45 f8             	mov    -0x8(%ebp),%eax
 70e:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 711:	77 24                	ja     737 <free+0x4f>
 713:	8b 45 fc             	mov    -0x4(%ebp),%eax
 716:	8b 00                	mov    (%eax),%eax
 718:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 71b:	77 1a                	ja     737 <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 71d:	8b 45 fc             	mov    -0x4(%ebp),%eax
 720:	8b 00                	mov    (%eax),%eax
 722:	89 45 fc             	mov    %eax,-0x4(%ebp)
 725:	8b 45 f8             	mov    -0x8(%ebp),%eax
 728:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 72b:	76 d4                	jbe    701 <free+0x19>
 72d:	8b 45 fc             	mov    -0x4(%ebp),%eax
 730:	8b 00                	mov    (%eax),%eax
 732:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 735:	76 ca                	jbe    701 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 737:	8b 45 f8             	mov    -0x8(%ebp),%eax
 73a:	8b 40 04             	mov    0x4(%eax),%eax
 73d:	c1 e0 03             	shl    $0x3,%eax
 740:	89 c2                	mov    %eax,%edx
 742:	03 55 f8             	add    -0x8(%ebp),%edx
 745:	8b 45 fc             	mov    -0x4(%ebp),%eax
 748:	8b 00                	mov    (%eax),%eax
 74a:	39 c2                	cmp    %eax,%edx
 74c:	75 24                	jne    772 <free+0x8a>
    bp->s.size += p->s.ptr->s.size;
 74e:	8b 45 f8             	mov    -0x8(%ebp),%eax
 751:	8b 50 04             	mov    0x4(%eax),%edx
 754:	8b 45 fc             	mov    -0x4(%ebp),%eax
 757:	8b 00                	mov    (%eax),%eax
 759:	8b 40 04             	mov    0x4(%eax),%eax
 75c:	01 c2                	add    %eax,%edx
 75e:	8b 45 f8             	mov    -0x8(%ebp),%eax
 761:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 764:	8b 45 fc             	mov    -0x4(%ebp),%eax
 767:	8b 00                	mov    (%eax),%eax
 769:	8b 10                	mov    (%eax),%edx
 76b:	8b 45 f8             	mov    -0x8(%ebp),%eax
 76e:	89 10                	mov    %edx,(%eax)
 770:	eb 0a                	jmp    77c <free+0x94>
  } else
    bp->s.ptr = p->s.ptr;
 772:	8b 45 fc             	mov    -0x4(%ebp),%eax
 775:	8b 10                	mov    (%eax),%edx
 777:	8b 45 f8             	mov    -0x8(%ebp),%eax
 77a:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 77c:	8b 45 fc             	mov    -0x4(%ebp),%eax
 77f:	8b 40 04             	mov    0x4(%eax),%eax
 782:	c1 e0 03             	shl    $0x3,%eax
 785:	03 45 fc             	add    -0x4(%ebp),%eax
 788:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 78b:	75 20                	jne    7ad <free+0xc5>
    p->s.size += bp->s.size;
 78d:	8b 45 fc             	mov    -0x4(%ebp),%eax
 790:	8b 50 04             	mov    0x4(%eax),%edx
 793:	8b 45 f8             	mov    -0x8(%ebp),%eax
 796:	8b 40 04             	mov    0x4(%eax),%eax
 799:	01 c2                	add    %eax,%edx
 79b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 79e:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 7a1:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7a4:	8b 10                	mov    (%eax),%edx
 7a6:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7a9:	89 10                	mov    %edx,(%eax)
 7ab:	eb 08                	jmp    7b5 <free+0xcd>
  } else
    p->s.ptr = bp;
 7ad:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7b0:	8b 55 f8             	mov    -0x8(%ebp),%edx
 7b3:	89 10                	mov    %edx,(%eax)
  freep = p;
 7b5:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7b8:	a3 74 09 00 00       	mov    %eax,0x974
}
 7bd:	c9                   	leave  
 7be:	c3                   	ret    

000007bf <morecore>:

static Header*
morecore(uint nu)
{
 7bf:	55                   	push   %ebp
 7c0:	89 e5                	mov    %esp,%ebp
 7c2:	83 ec 28             	sub    $0x28,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 7c5:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 7cc:	77 07                	ja     7d5 <morecore+0x16>
    nu = 4096;
 7ce:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 7d5:	8b 45 08             	mov    0x8(%ebp),%eax
 7d8:	c1 e0 03             	shl    $0x3,%eax
 7db:	89 04 24             	mov    %eax,(%esp)
 7de:	e8 59 fc ff ff       	call   43c <sbrk>
 7e3:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(p == (char*)-1)
 7e6:	83 7d f0 ff          	cmpl   $0xffffffff,-0x10(%ebp)
 7ea:	75 07                	jne    7f3 <morecore+0x34>
    return 0;
 7ec:	b8 00 00 00 00       	mov    $0x0,%eax
 7f1:	eb 22                	jmp    815 <morecore+0x56>
  hp = (Header*)p;
 7f3:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7f6:	89 45 f4             	mov    %eax,-0xc(%ebp)
  hp->s.size = nu;
 7f9:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7fc:	8b 55 08             	mov    0x8(%ebp),%edx
 7ff:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 802:	8b 45 f4             	mov    -0xc(%ebp),%eax
 805:	83 c0 08             	add    $0x8,%eax
 808:	89 04 24             	mov    %eax,(%esp)
 80b:	e8 d8 fe ff ff       	call   6e8 <free>
  return freep;
 810:	a1 74 09 00 00       	mov    0x974,%eax
}
 815:	c9                   	leave  
 816:	c3                   	ret    

00000817 <malloc>:

void*
malloc(uint nbytes)
{
 817:	55                   	push   %ebp
 818:	89 e5                	mov    %esp,%ebp
 81a:	83 ec 28             	sub    $0x28,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 81d:	8b 45 08             	mov    0x8(%ebp),%eax
 820:	83 c0 07             	add    $0x7,%eax
 823:	c1 e8 03             	shr    $0x3,%eax
 826:	83 c0 01             	add    $0x1,%eax
 829:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((prevp = freep) == 0){
 82c:	a1 74 09 00 00       	mov    0x974,%eax
 831:	89 45 f0             	mov    %eax,-0x10(%ebp)
 834:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 838:	75 23                	jne    85d <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 83a:	c7 45 f0 6c 09 00 00 	movl   $0x96c,-0x10(%ebp)
 841:	8b 45 f0             	mov    -0x10(%ebp),%eax
 844:	a3 74 09 00 00       	mov    %eax,0x974
 849:	a1 74 09 00 00       	mov    0x974,%eax
 84e:	a3 6c 09 00 00       	mov    %eax,0x96c
    base.s.size = 0;
 853:	c7 05 70 09 00 00 00 	movl   $0x0,0x970
 85a:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 85d:	8b 45 f0             	mov    -0x10(%ebp),%eax
 860:	8b 00                	mov    (%eax),%eax
 862:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(p->s.size >= nunits){
 865:	8b 45 ec             	mov    -0x14(%ebp),%eax
 868:	8b 40 04             	mov    0x4(%eax),%eax
 86b:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 86e:	72 4d                	jb     8bd <malloc+0xa6>
      if(p->s.size == nunits)
 870:	8b 45 ec             	mov    -0x14(%ebp),%eax
 873:	8b 40 04             	mov    0x4(%eax),%eax
 876:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 879:	75 0c                	jne    887 <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 87b:	8b 45 ec             	mov    -0x14(%ebp),%eax
 87e:	8b 10                	mov    (%eax),%edx
 880:	8b 45 f0             	mov    -0x10(%ebp),%eax
 883:	89 10                	mov    %edx,(%eax)
 885:	eb 26                	jmp    8ad <malloc+0x96>
      else {
        p->s.size -= nunits;
 887:	8b 45 ec             	mov    -0x14(%ebp),%eax
 88a:	8b 40 04             	mov    0x4(%eax),%eax
 88d:	89 c2                	mov    %eax,%edx
 88f:	2b 55 f4             	sub    -0xc(%ebp),%edx
 892:	8b 45 ec             	mov    -0x14(%ebp),%eax
 895:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 898:	8b 45 ec             	mov    -0x14(%ebp),%eax
 89b:	8b 40 04             	mov    0x4(%eax),%eax
 89e:	c1 e0 03             	shl    $0x3,%eax
 8a1:	01 45 ec             	add    %eax,-0x14(%ebp)
        p->s.size = nunits;
 8a4:	8b 45 ec             	mov    -0x14(%ebp),%eax
 8a7:	8b 55 f4             	mov    -0xc(%ebp),%edx
 8aa:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 8ad:	8b 45 f0             	mov    -0x10(%ebp),%eax
 8b0:	a3 74 09 00 00       	mov    %eax,0x974
      return (void*)(p + 1);
 8b5:	8b 45 ec             	mov    -0x14(%ebp),%eax
 8b8:	83 c0 08             	add    $0x8,%eax
 8bb:	eb 38                	jmp    8f5 <malloc+0xde>
    }
    if(p == freep)
 8bd:	a1 74 09 00 00       	mov    0x974,%eax
 8c2:	39 45 ec             	cmp    %eax,-0x14(%ebp)
 8c5:	75 1b                	jne    8e2 <malloc+0xcb>
      if((p = morecore(nunits)) == 0)
 8c7:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8ca:	89 04 24             	mov    %eax,(%esp)
 8cd:	e8 ed fe ff ff       	call   7bf <morecore>
 8d2:	89 45 ec             	mov    %eax,-0x14(%ebp)
 8d5:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 8d9:	75 07                	jne    8e2 <malloc+0xcb>
        return 0;
 8db:	b8 00 00 00 00       	mov    $0x0,%eax
 8e0:	eb 13                	jmp    8f5 <malloc+0xde>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 8e2:	8b 45 ec             	mov    -0x14(%ebp),%eax
 8e5:	89 45 f0             	mov    %eax,-0x10(%ebp)
 8e8:	8b 45 ec             	mov    -0x14(%ebp),%eax
 8eb:	8b 00                	mov    (%eax),%eax
 8ed:	89 45 ec             	mov    %eax,-0x14(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 8f0:	e9 70 ff ff ff       	jmp    865 <malloc+0x4e>
}
 8f5:	c9                   	leave  
 8f6:	c3                   	ret    
