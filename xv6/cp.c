#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

// Because I need to get used to documenting :'(
// cp.c is used for the purpose of copying data from 1 file to another
// Usage: cp srcname destname

int
main(int argc, char *argv[])
{
  char buf[512];
  int filenameF, copynameF;
  int  r, w;
  char *orig;
  char *copy;

// If not the correct ammount of arguements throw an error at the user
  if(argc != 3){
    printf(2, "Please use the format specified in the documentation: cp originalfilename copyfilename\n");
    exit();
  }

// Read the first and second arguements into the orig and copy "strings"
  orig = argv[1];
  copy = argv[2];

// Check to see if orig can be opened and the copy can be made
  if ((filenameF = open(orig, O_RDONLY)) < 0) {
    printf(2, "Error, cannot open %s\n", orig);
    exit();
  }
  if ((copynameF = open(copy, O_CREATE|O_WRONLY)) < 0) {
    printf(2, "Error, cannot open %s\n", copy);
    exit();
  }
// Read from orig and write into copy simultaneously
// Breaks it into 512 bits per block or execution
  while ((r = read(filenameF, buf, sizeof(buf))) > 0) {
    w = write(copynameF, buf, r);
    if (w != r || w < 0) 
      break;
  }
// Check to see if we are actually reading or writing anything.
// Broad Error Catcher
  if (r < 0 || w < 0)
    printf(2, "cp: error copying %s to %s\n", orig, copy);
// Close the files
  close(filenameF);
  close(copynameF);

  exit();
}
